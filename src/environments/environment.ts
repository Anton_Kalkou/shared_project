// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  barcodeLookupKey: 'vo7mzlr07pvgr6sgvxdci1c3ur3bro',
  apiUrl: 'https://dev-api.rockspoon.io',
  payApiURL: 'https://dev-pay.rockspoon.io',
  tenantKey: 'a0e29ae7c41d11143083894491bd2890cc71ec7a374ccd4d72860aeed783ba3c026e2c3fecee872e299afce2a5481363158e0991092da11c65fd6a1f843921e412a53d56cc4c6ea614a709c2cb11cad911665846e4c8c1e17dd4fd5872c1aca7'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
