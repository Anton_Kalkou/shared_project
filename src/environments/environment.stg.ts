// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  barcodeLookupKey: 'vo7mzlr07pvgr6sgvxdci1c3ur3bro',
  apiUrl: 'https://stg-api.rockspoon.io',
  payApiURL: 'https://stg-pay.rockspoon.io',
  tenantKey: '56e379ffa58d2ac1a854abd75b2d76e5b609208352e0725544d95dc65b22a4bd2ed1db65b5a881ed3326ca24ca43d6d92c00438d7aeeeca75410e9ff97963c75b11b160a1b87956c56f7ab8329f000ca12d8b85226d7a1582cc8a84bbb88e6b4'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
