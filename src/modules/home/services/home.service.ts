import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { ApiService } from 'src/modules/shared/services/api.service';
import { Update } from 'src/modules/shared/models/update.model';
import { APP } from 'src/modules/shared/constants';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(
    private apiService: ApiService
  ) {}

  public getLastUpdates(): Observable<Update[]> {
    // return this.apiService.get(APP.endpoints.updates);
    return of([
      { id: '09881', date: '07/15/2020 04:51PM', actor: 'Thyme', content: 'has been approved and started processing.' },
      { id: '09882', date: '07:03PM', content: 'Problems with Payment Tablet' },
      { id: '09881', date: '07/15/2020 04:51PM', actor: 'Thyme', content: 'has been approved and started processing.' },
      { id: '09882', date: '07:03PM', content: 'Problems with Payment Tablet' },
      { id: '09881', date: '07/15/2020 04:51PM', actor: 'Thyme', content: 'has been approved and started processing.' },
      { id: '09882', date: '07:03PM', content: 'Problems with Payment Tablet' },
      { id: '09881', date: '07/15/2020 04:51PM', actor: 'Thyme', content: 'has been approved and started processing.' },
      { id: '09882', date: '07:03PM', content: 'Problems with Payment Tablet' },
      { id: '09881', date: '07/15/2020 04:51PM', actor: 'Thyme', content: 'has been approved and started processing.' },
      { id: '09882', date: '07:03PM', content: 'Problems with Payment Tablet' },
      { id: '09881', date: '07/15/2020 04:51PM', actor: 'Thyme', content: 'has been approved and started processing.' },
      { id: '09882', date: '07:03PM', content: 'Problems with Payment Tablet' }
    ])
      .pipe(
        delay(600)
      );
  }

}
