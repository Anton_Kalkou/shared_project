import { NgModule } from '@angular/core';

import { NgxsModule } from '@ngxs/store';

import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { HomeComponent } from '../components/home/home.component';
import { SearchModule } from 'src/modules/shared/components/search/search.module';
import { HomeState } from '../ngxs/home.state';
import { LastUpdatesComponent } from '../components/last-updates/last-updates.component';
import { PipesModule } from 'src/modules/shared/pipes/pipes.module';
import { NavItemModule } from 'src/modules/shared/components/nav-item/nav-item.module';

@NgModule({
  imports: [
    SharedModule,
    SearchModule,
    NgxsModule.forFeature([HomeState]),
    PipesModule,
    NavItemModule
  ],
  declarations: [
    HomeComponent,
    LastUpdatesComponent
  ],
  exports: [
    HomeComponent
  ]
})
export class HomeModule {}
