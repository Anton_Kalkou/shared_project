export class GetHomeInitData {
  static readonly type = '[Init Home Data] Get';
}

export class SearchLastUpdates {
  static readonly type = '[Last Updates] Search';

  constructor(
    public key: string
  ) {}
}
