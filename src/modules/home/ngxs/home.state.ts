import { Injectable } from '@angular/core';

import { State, Action, StateContext, Store } from '@ngxs/store';
import { forkJoin } from 'rxjs';
import { tap } from 'rxjs/operators';

import { Update } from 'src/modules/shared/models/update.model';
import { GetHomeInitData, SearchLastUpdates } from './home.actions';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { HomeService } from '../services/home.service';

class HomeStateModel {
  lastUpdates: Update[];
  searchKeyForLastUpdates: string;
}

@State<HomeStateModel>({
  name: 'home',
  defaults: {
    lastUpdates: [],
    searchKeyForLastUpdates: null
  }
})
@Injectable()
export class HomeState {

  constructor(
    private store: Store,
    private homeService: HomeService
  ) {}

  @Action(GetHomeInitData)
  getHomeInitData({patchState}: StateContext<HomeStateModel>) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return forkJoin([
      this.homeService.getLastUpdates()
    ])
      .pipe(
        tap(responses => {
          patchState({
            lastUpdates: responses[0].reverse()
          });
          this.store.dispatch(new SetSpinnerVisibility(false));
        })
      );
  }

  @Action(SearchLastUpdates)
  searchLastUpdates({patchState}: StateContext<HomeStateModel>, {key}: SearchLastUpdates) {
    patchState({
      searchKeyForLastUpdates: key
    });
  }

}
