import { Component, OnInit } from '@angular/core';

import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { Update } from 'src/modules/shared/models/update.model';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';

@Component({
  selector: 'app-last-updates',
  templateUrl: 'last-updates.component.html',
  styleUrls: ['last-updates.component.scss']
})
export class LastUpdatesComponent extends Unsubscribe implements OnInit {

  @Select(state => state.home.lastUpdates) lastUpdates$: Observable<Update[]>;
  @Select(state => state.home.searchKeyForLastUpdates) searchKeyForLastUpdates$: Observable<string>;

  public displayedLastUpdates: Update[];

  private lastUpdates: Update[];

  ngOnInit() {
    this.initSubscriptions();
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.lastUpdates$
      .subscribe(lastUpdates => {
        this.lastUpdates = lastUpdates;
        this.displayedLastUpdates = lastUpdates;
      });
    this.subscribeTo = this.searchKeyForLastUpdates$
      .subscribe(searchKeyForLastUpdates => {
        this.filterLastUpdates(searchKeyForLastUpdates);
      });
  }

  private filterLastUpdates(key: string): void {
    if (key) {
      this.displayedLastUpdates = this.lastUpdates.filter(update => {
        if (
          update.id.toLowerCase().includes(key.toLowerCase()) ||
          update.actor?.toLowerCase().includes(key.toLowerCase()) ||
          update.content.toLowerCase().includes(key.toLowerCase())
        ) {
          return true;
        }

        return false;
      });
    } else {
      this.displayedLastUpdates = [...this.lastUpdates];
    }
  }

}
