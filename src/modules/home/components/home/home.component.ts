import { Component, OnInit } from '@angular/core';

import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { SharedDataService } from 'src/modules/shared/services/shared-data.service';
import { NavItem } from 'src/modules/shared/models/nav-item.model';
import { APP } from 'src/modules/shared/constants';
import { GetHomeInitData, SearchLastUpdates } from '../../ngxs/home.actions';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { User } from 'src/modules/shared/models/user.model';
import { SetHeaderTitle } from 'src/modules/layout/ngxs/layout.actions';

@Component({
  selector: 'app-home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.scss']
})
export class HomeComponent extends Unsubscribe implements OnInit {

  @Select(state => state.layout.userProfileData) userProfileData$: Observable<User>;

  public navItems: NavItem[] = APP.homeNavItems;
  public userProfileData: User;

  constructor(
    public sharedDataService: SharedDataService,
    private store: Store
  ) {
    super();
  }

  ngOnInit() {
    this.dispatchActions();
    this.initSubscriptions();
  }

  public reactOnSearch(key: string): void {
    this.store.dispatch(new SearchLastUpdates(key));
  }

  private dispatchActions(): void {
    this.store.dispatch(new GetHomeInitData());
    this.store.dispatch(new SetHeaderTitle('header.rockspoon-admin-portal'));
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.userProfileData$
    .subscribe(userProfileData => this.userProfileData = userProfileData);
  }

}
