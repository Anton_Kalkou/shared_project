import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Store } from '@ngxs/store';
import { OpenFilters } from 'src/modules/shared/components/filters/ngxs/filters.actions';

@Component({
  selector: 'app-section-header',
  templateUrl: './section-header.component.html',
  styleUrls: ['./section-header.component.scss']
})
export class SectionHeaderComponent implements OnInit {

  @Input() titleTranslationKey: string;
  @Input() searchPlaceholderTranslationKey: string;
  @Input() filtersEndpoint: string;
  @Input() filtersItem: boolean = true;

  @Output() search: EventEmitter<string> = new EventEmitter();
  @Output() showFilters: EventEmitter<void> = new EventEmitter();

  constructor(private store: Store) {}

  ngOnInit(): void {}

  public openFilters(): void {
    this.store.dispatch(new OpenFilters(this.filtersEndpoint));
    this.showFilters.emit();
  }

  public onSearch(key: string): void {
    this.search.emit(key);
  }

}
