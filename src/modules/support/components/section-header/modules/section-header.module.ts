import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IconButtonModule } from 'src/modules/shared/components/icon-button/icon-button.module';
import { SearchModule } from 'src/modules/shared/components/search/search.module';
import { SectionHeaderComponent } from '../components/section-header/section-header.component';

@NgModule({
  imports: [
    CommonModule,
    SearchModule,
    IconButtonModule,
    TranslateModule
  ],
  declarations: [
    SectionHeaderComponent
  ],
  exports: [
    SectionHeaderComponent
  ]
})
export class SectionHeaderModule {}
