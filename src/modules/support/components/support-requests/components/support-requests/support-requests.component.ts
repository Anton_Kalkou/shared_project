import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { SetHeaderTitle } from 'src/modules/layout/ngxs/layout.actions';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { ActionType, Icon, TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum';
import { SupportRequest } from 'src/modules/shared/models/customer-support/support-request.model';
import { Column } from 'src/modules/shared/models/data-table.model';
import { FiltersService } from 'src/modules/shared/services/filters.service';
import { ColumnDataKey, Translation as SupportRequestsTranslation } from '../../enum/support-requests.enum';
import { FetchSupportRequests } from '../../ngxs/support-requests.actions';

@Component({
  selector: 'app-support-requests',
  templateUrl: './support-requests.component.html',
  styleUrls: ['./support-requests.component.scss']
})
export class SupportRequestsComponent extends Unsubscribe {

  @Select(state => state.supportRequests.supportRequests) supportRequests$: Observable<SupportRequest[]>;

  public displayedColumns: Column[] = [
    { title: ColumnDataKey.Time, translationKey: SupportRequestsTranslation.Time, valueType: ValueType.Text, textType: TextType.DateTime },
    { title: ColumnDataKey.RestaurantName, translationKey: SupportRequestsTranslation.RestaurantName, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.CallerName, translationKey: SupportRequestsTranslation.CallerName, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.MerchantId, translationKey: SupportRequestsTranslation.MerchantId, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.MerchantStatus, translationKey: SupportRequestsTranslation.MerchantStatus, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.RockspoonPlan, translationKey: SupportRequestsTranslation.RockspoonPlan, valueType: ValueType.Text, textType: TextType.Simple },
    {
      title: ColumnDataKey.SupportRequest,
      valueType: ValueType.Action,
      actionType: ActionType.SquareButton,
      icon: Icon.Visibility,
      path: '/customer-support/requests/:id/navigation',
      parentInterractionAction: true
    }
  ];
  public totalItems: number = 1000;
  public pageSize: number = 20;
  public pageSizeOptions: number[] = [10, 20, 50, 100, 200,  500, 1000];

  public supportRequests: SupportRequest[];
  public filteredSource: SupportRequest[];

  constructor(private store: Store, private filtersService: FiltersService) {
    super();
  }

  ngOnInit(): void {
    this.initSubscriptions();
    this.dispatchActions();
  }

  public onSearch(key: string): void {
    if (key) {
      this.filteredSource = this.filtersService.filterDataOnSearch(this.supportRequests, key, ColumnDataKey.RestaurantName);
    } else {
      this.filteredSource = this.supportRequests;
    }
  }

  public onAction({ item, column }): void {
    window.open(column.path.replace(':id', item.venueId));
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.supportRequests$.subscribe(supportRequests => {
      this.supportRequests = supportRequests;
      this.filteredSource = supportRequests;
    });
  }

  private dispatchActions(): void {
    this.store.dispatch(new SetSpinnerVisibility(true));
    this.store.dispatch(new FetchSupportRequests());
    this.store.dispatch(new SetHeaderTitle(SupportRequestsTranslation.Header));
  }

}
