import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { APP } from 'src/modules/shared/constants';
import { ApiService } from 'src/modules/shared/services/api.service';
import { TimezoneService } from 'src/modules/support/services/time-zone.service';
import {
  OpenHours,
  SelectedSupportRequest,
  SupportRequest,
  SupportRequestResponse
} from 'src/modules/shared/models/customer-support/support-request.model';
@Injectable({ providedIn: 'root' })
export class SupportRequestsService {

  constructor(private apiService: ApiService, private timeZoneService: TimezoneService) {}
  
  public loadSupportRequestsData(): Observable<SupportRequest[]> {
    return this.apiService.get(APP.endpoints.customerSupport).pipe(
      switchMap((supportRequests: SupportRequestResponse[]) => {
        return of(supportRequests.map(supportRequest => ({
          time: supportRequest.createdAt,
          restaurantName: supportRequest.venueName,
          callerName: supportRequest.contact.name,
          venueId: supportRequest.venueId,
          merchantId: supportRequest.merchantId,
          merchantStatus: supportRequest.merchantStatus,
          rockspoonPlan: supportRequest.rockspoonPlan
        })));
      })
    );
  }

  public loadSupportRequestData(id: string): Observable<SelectedSupportRequest> {
    return this.apiService.get(APP.endpoints.customerSupport).pipe(
      switchMap((supportRequests: SupportRequestResponse[]) => {
        const supportRequest = supportRequests.find(item => item.venueId == id);
        const selectedSupportRequest: SelectedSupportRequest = {
          id: supportRequest.venueId,
          caller: {
            id: supportRequest.contact.profile_id,
            letters: this.getCallerNameLetters(supportRequest.contact.name),
            name: supportRequest.contact.name,
            position: supportRequest.contact.job_title
          },
          restaurant: {
            address: supportRequest.venueAddress.address1,
            name: supportRequest.venueName,
            type: this.makeFirstLetterInUpperCase(supportRequest.rockspoonPlan),
            localTime: {
              time: this.timeZoneService.getTimezoneTime(supportRequest.venueAddress.timezone),
              offset: this.timeZoneService.getTimezoneOffset(supportRequest.venueAddress.timezone)
            },
            openningHours: this.sortOpenningHours(supportRequest.venueAvailability.openingHours),
            timezone: supportRequest.venueAddress.timezone
          },
          venue: { id: supportRequest.venueId, code: supportRequest.venueCode },
          createdTime: this.timeZoneService.getTimezoneTime(null, supportRequest.createdAt),
          merchantId: supportRequest.merchantId
        };

        return of(selectedSupportRequest);
      })
    );
  }

  private sortOpenningHours(openningHours: OpenHours[]): OpenHours[] {
    return openningHours.map(item => {
      const weekDays = item.daysOfWeek;
      const periods = JSON.parse(JSON.stringify(item.periods));

      if (periods.length === 1) {
        return { daysOfWeek: this.sortWeekDays(weekDays), periods };
      } else {
        return this.sortOpenningHoursItem(item);
      }
    });
  }

  private sortWeekDays(weekDays: string[]): string[] {
    return weekDays
      .map(dayOfWeek => this.weekToLowerCase(dayOfWeek))
      .sort((a, b) => {
        const aIndex = APP.weeksDays.findIndex(week => week === a);
        const bIndex = APP.weeksDays.findIndex(week => week === b);

        return aIndex < bIndex ? -1 : aIndex > bIndex ? 1 : 0;
      });
  }

  private sortOpenningHoursItem(openningHoursItem: OpenHours): OpenHours {
    const result = { daysOfWeek: [], periods: [] };
    
    openningHoursItem.daysOfWeek
      .map((dayOfWeek, index) => ({ dayOfWeek: this.weekToLowerCase(dayOfWeek), period: openningHoursItem.periods[index] }))
      .sort((a, b) => {
        const aIndex = APP.weeksDays.findIndex(week => week === a.dayOfWeek);
        const bIndex = APP.weeksDays.findIndex(week => week === b.dayOfWeek);

        return aIndex < bIndex ? -1 : aIndex > bIndex ? 1 : 0;
      })
      .forEach(item => {
        result.daysOfWeek.push(item.dayOfWeek);
        result.periods.push(item.period);
      });

    return result;
  }

  private weekToLowerCase(week: string): string {
    return week[0] + week.substr(1).toLowerCase();
  }

  private makeFirstLetterInUpperCase(string: string): string {
    return string.toUpperCase();
    // return `${string[0].toUpperCase()}${string.slice(1)}`;
  }

  private getCallerNameLetters(fullName: string): string {
    const [name, surname] = fullName.split(' ');

    return name[0] + surname[0];
  }

}
