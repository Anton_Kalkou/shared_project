export enum Translation {
  Time = 'support-requests.time',
  RestaurantName = 'support-requests.restaurant-name',
  CallerName = 'support-requests.caller-name',
  MerchantId = 'support-requests.merchant-id',
  MerchantStatus = 'support-requests.merchant-status',
  RockspoonPlan = 'support-requests.rockspoon-plan',
  Header = 'header.customer-support-requests'
}

export enum ColumnDataKey {
  Time = 'time',
  RestaurantName = 'restaurantName',
  CallerName = 'callerName',
  MerchantId = 'merchantId',
  MerchantStatus = 'merchantStatus',
  RockspoonPlan = 'rockspoonPlan',
  SupportRequest = 'supportRequest'
}
