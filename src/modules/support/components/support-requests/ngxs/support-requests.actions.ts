export class FetchSupportRequests {
  static readonly type = '[Support Requests] Fetch';
}

export class FetchSupportRequest {
  static readonly type = '[Support Reqest] Fetch';

  constructor(public id: string) {}
}
