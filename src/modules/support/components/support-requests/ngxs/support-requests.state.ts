import { Injectable } from '@angular/core';
import { Action, State, StateContext, Store } from '@ngxs/store';
import { SetHeaderTitle } from 'src/modules/layout/ngxs/layout.actions';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { SelectedSupportRequest, SupportRequest } from 'src/modules/shared/models/customer-support/support-request.model';
import { SupportRequestsService } from '../services/support-requests.service';
import { FetchSupportRequests, FetchSupportRequest } from './support-requests.actions';

interface SupportRequestsStateModel {
  supportRequests: SupportRequest[];
  selectedSupportRequest: SelectedSupportRequest;
}

@State<SupportRequestsStateModel>({
  name: 'supportRequests',
  defaults: {
    supportRequests: [],
    selectedSupportRequest: null
  }
})
@Injectable()
export class SupportRequestsState {

  constructor(private store: Store, private supportRequestsService: SupportRequestsService) {}

  @Action(FetchSupportRequests)
  fetchSuppportRequests({ patchState }: StateContext<SupportRequestsStateModel>) {
    return this.supportRequestsService.loadSupportRequestsData().subscribe(supportRequests => {
      this.store.dispatch(new SetSpinnerVisibility(false));
      patchState({ supportRequests });
    });
  }

  @Action(FetchSupportRequest)
  fetchSuppportRequest({ patchState }: StateContext<SupportRequestsStateModel>, action: FetchSupportRequest) {
    return this.supportRequestsService.loadSupportRequestData(action.id).subscribe((selectedSupportRequest: SelectedSupportRequest) => {
      this.store.dispatch(new SetHeaderTitle(`${selectedSupportRequest.restaurant.name} - ${selectedSupportRequest.restaurant.address}`));

      patchState({ selectedSupportRequest });
    });
  }
}
