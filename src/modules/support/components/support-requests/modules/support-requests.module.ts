import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgxsModule } from '@ngxs/store';
import { SearchModule } from 'src/modules/shared/components/search/search.module';
import { IconButtonModule } from 'src/modules/shared/components/icon-button/icon-button.module';
import { SupportRequestsComponent } from '../components/support-requests/support-requests.component';
import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { LabelModule } from 'src/modules/shared/components/label/label.module';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { SquareButtonModule } from 'src/modules/shared/components/square-button/square-button.module';
import { DataTableModule } from 'src/modules/shared/components/data-table/data-table.module';
import { SupportRequestsState } from '../ngxs/support-requests.state';
import { ButtonFiltersModule } from '../../button-filters/modules/button-filters.module';

@NgModule({
  imports: [
    RouterModule,
    SharedModule,
    MaterialModule,
    SearchModule,
    IconButtonModule,
    LabelModule,
    SquareButtonModule,
    ButtonFiltersModule,
    DataTableModule,
    NgxsModule.forFeature([SupportRequestsState])
  ],
  declarations: [
    SupportRequestsComponent
  ],
  exports: [
    SupportRequestsComponent
  ]
})
export class SupportRequestsModule {}
