import { Injectable } from '@angular/core';
import { flatten } from 'lodash';
import * as moment from 'moment';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { APP } from 'src/modules/shared/constants';
import { ApiService } from 'src/modules/shared/services/api.service';
import { Status, ColumnDataKey } from '../enum/access-rights.enum';
import { FilterGroup } from 'src/modules/shared/models/filter.model';
import { AccessRights, AccessRightsDetails } from 'src/modules/shared/models/customer-support/access-rights.model';
import { FilterType } from 'src/modules/support/enum/support.enum';
import { isSubstring } from 'src/modules/shared/helpers';
import { Translation as FiltersTranslation } from 'src/modules/shared/components/filters/enum/filters.enum';
import { FiltersService } from 'src/modules/shared/services/filters.service';

@Injectable({ providedIn: 'root' })
export class AccessRightsService {

  constructor(private apiService: ApiService, private filtersService: FiltersService) {}

  public loadAccessRightsData(venueId: string): Observable<AccessRights.Data[]> {
    return this.apiService.get(APP.endpoints.adminProfile, { venueId }).pipe(
      switchMap((adminProfileData: AccessRights.AdminProfileResponse) => {
        const employees = this.getAdminProfileEmployeesData(adminProfileData, venueId);

        return this.apiService.get(APP.endpoints.adminRole, { venueId }).pipe(
          switchMap((adminRoleData: AccessRights.AdminRoleResponse[]) => {
            return of(this.prepareAccessRightsData(employees, adminRoleData, venueId));
          })
        );
      })
    );
  }

  public loadAccessRightsDetailsData(settings: AccessRightsDetails.AccessRightsDetailsSettings): Observable<AccessRightsDetails.Data[]> {
    return this.apiService.get(this.prepareAccesRightsDetailsEndpoint(APP.endpoints.accessRightsDetails, settings)).pipe(
      switchMap((accessRightsDetailsResponse: AccessRightsDetails.DataResponse[]) => {
        const accessRightsDetails = this.getAccessRights(accessRightsDetailsResponse, this.getAccessRightsDetailsCategories(accessRightsDetailsResponse));

        return of(accessRightsDetails);
      })
    );
  }

  public filterAccessRights(dataSource: AccessRights.Data[], filterType: string, filterValue: string | FilterGroup[], searchColumnKey: string): AccessRights.Data[] {
    if (filterType === FilterType.Search) {
      return this.filtersService.filterDataOnSearch(dataSource, filterValue as string, searchColumnKey);
    } else {
      const filter = filterValue as FilterGroup[];

      if (filter.length) {
        return dataSource.filter(dataSourceItem => {
          if (filter.length === 1) {
            return filter.some(filterValueItem => {
              return this.isFilterValueMatch(dataSourceItem[this.getColumnDataKey(filterValueItem.title)], filterValueItem.filters);
            });
          } else {
            const columnDataKeys = filter.map(filterValueItem => this.getColumnDataKey(filterValueItem.title));

            return filter.every((filterValueItem, filterValueItemIndex) => {
              return this.isFilterValueMatch(dataSourceItem[columnDataKeys[filterValueItemIndex]], filterValueItem.filters);
            });
          }
        });
      } else {
        return dataSource;
      }
    }
  }

  public filterAccessRightsDetails(dataSource: AccessRightsDetails.Data[], filterType: string, filterValue: string | FilterGroup[]): AccessRightsDetails.Data[] {
    let filteredAccessRights;

    if (filterType === FilterType.Search) {
      const filter = filterValue as string;

      if (filterValue) {
        filteredAccessRights = dataSource
          .map(accessRightsItem => {
            const accessRightsCategoryMatch = isSubstring(accessRightsItem.categoryTitle, filter);
            if (accessRightsCategoryMatch) {
              return accessRightsItem;
            } else {
              const filteredAccessRightsItems = accessRightsItem.items.filter(item => isSubstring(item.name, filter));
              if (filteredAccessRightsItems.length) {
                return { ...accessRightsItem, items: filteredAccessRightsItems };
              } else {
                return undefined;
              }
            }
          })
          .filter(accessRightsItem => accessRightsItem);
      } else {
        return dataSource;
      }
    } else {
      const filter = filterValue as FilterGroup[];

      if (filter.length) {
        filteredAccessRights = dataSource
          .map(accessRightsItem => {
            const accessRightsItemItems = accessRightsItem.items.filter(item => filter[0].filters.includes(item.status));
            if (accessRightsItemItems.length) {
              return { ...accessRightsItem, items: accessRightsItemItems };
            } else {
              return undefined;
            }
          })
          .filter(accessRightsItem => accessRightsItem);
      } else {
        filteredAccessRights = dataSource;
      }
    }

    return filteredAccessRights;
  }

  public getAccessRightsDetailsFilterValue(filterValue: string): string {
    if (filterValue === FiltersTranslation.Common.NoAccess) {
      return Status.NO_ACCESS;
    } else if (filterValue === FiltersTranslation.Common.Read) {
      return Status.READ;
    } else if (filterValue === FiltersTranslation.Common.Write) {
      return Status.WRITE;
    }
  }

  private isFilterValueMatch(sourceColumnValue: string, filters: string[]): boolean {
    const columnDataValues = sourceColumnValue.split(', ');

    return filters.some(filter => columnDataValues.includes(filter));
  }

  private getAccessRightsDetailsCategories(accessRightsDetails: AccessRightsDetails.DataResponse[]): string[] {
    const categories = [];

    accessRightsDetails[0].accessRights.forEach(accessRightsItem => {
      if (!categories.includes(accessRightsItem.category)) {
        categories.push(accessRightsItem.category);
      }
    });

    return categories;
  }

  private getAccessRights(accessRightsDetails: AccessRightsDetails.DataResponse[], categories: string[]): AccessRightsDetails.Data[] {
    const items = [];

    categories.forEach(category => {
      const accessRightsItems = accessRightsDetails[0].accessRights.filter(accessRightsItem => accessRightsItem.category === category);
      const preparedAccessRightsItems = accessRightsItems.map(accessRightsItem => ({
        id: accessRightsItem.id,
        name: accessRightsItem.name,
        status: Status[accessRightsItem.level]
      }));
      const itemResult = { categoryTitle: category, items: preparedAccessRightsItems };

      items.push(itemResult);
    });


    return items;
  }

  private prepareAccesRightsDetailsEndpoint(endpoint: string, settings: AccessRightsDetails.AccessRightsDetailsSettings): string {
    return `${endpoint.replace('{profileId}', settings.userProfile.id)}?venueId=${settings.venue.id}&venueCode=${settings.venue.code}`;
  }

  private getColumnDataKey(filterValueTitle: string): string {
    switch(filterValueTitle) {
      case FiltersTranslation.Common.Staff: {
        return ColumnDataKey.Employee;
      }
      case FiltersTranslation.Common.Roles: {
        return ColumnDataKey.Roles;
      }
    }
  }

  private getAdminProfileEmployeesData(adminProfileData: AccessRights.AdminProfileResponse, venueId: string): AccessRights.AdminProfileEmployeeResponse[] {
    const employees = flatten<any>(adminProfileData.results.filter(resultItem => {
      return resultItem.venues.find(venueItem => venueItem.id === venueId);
    }));

    return employees;
  }

  private prepareAccessRightsData(employees: AccessRights.AdminProfileEmployeeResponse[], adminRoleData: AccessRights.AdminRoleResponse[], venueId: string): AccessRights.Data[] {
    const accessRightsData = employees
      .filter(employee => employee.id)
      .map(employee => {
        const venue = employee.venues.find(venue => venue.id === venueId);
        const roles = this.getVenueRoles(adminRoleData, venue);
        const accessRightsItem = {
          id: employee.id,
          employee: `${venue.firstName} ${venue.lastName}`,
          roles: this.getVenueRolesNames(roles),
          dateLastModified: this.getVenueRolesDates(roles)
        };

        return accessRightsItem;
      });

    return accessRightsData;
  }

  private getVenueRoles(adminRoleData: AccessRights.AdminRoleResponse[], venue: AccessRights.Venue): AccessRights.Role[] {
    return adminRoleData
      .map(adminRoleDataItem => {
        const venueRole = venue.roles.find(role => role.id === adminRoleDataItem.id);
        if (!venueRole) {
          return undefined;
        }
        return { ...venueRole, name: adminRoleDataItem.name };
      })
      .filter(adminRoleDataItem => adminRoleDataItem)
      .sort((a: AccessRights.Role, b: AccessRights.Role) => {
        const aDate = moment(a.startDate);
        const bDate = moment(b.startDate);

        return aDate < bDate ? 1 : aDate > bDate ? -1 : 0;
      });
  }

  private getVenueRolesNames(roles: any[]): string {
    return roles.map(role => role.name).join(', ');
  }

  private getVenueRolesDates(roles: any[]): string {
    return roles.length ? roles[0].startDate : '-';
  }
}
