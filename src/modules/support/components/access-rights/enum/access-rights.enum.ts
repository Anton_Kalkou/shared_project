export enum Translation {
  AccessRights = 'support-requests.access-rights.access-rights',
  Status = 'support-requests.access-rights.status',
  Employee = 'support-requests.access-rights.employee',
  Roles = 'support-requests.access-rights.roles',
  DateLastModified = 'support-requests.access-rights.date-last-modified'
}

export enum Role {
  Role = 'Role',
  Waiter = 'Waiter'
}

export enum Status {
  NO_ACCESS = 'NO ACCESS',
  WRITE = 'WRITE',
  READ = 'READ'
}

export enum ColumnDataKey {
  Id = 'id',
  Employee = 'employee',
  Roles = 'roles',
  DateLastModified = 'dateLastModified',
  Action = 'action',
  AccessRights = 'accessRights',
  Status = 'status'
}
