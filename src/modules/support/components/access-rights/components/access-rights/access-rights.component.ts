import { Component, OnInit } from '@angular/core';
import { PRIMARY_OUTLET, Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { FiltersService as FiltersSliderService } from 'src/modules/shared/components/filters/services/filters.service';
import { APP } from 'src/modules/shared/constants';
import { AccessRights } from 'src/modules/shared/models/customer-support/access-rights.model';
import { Translation as SearchTranslation } from 'src/modules/shared/components/search/enum/search.enum';
import { FiltersService } from 'src/modules/shared/services/filters.service';
import { ActionType, Icon, TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum';
import { Column } from 'src/modules/shared/models/data-table.model';
import { ColumnDataKey, Translation as AccessRightsTranslation } from '../../enum/access-rights.enum';
import { FetchAccessRights } from '../../ngxs/access-rights.actions';
import { AccessRightsService } from '../../services/access-rights.service';

@Component({
  selector: 'app-access-rights',
  templateUrl: './access-rights.component.html',
  styleUrls: ['./access-rights.component.scss']
})
export class AccessRightsComponent extends Unsubscribe implements OnInit {

  @Select(state => state.accessRights.accessRights) accessRights$: Observable<AccessRights.Data[]>;
  @Select(state => state.filters.filtersVisibility) filtersVisibility$: Observable<boolean>;

  public displayedColumns: Column[] = [
    { title: ColumnDataKey.Employee, translationKey: AccessRightsTranslation.Employee, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.Roles, translationKey: AccessRightsTranslation.Roles, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.DateLastModified, translationKey: AccessRightsTranslation.DateLastModified, valueType: ValueType.Text, textType: TextType.DateTime },
    {
      title: ColumnDataKey.Action,
      valueType: ValueType.Action,
      actionDataKey: ColumnDataKey.Id,
      actionType: ActionType.SquareButton,
      icon: Icon.Visibility,
      path: 'details',
      isSortableDisabled: true
    }
  ];

  public headerTitleKey: string = AccessRightsTranslation.AccessRights;
  public headerSearchPlaceholderKey: string = SearchTranslation.SearchStaff;

  public filtersEndpoint: string = APP.endpoints.accessRightsFilters;
  public filteredSource: AccessRights.Data[];

  private accessRights: AccessRights.Data[];
  private filterType: string;
  private filteredFiltersSource: AccessRights.Data[];
  private filteredSearchSource: AccessRights.Data[];

  constructor(
    private store: Store,
    private router: Router,
    private filtersSliderService: FiltersSliderService,
    private filtersService: FiltersService,
    private accessRightsService: AccessRightsService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.initSubscriptions();
    this.dispatchActions();
  }

  ngOnDestroy(): void {
    this.store.dispatch(new SetSpinnerVisibility(false));
    this.filtersSliderService.resetActivatedFilters();
  }

  public onSearch(key: string): void {
    this.filtersService.handleFilteringDataOnSearch(
      this,
      this.accessRights,
      this.accessRightsService.filterAccessRights.bind(this.accessRightsService),
      key,
      ColumnDataKey.Employee
    );
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.accessRights$.subscribe(accessRights => {
      this.accessRights = accessRights;
      this.filteredSource = accessRights;
    });
    this.subscribeTo = this.filtersVisibility$.subscribe(filtersVisibility => {
      if (!filtersVisibility && this.accessRights) {
        const filtersValues = this.filtersService.getFiltersValues(this.filtersSliderService.getActivatedFilters());

        this.filtersService.handleFilteringDataOnFilters(
          this,
          this.accessRights,
          this.accessRightsService.filterAccessRights.bind(this.accessRightsService),
          filtersValues
        );
      }
    });
  }

  private dispatchActions(): void {
    const id = this.router.parseUrl(this.router.url).root.children[PRIMARY_OUTLET].segments[2].path;

    this.store.dispatch(new SetSpinnerVisibility(true));
    this.store.dispatch(new FetchAccessRights(id));
  }

}
