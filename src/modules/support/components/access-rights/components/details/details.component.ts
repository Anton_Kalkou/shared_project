import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { combineLatest, Observable } from 'rxjs';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum';
import { OpenFilters } from 'src/modules/shared/components/filters/ngxs/filters.actions';
import { FiltersService as FiltersSliderService } from 'src/modules/shared/components/filters/services/filters.service';
import { APP } from 'src/modules/shared/constants';
import { AccessRightsDetails } from 'src/modules/shared/models/customer-support/access-rights.model';
import { SelectedSupportRequest } from 'src/modules/shared/models/customer-support/support-request.model';
import { Column } from 'src/modules/shared/models/data-table.model';
import { FilterGroup, ParentFilter } from 'src/modules/shared/models/filter.model';
import { User } from 'src/modules/shared/models/user.model';
import { FiltersService } from 'src/modules/shared/services/filters.service';
import { FetchSupportRequest } from '../../../support-requests/ngxs/support-requests.actions';
import { ColumnDataKey, Translation as AccessRightsTranslation } from '../../enum/access-rights.enum';
import { FetchAccessRightsDetails } from '../../ngxs/access-rights.actions';
import { AccessRightsService } from '../../services/access-rights.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent extends Unsubscribe implements OnInit {

  @Select(state => state.layout.userProfileData) userProfileData$: Observable<User>;
  @Select(state => state.supportRequests.selectedSupportRequest) selectedSupportRequest$: Observable<SelectedSupportRequest>;
  @Select(state => state.accessRights.accessRightsDetails) accessRightsDetails$: Observable<AccessRightsDetails.Data[]>;
  @Select(state => state.filters.filtersVisibility) filtersVisibility$: Observable<boolean>;

  public employeeName: string;
  
  public detailsColumns: Column[] = [
    { title: ColumnDataKey.AccessRights, translationKey: AccessRightsTranslation.AccessRights, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.Status, translationKey: AccessRightsTranslation.Status, valueType: ValueType.Text, textType: TextType.Simple }
  ];

  public dropdownFilterOptions: string[] = APP.accessRightsDetailsOptions;

  public filteredSource: AccessRightsDetails.Data[];

  private filtersEndpoint: string = APP.endpoints.accessRightsDetailsFilters;
  private accessRightsDetails: AccessRightsDetails.Data[];
  private filterType: string;
  private searchFilterValue: string;
  private filteredFiltersSource: AccessRightsDetails.Data[];
  private filteredSearchSource: AccessRightsDetails.Data[];

  constructor(
    private store: Store,
    private route: ActivatedRoute,
    private filtersSliderService: FiltersSliderService,
    private filtersService: FiltersService,
    private accessRightsService: AccessRightsService
  ) {
    super();
  }

  ngOnInit(): void {
    this.initSubscriptions();
    this.dispatchActions();
  }

  ngOnDestroy(): void {
    this.store.dispatch(new SetSpinnerVisibility(false));
    this.filtersSliderService.resetActivatedFilters();
  }

  public onSearch(key: string): void {
    this.searchFilterValue = key;
  }

  public onApply(): void {
    this.filtersService.handleFilteringDataOnSearch(
      this,
      this.accessRightsDetails,
      this.accessRightsService.filterAccessRightsDetails.bind(this.accessRightsService),
      this.searchFilterValue
    );
  }

  public openFilters(): void {
    this.store.dispatch(new OpenFilters(this.filtersEndpoint));
  }

  private initSubscriptions(): void {
    this.subscribeTo = combineLatest(this.userProfileData$, this.selectedSupportRequest$).subscribe(([userProfileId, selectedSupportRequest]) => {
      if (selectedSupportRequest) {
        const accessRightsDetailsSettings = {
          userProfile: { id: userProfileId.id },
          venue: { id: selectedSupportRequest.venue.id, code: selectedSupportRequest.venue.code }
        };

        this.employeeName = selectedSupportRequest.caller.name;
        this.store.dispatch(new FetchAccessRightsDetails(accessRightsDetailsSettings));
      }
    });
    this.subscribeTo = this.accessRightsDetails$.subscribe(accessRightsDetails => {
      this.accessRightsDetails = accessRightsDetails;
      this.filteredSource = accessRightsDetails;
    });
    this.subscribeTo = this.filtersVisibility$.subscribe(filtersVisibility => {
      if (!filtersVisibility && this.accessRightsDetails) {
        const filtersValues = this.getFiltersValues(this.filtersSliderService.getActivatedFilters());

        this.filtersService.handleFilteringDataOnFilters(
          this,
          this.accessRightsDetails,
          this.accessRightsService.filterAccessRightsDetails.bind(this.accessRightsService),
          filtersValues
        );
      }
    });
  }

  private getFiltersValues(activatedFilters: ParentFilter[]): FilterGroup[] {
    const titles = this.filtersService.getActivatedFiltersTitles(activatedFilters);
    const filtersValues = titles.map(title => {
      const filters = activatedFilters
        .filter(activatedFilter => activatedFilter.parentFilterTitle === title)
        .map(activatedFilter => this.accessRightsService.getAccessRightsDetailsFilterValue(activatedFilter.parentFilterOption));

      return { title, filters };
    });

    return filtersValues;
  }

  private dispatchActions(): void {
    this.store.dispatch(new SetSpinnerVisibility(true));
    this.store.dispatch(new FetchSupportRequest(this.route.snapshot.paramMap.get('requestId')));
  }
}
