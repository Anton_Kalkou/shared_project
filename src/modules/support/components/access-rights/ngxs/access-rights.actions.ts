import { AccessRightsDetails } from 'src/modules/shared/models/customer-support/access-rights.model';

export class FetchAccessRights {
  static readonly type = '[Access Rights] Fetch';

  constructor(public venueId: string) {}
}

export class FetchAccessRightsDetails {
  static readonly type = '[Access Rights Details] Fetch';

  constructor(public accessRightsDetailsSettings: AccessRightsDetails.AccessRightsDetailsSettings) {}
}
