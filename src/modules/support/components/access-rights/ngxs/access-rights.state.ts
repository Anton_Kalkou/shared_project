import { Injectable } from '@angular/core';
import { Action, State, StateContext, Store } from '@ngxs/store';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { AccessRightsDetails } from 'src/modules/shared/models/customer-support/access-rights.model';
import { AccessRightsService } from '../services/access-rights.service';
import { FetchAccessRights, FetchAccessRightsDetails } from './access-rights.actions';

interface AccessRightsStateModel {
  accessRights: any[];
  accessRightsDetails: AccessRightsDetails.Data[];
}

@State<AccessRightsStateModel>({
  name: 'accessRights',
  defaults: {
    accessRights: [],
    accessRightsDetails: []
  }
})
@Injectable()
export class AccessRightsState {

  constructor(private store: Store, private accessRightsService: AccessRightsService) {}

  @Action(FetchAccessRights)
  fetchAccessRights({ patchState }: StateContext<AccessRightsStateModel>, action: FetchAccessRights) {
    return this.accessRightsService.loadAccessRightsData(action.venueId).subscribe(accessRights => {
      this.store.dispatch(new SetSpinnerVisibility(false));
      patchState({ accessRights });
    });
  }
  
  @Action(FetchAccessRightsDetails)
  fetchAccessRightsDetails({ patchState }: StateContext<AccessRightsStateModel>, action: FetchAccessRightsDetails) {
    return this.accessRightsService.loadAccessRightsDetailsData(action.accessRightsDetailsSettings).subscribe(accessRightsDetails => {
      this.store.dispatch(new SetSpinnerVisibility(false));
      patchState({ accessRightsDetails });
    });
  }
}
