import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { NgxsModule } from '@ngxs/store';
import { DataTableModule } from 'src/modules/shared/components/data-table/data-table.module';
import { IconButtonModule } from 'src/modules/shared/components/icon-button/icon-button.module';
import { SearchModule } from 'src/modules/shared/components/search/search.module';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { ButtonFiltersModule } from '../../button-filters/modules/button-filters.module';
import { SectionHeaderModule } from '../../section-header/modules/section-header.module';
import { AccessRightsComponent } from '../components/access-rights/access-rights.component';
import { DetailsComponent } from '../components/details/details.component';
import { AccessRightsState } from '../ngxs/access-rights.state';

@NgModule({
  imports: [
    CommonModule,
    SectionHeaderModule,
    MaterialModule,
    TranslateModule,
    SearchModule,
    IconButtonModule,
    ButtonFiltersModule,
    DataTableModule,
    NgxsModule.forFeature([AccessRightsState])
  ],
  declarations: [
    AccessRightsComponent,
    DetailsComponent
  ],
  exports: [
    AccessRightsComponent
  ]
})
export class AccessRightsModule {}
