import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { ButtonFilter, ButtonFilterEvent } from 'src/modules/shared/models/customer-support/filters.model';
import { DeviceAvailability, DevicesAvailabilityChart, DeviceStatus, FilterData } from 'src/modules/shared/models/customer-support/systems-health.model';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { Translation as FiltersTranslation, FilterKey } from 'src/modules/shared/components/filters/enum/filters.enum';
import { FetchSystemsHealth } from '../../ngxs/systems-health/systems-health.actions';
import { DevicesStatus } from '../../enum/systems-health.enum';
import { SystemsHealthService } from '../../services/systems-health.service';

@Component({
  selector: 'app-systems-health',
  templateUrl: './systems-health.component.html',
  styleUrls: ['./systems-health.component.scss']
})
export class SystemsHealthComponent extends Unsubscribe implements OnInit, OnDestroy {

  @Select(state => state.systemsHealth.devicesStatus) devicesStatus$: Observable<DeviceStatus[]>;
  @Select(state => state.systemsHealth.devicesAvailabilityChart) devicesAvailabilityChart$: Observable<DevicesAvailabilityChart>;

  public devicesStatus: DeviceStatus[];
  public filteredSource: DeviceStatus[];
  public supportRequestId: string;
  public filters: ButtonFilter[];

  constructor(private store: Store, private router: Router, private systemsHealthService: SystemsHealthService) {
    super();
  }

  ngOnInit(): void {
    this.initValues();
    this.initSubscriptions();
    this.dispatchActions();
  }

  ngOnDestroy(): void {
    this.store.dispatch(new SetSpinnerVisibility(false));
  }

  public onButtonFilter(buttonFilterEvent: ButtonFilterEvent): void {
    this.store.dispatch(new SetSpinnerVisibility(true));
    this.store.dispatch(new FetchSystemsHealth(this.supportRequestId, buttonFilterEvent));
  }

  public pointClick(clickedPointData: any): void {
    this.filteredSource = this.systemsHealthService.filterDevicesStatus(this.devicesStatus, clickedPointData);
  }

  private getFilterData(latestReport: DeviceAvailability): FilterData {
    return { day: latestReport.start.toISOString(), availability: latestReport.availability };
  }

  private initValues(): void {
    this.supportRequestId = this.router.url.split('/')[3];
    this.filters = [
      { translationKey: FiltersTranslation.Date.Now, key: FilterKey.Date.Now },
      { translationKey: FiltersTranslation.Date.LastDay, key: FilterKey.Date.LastDay },
      { translationKey: FiltersTranslation.Date.Last7Days, key: FilterKey.Date.Last7Days },
      { translationKey: FiltersTranslation.Date.ChooseDates, icon: DevicesStatus.Icon.Calendar, key: FilterKey.Date.ChooseDates },
    ];
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.devicesStatus$.subscribe(devicesStatus => {
      this.devicesStatus = devicesStatus;

      this.subscribeTo = this.devicesAvailabilityChart$.subscribe(devicesAvailabilityChart => {
        this.filteredSource = devicesAvailabilityChart.latestReport
          ? this.systemsHealthService.filterDevicesStatus(devicesStatus, this.getFilterData(devicesAvailabilityChart.latestReport))
          : devicesStatus;
      });
    });
  }

  private dispatchActions() {
    this.store.dispatch(new SetSpinnerVisibility(true));
    this.store.dispatch(new FetchSystemsHealth(this.supportRequestId));
  }
}
