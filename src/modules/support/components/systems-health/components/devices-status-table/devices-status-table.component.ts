import { Component, Input, OnChanges, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { HeaderType } from 'src/modules/shared/components/data-table/enum/data-table.enum';
import { DeviceStatus } from 'src/modules/shared/models/customer-support/systems-health.model';
import { Column } from 'src/modules/shared/models/data-table.model';
import { Translation as SystemsHealthTranslation, ColumnDataKey, DevicesStatus } from '../../enum/systems-health.enum';

@Component({
  selector: 'app-devices-status-table',
  templateUrl: './devices-status-table.component.html',
  styleUrls: ['./devices-status-table.component.scss']
})
export class DevicesStatusTableComponent implements OnChanges {

  @Select(state => state.root.spinnerVisibility) spinnerVisibility$: Observable<boolean>;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  @Input() items: DeviceStatus[];

  private isInitialized: boolean;

  public dataSource: MatTableDataSource<DeviceStatus>;
  public columns: Column[];
  public columnTitles: string[];

  ngOnChanges(): void {
    if (!this.isInitialized) {
      this.initValues();
    }

    this.prepareTable();
  }

  private initValues(): void {
    this.columns = [
      { title: ColumnDataKey.DeviceId, translationKey: SystemsHealthTranslation.DeviceId },
      { title: ColumnDataKey.Function, translationKey: SystemsHealthTranslation.Function },
      { title: ColumnDataKey.Status, translationKey: SystemsHealthTranslation.Status },
      { title: ColumnDataKey.Wifi, translationKey: SystemsHealthTranslation.Wifi },
      { title: ColumnDataKey.Battery, translationKey: SystemsHealthTranslation.Battery },
      { title: ColumnDataKey.AppVersion, headerType: HeaderType.Icon, iconClass: DevicesStatus.IconClass.IconApp },
      { title: ColumnDataKey.AndroidVersion, headerType: HeaderType.Icon, iconClass: DevicesStatus.IconClass.IconAndroid },
      { title: ColumnDataKey.Bluetooth, isSortableDisabled: true },
      { title: ColumnDataKey.CellularSignal, isSortableDisabled: true }
    ];
    this.columnTitles = this.columns.map(column => column.title);
    this.isInitialized = true;
  }

  private prepareTable(): void {
    this.dataSource = new MatTableDataSource<DeviceStatus>(this.items);
    this.dataSource.sort = this.sort;
  }
}
