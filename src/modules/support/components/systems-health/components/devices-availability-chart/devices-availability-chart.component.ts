import { Component, EventEmitter, Input, OnChanges, Output, ViewChild } from '@angular/core';
import { DxChartComponent } from 'devextreme-angular';
import * as moment from 'moment';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { FilterKey } from 'src/modules/shared/components/filters/enum/filters.enum';
import { APP } from 'src/modules/shared/constants';
import { DevicesAvailabilityChart, PointData } from 'src/modules/shared/models/customer-support/systems-health.model';
import { TimeUnit } from 'src/modules/support/enum/support.enum';
import { Chart } from '../../enum/systems-health.enum';

@Component({
  selector: 'app-devices-availability-chart',
  templateUrl: './devices-availability-chart.component.html',
  styleUrls: ['./devices-availability-chart.component.scss']
})
export class DevicesAvailabilityChartComponent extends Unsubscribe implements OnChanges {

  @ViewChild(DxChartComponent, { static: false }) dxChart: DxChartComponent;

  @Output() pointClick: EventEmitter<any> = new EventEmitter<any>();

  @Input() chart: DevicesAvailabilityChart;

  private selectedPointAvailability: Chart.BarStatus;
  private selectedPointDay: Date | null;

  public interval: string;
  public constantLine: Date | null;
  public constantLineLabel: string;

  constructor() {
    super();
  }

  ngOnChanges(): void {
    this.interval = this.getInterval(this.chart);
    
    if (this.chart.labelFormat === FilterKey.Date.ChooseDates && this.chart.devicesAvailability.length) {
      this.setConstantLine(true);
      this.setPointData(true);
    } else if (this.subscribeTo) {
      this.subscribeTo.unsubscribe();
    }
  }

  public formatter = (value: string): string => {
    return moment(value).format(this.getLabelFormat());
  }

  public customizeSeries(seriesName) {
    switch(seriesName) {
      case Chart.BarStatus.Active: {
        return { color: APP.colors.green1 };
      }
      case Chart.BarStatus.Warning: {
        return { color: APP.colors.orange };
      }
      case Chart.BarStatus.Bad: {
        return { color: APP.colors.red1 };
      }
    }
  }

  public onPointClick(event: any): void {
    const { value, data } = event.target;
    const pointData = { day: moment(value).toISOString(), availability: data.availability };
 
    if (this.isPointChanged(pointData.day) || this.isPointAvailabilityChanged(pointData.availability)) {
      this.pointClick.emit(pointData);
    }
  
    if (this.isPointChanged(pointData.day)) {
      this.setPointData(false, pointData);
      this.setConstantLine();
    }
  }

  private getInterval(chart: DevicesAvailabilityChart): string {
    switch(chart.labelFormat) {
      case FilterKey.Date.Now: {
        return FilterKey.Date.Now;
      }
      case FilterKey.Date.LastDay:
      case FilterKey.Date.Last7Days: {
        return FilterKey.Date.LastDay;
      }
      case FilterKey.Date.ChooseDates: {
        const amountWeekDays = 7;
        const from = moment(chart.valuesRange.startValue).add(1, TimeUnit.Weeks);
        const to = moment(chart.valuesRange.endValue);
        const diff = to.diff(from, TimeUnit.Days);

        return diff >= amountWeekDays ? FilterKey.Date.Last7Days : FilterKey.Date.LastDay;
      }
    }
  }

  private getLabelFormat(): string {
    return this.interval === FilterKey.Date.Now ? Chart.LabelFormat.Time : Chart.LabelFormat.Date;
  }

  private setConstantLine(isDeafultValue?: boolean): void {
    const date = isDeafultValue ? this.chart.latestReport.start : this.selectedPointDay;

    this.constantLine = date;
    this.constantLineLabel = moment(date).format(Chart.LabelFormat.Date);
  }

  private setPointData(isDeafultData: boolean, pointData?: PointData): void {
    this.selectedPointAvailability = isDeafultData ? this.chart.latestReport.availability : pointData.availability;
    this.selectedPointDay = isDeafultData ? this.chart.latestReport.start : moment(pointData.day).toDate();
  }

  private isPointChanged(day: string): boolean {
    return moment(this.selectedPointDay).diff(moment(day), TimeUnit.Minutes) !== 0;
  }

  private isPointAvailabilityChanged(availability: Chart.BarStatus): boolean {
    return availability !== this.selectedPointAvailability;
  }
}
