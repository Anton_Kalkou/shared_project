import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';
import { NgxsModule } from '@ngxs/store';
import { DxChartModule } from 'devextreme-angular';
import { SpinnerModule } from 'src/modules/shared/components/spinner/spinner.module';
import { DirectivesModule } from 'src/modules/shared/directives/directives.module';
import { IconButtonModule } from 'src/modules/shared/components/icon-button/icon-button.module';
import { SquareButtonModule } from 'src/modules/shared/components/square-button/square-button.module';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { BatteryLevelModule } from '../../battery-level/modules/battery-level.module';
import { ButtonFiltersModule } from '../../button-filters/modules/button-filters.module';
import { DevicesStatusTableComponent } from '../components/devices-status-table/devices-status-table.component';
import { SystemsHealthComponent } from '../components/systems-health/systems-health.component';
import { DevicesAvailabilityChartComponent } from '../components/devices-availability-chart/devices-availability-chart.component';
import { SystemsHealthState } from '../ngxs/systems-health/systems-health.state';
import { DevicesAvailabilityChartState } from '../ngxs/devices-availability-chart/devices-availability-chart.state';
import { DoubleStatusPipe } from '../pipe/double-status.pipe';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    TranslateModule,
    MaterialModule,
    IconButtonModule,
    ButtonFiltersModule,
    BatteryLevelModule,
    SquareButtonModule,
    DirectivesModule,
    DxChartModule,
    SpinnerModule,
    NgxsModule.forFeature([SystemsHealthState, DevicesAvailabilityChartState])
  ],
  declarations: [
    SystemsHealthComponent,
    DevicesAvailabilityChartComponent,
    DevicesStatusTableComponent,
    DoubleStatusPipe
  ],
  exports: [
    SystemsHealthComponent
  ]
})
export class SystemshealtsModule {}
