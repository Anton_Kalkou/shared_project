import { ElementRef, Injectable } from '@angular/core';
import { APP } from 'src/modules/shared/constants';
import { isBetween } from 'src/modules/shared/helpers';
import { DevicesStatus } from '../enum/systems-health.enum';

@Injectable({ providedIn: 'root' })
export class SystemsHealthBehaviourService {
  private classNames: string[];

  constructor() {
    this.classNames = [];
  }

  public handleStatus(elementRef: ElementRef, value: DevicesStatus.Status): void {
    switch (value) {
      case DevicesStatus.Status.Online: {
        this.classNames.push(DevicesStatus.Status.GoodClass);
        break;
      }
      case DevicesStatus.Status.PoorSignal:
      case DevicesStatus.Status.LowBattery: {
        this.classNames.push(DevicesStatus.Status.WarningClass);
        break;
      }
      case DevicesStatus.Status.Offline_BatteryEmpty:
      case DevicesStatus.Status.Offline:
      case DevicesStatus.Status.BatteryEmpty: {
        this.classNames.push(DevicesStatus.Status.BadClass);
        break;
      }
    }

    this.setClassNames(elementRef, this.classNames);
  }

  public handleWifiSignal(elementRef: ElementRef, value: any): void {
    if (value >= APP.wifiSignals.maxRSSI) {
      this.classNames.push(DevicesStatus.Wifi.HighSignal);
    } else if (isBetween<number>(value, APP.wifiSignals.minRSSI, APP.wifiSignals.maxRSSI)) {
      this.classNames.push(DevicesStatus.Wifi.LowSignal);
    }

    this.setClassNames(elementRef, this.classNames);
  }

  public handleWifiSignalIcon(elementRef: ElementRef, value: any): void {
    if (value >= APP.wifiSignals.maxRSSI) {
      this.classNames.push(DevicesStatus.Wifi.HighSignalIcon);
    } else if (isBetween<number>(value, APP.wifiSignals.minRSSI, APP.wifiSignals.maxRSSI)) {
      this.classNames.push(DevicesStatus.Wifi.LowSignalIcon);
    }

    this.setClassNames(elementRef, this.classNames);
  }

  public handleBatteryChargingIcon(elementRef: ElementRef, value: any): void {
    this.classNames.push(DevicesStatus.Battery.Level);

    if (value === DevicesStatus.Battery.NotCharging) {
      this.classNames.push(DevicesStatus.Battery.NotChargingIcon);
    } else {
      this.classNames.push(DevicesStatus.Battery.ChargingIcon);
    }

    this.setClassNames(elementRef, this.classNames);
  }

  public handleBluetooth(elementRef: ElementRef, value: any): void {
    if (value) {
      this.classNames.push(DevicesStatus.Bluetooth.Active);
    } else {
      this.classNames.push(DevicesStatus.Bluetooth.Off);;
    }

    this.setClassNames(elementRef, this.classNames);
  }

  public handleCellularSignal(elementRef: ElementRef, value: any): void {
    switch (true) {
      case value >= APP.cellularSignals.maxRSSI: {
        this.classNames.push(DevicesStatus.CellularSignal.HighSignalIcon);
        break;
      }
      case isBetween<number>(value, APP.cellularSignals.minRSSI, APP.cellularSignals.maxRSSI): {
        this.classNames.push(DevicesStatus.CellularSignal.PoorSignalIcon);
        break;
      }
      case value <= APP.cellularSignals.minRSSI: {
        this.classNames.push(DevicesStatus.CellularSignal.NoSignalIcon);
        break;
      }
    }

    this.setClassNames(elementRef, this.classNames);
  }

  private setClassNames(elementRef: ElementRef, classNames: string[]): void {
    elementRef.nativeElement.classList.add(...classNames);
    this.classNames = [];
  }
}
