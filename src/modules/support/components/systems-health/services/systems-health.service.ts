import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { flatten } from 'lodash';
import { APP } from 'src/modules/shared/constants';
import { isBetween } from 'src/modules/shared/helpers';
import { ApiService } from 'src/modules/shared/services/api.service';
import { TimeFormat, TimeUnit } from 'src/modules/support/enum/support.enum';
import { FilterKey } from 'src/modules/shared/components/filters/enum/filters.enum';
import {
  DateFilter,
  DeviceAvailability,
  DevicesAvailabilityChart,
  DeviceStatusResponse,
  SystemsHealthData,
  DeviceStatusReport,
  DateFilterValues,
  DeviceStatus,
  FilterData,
  ChosenDatesRange
} from 'src/modules/shared/models/customer-support/systems-health.model';
import { Chart, DevicesStatus } from '../enum/systems-health.enum';


@Injectable({ providedIn: 'root' })
export class SystemsHealthService {

  constructor(private apiService: ApiService) {}

  public loadSystemsHealthData(id: string, dateFilter?: DateFilter): Observable<SystemsHealthData> {
    return this.apiService.get(this.getDevicesStatusDataEndpoint(id, dateFilter)).pipe(
      switchMap((devicesStatusResponse: DeviceStatusResponse[]) => {
        const reports = this.getDevicesStatusResponseReports(devicesStatusResponse);
        const devicesAvailabilityChart = this.getDevicesAvailabilityChart(devicesStatusResponse, dateFilter);
        const devicesStatus: DeviceStatus[] = [];

        reports.forEach(report => devicesStatus.push(this.prepareDeviceReport(report, devicesAvailabilityChart.devicesAvailability)));

        return of({ devicesAvailabilityChart, devicesStatus });
      })
    );
  }

  public filterDevicesStatus(devicesStatus: DeviceStatus[], filterData: FilterData): DeviceStatus[] {
    return devicesStatus.filter(device => {
      const isReportDay = this.isReportDay(filterData.day, device.datesRange);
      let isAvailabilityMatched: boolean;

      if (filterData.availability) {
        isAvailabilityMatched = this.getDeviceReportAvailability(null, device.status) === filterData.availability;
      }

      return filterData.availability ? isReportDay && isAvailabilityMatched : isReportDay;
    });
  }

  private getDevicesStatusResponseReports(devicesStatusResponse: DeviceStatusResponse[]): DeviceStatusReport[] {
    const reports = devicesStatusResponse.map(deviceStatus => {
      return deviceStatus.report.map(reportItem => ({ ...reportItem, deviceId: deviceStatus.deviceId }));
    });

    return this.sortReportsDescending(flatten(reports));
  }

  private sortReportsDescending(reports: DeviceStatusReport[]): DeviceStatusReport[] {
    return reports.sort((currentReport: DeviceStatusReport, previousReport: DeviceStatusReport) => {
      const currentReportDate = moment(currentReport.createdAt);
      const previousReportDate = moment(previousReport.createdAt);

      return currentReportDate < previousReportDate ? 1 : currentReportDate > previousReportDate ? -1 : 0;
    });
  }

  private prepareDeviceReport(report: DeviceStatusReport, devicesAvailability: DeviceAvailability[]): DeviceStatus {
    const deviceAvailability = devicesAvailability.find(deviceAvailability => deviceAvailability.id === report._id);
    const reportDatesRange = { start: deviceAvailability.start, end: deviceAvailability.end };

    return {
      deviceId: report.deviceId,
      status: this.getDeviceReportStatus(report),
      version: { android: report.androidVersionRelease, app: report.version },
      wifi: { signal: report.wirelessSignal, network: report.wifiNetwork },
      battery: { health: report.batteryHealth, level: report.batteryLevel, chargingStatus: report.powerStatus },
      bluetooth: report.bluetoothIsOk,
      cellularSignal: report.cellularSignal,
      function: DevicesStatus.Function[report.deviceType],
      datesRange: reportDatesRange
    };
  }

  private getDevicesAvailabilityChart(devicesData: DeviceStatusResponse[], dateFilter: DateFilter): DevicesAvailabilityChart {
    const { from, to } = this.getDateFilterValues(dateFilter);
    const devicesAvailability = this.getDevicesAvailabilityData(devicesData);

    return {
      labelFormat: dateFilter ? dateFilter.key : FilterKey.Date.Now,
      valuesRange: { startValue: from, endValue: to },
      latestReport: devicesAvailability.length ? this.getLatestDeviceAvailabilityReport(devicesAvailability) : null,
      devicesAvailability
    };
  }

  private getLatestDeviceAvailabilityReport(devicesAvailability: DeviceAvailability[]): DeviceAvailability {
    return devicesAvailability.reduce((previousValue: DeviceAvailability, nextValue: DeviceAvailability) => {
      return moment(previousValue.start) > moment(nextValue.start) ? previousValue : nextValue;
    });
  }

  private getDateFilterValues(dateFilter: DateFilter): DateFilterValues {
    let from: Date;
    let to: Date;

    if (dateFilter) {
      switch (dateFilter.key) {
        case FilterKey.Date.Now: {
          from = moment().subtract(8, TimeUnit.Hours).toDate();
          to = new Date();
          break;
        }
        case FilterKey.Date.LastDay: {
          from = moment().subtract(2, TimeUnit.Days).toDate();
          to = new Date();
          break;
        }
        case FilterKey.Date.Last7Days: {
          from = moment().subtract(8, TimeUnit.Days).toDate();
          to = new Date();
          break;
        }
        case FilterKey.Date.ChooseDates: {
          const fromMoment = moment(new Date(dateFilter.value[0]));
          const toMoment = moment(new Date(dateFilter.value[1]));
          const diff = toMoment.diff(fromMoment, TimeUnit.Days);
          
          from = diff >= 7 ? fromMoment.subtract(1, TimeUnit.Weeks).toDate() : fromMoment.subtract(1, TimeUnit.Days).toDate();
          to = toMoment.toDate();

          break;
        }
      }
    } else {
      from = moment(to).subtract(8, TimeUnit.Hours).toDate();
      to = new Date();
    }

    return { from, to };
  }

  private getDevicesAvailabilityData(devicesData: DeviceStatusResponse[]): DeviceAvailability[] {
    const result: DeviceAvailability[] = [];
    const reportsTypes = this.getReportsTypes(devicesData);

    for (let i = devicesData.length - 1; i >= 0; i--) {
      const device = devicesData[i];
      const deviceType = this.getDeviceType(reportsTypes, device);

      for (let j = device.report.length - 1; j >= 0; j--) {
        result.push(this.getDeviceAvailability(device.report[j], deviceType));
      }
    }

    return result;
  }

  private getReportsTypes(devicesData: DeviceStatusResponse[]): DevicesStatus.Function[] {
    return devicesData.map(device => device.report[0].deviceType);
  }

  private getDeviceType(reportsTypes: DevicesStatus.Function[], device: DeviceStatusResponse): DevicesStatus.Function {
    const deviceType = device.report[0].deviceType;

    return this.isMoreThanOneDeviceType(reportsTypes, deviceType)
      ? `${DevicesStatus.Function[deviceType]}${this.getRepeatedDeviceTypePosition(reportsTypes, deviceType)}`
      : DevicesStatus.Function[deviceType];
  }

  private getRepeatedDeviceTypePosition(reportsTypes: DevicesStatus.Function[], deviceType: DevicesStatus.Function): number {
    const position = reportsTypes.map(reportsType => reportsType[deviceType]).length;
    const lastReportsTypeIndex = reportsTypes.lastIndexOf(deviceType);

    reportsTypes.splice(lastReportsTypeIndex, 1);

    return position;
  }

  private getDeviceAvailability(report: DeviceStatusReport, deviceType: DevicesStatus.Function): DeviceAvailability {
    const start = moment(report.createdAt).toDate();
    const end = moment(report.createdAt).add(1, TimeUnit.Minutes).toDate();
    const availability = this.getDeviceReportAvailability(report);

    return {
      id: report._id,
      start,
      end,
      type: deviceType,
      availability
    };
  }

  private getDevicesStatusDataEndpoint(id: string, dateFilter: DateFilter): string {
    if (dateFilter) {
      if (dateFilter.key === FilterKey.Date.ChooseDates) {
        const from = moment(dateFilter.value[0]).format(DevicesStatus.DateFilter.Date);
        const to = moment(dateFilter.value[1]).format(DevicesStatus.DateFilter.Date);
  
        return `${APP.endpoints.customerSupport}/${id}/${APP.endpoints.devicesStatus}?from=${from}&to=${to}`;
      } else {
        return `${APP.endpoints.customerSupport}/${id}/${APP.endpoints.devicesStatus}?timespan=${dateFilter.key}`;
      }
    } else {
      const from = moment().format(DevicesStatus.DateFilter.Date);
      const to = moment().format(DevicesStatus.DateFilter.Date);

      return `${APP.endpoints.customerSupport}/${id}/${APP.endpoints.devicesStatus}?from=${from}&to=${to}`;
    }
  }

  private getDeviceReportAvailability(report: DeviceStatusReport, reportStatus?: DevicesStatus.Status): Chart.BarStatus {
    let deviceReportStatus: DevicesStatus.Status;

    if (!reportStatus) {
      deviceReportStatus = this.getDeviceReportStatus(report);
    }

    switch (!reportStatus ? deviceReportStatus : reportStatus) {
      case DevicesStatus.Status.Online: {
        return Chart.BarStatus.Active;
      }
      case DevicesStatus.Status.PoorSignal:
      case DevicesStatus.Status.LowBattery: {
        return Chart.BarStatus.Warning;
      }
      case DevicesStatus.Status.Offline_BatteryEmpty:
      case DevicesStatus.Status.Offline:
      case DevicesStatus.Status.BatteryEmpty: {
        return Chart.BarStatus.Bad;
      }
    }
  }

  private getDeviceReportStatus(report: DeviceStatusReport): DevicesStatus.Status {
    switch (true) {
      case this.checkBatteryStatus(report.batteryLevel, DevicesStatus.Battery.Good) && report.wirelessSignal >= APP.cellularSignals.maxRSSI: {
        return DevicesStatus.Status.Online;
      }
      case this.checkBatteryStatus(report.batteryLevel, DevicesStatus.Battery.Good) && isBetween<number>(report.wirelessSignal, APP.cellularSignals.minRSSI, APP.cellularSignals.maxRSSI): {
        return DevicesStatus.Status.PoorSignal;
      }
      case this.checkBatteryStatus(report.batteryLevel, DevicesStatus.Battery.Warning) && report.wirelessSignal >= APP.cellularSignals.maxRSSI: {
        return DevicesStatus.Status.LowBattery;
      }
      case this.checkBatteryStatus(report.batteryLevel, DevicesStatus.Battery.Bad) && report.wirelessSignal <= APP.cellularSignals.minRSSI: {
        return DevicesStatus.Status.Offline_BatteryEmpty;
      }
      case report.wirelessSignal <= APP.cellularSignals.minRSSI: {
        return DevicesStatus.Status.Offline;
      }
      case this.checkBatteryStatus(report.batteryLevel, DevicesStatus.Battery.Bad): {
        return DevicesStatus.Status.BatteryEmpty;
      }
    }
  }

  private checkBatteryStatus(batteryLevel: number, status: DevicesStatus.Battery): boolean {
    switch (status) {
      case DevicesStatus.Battery.Good: {
        return batteryLevel >= APP.batteryLevel.good;
      }
      case DevicesStatus.Battery.Warning: {
        return batteryLevel >= APP.batteryLevel.bad && batteryLevel < APP.batteryLevel.good;
      }
      case DevicesStatus.Battery.Bad: {
        return batteryLevel < APP.batteryLevel.bad;
      }
    }
  }

  private isReportDay(comparableISOValue: string, chosenDatesRange: ChosenDatesRange): boolean {
    const comparableMoment = moment(comparableISOValue);
    const startMoment = moment(chosenDatesRange.start);
    const endMoment = moment(chosenDatesRange.end);
    const dayEquality = this.isDayEqual(comparableMoment, startMoment, endMoment);
    const timeEquality = this.isTimeEqual(comparableMoment, startMoment, endMoment);

    return dayEquality && timeEquality;
  }

  private isDayEqual(comparableMoment: moment.Moment, startMoment: moment.Moment, endMoment: moment.Moment): boolean {
    const comparableDate = comparableMoment.format(TimeFormat.YearMonthDay);
    const startDate = startMoment.format(TimeFormat.YearMonthDay);
    const endDate = endMoment.format(TimeFormat.YearMonthDay);

    return comparableDate === startDate || comparableDate === endDate;
  }

  private isTimeEqual(comparableMoment: moment.Moment, startMoment: moment.Moment, endMoment: moment.Moment): boolean {
    const comparableTime = comparableMoment.format(TimeFormat.FullTimeWith12Format);
    const startTime = startMoment.format(TimeFormat.FullTimeWith12Format);
    const endTime = endMoment.format(TimeFormat.FullTimeWith12Format);

    return comparableTime === startTime || comparableTime === endTime;
  }

  private isMoreThanOneDeviceType(reportsTypes: DevicesStatus.Function[], deviceType: DevicesStatus.Function): boolean {
    return reportsTypes.includes(deviceType) && !!reportsTypes.reduce((count: number, reportsType: DevicesStatus.Function) => {
      return reportsType === deviceType ? count + 1 : count;
    }, 0);
  }
}
