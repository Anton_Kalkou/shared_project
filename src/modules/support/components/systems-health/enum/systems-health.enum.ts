export enum Translation {
  DeviceId = 'support-requests.systems-health.device-id',
  Function = 'support-requests.systems-health.function',
  Status = 'support-requests.systems-health.status',
  Wifi = 'support-requests.systems-health.network',
  Battery = 'support-requests.systems-health.battery'
}

export enum ColumnDataKey {
  DeviceId = 'deviceId',
  Function = 'function',
  Status = 'status',
  Wifi = 'wifi',
  Battery = 'battery',
  AndroidVersion = 'androidVersion',
  AppVersion = 'appVersion',
  Bluetooth = 'bluetooth',
  CellularSignal = 'cellularSignal'
}

export namespace Chart {
  export enum BarStatus {
    Active = 'active',
    Warning = 'warning',
    Bad = 'bad'
  }

  export enum LabelFormat {
    Time = 'LT',
    Date = 'MMM DD'
  }

  export enum Selector {
    Svg = 'svg',
    SeriesGroup = '.dxc-series-group',
    CrosshairCursor = '.dxc-crosshair-cursor'
  }

  export enum Event {
    Mouseover = 'mouseover'
  }
}

export namespace DevicesStatus {
  export enum Function {
    fixed_pos = 'Fixed POS',
    mobile_pos = 'Mobile POS',
    qsr = 'QSR POS',
    payment = 'Payment Tablet',
    kitchen = 'Kitchen Tablet',
    runner = 'Runner Tablet',
    printer = 'Printer',
    bar = 'Bar POS',
    grocery = 'Grocery POS',
  }

  export enum Status {
    Online = 'Online',
    PoorSignal = 'Poor Signal',
    LowBattery = 'Low Battery',
    Offline = 'Offline',
    BatteryEmpty = 'Battery Empty',
    Offline_BatteryEmpty = 'Offline, Battery Empty',
    GoodClass = 'rs-good',
    WarningClass = 'rs-warning',
    BadClass = 'rs-bad'
  }

  export enum Power {
    TurnedOff = 'Turned Off',
    BatteryEmpty = 'Battery empty',
    Offline = 'Offline',
    Off = 'Off',
    On = 'On',
    DeviceTurnedOn = 'Device turned on',
    Online = 'Online',
    Success = 'Success',
    Fail = 'Fail'
  }

  export enum Wifi {
    HighSignal = 'rs-high-signal',
    LowSignal = 'rs-low-signal',
    HighSignalIcon = 'rs-high-signal-icon',
    LowSignalIcon = 'rs-low-signal-icon'
  }

  export enum Battery {
    NotCharging = 'NOT_CHARGING',
    Level = 'rs-battery-level',
    ChargingIcon = 'rs-charging',
    NotChargingIcon = 'rs-not-charging',
    Good = 'good',
    Warning = 'warning',
    Bad = 'bad'
  }

  export enum Bluetooth {
    Active = 'icon-bluetooth-active',
    Off = 'icon-bluetooth-off'
  }

  export enum BluetoothError {
    NOT_ENABLED = 'Bluetooth not enabled!'
  }

  export enum CellularSignal {
    HighSignalIcon = 'rs-high-signal-icon',
    PoorSignalIcon = 'rs-poor-signal-icon',
    NoSignalIcon = 'rs-no-signal-icon'
  }

  export enum DateFilter {
    Date = 'YYYY-MM-DD'
  }
  
  export enum Icon {
    Calendar = 'calendar_today'
  }

  export enum IconClass {
    IconApp = 'icon-app',
    IconAndroid = 'icon-android'
  }
}
