import { Pipe, PipeTransform } from '@angular/core';
import { DevicesStatus } from '../enum/systems-health.enum';

@Pipe({ name: 'doubleStatus' })
export class DoubleStatusPipe implements PipeTransform {

  transform(status: DevicesStatus.Status): string[] {
    return status ? status.split(', ') : [];
  }
}
