import { DateFilter } from 'src/modules/shared/models/customer-support/systems-health.model';

export class FetchSystemsHealth {
  static readonly type = '[Systems Health] Fetch';

  constructor(public id: string, public dateFilter?: DateFilter) {}
}
