import { Injectable } from '@angular/core';
import { Action, State, StateContext, Store } from '@ngxs/store';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { DeviceStatus } from 'src/modules/shared/models/customer-support/systems-health.model';
import { SystemsHealthService } from '../../services/systems-health.service';
import { SetDevicesAvailabilityChartData } from '../devices-availability-chart/devices-availability-chart.actions';
import { DevicesAvailabilityChartState } from '../devices-availability-chart/devices-availability-chart.state';
import { FetchSystemsHealth } from './systems-health.actions';

export interface SystemsHealthStateModel {
  devicesStatus: DeviceStatus[];
}

@State<SystemsHealthStateModel>({
  name: 'systemsHealth',
  defaults: {
    devicesStatus: []
  },
  children: [DevicesAvailabilityChartState]
})
@Injectable()
export class SystemsHealthState {

  constructor(private store: Store, private systemsHealthService: SystemsHealthService) {}

  @Action(FetchSystemsHealth)
  fetchSystemsHealth({ patchState }: StateContext<SystemsHealthStateModel>, action: FetchSystemsHealth) {
    return this.systemsHealthService.loadSystemsHealthData(action.id, action.dateFilter).subscribe(systemsHealthData => {
      this.store.dispatch(new SetDevicesAvailabilityChartData(systemsHealthData.devicesAvailabilityChart));
      this.store.dispatch(new SetSpinnerVisibility(false));

      patchState({ devicesStatus: systemsHealthData.devicesStatus });
    });
  }
}
