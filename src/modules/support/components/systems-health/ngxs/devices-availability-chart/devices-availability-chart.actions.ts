import { DevicesAvailabilityChart } from 'src/modules/shared/models/customer-support/systems-health.model';

export class SetDevicesAvailabilityChartData {
  static readonly type = '[Devices Availability Chart] Set';

  constructor(public data: DevicesAvailabilityChart) {}
}
