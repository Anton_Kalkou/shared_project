import { Injectable } from '@angular/core';
import { Action, State, StateContext } from '@ngxs/store';
import { DeviceAvailability, ValueRange } from 'src/modules/shared/models/customer-support/systems-health.model';
import { SetDevicesAvailabilityChartData } from './devices-availability-chart.actions';

interface DevicesAvailabilityChartStateModel {
  labelFormat: string;
  valuesRange: ValueRange;
  latestReport: DeviceAvailability;
  devicesAvailability: DeviceAvailability[];
}

@State<DevicesAvailabilityChartStateModel>({
  name: 'devicesAvailabilityChart',
  defaults: {
    labelFormat: null,
    valuesRange: null,
    latestReport: null,
    devicesAvailability: []
  }
})
@Injectable()
export class DevicesAvailabilityChartState {
  
  @Action(SetDevicesAvailabilityChartData)
  setDevicesAvailabilityChartData({ setState }: StateContext<DevicesAvailabilityChartStateModel>, action: SetDevicesAvailabilityChartData) {
    setState({ ...action.data });
  }

}
