import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { APP } from 'src/modules/shared/constants';
import { Batches } from 'src/modules/shared/models/customer-support/payment.model';
import { PayApiService } from 'src/modules/shared/services/pay-api.service';
import { batchPayments } from '../ngxs/batches/mock-data';

@Injectable({ providedIn: 'root' })
export class BatchesService {

  constructor(private payApiService: PayApiService) {}

  public loadBatchesData(query: Batches.Query): Observable<Batches.Batch[]> {
    return this.payApiService.get(APP.endpoints.paymentBatches, query).pipe(
      switchMap((batchesResponse: Batches.BatchResponse[]) => {
        return of(batchesResponse.map(batcheResponse => ({
          date: batcheResponse.createdAt,
          batchId: batcheResponse.id,
          accountNumber: batcheResponse.accountNumber,
          numberOfPayments: batcheResponse.paymentCount,
          amount: `${batcheResponse.totalAmount.symbol} ${batcheResponse.totalAmount.amount}`
        })));
      })
    );
  }

  public loadBatchPaymentsData(id: string): Observable<any[]> {
    return of(batchPayments);
  }
}