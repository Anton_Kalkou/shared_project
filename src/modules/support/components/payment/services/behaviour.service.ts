import { ElementRef, Injectable } from '@angular/core';
import { Transaction } from '../enum/payment.enum';

@Injectable({ providedIn: 'root' })
export class PaymentBehaviourService {

  public handleTransactionsStatus(el: ElementRef, value: any): void {
    if (value === Transaction.Status.Success) {
      this.setClassName(el, Transaction.Status.SuccessClass);
    } else if (value ===Transaction.Status.Fail) {
      this.setClassName(el, Transaction.Status.FailClass);
    }
  }

  private setClassName(el: ElementRef, className: string): void {
    el.nativeElement.classList.add(className);
  }
}
