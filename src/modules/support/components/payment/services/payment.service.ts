import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { Translation as FiltersTranslation } from 'src/modules/shared/components/filters/enum/filters.enum';
import { Translation as TransactionsTranslation } from 'src/modules/shared/components/transactions/enum/transactions.enum';
import { APP } from 'src/modules/shared/constants';
import { DateQuery, Transactions } from 'src/modules/shared/models/customer-support/payment.model';
import { ChosenDatesRange, FilterGroup, ParentFilter } from 'src/modules/shared/models/filter.model';
import { FiltersService } from 'src/modules/shared/services/filters.service';
import { FilterType, TimeUnit } from 'src/modules/support/enum/support.enum';
import { ColumnDataKey } from '../enum/payment.enum';

@Injectable({ providedIn: 'root' })
export class PaymentService {

  constructor(private filtersService: FiltersService) {}

  public filterPaymentData(dataSource: Transactions.Transaction[], filterType: string, filterValue: string | FilterGroup[], searchColumnKey: string): Transactions.Transaction[] {
    if (filterType === FilterType.Search) {
      return this.filtersService.filterDataOnSearch(dataSource, filterValue as string, searchColumnKey);
    } else {
      const filter = filterValue as FilterGroup[];

      if (filter.length) {
        return dataSource.filter(dataSourceItem => {
          if (filter.length === 1) {
            return filter.some(filterItem => {
              const columnDataKey = this.getColumnDataKey(filterItem.title);

              return filterItem.filters.includes(this.getColumnDataByKey(dataSourceItem, columnDataKey));
            });
          } else {
            const columnDataKeys = filter.map(filterItem => this.getColumnDataKey(filterItem.title));

            return filter.every((filterItem, filterValueItemIndex) => {
              return filterItem.filters.includes(this.getColumnDataByKey(dataSourceItem, columnDataKeys[filterValueItemIndex]));
            });
          }
        });
      } else {
        return dataSource;
      }
    }
  }

  public isDateFiltering(filtersValues: ParentFilter[]): boolean {
    return filtersValues.some(filtersValue => APP.parentDatesRangeLabels.includes(filtersValue.parentFilterTitle));
  }

  public getDateFilter(filtersValues: ParentFilter[]): ParentFilter {
    return filtersValues.find(filtersValue => APP.parentDatesRangeLabels.includes(filtersValue.parentFilterTitle));
  }

  public getDateQuery(dateFilter: ParentFilter, datesRange?: ChosenDatesRange): DateQuery {
    let isoDates;

    if (dateFilter.parentFilterOption === FiltersTranslation.Date.LastDay) {
      isoDates = this.getISOStringDates({
        subtract: {
          from: { range: 1, label: TimeUnit.Days }
        }
      });
    } else if (dateFilter.parentFilterOption === FiltersTranslation.Date.Last7Days) {
      isoDates = this.getISOStringDates({
        isFromTo: true,
        isFromToISO: true,
        subtract: {
          from: { range: 7, label: TimeUnit.Days }
        }
      });
    } else if (dateFilter.parentFilterOption === FiltersTranslation.Date.ChooseDates) {
      isoDates = this.getISOStringDates({
        from: datesRange.begin,
        to: datesRange.end
      });
    }

    return this.setDateHours(isoDates.fromDate, isoDates.toDate);
  }

  public getDefaultDatesRange(): DateQuery {
    const fromDate = moment().startOf(TimeUnit.Day).toISOString();
    const toDate = moment().endOf(TimeUnit.Day).toISOString();

    return { fromDate, toDate };
  }

  private setDateHours(fromDate: string, toDate: string): DateQuery {
    return {
      fromDate: moment(fromDate).startOf(TimeUnit.Day).toISOString(),
      toDate: moment(toDate).endOf(TimeUnit.Day).toISOString()
    };
  }

  private getColumnDataByKey(dataSource: Transactions.Transaction, key: string): any {
    if (key === ColumnDataKey.Card) {
      return dataSource[key] ? dataSource[key][ColumnDataKey.Brand] : null
    } else {
      return dataSource[key];
    }
  }

  private getColumnDataKey(filterValueTitle: string): string {
    switch (filterValueTitle) {
      case TransactionsTranslation.Staff: {
        return ColumnDataKey.Staff;
      }
      case TransactionsTranslation.Status: {
        return ColumnDataKey.Stage;
      }
      case TransactionsTranslation.Card: {
        return ColumnDataKey.Card;
      }
    }
  }

  private getISOStringDates(options: any): any {
    let toDate = options.to ? moment(options.to) : moment();
    let fromDate = options.from ? moment(options.from) : options.isFromTo ? moment(options.isFromToISO ? toDate.toISOString() : toDate) : moment();

    if (options.subtract) {
      toDate = options.subtract.to ? toDate.subtract(options.subtract.to.range, options.subtract.to.label) : toDate;
      fromDate = options.subtract.from ? fromDate.subtract(options.subtract.from.range, options.subtract.from.label) : fromDate;
    }

    return { toDate: toDate.toISOString(), fromDate: fromDate.toISOString() };
  }
}
