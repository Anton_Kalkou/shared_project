import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Translation as TransactionsTranslation, Stage, PaymentMethod } from 'src/modules/shared/components/transactions/enum/transactions.enum';
import { APP } from 'src/modules/shared/constants';
import { AdminProfileResponse, DateQuery, Transactions } from 'src/modules/shared/models/customer-support/payment.model';
import { ApiService } from 'src/modules/shared/services/api.service';
import { PayApiService } from 'src/modules/shared/services/pay-api.service';

@Injectable({ providedIn: 'root' })
export class TransactionsService {

  constructor(private payApiService: PayApiService, private apiService: ApiService) {}

  public loadTransactionData(dateQuery: DateQuery): Observable<Transactions.Transaction[]> {
    return this.apiService.get(APP.endpoints.adminProfile).pipe( 
      switchMap((adminProfileData: AdminProfileResponse) => {
        const employees = this.getAdminProfileEmployeesData(adminProfileData);

        return this.payApiService.get(APP.endpoints.paymentTransactions, dateQuery).pipe(
          switchMap((transactionsResponse: Transactions.TransactionsResponse) => {
            if (transactionsResponse.results) {
              return of(
                transactionsResponse.results
                  .map(transaction => {
                    const employee = employees.find(employee => employee.id === transaction.employeeProfileId);

                    if (employee) {
                      const transactionResult: Transactions.Transaction = {
                        date: transaction.createdAt,
                        orderId: transaction.orderId,
                        paymentId: transaction.id,
                        cardHolderName: transaction.cardInfo ? transaction.cardInfo.cardHolderName : null,
                        card: transaction.cardInfo ? { number: transaction.cardInfo.redactedCardNumber, brand: transaction.cardInfo.cardBrand } : null,
                        paymentAmount: `${transaction.amount.amount} ${transaction.amount.symbol}`,
                        tipAmount: `${transaction.tip.amount} ${transaction.tip.symbol}`,
                        staff: `${employee.firstName} ${employee.lastName}`,
                        stage: this.getTranslationKey(transaction.status),
                        method: this.getTranslationKey(transaction.method)
                      };
            
                      return transactionResult;
                    } else {
                      return undefined;
                    }
                  })
                  .filter(transaction => transaction)
              );
            } else {
              return of([]);
            }
          })
        );
      })
    );
  }

  private getAdminProfileEmployeesData(adminProfileData: AdminProfileResponse): Transactions.AdminProfileEmployeeData[] {
    const employees = adminProfileData.results
      .map(resultItem => {
        if (resultItem.venues[0]) {
          return {
            firstName: resultItem.venues[0].firstName,
            lastName: resultItem.venues[0].lastName,
            id: resultItem.id,
          };
        } else {
          return undefined;
        }
      })
      .filter(employee => employee);

    return employees;
  }

  private getTranslationKey(key: string): string {
    switch(key) {
      case Stage.Captured: {
        return TransactionsTranslation.Captured;
      }
      case PaymentMethod.Cash: {
        return TransactionsTranslation.Cash;
      }
    }
  }

}