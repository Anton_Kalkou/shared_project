import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgxsModule } from '@ngxs/store';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from 'saturn-datepicker';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter'
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { APP } from 'src/modules/shared/constants';
import { DataTableModule } from 'src/modules/shared/components/data-table/data-table.module';
import { BatchesComponent } from '../components/batches/batches.component';
import { PaymentComponent } from '../components/payment/payment.component';
import { TransactionsComponent } from '../components/transactions/transactions.component';
import { PaymentState } from '../ngxs/payment/payments.state';
import { BatchComponent } from '../components/batch/batch.component';
import { TransactionsState } from '../ngxs/transactions/transactions.state';
import { BatchesState } from '../ngxs/batches/batches.state';
import { SectionHeaderModule } from '../../section-header/modules/section-header.module';

@NgModule({
  imports: [
    SharedModule,
    RouterModule,
    MaterialModule,
    NgxsModule.forFeature([PaymentState, TransactionsState, BatchesState]),
    SectionHeaderModule,
    DataTableModule
  ],
  declarations: [
    PaymentComponent,
    TransactionsComponent,
    BatchesComponent,
    BatchComponent
  ],
  providers: [
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    { provide: MAT_DATE_FORMATS, useValue: APP.filters.chooseDatesFormat }
  ],
  exports: [
    PaymentComponent
  ]
})
export class PaymentModule {}
