import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { FiltersService as FiltersSliderService } from 'src/modules/shared/components/filters/services/filters.service';
import { APP } from 'src/modules/shared/constants';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { SelectedSupportRequest } from 'src/modules/shared/models/customer-support/support-request.model';
import { Batches } from 'src/modules/shared/models/customer-support/payment.model';
import { Translation as TransactionsTranslation } from 'src/modules/shared/components/transactions/enum/transactions.enum';
import { Translation as SearchTranslation } from 'src/modules/shared/components/search/enum/search.enum';
import { FiltersService } from 'src/modules/shared/services/filters.service';
import { TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum';
import { Column } from 'src/modules/shared/models/data-table.model';
import { ColumnDataKey, Translation as PaymentTranslation } from '../../enum/payment.enum';
import { FetchBatches } from '../../ngxs/batches/batches.actions';
import { PaymentService } from '../../services/payment.service';

@Component({
  selector: 'app-batches',
  templateUrl: './batches.component.html',
  styleUrls: ['./batches.component.scss']
})
export class BatchesComponent extends Unsubscribe implements OnInit {

  @Select(state => state.supportRequests.selectedSupportRequest) selectedSupportRequest$: Observable<SelectedSupportRequest>;
  @Select(state => state.payment.batchesList.batches) batches$: Observable<Batches.Batch[]>;
  @Select(state => state.filters.filtersVisibility) filtersVisibility$: Observable<boolean>;

  public merchantId: string;
  public batches: any[];
  public batchItems: any[];
  public displayedColumns: Column[] = [
    { title: ColumnDataKey.Date, translationKey: TransactionsTranslation.Date, valueType: ValueType.Text, textType: TextType.DateTime },
    { title: ColumnDataKey.BatchId, translationKey: TransactionsTranslation.BatchId, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.AccountNumber, translationKey: TransactionsTranslation.AccountNumber, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.NumberOfPayments, translationKey: TransactionsTranslation.NumberOfPayments, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.Amount, translationKey: TransactionsTranslation.Amount, valueType: ValueType.Text, textType: TextType.Simple }
  ];
  totalItems: number = 1000;
  pageSize: number = 20;
  pageSizeOptions: number[] = [10, 20, 50, 100, 200,  500, 1000];

  headerTitleKey: string = PaymentTranslation.Batches;
  headerSearchPlaceholderKey: string = SearchTranslation.SearchId;
  
  filtersEndpoint: string = APP.endpoints.paymentBatchesFilters;
  filterType: string;
  filteredSource: any[];
  filteredFiltersSource: any[];
  filteredSearchSource: any[];

  constructor(
    private store: Store,
    private filtersSliderService: FiltersSliderService,
    private filtersService: FiltersService,
    private paymentService: PaymentService
  ) {
    super();
  }

  ngOnInit(): void {
    this.initSubscriptions();
  }

  ngOnDestroy(): void {
    this.store.dispatch(new SetSpinnerVisibility(false));
  }

  onSearch(key: string): void {
    this.filtersService.handleFilteringDataOnSearch(
      this,
      this.batches,
      this.paymentService.filterPaymentData.bind(this.paymentService),
      key,
      ColumnDataKey.BatchId
    );
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.selectedSupportRequest$.subscribe(selectedSupportRequest => {
      this.merchantId = selectedSupportRequest.merchantId;

      const defaultDatesRange = this.paymentService.getDefaultDatesRange();
      const query = { merchantId: this.merchantId, startDate: defaultDatesRange.fromDate, endDate: defaultDatesRange.toDate };

      this.store.dispatch(new SetSpinnerVisibility(true));
      this.store.dispatch(new FetchBatches(query));
    });
    this.subscribeTo = this.batches$.subscribe(batches => {
      this.batches = batches;
      this.filteredSource = batches;
    });
    this.subscribeTo = this.filtersVisibility$.subscribe(filtersVisibility => {
      if (!filtersVisibility && this.batches) {
        const activatedFilters = this.filtersSliderService.getActivatedFilters();
        const filtersValues = this.filtersService.getFiltersValues(activatedFilters);

        this.filtersService.handleFilteringDataOnFilters(
          this,
          this.batches,
          this.paymentService.filterPaymentData.bind(this.paymentService),
          filtersValues
        );
      }
    });
  }
}
