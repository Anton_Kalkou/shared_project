import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { SetHeaderTitle } from 'src/modules/layout/ngxs/layout.actions';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { APP } from 'src/modules/shared/constants';
import { Translation as TransactionsTranslation } from 'src/modules/shared/components/transactions/enum/transactions.enum';
import { Translation as SearchTranslation } from 'src/modules/shared/components/search/enum/search.enum';
import { TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum';
import { Column } from 'src/modules/shared/models/data-table.model';
import { FetchBatchPayments } from '../../ngxs/batches/batches.actions';
import { Translation as PaymentTranslation, ColumnDataKey } from '../../enum/payment.enum';

@Component({
  selector: 'app-batch',
  templateUrl: './batch.component.html',
  styleUrls: ['./batch.component.scss']
})
export class BatchComponent extends Unsubscribe {

  @Select(state => state.payment.batchesList.selectedBatchPayments) selectedBatchPayments$: Observable<any[]>;

  public selectedBatchPayments: any[];
  public displayedColumns: Column[] = [
    { title: ColumnDataKey.Date, translationKey: TransactionsTranslation.Date, valueType: ValueType.Text, textType: TextType.DateTime },
    { title: ColumnDataKey.PaymentId, translationKey: TransactionsTranslation.PaymentId, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.OrderId, translationKey: PaymentTranslation.OrderId, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.CardHolderName, translationKey: TransactionsTranslation.CardHolderName, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.Card, translationKey: TransactionsTranslation.Card, valueType: ValueType.Text, textType: TextType.BankCard },
    { title: ColumnDataKey.Source, translationKey: TransactionsTranslation.Source, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.PaymentAmount, translationKey: TransactionsTranslation.PaymentAmount, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.Staff, translationKey: TransactionsTranslation.Staff, valueType: ValueType.Text, textType: TextType.Simple }
  ];
  public totalItems: number = 1000;
  public pageSize: number = 20;
  public pageSizeOptions: number[] = [10, 20, 50, 100, 200,  500, 1000];

  public headerTitleKey: string = PaymentTranslation.BatchPayments;
  public headerSearchPlaceholderKey: string = SearchTranslation.SearchStaff;
  public filtersEndpoint: string = APP.endpoints.paymentBatchFilters;

  constructor(private store: Store, private route: ActivatedRoute) {
    super();
  }

  ngOnInit(): void {
    this.initSubscriptions();
    this.dispatchActions(this.route.snapshot.paramMap.get('id'));
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.selectedBatchPayments$
      .subscribe(selectedBatchPayments => this.selectedBatchPayments = selectedBatchPayments);
  }

  private dispatchActions(id: string): void {
    this.store.dispatch(new FetchBatchPayments(id));
    this.store.dispatch(new SetHeaderTitle(`support-requests.payment.batch ${id}`));
  }
}
