import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable, PartialObserver } from 'rxjs';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { FiltersService as FiltersSliderService } from 'src/modules/shared/components/filters/services/filters.service';
import { APP } from 'src/modules/shared/constants';
import { DateQuery, Transactions } from 'src/modules/shared/models/customer-support/payment.model';
import { FilterGroup } from 'src/modules/shared/models/filter.model';
import { FilterType } from 'src/modules/support/enum/support.enum';
import { Translation as SearchTranslation } from 'src/modules/shared/components/search/enum/search.enum';
import { Translation as TransactionsTranslation } from 'src/modules/shared/components/transactions/enum/transactions.enum';
import { Translation as FilterTranslation } from 'src/modules/shared/components/filters/enum/filters.enum';
import { FiltersService } from 'src/modules/shared/services/filters.service';
import { TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum';
import { Column } from 'src/modules/shared/models/data-table.model';
import { FetchTransactions } from '../../ngxs/transactions/transactions.actions';
import { PaymentService } from '../../services/payment.service';
import { TransactionsService } from '../../services/transactions.service';
import { ColumnDataKey, Translation as PaymentTranslation } from '../../enum/payment.enum';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent extends Unsubscribe implements OnInit {

  @Select(state => state.payment.transactionsList.transactions) transactions$: Observable<Transactions.Transaction[]>;
  @Select(state => state.filters.filtersVisibility) filtersVisibility$: Observable<boolean>;

  public transactions: Transactions.Transaction[];
  public displayedColumns: Column[] = [
    { title: ColumnDataKey.Date, translationKey: TransactionsTranslation.Date, valueType: ValueType.Text, textType: TextType.DateTime },
    { title: ColumnDataKey.OrderId, translationKey: PaymentTranslation.OrderId, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.PaymentId, translationKey: TransactionsTranslation.PaymentId, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.CardHolderName, translationKey: TransactionsTranslation.CardHolderName, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.Card, translationKey: TransactionsTranslation.Card, valueType: ValueType.Text, textType: TextType.BankCard },
    { title: ColumnDataKey.PaymentAmount, translationKey: TransactionsTranslation.PaymentAmount, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.TipAmount, translationKey: TransactionsTranslation.TipAmount, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.Staff, translationKey: TransactionsTranslation.Staff, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.Stage, translationKey: TransactionsTranslation.Stage, valueType: ValueType.Text, textType: TextType.Simple },
    { title: ColumnDataKey.Method, translationKey: TransactionsTranslation.Method, valueType: ValueType.Text, textType: TextType.Simple }
  ];

  public headerTitleKey: string = PaymentTranslation.Payments;
  public headerSearchPlaceholderKey: string = SearchTranslation.SearchStaff;

  public filtersEndpoint: string = APP.endpoints.paymentTransactionsFilters;
  public filteredSource: Transactions.Transaction[];

  private filterType: string;
  private filteredFiltersSource: Transactions.Transaction[];
  private filteredSearchSource: Transactions.Transaction[];

  constructor(
    private store: Store,
    private filtersSliderService: FiltersSliderService,
    private filtersService: FiltersService,
    private paymentService: PaymentService,
    private transactionsService: TransactionsService,
    private changeDetectorRef: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit(): void {
    this.initSubscriptions();
    this.dispatchActions(this.paymentService.getDefaultDatesRange());
  }

  ngOnDestroy(): void {
    this.store.dispatch(new SetSpinnerVisibility(false));
  }

  public onSearch(key: string): void {
    this.filtersService.handleFilteringDataOnSearch(
      this,
      this.transactions,
      this.paymentService.filterPaymentData.bind(this.paymentService),
      key,
      ColumnDataKey.Staff
    );
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.transactions$.subscribe(transactions => this.setTransactionsData(transactions));
    this.subscribeTo = this.filtersVisibility$.subscribe(filtersVisibility => {
      if (!filtersVisibility && this.transactions) {
        const activatedFilters = this.filtersSliderService.getActivatedFilters();
        let filtersValues = this.filtersService.getFiltersValues(activatedFilters);

        if (this.paymentService.isDateFiltering(activatedFilters)) {
          const transactionDateFilter = this.paymentService.getDateFilter(activatedFilters);
          const dateQuery = this.paymentService.getDateQuery(transactionDateFilter, transactionDateFilter.parentFilterDatesRange);
          
          filtersValues = filtersValues.filter(filtersValue => filtersValue.title !== FilterTranslation.Date.ChooseDates);

          this.store.dispatch(new SetSpinnerVisibility(true));

          this.transactionsService.loadTransactionData(dateQuery).subscribe(this.getDateFilteringObserver(filtersValues));
        } else if (activatedFilters.length) {
          this.filtersService.handleFilteringDataOnFilters(
            this,
            this.transactions,
            this.paymentService.filterPaymentData.bind(this.paymentService),
            filtersValues
          );
        } else {
          this.filteredSource = this.transactions;
        }
      }
    });
  }

  private dispatchActions(transactionDateQuery: DateQuery): void {
    this.store.dispatch(new SetSpinnerVisibility(true));
    this.store.dispatch(new FetchTransactions(transactionDateQuery));
  }

  private getDateFilteringObserver(filtersValues: FilterGroup[]): PartialObserver<Transactions.Transaction[]> {
    return {
      next: transactions => this.handleDateFilteringObserver(transactions, transactions, filtersValues),
      error: () => this.handleDateFilteringObserver([], [], filtersValues)
    };
  }

  private handleDateFilteringObserver(
    transactionsData: Transactions.Transaction[],
    filteredSource: Transactions.Transaction[],
    filtersValues: FilterGroup[]
  ): void {
    this.setTransactionsData(transactionsData);
    this.setFilteredSources(filteredSource);
    this.filtersService.handleFilteringDataOnFilters(
      this,
      this.transactions,
      this.paymentService.filterPaymentData.bind(this.paymentService),
      filtersValues
    );
    this.filtersSliderService.removeActivatedDateFilter();
    this.changeDetectorRef.detectChanges();
    this.store.dispatch(new SetSpinnerVisibility(false));
  }

  private setTransactionsData(transactions: Transactions.Transaction[]): void {
    this.transactions = transactions;
    this.filteredSource = transactions;
  }

  private setFilteredSources(transactions: Transactions.Transaction[]): void {
    if (this.filterType === FilterType.Search) {
      this.filteredSearchSource = transactions;
    } else if (this.filterType === FilterType.Filters) {
      this.filteredFiltersSource = transactions;
    }
  }
}
