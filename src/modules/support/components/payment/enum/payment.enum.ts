export namespace Transaction {
  export enum Status {
    Success = 'Success',
    Fail = 'Fail',
    SuccessClass = 'rs-success-status',
    FailClass = 'rs-fail-status'
  }
}

export enum Translation {
  TransactionDate = 'support-requests.payment.transaction-date',
  BatchDate = 'support-requests.payment.batch-date',
  Payments = 'support-requests.payment.payments',
  Batches = 'support-requests.payment.batches',
  OrderId = 'support-requests.payment.order-id',
  BatchPayments = 'support-requests.payment.batch-payments'
}

export enum ColumnDataKey {
  Date = 'date',
  PaymentId = 'paymentId',
  OrderId = 'orderId',
  CardHolderName = 'cardHolderName',
  Source = 'source',
  PaymentAmount = 'paymentAmount',
  Staff = 'staff',
  Stage = 'stage',
  Status = 'status',
  Card = 'card',
  Brand = 'brand',
  BatchId = 'batchId',
  Method = 'method',
  Amount = 'amount',
  TipAmount = 'tipAmount',
  NumberOfPayments = 'numberOfPayments',
  AccountNumber = 'accountNumber'
}
