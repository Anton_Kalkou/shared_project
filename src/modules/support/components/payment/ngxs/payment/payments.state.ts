import { Injectable } from '@angular/core';
import { State } from '@ngxs/store';
import { BatchesState } from '../batches/batches.state';
import { TransactionsState } from '../transactions/transactions.state';


interface PaymentStateModel {}

@State<PaymentStateModel>({
  name: 'payment',
  children: [TransactionsState, BatchesState]
})
@Injectable()
export class PaymentState {}
