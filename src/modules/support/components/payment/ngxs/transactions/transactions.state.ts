import { Injectable } from '@angular/core';
import { Action, State, StateContext, Store } from '@ngxs/store';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { Transactions } from 'src/modules/shared/models/customer-support/payment.model';
import { TransactionsService } from '../../services/transactions.service';
import { FetchTransactions } from './transactions.actions';

interface TransactionsStateModel {
  transactions: Transactions.Transaction[];
}

@State<TransactionsStateModel>({
  name: 'transactionsList',
  defaults: {
    transactions: [],
  }
})
@Injectable()
export class TransactionsState {

  constructor(private store: Store, private transactionsService: TransactionsService) {}

  @Action(FetchTransactions)
  fetchTransactions({ patchState }: StateContext<TransactionsStateModel>, action: FetchTransactions) {
    return this.transactionsService.loadTransactionData(action.dateQuery).subscribe(transactions => {
      this.store.dispatch(new SetSpinnerVisibility(false));
      patchState({ transactions });
    });
  }

}
