import { DateQuery } from 'src/modules/shared/models/customer-support/payment.model';

export class FetchTransactions {
  static readonly type = '[Transactions] Fetch';

  constructor(public dateQuery: DateQuery) {};
}
