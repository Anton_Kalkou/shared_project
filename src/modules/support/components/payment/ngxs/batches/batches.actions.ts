import { Batches } from 'src/modules/shared/models/customer-support/payment.model';

export class FetchBatches {
  static readonly type = '[Batches] Fetch';

  constructor(public query: Batches.Query) {}
}

export class FetchBatchPayments {
  static readonly type = '[Batch Payments] Fetch';

  constructor(public id: string) {}
}

