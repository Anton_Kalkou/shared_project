import { Injectable } from '@angular/core';
import { Action, State, StateContext, Store } from '@ngxs/store';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { Batches } from 'src/modules/shared/models/customer-support/payment.model';
import { BatchesService } from '../../services/batches.service';
import { FetchBatches, FetchBatchPayments } from './batches.actions';

interface BatchesStateModel {
  batches: Batches.Batch[];
  selectedBatchPayments: any[];
}

@State<BatchesStateModel>({
  name: 'batchesList',
  defaults: {
    batches: [],
    selectedBatchPayments: []
  }
})
@Injectable()
export class BatchesState {

  constructor(private batchesService: BatchesService, private store: Store) {}

  @Action(FetchBatches)
  fetchBatches({ patchState }: StateContext<BatchesStateModel>, action: FetchBatches) {
    return this.batchesService.loadBatchesData(action.query).subscribe(batches => {
      this.store.dispatch(new SetSpinnerVisibility(false));
      patchState({ batches });
    });
  }

  @Action(FetchBatchPayments)
  fetchBatchPayments({ patchState }: StateContext<BatchesStateModel>, action: FetchBatchPayments) {
    return this.batchesService.loadBatchPaymentsData(action.id).subscribe(batchPayments => {
      patchState({ selectedBatchPayments: batchPayments });
    });
  }
}
