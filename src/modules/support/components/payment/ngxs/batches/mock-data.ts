export const batches = [
  {
    date: new Date().toLocaleString(),
    stage: 'Capture',
    batchId: +'7654890',
    accountNumber: '***5693',
    numberOfPayments: '234',
    amount: '$ 1,225.00',
    executedBy: 'Marc C.'
  },
  {
    date: new Date().toLocaleString(),
    stage: 'Capture',
    batchId: +'7654890',
    accountNumber: '***5693',
    numberOfPayments: '234',
    amount: '$ 1,225.00',
    executedBy: 'Marc C.'
  },
  {
    date: new Date().toLocaleString(),
    stage: 'Capture',
    batchId: +'7654890',
    accountNumber: '***5693',
    numberOfPayments: '234',
    amount: '$ 1,225.00',
    executedBy: 'Marc C.'
  },
  {
    date: new Date().toLocaleString(),
    stage: 'Capture',
    batchId: +'7654890',
    accountNumber: '***5693',
    numberOfPayments: '234',
    amount: '$ 1,225.00',
    executedBy: 'Marc C.'
  },
  {
    date: new Date().toLocaleString(),
    stage: 'Capture',
    batchId: +'7654890',
    accountNumber: '***5693',
    numberOfPayments: '234',
    amount: '$ 1,225.00',
    executedBy: 'Marc C.'
  },
  {
    date: new Date().toLocaleString(),
    stage: 'Capture',
    batchId: +'7654890',
    accountNumber: '***5693',
    numberOfPayments: '234',
    amount: '$ 1,225.00',
    executedBy: 'Marc C.'
  },
  {
    date: new Date().toLocaleString(),
    stage: 'Capture',
    batchId: +'7654890',
    accountNumber: '***5693',
    numberOfPayments: '234',
    amount: '$ 1,225.00',
    executedBy: 'Marc C.'
  },
  {
    date: new Date().toLocaleString(),
    stage: 'Capture',
    batchId: +'7654890',
    accountNumber: '***5693',
    numberOfPayments: '234',
    amount: '$ 1,225.00',
    executedBy: 'Marc C.'
  },
  {
    date: new Date().toLocaleString(),
    stage: 'Capture',
    batchId: +'7654890',
    accountNumber: '***5693',
    numberOfPayments: '234',
    amount: '$ 1,225.00',
    executedBy: 'Marc C.'
  },
  {
    date: new Date().toLocaleString(),
    stage: 'Capture',
    batchId: +'7654890',
    accountNumber: '***5693',
    numberOfPayments: '234',
    amount: '$ 1,225.00',
    executedBy: 'Marc C.'
  },
  {
    date: new Date().toLocaleString(),
    stage: 'Capture',
    batchId: +'7654890',
    accountNumber: '***5693',
    numberOfPayments: '234',
    amount: '$ 1,225.00',
    executedBy: 'Marc C.'
  },
  {
    date: new Date().toLocaleString(),
    stage: 'Capture',
    batchId: +'7654890',
    accountNumber: '***5693',
    numberOfPayments: '234',
    amount: '$ 1,225.00',
    executedBy: 'Marc C.'
  },
  {
    date: new Date().toLocaleString(),
    stage: 'Capture',
    batchId: +'7654890',
    accountNumber: '***5693',
    numberOfPayments: '234',
    amount: '$ 1,225.00',
    executedBy: 'Marc C.'
  }
];

export const batchPayments = [
  {
    date: new Date().toLocaleString(),
    paymentId: +'123455',
    orderId: +'7654890',
    cardHolderName: 'Fabio Messi',
    card: { type: 'mastercard', value: '***5693' },
    source: 'PT2345',
    paymentAmount: '$ 1,225.00',
    staff: 'Mark C.'
  },
  {
    date: new Date().toLocaleString(),
    paymentId: +'123455',
    orderId: +'7654890',
    cardHolderName: 'Fabio Messi',
    card: { type: 'mastercard', value: '***5693' },
    source: 'PT2345',
    paymentAmount: '$ 1,225.00',
    staff: 'Mark C.'
  },
  {
    date: new Date().toLocaleString(),
    paymentId: +'123455',
    orderId: +'7654890',
    cardHolderName: 'Fabio Messi',
    card: { type: 'mastercard', value: '***5693' },
    source: 'PT2345',
    paymentAmount: '$ 1,225.00',
    staff: 'Mark C.'
  },
  {
    date: new Date().toLocaleString(),
    paymentId: +'123455',
    orderId: +'7654890',
    cardHolderName: 'Fabio Messi',
    card: { type: 'visa', value: '***5693' },
    source: 'PT2345',
    paymentAmount: '$ 1,225.00',
    staff: 'Mark C.'
  },
  {
    date: new Date().toLocaleString(),
    paymentId: +'123455',
    orderId: +'7654890',
    cardHolderName: 'Fabio Messi',
    card: { type: 'mastercard', value: '***5693' },
    source: 'PT2345',
    paymentAmount: '$ 1,225.00',
    staff: 'Mark C.'
  },
  {
    date: new Date().toLocaleString(),
    paymentId: +'123455',
    orderId: +'7654890',
    cardHolderName: 'Fabio Messi',
    card: { type: 'visa', value: '***5693' },
    source: 'PT2345',
    paymentAmount: '$ 1,225.00',
    staff: 'Mark C.'
  },
  {
    date: new Date().toLocaleString(),
    paymentId: +'123455',
    orderId: +'7654890',
    cardHolderName: 'Fabio Messi',
    card: { type: 'mastercard', value: '***5693' },
    source: 'PT2345',
    paymentAmount: '$ 1,225.00',
    staff: 'Mark C.'
  },
  {
    date: new Date().toLocaleString(),
    paymentId: +'123455',
    orderId: +'7654890',
    cardHolderName: 'Fabio Messi',
    card: { type: 'visa', value: '***5693' },
    source: 'PT2345',
    paymentAmount: '$ 1,225.00',
    staff: 'Mark C.'
  },
  {
    date: new Date().toLocaleString(),
    paymentId: +'123455',
    orderId: +'7654890',
    cardHolderName: 'Fabio Messi',
    card: { type: 'visa', value: '***5693' },
    source: 'PT2345',
    paymentAmount: '$ 1,225.00',
    staff: 'Mark C.'
  },
  {
    date: new Date().toLocaleString(),
    paymentId: +'123455',
    orderId: +'7654890',
    cardHolderName: 'Fabio Messi',
    card: { type: 'visa', value: '***5693' },
    source: 'PT2345',
    paymentAmount: '$ 1,225.00',
    staff: 'Mark C.'
  },
  {
    date: new Date().toLocaleString(),
    paymentId: +'123455',
    orderId: +'7654890',
    cardHolderName: 'Fabio Messi',
    card: { type: 'visa', value: '***5693' },
    source: 'PT2345',
    paymentAmount: '$ 1,225.00',
    staff: 'Mark C.'
  },
  {
    date: new Date().toLocaleString(),
    paymentId: +'123455',
    orderId: +'7654890',
    cardHolderName: 'Fabio Messi',
    card: { type: 'visa', value: '***5693' },
    source: 'PT2345',
    paymentAmount: '$ 1,225.00',
    staff: 'Mark C.'
  },
];
