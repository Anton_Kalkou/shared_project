import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NavItemModule } from 'src/modules/shared/components/nav-item/nav-item.module';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { NavigationComponent } from '../components/navigation-items/navigation.component';
import { SupportRequestComponent } from '../components/support-request/support-request.component';
import { WorkingStatusDirective } from '../directives/working-status.directive';
import { LocalTimePipe } from '../pipes/local-time.pipe';
import { OpenningHoursPipe } from '../pipes/openning-hours.pipe';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    MaterialModule,
    TranslateModule,
    NavItemModule
  ],
  declarations: [
    SupportRequestComponent,
    NavigationComponent,
    OpenningHoursPipe,
    LocalTimePipe,
    WorkingStatusDirective,
  ],
  exports: [
    SupportRequestComponent,
    NavigationComponent
  ]
})
export class SupportRequestModule {}
