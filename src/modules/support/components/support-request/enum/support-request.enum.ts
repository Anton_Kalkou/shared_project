export enum WorkingStatus {
  Opened = 'opened',
  Closed = 'closed'
}

export enum OpenningHoursType {
  Time = 'time',
  WeekDays = 'weekDays'
}
