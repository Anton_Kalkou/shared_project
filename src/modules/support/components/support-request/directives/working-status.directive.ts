import { Directive, ElementRef, Input } from '@angular/core';
import { OpenHours } from 'src/modules/shared/models/customer-support/support-request.model';
import { TimeFormat } from 'src/modules/support/enum/support.enum';
import { TimezoneService } from 'src/modules/support/services/time-zone.service';
import { WorkingStatus } from '../enum/support-request.enum';

@Directive({
  selector: '[appWorkingStatus]'
})
export class WorkingStatusDirective {

  @Input() openningHours: OpenHours[];
  @Input() timezone: string;

  constructor(private el: ElementRef, private timezoneService: TimezoneService) {}

  ngOnInit(): void {
    const now = this.timezoneService.getTimezoneDate(this.timezone);
    const currentWeek = now.format(TimeFormat.DayOfWeekName).toUpperCase();
    const currentTime = now.format(TimeFormat.HoursMinutes);

    this.setWorkingStatus(this.el, this.getWorkingStatus(this.openningHours, currentWeek, currentTime));
  }

  private getWorkingStatus(openningHours: OpenHours[], currentWeek: string, currentTime: string): string {
    let workingStatus;

    for (let i = 0; i < openningHours.length; i++) {
      const openningHoursData = this.openningHours[i];
      const workingWeekNameIndex = openningHoursData.daysOfWeek.findIndex(week => week === currentWeek);

      if (workingWeekNameIndex === -1) {
        workingStatus = WorkingStatus.Closed;
      } else {
        if (openningHoursData.periods.length === 1) {
          workingStatus = currentTime >= openningHoursData.periods[0].to ? WorkingStatus.Closed : WorkingStatus.Opened;
        } else {
          workingStatus = currentTime >= openningHoursData.periods[workingWeekNameIndex].to ? WorkingStatus.Closed : WorkingStatus.Opened;
        }
      }
    }

    return workingStatus;
  }

  private setWorkingStatus(el: ElementRef, workingStatus: string): void {
    if (workingStatus === WorkingStatus.Closed) {
      el.nativeElement.innerHTML = `(${WorkingStatus.Closed})`;
      el.nativeElement.classList.add(workingStatus);
    } else {
      el.nativeElement.innerHTML = `(${WorkingStatus.Opened})`;
      el.nativeElement.classList.add(workingStatus);
    }
  }
}
