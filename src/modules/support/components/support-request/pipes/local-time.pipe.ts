import { Pipe, PipeTransform } from '@angular/core';
import { LocalTime } from 'src/modules/shared/models/customer-support/support-request.model';

@Pipe({ name: 'localTime' })
export class LocalTimePipe implements PipeTransform {

  transform(value: LocalTime): string {
    return `${value.time} (GMT ${value.offset})`;
  }
  
}
