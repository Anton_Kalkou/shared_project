import { Pipe, PipeTransform } from '@angular/core';
import { OpeningHoursItem, OpeningHoursPeriod, OpenningHoursData } from 'src/modules/shared/models/customer-support/support-request.model';
import { TimezoneService } from 'src/modules/support/services/time-zone.service';
import { OpenningHoursType } from '../enum/support-request.enum';

@Pipe({ name: 'openningHours' })
export class OpenningHoursPipe implements PipeTransform {

  constructor(private timeZoneService: TimezoneService) {}
  
  transform(openningHoursItem: OpeningHoursItem | string, type: string, period?: OpeningHoursPeriod): string {
    const openningHoursData = this.getOpenningHoursData(openningHoursItem, period);

    if (type === OpenningHoursType.Time) {
      return this.getTime(openningHoursData.time, period);
    } else if (type === OpenningHoursType.WeekDays) {
      return this.getWeekDays(openningHoursData.weekDays);
    } else {
      return `${(openningHoursData.weekDays)}: ${this.getTime(openningHoursData.time)}`;
    }
  }

  private getOpenningHoursData(openningHoursItem: OpeningHoursItem | string, period?: OpeningHoursPeriod): OpenningHoursData {
    if (!period) {
      return { time: (openningHoursItem as OpeningHoursItem).periods[0], weekDays: (openningHoursItem as OpeningHoursItem).daysOfWeek };
    } else {
      return { time: period, weekDays: openningHoursItem as string };
    }
  }

  private getTime(timeData: OpeningHoursPeriod, period?: OpeningHoursPeriod): string {
    const timeFrom = this.timeZoneService.getTimezoneTime(null, period ? period.from : timeData.from);
    const timeTo = this.timeZoneService.getTimezoneTime(null, period ? period.to : timeData.to);
    const time = `${timeFrom} - ${timeTo}`;

    return time;
  }

  private getWeekDays(weekDaysData: string[] | string): string {
    if (typeof weekDaysData === 'string') {
      return weekDaysData;
    } else {
      return weekDaysData.join('-');
    }
  }
}
