import { Component, OnInit } from '@angular/core';
import { PRIMARY_OUTLET, Router } from '@angular/router';
import { APP } from 'src/modules/shared/constants';
import { NavItem } from 'src/modules/shared/models/nav-item.model';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  public navItems: NavItem[] = APP.customerSupportTicketNavItems;

  constructor(private router: Router) {}

  ngOnInit(): void {
    const requestId = this.router.parseUrl(this.router.url).root.children[PRIMARY_OUTLET].segments[2].path;

    this.navItems = APP.customerSupportTicketNavItems.map(item => {
      const link = item.link.replace(':id', requestId);

      return { ...item, link };
    });
  }

}
