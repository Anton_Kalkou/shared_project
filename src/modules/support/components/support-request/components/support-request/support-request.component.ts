import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { FetchSupportRequest } from 'src/modules/support/components/support-requests/ngxs/support-requests.actions';
import { SelectedSupportRequest } from 'src/modules/shared/models/customer-support/support-request.model';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';

@Component({
  selector: 'app-support-request',
  templateUrl: './support-request.component.html',
  styleUrls: ['./support-request.component.scss']
})
export class SupportRequestComponent extends Unsubscribe implements OnInit {

  @Select(state => state.supportRequests.selectedSupportRequest) selectedSupportRequest$: Observable<SelectedSupportRequest>;

  // public systemStatus: string = 'support-requests.rockspoon-core-system-status';

  constructor(private store: Store, private route: ActivatedRoute, private router: Router) {
    super();
  }

  ngOnInit(): void {
    this.initSubscriptions();
    this.dispatchActions();
  }

  public reactOnCallerAccessRights() {
    const path = `access-rights/${this.route.snapshot.paramMap.get('id')}/details`;

    this.router.navigate([path], { relativeTo: this.route });
  }
  private initSubscriptions(): void {
    this.subscribeTo = this.selectedSupportRequest$.subscribe(selectedSupportRequest => {
      if (selectedSupportRequest) {
        this.store.dispatch(new SetSpinnerVisibility(false));
      }
    });
  }

  private dispatchActions(): void {
    this.store.dispatch(new SetSpinnerVisibility(true));
    this.store.dispatch(new FetchSupportRequest(this.route.snapshot.paramMap.get('id')));
  }

}
