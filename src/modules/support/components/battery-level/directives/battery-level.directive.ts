import { Directive, ElementRef, Input } from '@angular/core';
import { BatteryLevel } from '../enum/battery-level.enum';

@Directive({
  selector: '[appBatteryLevel]'
})
export class BatteryLevelDirective {

  @Input() batteryLevel: number;

  constructor(private el: ElementRef) {}

  ngOnInit(): void {
   this.handleBatteryLevel();
  }

  private handleBatteryLevel(): void {
    if (this.batteryLevel > 0 && this.batteryLevel < 20) {
      this.el.nativeElement.classList.add(BatteryLevel.Low);
    }
    if (this.batteryLevel >= 20 && this.batteryLevel < 80) {
      this.el.nativeElement.classList.add(BatteryLevel.Medium);
    }
    if (this.batteryLevel >=80 && this.batteryLevel <= 100) {
      this.el.nativeElement.classList.add(BatteryLevel.High);
    }
  }
}
