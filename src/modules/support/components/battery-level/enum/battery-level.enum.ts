export enum BatteryLevel {
  Low = 'low',
  Medium = 'medium',
  High = 'high'
}
