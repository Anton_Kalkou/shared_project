import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-battery-level',
  templateUrl: './battery-level.component.html',
  styleUrls: ['./battery-level.component.scss']
})
export class BatteryLevelComponent implements OnInit {

  @Input() value: number;

  constructor() {}

  ngOnInit(): void {}

}
