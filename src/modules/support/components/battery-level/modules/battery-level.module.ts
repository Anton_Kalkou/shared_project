import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { BatteryLevelComponent } from '../components/battery-level/battery-level.component';
import { BatteryLevelDirective } from '../directives/battery-level.directive';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  declarations: [
    BatteryLevelComponent,
    BatteryLevelDirective
  ],
  exports: [
    BatteryLevelComponent
  ]
})
export class BatteryLevelModule {}
