import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IconButtonModule } from 'src/modules/shared/components/icon-button/icon-button.module';
import { ButtonFiltersComponent } from '../components/button-filters/button-filters.component';
import { SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { ButtonFilterDirective } from '../directives/button-filter.directive';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    IconButtonModule,
    TranslateModule,
    SatDatepickerModule,
    SatNativeDateModule
  ],
  declarations: [
    ButtonFiltersComponent,
    ButtonFilterDirective
  ],
  exports: [
    ButtonFiltersComponent
  ]
})
export class ButtonFiltersModule {}
