import { Directive, ElementRef, Input } from '@angular/core';
import { ButtonFilterBehaviourClass } from '../enum/button-filters.enum';

@Directive({
  selector: '[appButtonFilter]'
})
export class ButtonFilterDirective {

  @Input() activatedFilter: string;
  @Input() currentFilter: string;

  constructor(private element: ElementRef) {}

  ngOnChanges(): void {
    this.handleButtonFilter();
  }

  private handleButtonFilter(): void {
    const classList = this.element.nativeElement.classList;

    if (classList.contains(ButtonFilterBehaviourClass.Active) && this.currentFilter !== this.activatedFilter) {
      classList.remove(ButtonFilterBehaviourClass.Active);
    } else if (!classList.contains(ButtonFilterBehaviourClass.Active) && this.currentFilter === this.activatedFilter) {
      classList.add(ButtonFilterBehaviourClass.Active);
    }
  }

}
