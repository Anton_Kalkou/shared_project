import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ButtonFilter, ButtonFilterEvent } from 'src/modules/shared/models/customer-support/filters.model';
import { FilterKey } from 'src/modules/shared/components/filters/enum/filters.enum';

@Component({
  selector: 'app-button-filters',
  templateUrl: './button-filters.component.html',
  styleUrls: ['./button-filters.component.scss']
})
export class ButtonFiltersComponent {

  @Input() filters: ButtonFilter[];

  @Output() filter: EventEmitter<ButtonFilterEvent> = new EventEmitter<ButtonFilterEvent>();

  public formGroup: FormGroup;
  public activatedFilter: string;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      chooseDates: new FormControl({ value: { begin: null, end: null }, disabled: true })
    });
    this.activatedFilter = FilterKey.Date.Now;
  }

  public onButtonClick(key: string, value?: any[] | undefined): void {
    this.filter.emit({ key, value });
  }

  public handlePickerClosed(key: string): void {
    const chooseDates = this.formGroup.get(FilterKey.Date.ChooseDates).value;
    
    if (chooseDates.begin && chooseDates.end) {
      this.filter.emit({ key, value: [chooseDates.begin, chooseDates.end] });
    }
  }
}
