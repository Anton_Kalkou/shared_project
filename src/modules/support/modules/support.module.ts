import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AccessRightsModule } from '../components/access-rights/modules/access-rights.module';
import { PaymentModule } from '../components/payment/modules/payment.module';
import { SupportRequestModule } from '../components/support-request/modules/support-request.module';
import { SupportRequestsModule } from '../components/support-requests/modules/support-requests.module';
import { SupportComponent } from '../components/support/support.component';
import { SystemshealtsModule } from '../components/systems-health/modules/systems-health.module';

@NgModule({
  imports: [
    RouterModule,
    PaymentModule,
    SystemshealtsModule,
    AccessRightsModule,
    SupportRequestsModule,
    SupportRequestModule,
  ],
  declarations: [
    SupportComponent
  ],
  exports: [
    SupportComponent
  ]
})
export class SupportModule {}
