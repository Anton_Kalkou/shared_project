export enum FilterType {
  Search = 'search',
  Filters = 'filters'
}

export enum TimeUnit {
  Minutes = 'minutes',
  Hours = 'hours',
  Day = 'day',
  Days = 'days',
  Weeks = 'weeks',
}

export enum TimeFormat {
  DayOfWeekName = 'dddd',
  HoursMinutes = 'HH:mm',
  YearMonthDay = 'YYYY-MM-DD',
  TimeWith12Format = 'LT',
  FullTimeWith12Format = 'LTS'
}

export enum StringReplacementPart {
  RequestId = '{requestId}'
}
