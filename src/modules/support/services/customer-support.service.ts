import { Injectable } from '@angular/core';
import { PRIMARY_OUTLET, Router, UrlSegment } from '@angular/router';
import { NavigationButtonStates } from 'src/modules/layout/enum/layout.enum';
import { APP } from 'src/modules/shared/constants';
import { isSubstring } from 'src/modules/shared/helpers';
import { StringReplacementPart } from '../enum/support.enum';

@Injectable({ providedIn: 'root' })
export class CustomerSupportService {
  private homeRoute: string;

  constructor(private router: Router) {}

  public getNavigationButtonState(): NavigationButtonStates {
    this.setHomeRoute(this.router.parseUrl(this.router.url).root.children[PRIMARY_OUTLET].segments);

    if (this.router.url === `/${APP.pages.customerSupport}/${APP.customerSupportPages.rootRoute}`) {
      return NavigationButtonStates.back;
    }
    if (APP.customerSupportPages.routesWithNothing.some(route => isSubstring(this.router.url, route, true))) {
      return NavigationButtonStates.empty;
    }
    if (APP.customerSupportPages.routesWithClose.some(route => isSubstring(this.router.url, route, true))) {
      return NavigationButtonStates.close;
    }
    if (APP.customerSupportPages.routesBackToHome.some(route => isSubstring(this.router.url, route, true))) {
      return NavigationButtonStates.back;
    }

    return;
  }

  public getHomeRoute(): string {
    return this.homeRoute;
  }

  private setHomeRoute(urlSegments: UrlSegment[]): void {
    if (urlSegments.length > 2) {
      this.homeRoute = `/${APP.customerSupportPages.homeRoute.replace(StringReplacementPart.RequestId, urlSegments[2].path)}`;
    } else {
      this.homeRoute = `/${APP.pages.home}`;
    }
  }
}
