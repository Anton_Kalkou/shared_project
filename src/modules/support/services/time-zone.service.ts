import { Injectable } from '@angular/core';
import * as moment from 'moment-timezone';
import { TimeFormat } from '../enum/support.enum';

@Injectable({ providedIn: 'root' })
export class TimezoneService {

  public getTimezoneDate(timezone: string): moment.Moment {
    return moment().tz(timezone);
  }

  public getTimezoneTime(timezone?: string, time?: string): string {
    if (timezone && time) {
      return moment(time, TimeFormat.TimeWith12Format).tz(timezone).format(TimeFormat.TimeWith12Format);
    }
    if (timezone && !time) {
      return moment().tz(timezone).format(TimeFormat.TimeWith12Format);
    }
    if (!timezone && time) {
      return moment(time, TimeFormat.TimeWith12Format).format(TimeFormat.TimeWith12Format);
    }
  }

  public getTimezoneOffset(timezone: string): number {
    const date = moment().tz(timezone).format();
    const timeZone = date.match(/[+-]\d{2}:/)[0].substring(0, 3);

    return +timeZone;
  }
  
}
