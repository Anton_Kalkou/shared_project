import { Injectable } from '@angular/core';

import { State } from '@ngxs/store';

interface TransactionsStateModel {
  transactions: any[];
}

@State<TransactionsStateModel>({
  name: 'transactions',
  defaults: {
    transactions: [
      {
        merchant: 'Merchant 001',
        id: '12345678',
        type: 'Refunded',
        date: '02/02/2020 at 10:57 AM',
        amount: '$10.00',
        cost: '$1.00',
        netAmount: '$10.00',
        card: 'Visa 0101',
        cardType: 'Credit',
        batchId: '12345678',
        status: 'Pending'
      },
      {
        merchant: 'Merchant 002',
        id: '12345678',
        type: 'Uncaptured',
        date: '02/02/2020 at 10:57 AM',
        amount: '$10.00',
        cost: '$1.00',
        netAmount: '$10.00',
        card: 'Mastercard 0808',
        cardType: 'Credit',
        batchId: '12345678',
        status: 'Pending'
      },
      {
        merchant: 'Merchant 003',
        id: '12345678',
        type: 'Payout',
        date: '02/02/2020 at 10:57 AM',
        amount: '$10.00',
        cost: '$1.00',
        netAmount: '$10.00',
        card: 'Amex 6666',
        cardType: 'Credit',
        batchId: '12345678',
        status: 'Pending'
      },
      {
        merchant: 'Merchant 004',
        id: '12345678',
        type: 'Succeeded',
        date: '02/02/2020 at 10:57 AM',
        amount: '$10.00',
        cost: '$1.00',
        netAmount: '$10.00',
        card: 'Visa 6969',
        cardType: 'Credit',
        batchId: '12345678',
        status: 'Pending'
      }
    ]
  }
})
@Injectable()
export class TransactionsState {}
