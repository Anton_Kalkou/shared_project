import { NgModule } from '@angular/core';

import { NgxsModule } from '@ngxs/store';

import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { TransactionsModule } from 'src/modules/shared/components/transactions/modules/transactions.module';
import { MerchantTransactionsComponent } from '../components/merchants-transactions/merchants-transactions.component';
import { ModuleHeaderModule } from 'src/modules/shared/components/module-header/module-header.module';
import { TransactionsState } from '../ngxs/transactions.state';

@NgModule({
  imports: [
    SharedModule,
    TransactionsModule,
    ModuleHeaderModule,
    NgxsModule.forFeature([TransactionsState])
  ],
  declarations: [
    MerchantTransactionsComponent
  ],
  exports: [
    MerchantTransactionsComponent
  ]
})
export class MerchantsTransactionsModule {}
