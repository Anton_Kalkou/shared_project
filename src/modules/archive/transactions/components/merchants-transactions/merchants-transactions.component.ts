import { Component, OnInit } from '@angular/core';

import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { Transaction } from 'src/modules/shared/models/transaction.model';

@Component({
  selector: 'app-merchants-transactions',
  templateUrl: 'merchants-transactions.component.html',
  styleUrls: ['merchants-transactions.component.scss']
})
export class MerchantTransactionsComponent extends Unsubscribe implements OnInit {

  @Select(state => state.transactions.transactions) transaction$: Observable<Transaction[]>;

  public transactions: Transaction[];

  ngOnInit() {
    this.initSubscriptions();
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.transaction$
      .subscribe(transactions => this.transactions = transactions);
  }

}
