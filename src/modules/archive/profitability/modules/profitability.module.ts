import { NgModule } from '@angular/core';

import { NgxsModule } from '@ngxs/store';
import { LabelModule } from 'src/modules/shared/components/label/label.module';

import { ModuleHeaderModule } from 'src/modules/shared/components/module-header/module-header.module';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { ProfitabilityFiltersComponent } from '../components/profitability-filters/profitability-filters.component';
import { ProfitabilityComponent } from '../components/profitability/profitability.component';
import { TotalTableComponent } from '../components/total-table/total-table.component';
import { ProfitabilityState } from '../ngxs/profitability.state';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    ModuleHeaderModule,
    NgxsModule.forFeature([
      ProfitabilityState
    ]),
    LabelModule
  ],
  declarations: [
    ProfitabilityComponent,
    TotalTableComponent,
    ProfitabilityFiltersComponent
  ],
  exports: [
    ProfitabilityComponent
  ]
})
export class ProfitabilityModule {}
