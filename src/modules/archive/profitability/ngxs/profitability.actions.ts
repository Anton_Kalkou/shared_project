export class GetInitProfitabilityData {
  static readonly type = '[Init Profitability Data] Get';
}

export class GetInitProfitabilityMerchantProfileData {
  static readonly type = '[Init Profitability Merchant Profile Data] Get';
}
