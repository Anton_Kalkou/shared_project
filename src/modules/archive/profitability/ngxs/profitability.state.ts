import { Injectable } from '@angular/core';

import { tap } from 'rxjs/operators';
import { Action, State, StateContext, Store } from '@ngxs/store';

import { SpinnerState } from 'src/modules/shared/classes/spinner-state.class';
import { MerchantProfit, Profit } from 'src/modules/shared/models/profit.model';
import { ProfitabilityService } from '../services/profitability.service';
import { GetInitProfitabilityData, GetInitProfitabilityMerchantProfileData } from './profitability.actions';

interface ProfitabilityStateModel {
  profits: Profit[];
}

@State<ProfitabilityStateModel>({
  name: 'profitability',
  defaults: {
    profits: []
  }
})
@Injectable()
export class ProfitabilityState extends SpinnerState {

  constructor(
    private profitabilityService: ProfitabilityService,
    protected store: Store
  ) {
    super(store);
  }

  @Action(GetInitProfitabilityMerchantProfileData)
  getInitProfitabilityMerchantProfileData({patchState}: StateContext<ProfitabilityStateModel>) {
    this.showSpinner();

    return this.profitabilityService.getMerchantProfileProfits()
      .pipe(
        tap({
          next: (profits: MerchantProfit[]) => {
            patchState({
              profits
            });
          },
          complete: () => this.hideSpinner()
        })
      );
  }

  @Action(GetInitProfitabilityData)
  getInitProfitabilityData({patchState}: StateContext<ProfitabilityStateModel>) {
    this.showSpinner();

    return this.profitabilityService.getProfits()
      .pipe(
        tap({
          next: (profits: Profit[]) => {
            patchState({
              profits
            });
          },
          complete: () => this.hideSpinner()
        })
      );
  }

}
