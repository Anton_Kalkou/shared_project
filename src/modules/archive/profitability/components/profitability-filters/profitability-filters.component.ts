import { Component, EventEmitter } from '@angular/core';

@Component({
  selector: 'rs-profitability-filters',
  templateUrl: 'profitability-filters.component.html',
  styleUrls: ['profitability-filters.component.scss']
})
export class ProfitabilityFiltersComponent {

  public unselectSubject = new EventEmitter<void>();

  public reactOnChangeValue(value: boolean): void {
    if (value) {
      this.unselectSubject.next();
    }
  }

}
