import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';

import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { RenderOnResize } from 'src/modules/shared/classes/render-on-resize.class';
import { Profit } from 'src/modules/shared/models/profit.model';
import { GetInitProfitabilityData, GetInitProfitabilityMerchantProfileData } from '../../ngxs/profitability.actions';

@Component({
  selector: 'rs-profitability',
  templateUrl: 'profitability.component.html',
  styleUrls: ['profitability.component.scss']
})
export class ProfitabilityComponent extends RenderOnResize implements OnInit {

  @Input() isMerchantProfile: boolean;

  @Select(state => state.profitability.profits) profits$: Observable<Profit>;

  constructor(
    private store: Store,
    protected changeDetectorRef: ChangeDetectorRef
  ) {
    super(changeDetectorRef);
  }

  ngOnInit() {
    this.dispatchActions();
    super.ngOnInit();
  }

  private dispatchActions(): void {
    if (this.isMerchantProfile) {
      this.store.dispatch(new GetInitProfitabilityMerchantProfileData());
    } else {
      this.store.dispatch(new GetInitProfitabilityData());
    }
  }

}
