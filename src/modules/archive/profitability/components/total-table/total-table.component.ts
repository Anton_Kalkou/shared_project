import { Component, Input, OnChanges, ViewChild } from '@angular/core';

import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'rs-total-table',
  templateUrl: 'total-table.component.html',
  styleUrls: ['total-table.component.scss']
})
export class TotalTableComponent implements OnChanges {

  @Input() items: any[];
  @Input() isMerchantProfile: boolean;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public displayedColumns: string[];
  public dataSource: MatTableDataSource<any>;

  ngOnChanges() {
    this.prepareTableColumns();
    this.initTable();
  }

  public getTotal(attribute: string): number {
    return this.items.map(t => t[attribute]).reduce((acc, value) => acc + value, 0);
  }

  private prepareTableColumns(): void {
    if (this.isMerchantProfile) {
      this.displayedColumns = [
        'transactionID',
        'date',
        'card',
        'transactionType',
        'transactionAmount',
        'processingCoast',
        'rockSpoonRevenue',
        'bonusFeeMGM',
        'loyaltyGrant',
        'chargeBacks',
        'grossProfit'
      ];
    } else {
      this.displayedColumns = [
        'merchant',
        'transactionAmount',
        'processingCoast',
        'rockSpoonRevenue',
        'bonusFeeMGM',
        'loyaltyGrant',
        'chargeBacks',
        'grossProfit'
      ];
    }
  }

  private initTable(): void {
    console.log(this.items);
    this.dataSource = new MatTableDataSource(this.items);
    this.dataSource.sort = this.sort;
  }

}
