import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { MerchantProfit, Profit } from 'src/modules/shared/models/profit.model';
import { ApiService } from 'src/modules/shared/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class ProfitabilityService {

  constructor(
    private apiService: ApiService
  ) {}

  public getMerchantProfileProfits(): Observable<MerchantProfit[]> {
    return of([
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Visa', number: '*0101' },
        transactionType: 'Dine In',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Maestro', number: '*0808' },
        transactionType: 'Take out',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Visa', number: '*6666' },
        transactionType: 'Delivery',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Maestro', number: '*6969' },
        transactionType: 'Refund',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Visa', number: '*0101' },
        transactionType: 'Dine In',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Maestro', number: '*0808' },
        transactionType: 'Take out',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Visa', number: '*6666' },
        transactionType: 'Delivery',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Maestro', number: '*6969' },
        transactionType: 'Refund',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Visa', number: '*0101' },
        transactionType: 'Dine In',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Maestro', number: '*0808' },
        transactionType: 'Take out',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Visa', number: '*6666' },
        transactionType: 'Delivery',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Maestro', number: '*6969' },
        transactionType: 'Refund',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Visa', number: '*0101' },
        transactionType: 'Dine In',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Maestro', number: '*0808' },
        transactionType: 'Take out',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Visa', number: '*6666' },
        transactionType: 'Delivery',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Maestro', number: '*6969' },
        transactionType: 'Refund',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Visa', number: '*0101' },
        transactionType: 'Dine In',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Maestro', number: '*0808' },
        transactionType: 'Take out',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Visa', number: '*6666' },
        transactionType: 'Delivery',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        transactionID: '12345678',
        date: 'Fri, Feb 2 2020 10:57 AM',
        card: { type: 'Maestro', number: '*6969' },
        transactionType: 'Refund',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      }
    ])
  }

  public getProfits(): Observable<Profit[]> {
    return of([
      {
        merchant: 'Philz Rest',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        merchant: 'Burger & Co',
        transactionAmount: 10,
        processingCoast: 10,
        rockSpoonRevenue: 1,
        bonusFeeMGM: 10,
        loyaltyGrant: 10,
        chargeBacks: 10,
        grossProfit: 10
      },
      {
        merchant: 'Sushi Star',
        transactionAmount: 10,
        processingCoast: 10,
        rockSpoonRevenue: 1,
        bonusFeeMGM: 10,
        loyaltyGrant: 10,
        chargeBacks: 10,
        grossProfit: 10
      },
      {
        merchant: 'Let’s Eat',
        transactionAmount: 10,
        processingCoast: 10,
        rockSpoonRevenue: 1,
        bonusFeeMGM: 10,
        loyaltyGrant: 10,
        chargeBacks: 10,
        grossProfit: 10
      },
      {
        merchant: 'Philz Rest',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        merchant: 'Burger & Co',
        transactionAmount: 10,
        processingCoast: 10,
        rockSpoonRevenue: 1,
        bonusFeeMGM: 10,
        loyaltyGrant: 10,
        chargeBacks: 10,
        grossProfit: 10
      },
      {
        merchant: 'Sushi Star',
        transactionAmount: 10,
        processingCoast: 10,
        rockSpoonRevenue: 1,
        bonusFeeMGM: 10,
        loyaltyGrant: 10,
        chargeBacks: 10,
        grossProfit: 10
      },
      {
        merchant: 'Let’s Eat',
        transactionAmount: 10,
        processingCoast: 10,
        rockSpoonRevenue: 1,
        bonusFeeMGM: 10,
        loyaltyGrant: 10,
        chargeBacks: 10,
        grossProfit: 10
      },
      {
        merchant: 'Philz Rest',
        transactionAmount: 100,
        processingCoast: 12,
        rockSpoonRevenue: 2.8,
        bonusFeeMGM: 2,
        loyaltyGrant: 0.5,
        chargeBacks: 100,
        grossProfit: 9.5
      },
      {
        merchant: 'Burger & Co',
        transactionAmount: 10,
        processingCoast: 10,
        rockSpoonRevenue: 1,
        bonusFeeMGM: 10,
        loyaltyGrant: 10,
        chargeBacks: 10,
        grossProfit: 10
      },
      {
        merchant: 'Sushi Star',
        transactionAmount: 10,
        processingCoast: 10,
        rockSpoonRevenue: 1,
        bonusFeeMGM: 10,
        loyaltyGrant: 10,
        chargeBacks: 10,
        grossProfit: 10
      },
      {
        merchant: 'Let’s Eat',
        transactionAmount: 10,
        processingCoast: 10,
        rockSpoonRevenue: 1,
        bonusFeeMGM: 10,
        loyaltyGrant: 10,
        chargeBacks: 10,
        grossProfit: 10
      }
    ])
      .pipe(delay(1000));
  }

}
