import { NgModule } from '@angular/core';

import { NgxsModule } from '@ngxs/store';

import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { FeesComponent } from '../components/fees/fees.component';
import { FeesState } from '../ngxs/fee.state';
import { FeeComponent } from '../components/fee/fee.component';
import { SquareButtonModule } from 'src/modules/shared/components/square-button/square-button.module';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { DirectivesModule } from 'src/modules/shared/directives/directives.module';
import { ConfirmFeeEditComponent } from '../components/confirm-fee-edit/confirm-fee-edit.component';
import { PopupModule } from 'src/modules/shared/components/popup/popup.module';
import { OpenConfirmEditFeeDirective } from '../directives/open-confirm-edit-fee.directive';
import { ChangelogModule } from 'src/modules/shared/components/changelog/changelog-table.module';
import { FeeMobileItemComponent } from '../components/fee-mobile-item/fee-mobile-item.component';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    NgxsModule.forFeature([FeesState]),
    SquareButtonModule,
    DirectivesModule,
    PopupModule,
    ChangelogModule
  ],
  declarations: [
    FeesComponent,
    FeeComponent,
    ConfirmFeeEditComponent,
    OpenConfirmEditFeeDirective,
    FeeMobileItemComponent
  ],
  exports: [
    FeesComponent
  ]
})
export class FeesModule {}
