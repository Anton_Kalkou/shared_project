import { Injectable } from '@angular/core';

import { State, Store, Action, StateContext } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';
import { forkJoin } from 'rxjs';

import { Fee } from 'src/modules/shared/models/fee.model';
import { GetFeesInitData, ChangeFee } from 'src/modules/merchants/fees/ngxs/fee.actions';
import { FeesService } from '../services/fees.service';
import { DialogsState } from 'src/modules/shared/classes/dialogs-state.class';
import { Changelog } from 'src/modules/shared/models/changelog.model';

interface FeesStateModel {
  fees: Fee[];
  changelog: Changelog[];
}

@State<FeesStateModel>({
  name: 'fees',
  defaults: {
    fees: [],
    changelog: []
  }
})
@Injectable()
export class FeesState extends DialogsState {

  constructor(
    protected store: Store,
    protected dialog: MatDialog,
    private feesService: FeesService
  ) {
    super(store, dialog);
  }

  @Action(ChangeFee)
  changeFee(stateContext: StateContext<FeesStateModel>, {fee}: ChangeFee) {
    this.showSpinner();

    return this.feesService.changeFee(fee)
      .pipe(
        tap({
          complete: () => this.hideSpinnerAndClosePopups()
        })
      );
  }

  @Action(GetFeesInitData)
  getFeesInitData({patchState}: StateContext<FeesStateModel>) {
    this.showSpinner();

    return forkJoin([
      this.feesService.getFees(),
      this.feesService.getFeesChangelog()
    ])
      .pipe(
        tap({
          next: ([fees, changelog]: [Fee[], Changelog[]]) => {
            patchState({
              fees,
              changelog
            });
          },
          complete: () => this.hideSpinner()
        })
      );
  }

}
