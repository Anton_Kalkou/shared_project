import { Fee } from 'src/modules/shared/models/fee.model';

export class GetFeesInitData {
  static readonly type = '[Fees Init Data] Get';
}

export class ChangeFee {
  static readonly type = '[] Get';

  constructor(
    public fee: Fee
  ) {}
}
