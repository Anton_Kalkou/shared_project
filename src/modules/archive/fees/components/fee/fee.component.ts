import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { Fee } from 'src/modules/shared/models/fee.model';
import { BaseFee } from 'src/modules/shared/classes/base-fee.class';
import { copy } from 'src/modules/shared/helpers';

@Component({
  selector: 'rs-fee',
  templateUrl: 'fee.component.html',
  styleUrls: ['fee.component.scss']
})
export class FeeComponent extends BaseFee implements OnInit {

  @Input() fee: Fee;

  constructor(
    protected formBuilder: FormBuilder
  ) {
    super(formBuilder);
  }

  ngOnInit() {
    super.fee = copy(this.fee);
    super.ngOnInit();
  }
  

}
