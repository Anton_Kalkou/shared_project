import { Component, Inject } from '@angular/core';

import { StepperSelectionEvent } from '@angular/cdk/stepper';
import { Store } from '@ngxs/store';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { APP } from 'src/modules/shared/constants';
import { Fee } from 'src/modules/shared/models/fee.model';
import { ChangeFee } from '../../ngxs/fee.actions';


@Component({
  selector: 'rs-confirm-fee-edit',
  templateUrl: 'confirm-fee-edit.component.html',
  styleUrls: ['confirm-fee-edit.component.scss']
})
export class ConfirmFeeEditComponent {

  public nextVisibility = true;

  constructor(
    private store: Store,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public fee: Fee
  ) {}

  public getTranslationValue(value: string): {value: string} {
    return {
      value
    };
  }

  public reactOnStarted(): void {
    this.store.dispatch(new ChangeFee({} as Fee));
  }

  public reactOnCanceled(): void {
    this.dialog.getDialogById(APP.dialogs.confirmEditFee).close();
  }

  public reactOnChangeStep(event: StepperSelectionEvent) {
    if (event.selectedIndex === 1) {
      this.nextVisibility = false;
    } else {
      this.nextVisibility = true;
    }
  }

}
