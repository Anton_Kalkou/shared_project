import { Component, OnInit, ChangeDetectorRef } from '@angular/core';

import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { GetFeesInitData } from '../../ngxs/fee.actions';
import { Fee } from 'src/modules/shared/models/fee.model';
import { Changelog } from 'src/modules/shared/models/changelog.model';
import { RenderOnResize } from 'src/modules/shared/classes/render-on-resize.class';
import { ResolutionService } from 'src/modules/shared/services/resolution.service';

@Component({
  selector: 'rs-fees',
  templateUrl: 'fees.component.html',
  styleUrls: ['fees.component.scss']
})
export class FeesComponent extends RenderOnResize implements OnInit {

  @Select(state => state.fees.fees) fees$: Observable<Fee[]>;
  @Select(state => state.fees.changelog) changelog$: Observable<Changelog[]>;

  constructor(
    protected changeDetectorRef: ChangeDetectorRef,
    private store: Store,
    private resolutionService: ResolutionService
  ) {
    super(changeDetectorRef);
  }

  ngOnInit() {
    super.ngOnInit();
    this.dispatchActions();
  }

  public mobileItemsVisibility(): boolean {
    return this.resolutionService.mobileOnly();
  }

  private dispatchActions(): void {
    this.store.dispatch(new GetFeesInitData());
  }

}
