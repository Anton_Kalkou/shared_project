import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';

import { Fee } from 'src/modules/shared/models/fee.model';
import { BaseFee } from 'src/modules/shared/classes/base-fee.class';

@Component({
  selector: 'rs-fee-mobile-item',
  templateUrl: 'fee-mobile-item.component.html',
  styleUrls: ['fee-mobile-item.component.scss']
})
export class FeeMobileItemComponent extends BaseFee implements OnInit {

  @Input() fee: Fee;

  constructor(
    protected formBuilder: FormBuilder
  ) {
    super(formBuilder);
  }

  ngOnInit() {
    super.fee = this.fee;
    super.ngOnInit();
  }

}
