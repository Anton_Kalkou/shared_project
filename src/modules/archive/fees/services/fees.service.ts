import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { ApiService } from 'src/modules/shared/services/api.service';
import { Fee } from 'src/modules/shared/models/fee.model';
import { Changelog } from 'src/modules/shared/models/changelog.model';

@Injectable({
  providedIn: 'root'
})
export class FeesService {

  constructor(
    private apiService: ApiService
  ) {}

  public getFeesChangelog(): Observable<Changelog[]> {
    return of([
      {
        date: '05/05/2020',
        time: '10:10 AM',
        madeBy: 'Davon Philips',
        change: 'Edited 1 record',
        action: 'Edited',
        changes: [
          {from: 'xxxxx', to: 'yyyyy', field: 'Fee'},
          {from: 'yyyyy', to: 'xxxxx', field: 'Fee'},
          {from: 'xxxxx', to: 'yyyyy', field: 'Fee'},
          {from: 'yyyyy', to: 'xxxxx', field: 'Fee'}
        ]
      },
      {
        date: '05/05/2020',
        time: '10:10 AM',
        madeBy: 'Davon Philips',
        change: 'Edited 1 record',
        action: 'Edited',
        changes: [
          {from: 'xxxxx', to: 'yyyyy', field: 'Fee'},
          {from: 'yyyyy', to: 'xxxxx', field: 'Fee'},
          {from: 'xxxxx', to: 'yyyyy', field: 'Fee'},
          {from: 'yyyyy', to: 'xxxxx', field: 'Fee'}
        ]
      },
      {
        date: '05/05/2020',
        time: '10:10 AM',
        madeBy: 'Davon Philips',
        change: 'Edited 1 record',
        action: 'Edited',
        changes: [
          {from: 'xxxxx', to: 'yyyyy', field: 'Fee'},
          {from: 'yyyyy', to: 'xxxxx', field: 'Fee'},
          {from: 'xxxxx', to: 'yyyyy', field: 'Fee'},
          {from: 'yyyyy', to: 'xxxxx', field: 'Fee'}
        ]
      },
      {
        date: '05/05/2020',
        time: '10:10 AM',
        madeBy: 'Davon Philips',
        change: 'Edited 1 record',
        action: 'Edited',
        changes: [
          {from: 'xxxxx', to: 'yyyyy', field: 'Fee'},
          {from: 'yyyyy', to: 'xxxxx', field: 'Fee'},
          {from: 'xxxxx', to: 'yyyyy', field: 'Fee'},
          {from: 'yyyyy', to: 'xxxxx', field: 'Fee'}
        ]
      }
    ])
      .pipe(delay(500));
  }

  public getFees(): Observable<Fee[]> {
    return of([
      {
        title: 'Card Present',
        items: [
          {
            selected: true,
            type: 'Fixed/Flat',
            percent: 7,
            price: 0
          },
          {
            selected: false,
            type: 'Interchange +',
            percent: 7,
            price: 0
          }
        ]
      },
      {
        title: 'Card on file (Rockspoon GO, and Consumer App)',
        items: [
          {
            selected: true,
            type: 'Fixed/Flat',
            percent: 7,
            price: 0
          },
          {
            selected: false,
            type: 'Interchange +',
            percent: 7,
            price: 0
          }
        ]
      },
      {
        title: 'Keyed',
        items: [
          {
            selected: true,
            type: 'Fixed/Flat',
            percent: 7,
            price: 0
          },
          {
            selected: false,
            type: 'Interchange +',
            percent: 7,
            price: 0
          }
        ]
      },
      {
        title: 'Other Card not present',
        items: [
          {
            selected: true,
            type: 'Fixed/Flat',
            percent: 7,
            price: 0
          },
          {
            selected: false,
            type: 'Interchange +',
            percent: 7,
            price: 0
          }
        ]
      }
    ])
      .pipe(delay(500));
  }

  public changeFee(fee: Fee): Observable<void> {
    return of(null)
      .pipe(delay(500));
  }

}
