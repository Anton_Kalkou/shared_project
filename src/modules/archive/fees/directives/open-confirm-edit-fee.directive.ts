import { Directive, HostListener, Input, Output, EventEmitter } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';

import { ConfirmFeeEditComponent } from '../components/confirm-fee-edit/confirm-fee-edit.component';
import { APP } from 'src/modules/shared/constants';
import { Fee } from 'src/modules/shared/models/fee.model';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';

@Directive({
  selector: '[rsOpenConfirmEditFee]'
})
export class OpenConfirmEditFeeDirective extends Unsubscribe {

  @Output() close = new EventEmitter<void>();

  @Input() fee: Fee;

  constructor(
    private dialog: MatDialog
  ) {
    super();
  }

  @HostListener('click')
  openConfirmEditFeeDialog(): void {
    const dialogRef = this.dialog.open(ConfirmFeeEditComponent, {
      id: APP.dialogs.confirmEditFee,
      width: '800px',
      data: this.fee
    });

    this.subscribeTo = dialogRef.afterClosed()
      .subscribe(() => this.close.emit());
  }

}
