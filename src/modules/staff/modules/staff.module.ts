import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NgxsModule } from '@ngxs/store';

import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { StaffComponent } from '../components/staff/staff.component';
import { StaffListComponent } from '../components/staff-list/staff-list.component';
import { SearchModule } from 'src/modules/shared/components/search/search.module';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { StaffState } from '../ngxs/staff.state';
import { SquareButtonModule } from 'src/modules/shared/components/square-button/square-button.module';
import { PopupModule } from 'src/modules/shared/components/popup/popup.module';
import { OpenAddStaffDirective } from '../directives/open-add-staff.directive';
import { AddStaffComponent } from '../components/add-staff/add-staff.component';
import { UserIconModule } from 'src/modules/shared/components/user-icon/user-icon.module';
import { CropperModule } from 'src/modules/shared/components/cropper/cropper.module';
import { TakePhotoModule } from 'src/modules/shared/components/take-photo/take-photo.module';
import { PipesModule } from 'src/modules/shared/pipes/pipes.module';
import { ConfirmationModule } from 'src/modules/shared/components/confirmation/confirmation.module';
import { AddButtonModule } from 'src/modules/shared/components/add-button/add-button.module';
import { IconButtonModule } from 'src/modules/shared/components/icon-button/icon-button.module';

@NgModule({
  imports: [
    SharedModule,
    RouterModule,
    NgxsModule.forFeature([StaffState]),
    MaterialModule,
    IconButtonModule,
    SearchModule,
    SquareButtonModule,
    PopupModule,
    UserIconModule,
    CropperModule,
    TakePhotoModule,
    PipesModule,
    ConfirmationModule,
    AddButtonModule
  ],
  declarations: [
    StaffComponent,
    StaffListComponent,
    OpenAddStaffDirective,
    AddStaffComponent
  ],
  exports: [
    StaffComponent
  ]
})
export class StaffModule {}
