import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { ApiService } from 'src/modules/shared/services/api.service';
import { User } from 'src/modules/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class StaffService {

  constructor(
    private apiService: ApiService
  ) {}

  public deleteUser(userId: number): Observable<void> {
    // return this.apiService.delete(`${APP.endpoints.users}/${userId}`);
    return of(null)
      .pipe(
        delay(500)
      );
  }

  public disableUser(userId: number): Observable<void> {
    // return this.apiService.patch(`${APP.endpoints.users}/${userId}`, {disabled: true});
    return of(null)
      .pipe(
        delay(500)
      );
  }

  public getUsers(): Observable<any[]> {
    // return this.apiService.get(APP.endpoints.users);
    return of([
      {
        id: 1,
        firstName: 'Abbie',
        middleName: 'Qwerty',
        lastName: 'Logan',
        birthDate: '1996-03-21',
        image: 'https://www.dictionary.com/e/wp-content/uploads/2018/04/square-up.jpg',
        role: 'Viewer',
        accessRights: 'Sales Portal, Editor’s Portal, Merchants'
      },
      {
        id: 2,
        firstName: 'Joshua',
        middleName: 'Qwerty',
        lastName: 'Perez',
        birthDate: '1996-03-21',
        image: 'https://www.dictionary.com/e/wp-content/uploads/2018/04/square-up.jpg',
        role: 'Editor',
        accessRights: 'Sales Portal'
      },
      {
        id: 3,
        firstName: 'Nettie',
        middleName: 'Qwerty',
        lastName: 'Greer',
        birthDate: '1996-03-21',
        image: 'https://www.dictionary.com/e/wp-content/uploads/2018/04/square-up.jpg',
        role: 'Editor',
        accessRights: 'Merchants'
      },
      {
        id: 4,
        firstName: 'Norman',
        middleName: 'Qwerty',
        lastName: 'Daniel',
        birthDate: '1996-03-21',
        image: 'https://www.dictionary.com/e/wp-content/uploads/2018/04/square-up.jpg',
        role: 'Admin',
        accessRights: 'Staff Management'
      }
    ])
      .pipe(
        delay(500)
      );
  }

  public addUser(user: User): Observable<void> {
    // return this.apiService.post(APP.endpoints.users, user);
    return of(null)
      .pipe(
        delay(500)
      );
  }

  public editUser(user: User): Observable<void> {
    // return this.apiService.put(APP.endpoints.users, user);
    return of(null)
      .pipe(
        delay(500)
      );
  }

}
