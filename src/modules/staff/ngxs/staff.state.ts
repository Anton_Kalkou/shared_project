import { Injectable } from '@angular/core';

import { State, Action, StateContext, Store } from '@ngxs/store';
import { tap, first } from 'rxjs/operators';
import { forkJoin } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

import { GetStaffInitData, AddStaff, SetPhoto, DeleteUser, DisableUser, EditStaff } from './staff.actions';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { StaffService } from '../services/staff.service';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { TranslationService } from 'src/modules/shared/services/translation.service';

class StaffStateModel {
  users: any[];
  photoUrl: string | ArrayBuffer;
}

@State<StaffStateModel>({
  name: 'staff',
  defaults: {
    users: [],
    photoUrl: null
  }
})
@Injectable()
export class StaffState {

  constructor(
    private store: Store,
    private staffService: StaffService,
    private notificationService: NotificationService,
    private dialog: MatDialog,
    private translationService: TranslationService
  ) {}

  @Action(EditStaff)
  editStaff({patchState, getState}: StateContext<StaffStateModel>, {user}: EditStaff) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.staffService.editUser(user)
      .pipe(
        tap(() => {
          const users = [...getState().users];
          const userIndex = users.findIndex(currentUser => currentUser.id === user.id);

          users.splice(userIndex, 1, user);
          patchState({
            users
          });
          this.finishRequestWithMessage('notification-titles.edit-user', 'success-message.edit-user');
        })
      );
  }

  @Action(DeleteUser)
  deleteUser({patchState, getState}: StateContext<StaffStateModel>, {userId}: DeleteUser) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.staffService.deleteUser(userId)
      .pipe(
        tap(() => {
          const users = [...getState().users];
          const userIndex = users.findIndex(user => user.id === userId);

          users.splice(userIndex, 1);
          patchState({
            users
          });
          this.finishRequestWithMessage('notification-titles.delete-user', 'success-message.delete-user');
        })
      );
  }

  @Action(DisableUser)
  disableUser({patchState, getState}: StateContext<StaffStateModel>, {userId}: DeleteUser) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.staffService.disableUser(userId)
      .pipe(
        tap(() => {
          const users = [...getState().users];
          const userIndex = users.findIndex(user => user.id === userId);

          users.splice(userIndex, 1);
          patchState({
            users
          });
          this.finishRequestWithMessage('notification-titles.disable-user', 'success-message.disable-user');
        })
      );
  }

  @Action(SetPhoto)
  setPhoto({patchState}: StateContext<StaffStateModel>, {photoUrl}: SetPhoto) {
    patchState({
      photoUrl
    });
  }

  @Action(GetStaffInitData)
  getStaffInitData({patchState}: StateContext<StaffStateModel>) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return forkJoin([
      this.staffService.getUsers()
    ])
      .pipe(
        tap(responses => {
          patchState({
            users: responses[0]
          });
          this.store.dispatch(new SetSpinnerVisibility(false));
        })
      );
  }

  @Action(AddStaff)
  addStaff({patchState, getState}: StateContext<StaffStateModel>, {user}: AddStaff) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.staffService.addUser(user)
      .pipe(
        tap(() => {
          const users = [...getState().users, user];

          patchState({
            users
          });
          this.finishRequestWithMessage('notification-titles.add-user', 'success-message.add-user');
        })
      );
  }

  private finishRequestWithMessage(title: string, content: string): void {
    forkJoin([
      this.translationService.translate(title),
      this.translationService.translate(content)
    ])
      .pipe(
        first()
      )
      .subscribe(translatedResults => {
        this.store.dispatch(new SetSpinnerVisibility(false));
        this.notificationService.showSuccessMessage(translatedResults[0], translatedResults[1]);
        this.dialog.closeAll();
      });
  }

}
