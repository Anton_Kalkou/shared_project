import { User } from 'src/modules/shared/models/user.model';

export class GetStaffInitData {
  static readonly type = '[Staff Init Data] Get';
}

export class AddStaff {
  static readonly type = '[Staff] Add';

  constructor(
    public user: User
  ) {}
}

export class EditStaff {
  static readonly type = '[Staff] Edit';

  constructor(
    public user: User
  ) {}
}

export class SetPhoto {
  static readonly type = '[Photo] Set';

  constructor(
    public photoUrl: string | ArrayBuffer
  ) {}
}

export class DisableUser {
  static readonly type = '[User] Disable';

  constructor(
    public userId: number
  ) {}
}

export class DeleteUser {
  static readonly type = '[User] Delete';

  constructor(
    public userId: number
  ) {}
}
