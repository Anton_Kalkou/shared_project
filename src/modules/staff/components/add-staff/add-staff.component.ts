import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Store, Select } from '@ngxs/store';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';

import { APP } from 'src/modules/shared/constants';
import { AddStaff, SetPhoto, DeleteUser, DisableUser, EditStaff } from '../../ngxs/staff.actions';
import { CustomCropperComponent } from 'src/modules/shared/components/cropper/cropper.component';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { TakePhotoComponent } from 'src/modules/shared/components/take-photo/take-photo.component';
import { validateDate } from 'src/modules/shared/validators';
import { ResolutionService } from 'src/modules/shared/services/resolution.service';

@Component({
  selector: 'app-staff',
  templateUrl: 'add-staff.component.html',
  styleUrls: ['add-staff.component.scss']
})
export class AddStaffComponent extends Unsubscribe implements OnInit {

  @Select(state => state.staff.photoUrl) photoUrl$: Observable<string | ArrayBuffer>;

  public roles: string[] = APP.roles;
  public addStaffForm: FormGroup;
  public imgUrl: string | ArrayBuffer;
  public user: any;

  constructor(
    private store: Store,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) private dialogData,
    public resolutionService: ResolutionService
  ) {
    super();

    if (this.dialogData.user) {
      this.user = this.dialogData.user;
      this.store.dispatch(new SetPhoto(this.user.image));
    } else {
      this.store.dispatch(new SetPhoto(null));
    }
  }

  ngOnInit() {
    this.initForm();
    this.initSubscriptions();
  }

  public deleteUser(): void {
    this.store.dispatch(new DeleteUser(this.user.id));
  }

  public disableUser(): void {
    this.store.dispatch(new DisableUser(this.user.id));
  }

  public openTakePhoto(): void {
    this.dialog.open(TakePhotoComponent, {
      width: '400px',
      id: APP.dialogs.takePhoto,
      data: {
        caption: 'dialogs.take-photo'
      }
    });
  }

  public reactOnFileSelection(event: Event): void {
    const reader = new FileReader();

    reader.readAsDataURL((event.target as HTMLInputElement).files[0]);
    reader.onload = () => {
      this.openCropper(reader.result);
    };
  }


  public nameVisibility(): boolean {
    return this.addStaffForm.value.firstName || this.addStaffForm.value.middleName || this.addStaffForm.value.lastName;
  }

  public save(): void {
    if (this.user) {
      this.store.dispatch(new EditStaff({...this.addStaffForm.value, id: this.user.id}));
    } else {
      this.store.dispatch(new AddStaff(this.addStaffForm.value));
    }
  }

  public close(): void {
    this.dialog.getDialogById(APP.dialogs.addStaff).close();
  }

  private initForm(): void {
    this.addStaffForm = this.formBuilder.group({
      firstName: [this.user ? this.user.firstName : '', Validators.required],
      middleName: [this.user ? this.user.middleName : '', Validators.required],
      lastName: [this.user ? this.user.lastName : '', Validators.required],
      image: ['', Validators.required],
      birthDate: [this.user ? this.user.birthDate : '', [Validators.required, validateDate]],
      role: [this.user ? APP.roles.find(role => role === this.user.role) : '', Validators.required],
      accessRights: ['Sales Portal', Validators.required]
    });
  }

  private openCropper(imgUrl: string | ArrayBuffer): void {
    this.dialog.open(CustomCropperComponent, {
      width: '800px',
      id: APP.dialogs.cropper,
      data: {
        caption: 'dialogs.crop-your-photo',
        imgUrl
      }
    });
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.photoUrl$
      .subscribe(photoUrl => {
        this.imgUrl = photoUrl;
        this.addStaffForm.controls.image.reset(this.imgUrl);
      });
  }

}
