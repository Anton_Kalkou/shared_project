import { Component, OnInit, ViewChild } from '@angular/core';

import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { GetStaffInitData, DeleteUser } from '../../ngxs/staff.actions';

@Component({
  selector: 'app-staff-list',
  templateUrl: 'staff-list.component.html',
  styleUrls: ['staff-list.component.scss']
})
export class StaffListComponent extends Unsubscribe implements OnInit {

  @Select(state => state.staff.users) users$: Observable<any[]>;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public displayedColumns: string[] = [
    'name',
    'role',
    'accessRights',
    'actions'
  ];
  public displayedUsers: any[];
  public dataSource: MatTableDataSource<any>;

  private users: any[];

  constructor(
    private store: Store
  ) {
    super();
  }

  ngOnInit() {
    this.dispatchActions();
    this.initSubscriptions();
  }

  public reactOnDeleteUser(userId: number): void {
    this.store.dispatch(new DeleteUser(userId));
  }

  public reactOnSearch(key: string): void {
    this.displayedUsers = this.users.filter(user => {
      if (
        `${user?.firstName} ${user?.lastName}`.toLowerCase().includes(key.toLowerCase()) ||
        user.role.toLowerCase().includes(key.toLowerCase())  ||
        user.accessRights.toLowerCase().includes(key.toLowerCase())
      ) {
        return true;
      }

      return false;
    });

    this.dataSource.data = this.displayedUsers;
  }

  private dispatchActions(): void {
    this.store.dispatch(new GetStaffInitData());
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.users$
      .subscribe(users => {
        this.users = users;
        this.displayedUsers = [...users];
        this.dataSource = new MatTableDataSource(this.displayedUsers);
        this.dataSource.sort = this.sort;
        this.defineSortingAccessor();
      });
  }

  private defineSortingAccessor(): void {
    this.dataSource.sortingDataAccessor = (user, property) => {
      switch (property) {
        case 'name':
          return `${user?.firstName} ${user?.lastName}`;
        case 'role':
          return user?.role;
        case 'accessRights':
          return user?.accessRights;
        default:
          console.warn('unknown sort property');

          return 0;
      }
    };
  }

}
