import { Directive, HostListener, Input } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';

import { AddStaffComponent } from '../components/add-staff/add-staff.component';
import { APP } from 'src/modules/shared/constants';
import { User } from 'src/modules/shared/models/user.model';
import { ResolutionService } from 'src/modules/shared/services/resolution.service';

@Directive({
  selector: '[appOpenAddStaff]'
})
export class OpenAddStaffDirective {

  @Input() user: User;

  constructor(
    private dialog: MatDialog,
    private resolutionService: ResolutionService
  ) {}

  @HostListener('click')
  openAddStaffDialog(): void {
    this.dialog.open(AddStaffComponent, {
      width: '1200px',
      height: this.getPopupHeight(),
      id: APP.dialogs.addStaff,
      data: {
        user: this.user
      }
    });
  }

  private getPopupHeight(): string {
    if (this.resolutionService.laptopAndLess()) {
      return '100vh';
    } else {
      return '84vh';
    }
  }

}
