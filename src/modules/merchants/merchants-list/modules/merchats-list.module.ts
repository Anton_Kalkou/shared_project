import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NgxsModule } from '@ngxs/store';
import { ClickOutsideModule } from 'ng-click-outside';

import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { MerchantsListComponent } from '../components/merchant-list/merchants-list.component';
import { MerchantPreviewComponent } from '../components/preview/merchant-preview.component';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { SquareButtonModule } from 'src/modules/shared/components/square-button/square-button.module';
import { MerchantMobileItemComponent } from '../components/mobile-item/merchant-mobile-item.component';
import { MerchantStatusesModule } from 'src/modules/shared/components/merchant-statuses/merchant-statuses.module';
import { FiltersModule } from 'src/modules/shared/components/filters/modules/filters.module';
import { ExportModule } from 'src/modules/shared/components/export/modules/export.module';
import { AddButtonModule } from 'src/modules/shared/components/add-button/add-button.module';
import { ModuleHeaderModule } from 'src/modules/shared/components/module-header/module-header.module';
import { MerchantsListState } from '../ngxs/merchants-list.state';
import { PipesModule } from 'src/modules/shared/pipes/pipes.module';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    SquareButtonModule,
    MerchantStatusesModule,
    ClickOutsideModule,
    FiltersModule,
    ExportModule,
    AddButtonModule,
    RouterModule,
    NgxsModule.forFeature([MerchantsListState]),
    ModuleHeaderModule,
    PipesModule
  ],
  declarations: [
    MerchantsListComponent,
    MerchantMobileItemComponent,
    MerchantPreviewComponent
  ]
})
export class MerchantsListModule {}
