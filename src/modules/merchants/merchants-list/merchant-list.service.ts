import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { ENDPOINTS } from 'src/modules/shared/constants/endpoints';
import { BusinessEntity } from 'src/modules/shared/models/business-entity.model';
import { ApiService } from 'src/modules/shared/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class MerchantListService {

  constructor(
    private apiService: ApiService
  ) {}

  public getBusinessEntities(): Observable<BusinessEntity[]> {
    return this.apiService.get(ENDPOINTS.BUSINESS_ENTITY);
  }

}
