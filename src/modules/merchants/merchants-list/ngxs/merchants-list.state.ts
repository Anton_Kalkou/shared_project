import { Injectable } from '@angular/core';

import { State, Action, StateContext, Store } from '@ngxs/store';
import { forkJoin } from 'rxjs';
import { tap } from 'rxjs/operators';

import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { GetMerchantsInitData, OpenMerchantPreview, CloseMerchantPreview, CopyValue } from './merchants-list.atctions';
import { MerchantBackend } from 'src/modules/shared/models/merchant.model';
import { BankingInformation } from 'src/modules/shared/models/banking-info.model';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { TranslationService } from 'src/modules/shared/services/translation.service';
import { APP } from 'src/modules/shared/constants';
import { MerchantListService } from '../merchant-list.service';
import { MerchantProfileService } from '../../merchant-profile/merchant-profile/merchant-profile.service';
import { BusinessEntity } from 'src/modules/shared/models/business-entity.model';

interface MerchantsListStateModel {
  merchants: BusinessEntity[];
  merchantPreviewVisibility: boolean;
  merchantPreview: MerchantBackend;
  merchantPreviewId: string;
  merchantPreviewBankingInformation: BankingInformation;
}

@State<MerchantsListStateModel>({
  name: 'merchantsList',
  defaults: {
    merchants: [],
    merchantPreviewVisibility: false,
    merchantPreview: null,
    merchantPreviewId: null,
    merchantPreviewBankingInformation: null
  }
})
@Injectable()
export class MerchantsListState {

  constructor(
    private store: Store,
    private merchantListService: MerchantListService,
    private merchantProfileService: MerchantProfileService,
    private notificationService: NotificationService,
    private translationService: TranslationService
  ) {}

  @Action(CopyValue)
  copyValue(stateContext, {value}: CopyValue) {
    this.translationService.translate(APP.notificationTitles.valueCopied)
      .subscribe(title => {
        this.notificationService.showSuccessMessage(title, '', {timeOut: 1500});
        navigator.clipboard.writeText(value);
      });
  }

  @Action(CloseMerchantPreview)
  closeMerchantPreview({patchState}: StateContext<MerchantsListStateModel>) {
    patchState({
      merchantPreviewVisibility: false
    });
  }

  @Action(OpenMerchantPreview)
  setMerchantPreviewVisibility({patchState}: StateContext<MerchantsListStateModel>, {merchantId}: OpenMerchantPreview) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    // return forkJoin([
    //   this.merchantProfileService.getBusinessEntity(merchantId),
    //   this.merchantListService.getMerchantBankingInfo(merchantId)
    // ])
    //   .pipe(
    //     tap((responses: [MerchantBackend, BankingInformation]) => {
    //       console.log(responses);
    //       patchState({
    //         merchantPreviewVisibility: true,
    //         merchantPreview: responses[0],
    //         merchantPreviewId: merchantId,
    //         merchantPreviewBankingInformation: responses[1]
    //       });
    //       this.store.dispatch(new SetSpinnerVisibility(false));
    //     })
    //   );
  }

  @Action(GetMerchantsInitData)
  getMerchantsInitData({patchState}: StateContext<MerchantsListStateModel>) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return forkJoin([
      this.merchantListService.getBusinessEntities()
    ])
      .pipe(
        tap(responses => {
          patchState({
            merchants: responses[0]
          });
          this.store.dispatch(new SetSpinnerVisibility(false));
        })
      );
  }

}
