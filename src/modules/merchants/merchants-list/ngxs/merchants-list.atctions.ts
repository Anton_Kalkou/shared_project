export class GetMerchantsInitData {
  static readonly type = '[Merchants Init Data] Get';
}

export class OpenMerchantPreview {
  static readonly type = '[Merchant Preview Visibility] Set';

  constructor(
    public merchantId: string
  ) {}
}

export class CloseMerchantPreview {
  static readonly type = '[Merchant Preview] Close';
}

export class CopyValue {
  static readonly type = '[Value] Copy';

  constructor(
    public value: string
  ) {}
}
