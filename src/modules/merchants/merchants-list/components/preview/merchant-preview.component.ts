import { Component, OnInit } from '@angular/core';

import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { darkness, fromRightToLeft } from 'src/modules/shared/animations';
import { CloseMerchantPreview, CopyValue } from '../../ngxs/merchants-list.atctions';
import { MerchantBackend } from 'src/modules/shared/models/merchant.model';

@Component({
  selector: 'app-merchant-preview',
  templateUrl: 'merchant-preview.component.html',
  styleUrls: ['merchant-preview.component.scss'],
  animations: [
    darkness,
    fromRightToLeft
  ]
})
export class MerchantPreviewComponent extends Unsubscribe implements OnInit {

  @Select(state => state.merchantsList.merchantPreviewVisibility) merchantPreviewVisibility$: Observable<boolean>;
  @Select(state => state.merchantsList.merchantPreview) merchantPreview$: Observable<MerchantBackend>;
  @Select(state => state.merchantsList.merchantPreviewId) merchantPreviewId$: Observable<string>;

  public merchantPreviewVisibility: boolean;
  public merchantPreview: MerchantBackend;
  public merchantPreviewId: string;

  constructor(
    private store: Store
  ) {
    super();
  }

  ngOnInit() {
    this.initSubscriptions();
  }

  public copyValue(value: string): void {
    this.store.dispatch(new CopyValue(value));
  }

  public closePreview(): void {
    this.store.dispatch(new CloseMerchantPreview());
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.merchantPreviewVisibility$
      .subscribe((merchantPreviewVisibility: boolean) => this.merchantPreviewVisibility = merchantPreviewVisibility);
    this.subscribeTo = this.merchantPreview$
      .subscribe((merchantPreview: MerchantBackend) => this.merchantPreview = merchantPreview);
    this.subscribeTo = this.merchantPreviewId$
      .subscribe((merchantPreviewId: string) => this.merchantPreviewId = merchantPreviewId);
  }


}
