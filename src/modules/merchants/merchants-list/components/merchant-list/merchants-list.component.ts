import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';

import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { Merchant } from 'src/modules/shared/models/merchant-mock.model';
import { RenderOnResize } from 'src/modules/shared/classes/render-on-resize.class';
import { GetMerchantsInitData, OpenMerchantPreview } from '../../ngxs/merchants-list.atctions';
import { ResolutionService } from 'src/modules/shared/services/resolution.service';

@Component({
  selector: 'app-merchants-list',
  templateUrl: 'merchants-list.component.html',
  styleUrls: ['merchants-list.component.scss']
})
export class MerchantsListComponent extends RenderOnResize implements OnInit {

  @Select(state => state.merchantsList.merchants) merchants$: Observable<Merchant[]>;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public dataSource: MatTableDataSource<Merchant>;
  public displayedColumns: string[] = [
    'name',
    'location',
    'id',
    'status',
    'plan',
    'actions'
  ];
  public displayedMerchants: Merchant[];

  private merchants: Merchant[];

  constructor(
    private store: Store,
    private resolutionService: ResolutionService,
    protected changeDetectorRef: ChangeDetectorRef
  ) {
    super(changeDetectorRef);
  }

  ngOnInit() {
    super.ngOnInit();
    this.dispatchActions();
    this.initSubscriptions();
  }

  public openMerchantPreview(merchantId: string, event?: MouseEvent): void {
    if (event) {
      event.stopPropagation();
    }

    this.store.dispatch(new OpenMerchantPreview(merchantId));
  }

  public tableVisibility(): boolean {
    return this.resolutionService.laptopAndMore();
  }

  public mobileItemsVisibility(): boolean {
    return this.resolutionService.tabletAndLess();
  }

  public searchMerchants(key: string): void {
    this.displayedMerchants = this.merchants.filter(merchant => {
      if (
        merchant.name.toLowerCase().includes(key.toLowerCase()) ||
        merchant.id.toLowerCase().includes(key.toLowerCase()) ||
        merchant.location.toLowerCase().includes(key.toLowerCase()) ||
        merchant.plan.toLowerCase().includes(key.toLowerCase()) ||
        merchant.status.toLowerCase().includes(key.toLowerCase())
      ) {
        return true;
      }

      return false;
    });

    this.dataSource.data = this.displayedMerchants;
  }

  private dispatchActions(): void {
    this.store.dispatch(new GetMerchantsInitData());
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.merchants$
      .subscribe(merchants => {
        this.merchants = merchants;
        this.displayedMerchants = [...merchants];
        this.dataSource = new MatTableDataSource(this.displayedMerchants);
        this.dataSource.sort = this.sort;
      });
  }

}
