import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Merchant } from 'src/modules/shared/models/merchant-mock.model';

@Component({
  selector: 'app-merchant-mobile-item',
  templateUrl: 'merchant-mobile-item.component.html',
  styleUrls: ['merchant-mobile-item.component.scss']
})
export class MerchantMobileItemComponent {

  @Output() openPreviewEvent = new EventEmitter<void>();

  @Input() item: Merchant;

  public openPreview(event: MouseEvent): void {
    event.stopPropagation();
    this.openPreviewEvent.emit();
  }

}
