import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../shared/modules/shared.module';
import { MerchantProfileModule } from './merchant-profile/merchant-profile/merchant-profile.module';
import { MerchantsListModule } from './merchants-list/modules/merchats-list.module';
import { MerchantsComponent } from './merchants.component';

@NgModule({
  imports: [
    SharedModule,
    RouterModule,
    MerchantProfileModule,
    MerchantsListModule
  ],
  declarations: [
    MerchantsComponent
  ],
  exports: [
    MerchantsComponent
  ]
})
export class MerchantsModule {}
