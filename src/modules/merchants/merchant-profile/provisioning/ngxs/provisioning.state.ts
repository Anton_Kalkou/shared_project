import { Injectable, NgZone } from '@angular/core';

import { Action, State, StateContext } from '@ngxs/store';

import { Provisioning } from 'src/modules/shared/models/provisioning.model';
import { History } from 'src/modules/shared/models/history.model';
import { OpenResponseLogPopup } from './provisioning.actions';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ComponentType } from '@angular/cdk/portal';
import { DIALOGS } from 'src/modules/shared/constants/dialogs';
import { ProvisioningResponseLogPopupComponent } from '../components/provisioning-response-log-popup/provisioning-response-log-popup.component';

interface ProvisioningStateModel {
  provisioning: Provisioning[];
  provisioningHistory: History[];
}

@State<ProvisioningStateModel>({
  name: 'provisioning',
  defaults: {
    provisioning: [
      { title: 'Elavon', status: 'Pending' },
      { title: 'Ingenico', status: 'Pending' }
    ],
    provisioningHistory: [
      {
        date: '05/05/2020 at 10:10 AM',
        company: 'Elavon',
        type: 'Update',
        reason: 'Auto Revalidation',
        status: 'Pending'
      },
      {
        date: '05/05/2020 at 10:10 AM',
        company: 'Ingenico',
        type: 'Update',
        reason: 'Auto Revalidation',
        status: 'Pending'
      },
      {
        date: '05/05/2020 at 10:10 AM',
        company: 'Ingenico',
        type: 'Create',
        reason: 'Validation',
        status: 'Denied'
      },
      {
        date: '05/05/2020 at 10:10 AM',
        company: 'Elavon',
        type: 'Create',
        reason: 'Validation',
        status: 'Denied'
      }
    ]
  }
})
@Injectable()
export class ProvisioningState {

  constructor(
    private readonly dialog: MatDialog,
    private readonly ngZone: NgZone
  ){}

  @Action(OpenResponseLogPopup)
  openEditorsTagDialog() {
    this.openPopup(ProvisioningResponseLogPopupComponent);
  }

  private openPopup(component: ComponentType<any>) {
    const matDialogConfig: MatDialogConfig = {
      width: DIALOGS.widths.provisioning,
      height: DIALOGS.height.provisioning,
      id: DIALOGS.ids.provisioning,
      disableClose: true
    }  as MatDialogConfig;

    this.ngZone.run(() => {
      this.dialog.open(component, matDialogConfig);
    });
  }

}
