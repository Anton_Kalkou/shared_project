import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { History } from 'src/modules/shared/models/history.model';

@Component({
  selector: 'rs-provisioning-response-log-popup',
  templateUrl: 'provisioning-response-log-popup.component.html',
  styleUrls: ['provisioning-response-log-popup.component.scss']
})
export class ProvisioningResponseLogPopupComponent extends Unsubscribe implements OnInit {

  @Select(state => state.provisioning.provisioningHistory) provisioningHistory$: Observable<History[]>;

  public lastTesponseForm: FormGroup;
  public provisioningHistory: History;

  public ngOnInit(): void {
    this.provisioningHistory$
      .subscribe( (histories: History[]) => {
        if (histories.length) {
          this.provisioningHistory = histories[histories.length - 1];
        }
      });
    this.initForm();
  }

  private initForm(): void {
    this.lastTesponseForm = new FormGroup({
      date: new FormControl(this.provisioningHistory ? this.provisioningHistory.date.split(' at ')[0] : null),
      time:  new FormControl(this.provisioningHistory ? this.provisioningHistory.date.split(' at ')[1] : null),
      company:  new FormControl(this.provisioningHistory ? this.provisioningHistory.company : null),
      status:  new FormControl(this.provisioningHistory ? this.provisioningHistory.status: null),
      fullReason: new FormControl(this.provisioningHistory ? this.provisioningHistory.reason: null)
    });
    this.lastTesponseForm.disable();
  }

}
