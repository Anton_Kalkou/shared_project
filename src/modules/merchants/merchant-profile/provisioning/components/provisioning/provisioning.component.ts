import { Component } from '@angular/core';

import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { Provisioning } from 'src/modules/shared/models/provisioning.model';
import { History } from 'src/modules/shared/models/history.model';

@Component({
  selector: 'app-provisioning',
  templateUrl: 'provisioning.component.html',
  styleUrls: ['provisioning.component.scss']
})
export class ProvisioningComponent {

  @Select(state => state.provisioning.provisioning) provisioning$: Observable<Provisioning[]>;
  @Select(state => state.provisioning.provisioningHistory) provisioningHistory$: Observable<History[]>;

}
