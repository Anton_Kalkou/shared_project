import { Component, Input } from '@angular/core';
import { Store } from '@ngxs/store';

import { Provisioning } from 'src/modules/shared/models/provisioning.model';
import { OpenResponseLogPopup } from '../../ngxs/provisioning.actions';

@Component({
  selector: 'app-provisioning-status',
  templateUrl: 'provisioning-status.component.html',
  styleUrls: ['provisioning-status.component.scss']
})
export class ProvisioningStatusComponent {

  @Input() provisioning: Provisioning[];

  constructor(private readonly store: Store){}

  public showResponseLogPopup(): void {
    this.store.dispatch(new OpenResponseLogPopup());
  }

}
