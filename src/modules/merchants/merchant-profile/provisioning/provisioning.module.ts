import { NgModule } from '@angular/core';

import { NgxsModule } from '@ngxs/store';

import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { HistoryTableModule } from 'src/modules/shared/components/history-table/history-table.module';
import { ConfirmationModule } from 'src/modules/shared/components/confirmation/confirmation.module';
import { AddButtonModule } from 'src/modules/shared/components/add-button/add-button.module';
import { ProvisioningState } from './ngxs/provisioning.state';
import { ProvisioningComponent } from './components/provisioning/provisioning.component';
import { ProvisioningStatusComponent } from './components/provisioning-status/provisioning-status.component';
import { PopupModule } from 'src/modules/shared/components/popup/popup.module';
import { ProvisioningResponseLogPopupComponent } from './components/provisioning-response-log-popup/provisioning-response-log-popup.component';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    NgxsModule.forFeature([ProvisioningState]),
    HistoryTableModule,
    ConfirmationModule,
    AddButtonModule,
    PopupModule
  ],
  declarations: [
    ProvisioningComponent,
    ProvisioningStatusComponent,
    ProvisioningResponseLogPopupComponent,
  ],
  exports: [
    ProvisioningComponent
  ]
})
export class ProvisioningModule {}
