import { NgModule } from '@angular/core';

import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { RiskComponent } from './component/risk.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    RiskComponent
  ],
  exports: [
    RiskComponent
  ]
})
export class RiskModule {}
