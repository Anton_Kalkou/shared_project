import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { APP } from 'src/modules/shared/constants';
import { PayApiService } from 'src/modules/shared/services/pay-api.service';

@Injectable({
  providedIn: 'root'
})
export class DevicesService {

  constructor(
    private payApiService: PayApiService
  ) {}

  public getDevices(merchantId: string): Observable<any> {
    return this.payApiService.get(`${APP.endpoints.payment}/${APP.endpoints.deviceProfile}`, {assigned: true});
  }

}
