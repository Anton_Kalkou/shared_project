import { Injectable } from '@angular/core';

import { State, Store, Action, StateContext } from '@ngxs/store';
import { tap } from 'rxjs/operators';

import { Device } from 'src/modules/shared/models/device.model';
import { DevicesService } from '../services/devices.service';
import { GetDevices } from './devices.actions';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';

interface DevicesStateModel {
  devices: Device[];
}

@State<DevicesStateModel>({
  name: 'devices',
  defaults: {
    devices: [
      {
        terminalID: '12341234',
        locationAddresses: '1200 1st St., San Jose, CA',
        venue: 'Venue 1',
        brand: 'Ingenico',
        model: 'Ing M70',
        serialNumber: '1234567890'
      },
      {
        terminalID: '12345678',
        locationAddresses: '1200 1st St., San Jose, CA',
        venue: 'Venue 1',
        brand: 'Samsung',
        model: 'Galaxy Tab A10',
        serialNumber: '6789067890'
      },
      {
        terminalID: '56785678',
        locationAddresses: '1200 1st St., San Jose, CA',
        venue: 'Venue 1',
        brand: 'Motorola',
        model: 'Ing M70',
        serialNumber: '1234512345'
      },
      {
        terminalID: '87654321',
        locationAddresses: '1200 1st St., San Jose, CA',
        venue: 'Venue 1',
        brand: 'Ingenico',
        model: 'Ing M70',
        serialNumber: '6789012345'
      }
    ]
  }
})
@Injectable()
export class DevicesState {

  constructor(
    private store: Store,
    private devicesService: DevicesService
  ) {}

  @Action(GetDevices)
  getDevices({patchState}: StateContext<DevicesStateModel>, {merchantId}: GetDevices) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.devicesService.getDevices(merchantId)
      .pipe(
        tap(devices => {
          console.log(devices);
        })
      );
  }

}
