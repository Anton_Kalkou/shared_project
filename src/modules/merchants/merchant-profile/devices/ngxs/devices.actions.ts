export class GetDevices {
  static readonly type = '[Devices] Get';

  constructor(
    public merchantId: string
  ) {}
}
