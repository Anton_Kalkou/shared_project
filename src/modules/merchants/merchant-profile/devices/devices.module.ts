import { NgModule } from '@angular/core';

import { NgxsModule } from '@ngxs/store';

import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { SquareButtonModule } from 'src/modules/shared/components/square-button/square-button.module';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { DevicesState } from './ngxs/devices.state';
import { DevicesComponent } from './components/devices/devices.component';
import { DeviceMobileItemComponent } from './components/device-mobile-item/device-mobile-item.component';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    SquareButtonModule,
    NgxsModule.forFeature([DevicesState])
  ],
  declarations: [
    DevicesComponent,
    DeviceMobileItemComponent
  ],
  exports: [
    DevicesComponent
  ]
})
export class DevicesModule {}
