import { Component, Input } from '@angular/core';

import { Device } from 'src/modules/shared/models/device.model';

@Component({
  selector: 'app-device-mobile-item',
  templateUrl: 'device-mobile-item.component.html'
})
export class DeviceMobileItemComponent {

  @Input() device: Device;

}
