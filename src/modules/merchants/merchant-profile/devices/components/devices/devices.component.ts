import { Component, OnInit, ViewChild } from '@angular/core';

import { MatSort } from '@angular/material/sort';
import { Select, Store } from '@ngxs/store';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { Device } from 'src/modules/shared/models/device.model';
import { SetExportVisibility } from 'src/modules/shared/components/export/ngxs/export.actions';
import { OpenFilters } from 'src/modules/shared/components/filters/ngxs/filters.actions';
import { APP } from 'src/modules/shared/constants';
import { ResolutionService } from 'src/modules/shared/services/resolution.service';

@Component({
  selector: 'app-devices',
  templateUrl: 'devices.component.html',
  styleUrls: ['devices.component.scss']
})
export class DevicesComponent extends Unsubscribe implements OnInit {

  @Select(state => state.devices.devices) devices$: Observable<Device[]>;

  @ViewChild(MatSort, { static: true }) sort: MatSort;
  public displayedColumns = [
    'terminalID',
    'locationAddresses',
    'venue',
    'brand',
    'model',
    'serialNumber'
  ];
  public devices: Device[];
  public sourceData: MatTableDataSource<Device>;

  constructor(
    private store: Store,
    private resolutionService: ResolutionService
  ) {
    super();
  }

  ngOnInit() {
    this.initSubscriptions();
  }

  public tableVisibility(): boolean {
    return this.resolutionService.laptopAndMore();
  }

  public mobileItemsVisibility(): boolean {
    return this.resolutionService.tabletAndLess();
  }

  public openExport(): void {
    this.store.dispatch(new SetExportVisibility(true, 'devices.devices-list'));
  }

  public openFilters(): void {
    this.store.dispatch(new OpenFilters(APP.endpoints.devicesFilters));
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.devices$
      .subscribe(devices => this.initTable(devices));
  }

  private initTable(devices: Device[]): void {
    this.devices = devices;
    this.sourceData = new MatTableDataSource(devices);
    this.sourceData.sort = this.sort;
  }

}
