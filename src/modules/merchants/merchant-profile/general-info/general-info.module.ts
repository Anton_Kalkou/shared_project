import { NgModule } from '@angular/core';

import { NgxsModule } from '@ngxs/store';

import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { SquareButtonModule } from 'src/modules/shared/components/square-button/square-button.module';
import { PopupModule } from 'src/modules/shared/components/popup/popup.module';
import { MerchantAccordionItemModule } from 'src/modules/shared/components/merchant-accordion-item/merchant-accordion-item.module';
import { ChangelogModule } from 'src/modules/shared/components/changelog/changelog-table.module';
import { ConfirmationModule } from 'src/modules/shared/components/confirmation/confirmation.module';
import { GeneralInfoState } from './ngxs/general-info.state';
import { CompanyOwnersInformationComponent } from './components/company-owners-information/company-owners-information.component';
import { GeneralInfoComponent } from './components/general-info/general-info.component';
import { IndividualOwnersInformationComponent } from './components/individual-owners-information/individual-owners-information.component';
import { BankInformationComponent } from './components/bank-information/bank-information.component';
import { SalesInformationComponent } from './components/sales-information/sales-information.component';
import { RestaurantSetUpComponent } from './components/restaurant-set-up/restaurant-set-up.component';
import { LegalEntityInformationComponent } from './components/legal-entity-information/legal-entity-information.component';
import { ControlWithMaskModule } from 'src/modules/shared/components/control-with-mask/control-with-mask.module';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    SquareButtonModule,
    PopupModule,
    MerchantAccordionItemModule,
    ChangelogModule,
    ConfirmationModule,
    NgxsModule.forFeature([GeneralInfoState]),
    ControlWithMaskModule
  ],
  declarations: [
    CompanyOwnersInformationComponent,
    GeneralInfoComponent,
    IndividualOwnersInformationComponent,
    BankInformationComponent,
    SalesInformationComponent,
    RestaurantSetUpComponent,
    LegalEntityInformationComponent
  ],
  exports: [
    GeneralInfoComponent
  ]
})
export class GeneralInfoModule {}
