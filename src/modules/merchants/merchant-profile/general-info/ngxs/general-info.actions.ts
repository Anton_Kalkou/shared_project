import { IndividualOwner, CompanyOwner } from 'src/modules/shared/models/merchant.model';

export class SaveIndividualOwners {
  static readonly type = '[Individual Owners] Save';

  constructor(
    public individual: IndividualOwner[],
    public merchantId: string
  ) {}
}

export class SaveContactInformation {
  static readonly type = '[Contact Information] Save';

  constructor(
    public contactInformation: any
  ) {}
}

export class SaveCompanyOwners {
  static readonly type = '[Save Company Owner] Save';

  constructor(
    public companyOwners: CompanyOwner[]
  ) {}
}
