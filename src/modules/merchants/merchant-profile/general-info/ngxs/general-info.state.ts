import { Injectable } from '@angular/core';

import { State, Action, Store } from '@ngxs/store';
import { tap } from 'rxjs/operators';

import { SaveIndividualOwners, SaveContactInformation, SaveCompanyOwners } from './general-info.actions';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { GeneralInfoService } from '../services/general-info.service';

@State({
  name: 'generalInfo'
})
@Injectable()
export class GeneralInfoState {

  constructor(
    private store: Store,
    private generalInfoService: GeneralInfoService
  ) {}

  @Action(SaveCompanyOwners)
  saveCompanyOwners(stateContext, {companyOwners}: SaveCompanyOwners) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.generalInfoService.saveContactInformation(companyOwners)
      .pipe(
        tap(() => {
          this.store.dispatch(new SetSpinnerVisibility(false));
        })
      );
  }

  @Action(SaveContactInformation)
  saveContactInformation(stateContext, {contactInformation}: SaveContactInformation) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.generalInfoService.saveContactInformation(contactInformation)
      .pipe(
        tap(() => {
          this.store.dispatch(new SetSpinnerVisibility(false));
        })
      );
  }

  @Action(SaveIndividualOwners)
  saveIndividualOwners(stateContext, {individual, merchantId}: SaveIndividualOwners) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.generalInfoService.saveIndividual(individual, merchantId)
      .pipe(
        tap(() => {
          this.store.dispatch(new SetSpinnerVisibility(false));
        })
      );
  }

}
