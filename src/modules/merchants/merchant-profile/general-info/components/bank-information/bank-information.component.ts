import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { BusinessEntity } from 'src/modules/shared/models/business-entity.model';

@Component({
  selector: 'rs-bank-information',
  templateUrl: 'bank-information.component.html'
})
export class BankInformationComponent implements OnInit {

  @Input() merchant: BusinessEntity;

  public bankInformationForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.initForm();
  }

  private initForm(): void {
    this.bankInformationForm = this.formBuilder.group({
      bank: this.merchant.bankingInformation.bankName,
      accountType: this.merchant.bankingInformation.accountType,
      routingNumber: this.merchant.bankingInformation.routingNumber,
      accountNumber: this.merchant.bankingInformation.accountNumber
    });
    this.bankInformationForm.disable();
  }

}
