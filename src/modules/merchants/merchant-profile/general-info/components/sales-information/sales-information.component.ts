import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { MerchantBackend } from 'src/modules/shared/models/merchant.model';

@Component({
  selector: 'rs-sales-information',
  templateUrl: 'sales-information.component.html'
})
export class SalesInformationComponent implements OnInit {

  @Input() merchant: MerchantBackend;

  public salesForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.initForm();
  }

  private initForm(): void {
    this.salesForm = this.formBuilder.group({
      monthlyTotalRevenue: this.merchant.salesInformation.monthlyTotalRevenue,
      monthlyTotalCardRevenue: this.merchant.salesInformation.monthlyTotalCardRevenue,
      cardPresentTransactions: this.merchant.salesInformation.cardPresentTransactions,
      mailPhoneTransactions: this.merchant.salesInformation.mailPhoneTransactions,
      internetTransactions: this.merchant.salesInformation.internetTransactions,
      highestCardTransactionYear: this.merchant.salesInformation.highestCardTransactionYear,
      averageCheck: this.merchant.salesInformation.averageCheck
    });
  }

}
