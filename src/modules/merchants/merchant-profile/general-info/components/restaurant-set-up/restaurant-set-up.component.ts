import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { BusinessEntity } from 'src/modules/shared/models/business-entity.model';

@Component({
  selector: 'rs-restaurant-set-up',
  templateUrl: 'restaurant-set-up.component.html'
})
export class RestaurantSetUpComponent implements OnInit {

  @Input() merchant: BusinessEntity;

  public restaurantSetUpForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.initForm();
  }

  private initForm(): void {
    this.restaurantSetUpForm = this.formBuilder.group({
      tablesCount: this.merchant.salesInformation.tablesCount,
      floorServersAmount: this.merchant.salesInformation.floorServersAmount,
      kitchenStationsCount: this.merchant.salesInformation.kitchenStationsCount
    });
  }

}
