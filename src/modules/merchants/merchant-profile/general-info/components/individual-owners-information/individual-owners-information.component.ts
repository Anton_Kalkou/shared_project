import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { IndividualOwner } from 'src/modules/shared/models/merchant.model';
import { BusinessEntity } from 'src/modules/shared/models/business-entity.model';

@Component({
  selector: 'app-individual-owners-information',
  templateUrl: 'individual-owners-information.component.html'
})
export class IndividualOwnersInformationComponent implements OnInit {

  @Input() merchant: BusinessEntity;

  public individualOwnersForms: FormGroup[] = [];

  constructor(
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.initForms();
  }

  private initForms(): void {
    if (this.merchant) {
      this.merchant.principals.individual.forEach((owner: IndividualOwner) => {
        const individualForm = this.formBuilder.group({
          firstName: owner.firstName,
          lastName: owner.lastName,
          email: owner.email,
          phoneNumber: owner.phoneNumber,
          nationality: owner.nationality,
          secondaryNationality: owner.secondaryNationality,
          residingCountry: owner.residingCountry,
          socialSecurityNumber: owner.socialSecurityNumber,
          birthDate: owner.dateOfBirth,
          share: owner.share,
          address: '6754 Carter Plaza',
          zipcode: '90210',
          city: 'San Jose',
          state: 'California',
          country: 'United States'
        });

        this.individualOwnersForms.push(individualForm);
        individualForm.disable();
      });
    }
  }

}
