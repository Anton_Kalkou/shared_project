import { Component, Input } from '@angular/core';

import { MerchantExtended } from 'src/modules/shared/models/merchant-mock.model';
import { Changelog } from 'src/modules/shared/models/changelog.model';

@Component({
  selector: 'app-general-info',
  templateUrl: 'general-info.component.html',
  styleUrls: ['general-info.component.scss']
})
export class GeneralInfoComponent {

  @Input() merchantProfile: MerchantExtended;

  public changelog: Changelog[] = [
    {
      date: '05/05/2020',
      time: '10:10 AM',
      madeBy: 'Davon Philips',
      action: 'Edited',
      change: 'Edited 1 record',
      changes: [
        {
          field: 'Business Name (DBA)',
          from: 'xxxxx',
          to: 'yyyyy'
        }
      ]
      
    },
    {
      date: '05/05/2020',
      time: '10:10 AM',
      madeBy: 'Davon Philips',
      action: 'Edited',
      change: 'Edited 1 record',
      changes: [
        {
          field: 'Business Name (DBA)',
          from: 'yyyyy',
          to: 'xxxxx'
        }
      ]
    },
    {
      date: '05/05/2020',
      time: '10:10 AM',
      madeBy: 'Davon Philips',
      action: 'Edited',
      change: 'Edited 1 record',
      changes: [
        {
          field: 'Business Name (DBA)',
          from: 'xxxxx',
          to: 'yyyyy'
        }
      ]
    },
    {
      date: '05/05/2020',
      time: '10:10 AM',
      madeBy: 'Davon Philips',
      action: 'Edited',
      change: 'Edited 1 record',
      changes: [
        {
          field: 'Business Name (DBA)',
          from: 'yyyyy',
          to: 'xxxxx'
        }
      ]
    }
  ];

}
