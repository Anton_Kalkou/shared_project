import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { Store } from '@ngxs/store';

import { SubjectService } from 'src/modules/shared/services/subject.service';
import { APP } from 'src/modules/shared/constants';
import { BusinessEntity } from 'src/modules/shared/models/business-entity.model';

@Component({
  selector: 'rs-legal-entity-information',
  templateUrl: 'legal-entity-information.component.html'
})
export class LegalEntityInformationComponent implements OnInit {

  @Input() merchant: BusinessEntity;
  @Input() isAccordion: boolean;

  public restaurantForm: FormGroup;
  public businessCategories: string[] = APP.businessCategories;

  constructor(
    protected store: Store,
    protected subjectService: SubjectService,
    private formBuilder: FormBuilder
  ) {
  }

  ngOnInit() {
    this.initForm();
  }

  private initForm(): void {
    this.restaurantForm = this.formBuilder.group({
      DBAName: [this.merchant ? this.merchant.name : ''],
      legalEntityName: [this.merchant ? this.merchant.businessInformation.legalEntityName : ''],
      restaurantURL: [this.merchant ? this.merchant.url : ''],
      SEIN: [this.merchant ? this.merchant.businessInformation.sein : ''],
      TIN: [this.merchant ? this.merchant.businessInformation.tin : ''],
      category: [this.merchant ? this.merchant.businessInformation.category : ''],
      legalAddress: [this.merchant ? this.merchant.businessInformation.legalAddress?.address1 : ''],
      legalPostCode: [this.merchant ? this.merchant.businessInformation.legalAddress?.zipcode : ''],
      legalCountry: [this.merchant ? this.merchant.businessInformation.legalAddress?.country : ''],
      legalState: [this.merchant ? this.merchant.businessInformation.legalAddress?.state : ''],
      legalCity: [this.merchant ? this.merchant.businessInformation.legalAddress?.city : ''],
      businessAddress: [
        this.merchant ?
          this.merchant.businessInformation.businessAddress?.address1 ?? this.merchant.businessInformation.legalAddress?.address1 : null
      ],
      businessPostCode: [
        this.merchant ?
          this.merchant.businessInformation.businessAddress?.zipcode ?? this.merchant.businessInformation.legalAddress?.zipcode : null
      ],
      businessCountry: [
        this.merchant ?
          this.merchant.businessInformation.businessAddress?.country ?? this.merchant.businessInformation.legalAddress?.country : null
      ],
      businessState: [
        this.merchant ?
          this.merchant.businessInformation.businessAddress?.state ?? this.merchant.businessInformation.legalAddress?.state : null
      ],
      businessCity: [
        this.merchant ?
          this.merchant.businessInformation.businessAddress?.city ?? this.merchant.businessInformation.legalAddress?.city : null
      ]
    });
    this.restaurantForm.disable();
  }

}
