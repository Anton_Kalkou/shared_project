import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { MerchantBackend, CompanyOwner } from 'src/modules/shared/models/merchant.model';

@Component({
  selector: 'app-company-owners-information',
  templateUrl: 'company-owners-information.component.html'
})
export class CompanyOwnersInformationComponent implements OnInit {

  @Input() merchant: MerchantBackend;

  public companyOwnersForms: FormGroup[] = [];

  constructor(
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.initForms();
  }

  private initForms(): void {
    if (this.merchant) {
      this.merchant.principals.companies.forEach((owner: CompanyOwner) => {
        const companyOwnerForm = this.formBuilder.group({
          legalEntityName: owner.businessName,
          tin: owner.tin,
          email: owner.email,
          phoneNumber: owner.phone,
          share: owner.share,
          legalAddress: owner.address.address1,
          legalPostCode: owner.address.zipcode,
          legalCountry: owner.address.country,
          legalState: owner.address.state,
          legalCity: owner.address.city
        });

        companyOwnerForm.disable();
        this.companyOwnersForms.push(companyOwnerForm);
      });
    }
  }

}
