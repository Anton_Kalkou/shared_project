import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { ApiService } from 'src/modules/shared/services/api.service';
import { IndividualOwner, CompanyOwner } from 'src/modules/shared/models/merchant.model';
import { APP } from 'src/modules/shared/constants';

@Injectable({
  providedIn: 'root'
})
export class GeneralInfoService {

  constructor(
    private apiService: ApiService
  ) {}

  public saveIndividual(individual: IndividualOwner[], merchantId: string): Observable<void> {
    return this.apiService.put(`${APP.endpoints.merchants}/${merchantId}/${APP.endpoints.individual}`, individual);
  }

  public saveContactInformation(contactInformation: any): Observable<void> {
    return of(null)
      .pipe(
        delay(500)
      );
  }

  public saveCompanyOwner(companyOwners: CompanyOwner[]): Observable<void> {
    return of(null)
      .pipe(
        delay(500)
      );
  }

}
