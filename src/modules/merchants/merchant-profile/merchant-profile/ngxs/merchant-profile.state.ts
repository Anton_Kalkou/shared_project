import { Injectable } from '@angular/core';

import { Action, State, StateContext, Store } from '@ngxs/store';
import { tap } from 'rxjs/operators';

import { SpinnerState } from 'src/modules/shared/classes/spinner-state.class';
import { MerchantProfileTabs } from 'src/modules/shared/enums';
import { BusinessEntity } from 'src/modules/shared/models/business-entity.model';
import { MerchantProfileService } from '../merchant-profile.service';
import { GetGeneralInformation, SetSelectedMerchantProfileTab } from './merchant-profile.actions';

interface MerchantProfileStateModel {
  businessEntity: BusinessEntity;
  selectedTab: MerchantProfileTabs;
}

@State<MerchantProfileStateModel>({
  name: 'merchantProfile'
})
@Injectable()
export class MerchantProfileState extends SpinnerState {

  constructor(
    protected store: Store,
    private merchantProfileService: MerchantProfileService
  ) {
    super(store);
  }

  @Action(SetSelectedMerchantProfileTab)
  setSelectedMerchantProfileTab({patchState}: StateContext<MerchantProfileStateModel>, {selectedTab}: SetSelectedMerchantProfileTab) {
    patchState({
      selectedTab
    });
  }


  @Action(GetGeneralInformation)
  getGeneralInformation({patchState}: StateContext<MerchantProfileStateModel>, {businessEntityId}: GetGeneralInformation) {
    this.showSpinner();

    return this.merchantProfileService.getBusinessEntity(businessEntityId)
      .pipe(
        tap({
          next: (businessEntity: BusinessEntity) => {
            patchState({
              businessEntity
            });
          },
          complete: () => this.hideSpinner()
        })
      );
  }

}
