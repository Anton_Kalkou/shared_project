import { MerchantProfileTabs } from "src/modules/shared/enums";

export class SetSelectedMerchantProfileTab {
  static readonly type = '[Selected Merchant Profile Tab] Set';

  constructor(
    public selectedTab: MerchantProfileTabs
  ) {}
}

export class GetGeneralInformation {
  static readonly type = '[General Information] Get';

  constructor(
    public businessEntityId: string
  ) {}
}
