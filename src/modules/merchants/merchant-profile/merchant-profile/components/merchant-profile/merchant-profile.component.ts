import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { MerchantProfileTabIndexes, MerchantProfileTabs } from 'src/modules/shared/enums';
import { BusinessEntity } from 'src/modules/shared/models/business-entity.model';
import { GetDevices } from '../../../devices/ngxs/devices.actions';
import { GetGeneralInformation, SetSelectedMerchantProfileTab } from '../../ngxs/merchant-profile.actions';

@Component({
  selector: 'app-merchant-profile',
  templateUrl: 'merchant-profile.component.html',
  styleUrls: ['merchant-profile.component.scss']
})
export class MerchantProfileComponent extends Unsubscribe implements OnInit {

  @Select(state => state.merchantProfile.businessEntity) businessEntity$: Observable<BusinessEntity>;

  public businessEntity: BusinessEntity;
  public merchantId: string;
  public defaultIndex = 0;

  constructor(
    private route: ActivatedRoute,
    private store: Store
  ) {
    super();
  }

  ngOnInit() {
    this.getRouteParams();
    this.initSubscriptions();
    this.dispatchActions();
    this.reactOnGoToTab(this.defaultIndex);
  }

  public reactOnGoToTab(tabIndex: number): void {
    switch (tabIndex) {
      case MerchantProfileTabIndexes.generalInformation:
        this.store.dispatch(new SetSelectedMerchantProfileTab(MerchantProfileTabs.generalInformation));
        break;
      case MerchantProfileTabIndexes.risk:
        this.store.dispatch(new SetSelectedMerchantProfileTab(MerchantProfileTabs.risk));
        break;
      case MerchantProfileTabIndexes.devices:
        //TODO: To find out if need the action below
        // this.store.dispatch(new GetDevices(this.merchantId));
        this.store.dispatch(new SetSelectedMerchantProfileTab(MerchantProfileTabs.devices));
        break;
      case MerchantProfileTabIndexes.provisioning:
        this.store.dispatch(new SetSelectedMerchantProfileTab(MerchantProfileTabs.provisioning));
      default:
        console.warn('Unknown tab');
    }
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.businessEntity$
      .subscribe(businessEntity => this.businessEntity = businessEntity);
  }

  private dispatchActions(): void {
    this.store.dispatch(new GetGeneralInformation(this.merchantId));
  }

  private getRouteParams(): void {
    this.merchantId = this.route.snapshot.params.id;
  }

}
