import { Component, Input, OnInit } from '@angular/core';

import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { MerchantProfileTabs } from 'src/modules/shared/enums';

@Component({
  selector: 'rs-merchant-profile-navigation',
  templateUrl: 'navigation.component.html',
  styleUrls: ['navigation.component.scss']
})
export class NavigationComponent extends Unsubscribe implements OnInit {

  @Input() merchantProfileName: string;

  @Select(state => state.merchantProfile.selectedTab) selectedTab$: Observable<MerchantProfileTabs>;

  public selectedTab: MerchantProfileTabs;

  ngOnInit() {
    this.initSubscriptions();
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.selectedTab$
      .subscribe(selectedTab => this.selectedTab = selectedTab);
  }

}
