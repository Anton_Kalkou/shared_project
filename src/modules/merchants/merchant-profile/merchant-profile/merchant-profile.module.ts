import { NgModule } from '@angular/core';

import { NgxsModule } from '@ngxs/store';

import { MerchantStatusesModule } from 'src/modules/shared/components/merchant-statuses/merchant-statuses.module';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { DevicesModule } from '../devices/devices.module';
import { GeneralInfoModule } from '../general-info/general-info.module';
import { NavigationComponent } from './components/navigation/navigation.component';
import { ProvisioningModule } from '../provisioning/provisioning.module';
import { RiskModule } from '../risk/risk.module';
import { MerchantProfileState } from './ngxs/merchant-profile.state';
import { MerchantProfileComponent } from './components/merchant-profile/merchant-profile.component';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    NgxsModule.forFeature([MerchantProfileState]),
    ProvisioningModule,
    DevicesModule,
    GeneralInfoModule,
    MerchantStatusesModule,
    RiskModule,
  ],
  declarations: [
    MerchantProfileComponent,
    NavigationComponent
  ],
  exports: [
    MerchantProfileComponent
  ]
})
export class MerchantProfileModule {}
