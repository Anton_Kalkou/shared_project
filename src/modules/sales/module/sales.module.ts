import { NgModule } from '@angular/core';

import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { SalesComponent } from '../component/sales.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    SalesComponent
  ],
  exports: [
    SalesComponent
  ]
})
export class SalesModule {}
