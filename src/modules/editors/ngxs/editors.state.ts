import { Injectable, NgZone } from '@angular/core';

import { Action, State, StateContext, Store } from '@ngxs/store';
import { forkJoin, Observable, empty, throwError } from 'rxjs';
import { first, tap, catchError, map } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';

import { Filter } from 'src/modules/shared/models/filter.model';
import {
  DeleteGlobalItem,
  DeleteItem,
  GetEditorsHomeInitData,
  GetEditorsInitData,
  GetEditorsTempItemsData,
  GetGlobalItemById,
  GetNextItem,
  GetPreviousItem,
  OpenEditorsTagDialog,
  NewOntology,
  OpenGlobalItemForm,
  OpenItemForm,
  SaveGlobalItem,
  SaveItem,
  SetSelectedAttributes,
  SetSelectedCategory,
  OpenAddGlobalItemForm,
  OpenApproveItems,
  NewGlobalItem,
  GetEditorsOntologiesData,
  OpenAddOntologyForm,
  OpenOntologyForm,
  SaveOntology,
  GetCatalogItemsPendingApprovalData,
  IgnoreCatalogItemVisibility,
  NewGlobalItemFromCatalog, GetVenues,
} from './editors.actions';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { EditorsService } from '../services/editors.service';
import { EditorsItem } from 'src/modules/shared/models/editors-item.model';
import { NavItem } from 'src/modules/shared/models/nav-item.model';
import { EditorsFormComponent } from '../components/form/editors-form.component';
import { TranslationService } from 'src/modules/shared/services/translation.service';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { EditorsTagPopupTypes } from 'src/modules/shared/enums';
import { TagPopupComponent } from '../components/tag-popup/tag-popup.component';
import { Tag } from 'src/modules/shared/models/tag.model';
import { TagsPopupData } from 'src/modules/shared/models/popups-data/tags.model';
import { APP } from 'src/modules/shared/constants';
import { Product } from 'src/modules/shared/models/products/product.model';
import { GlobalItem, GlobalItems } from '../components/temp-global-items/items/items.component';
import { CatalogItems, CatalogItem } from '../components/temp-global-items/approve-items/approve-items.component';
import { TempEditorsFormComponent } from '../components/temp-global-items/form/editors-form.component';
import { of } from 'rxjs/internal/observable/of';
import { TempAddItemEditorsFormComponent } from '../components/temp-global-items/add-item-form/add-item-form.component';
import { TempApproveItemsComponent } from '../components/temp-global-items/approve-items/approve-items.component';
import { AddOntologyEditorsFormComponent } from '../components/ontology/add-ontology-form/add-ontology-form.component';
import { Ontology, Ontologies } from '../components/ontology/ontologies/ontologies.component';
import {OntologyEditorsFormComponent} from '../components/ontology/form/edit-ontology-form.component';
import { Transaction } from '../../shared/models/transaction.model';
import { PaginationResult } from '../../shared/models/pagination-result.model';

interface EditorsStateModel {
  filters: Filter[];
  items: EditorsItem[];
  homeItems: NavItem[];
  item: Product;
  categories: Tag[];
  mainCategories: Tag[];
  selectedAttributes: Tag[];
  selectedCategory: Tag;

  globalItems: GlobalItems;
  globalItemsTotal: number;
  globalItem: GlobalItem;
  globalItemExternal: any;
  barcodeLookupItem: any;

  catalogItems: CatalogItems;
  catalogItemsTotal: number;
  catalogItem: CatalogItem;

  ontology: Ontology;
  ontologiesTotal: number;
  ontologies: Ontologies;

  venues: any[];

}

@State<EditorsStateModel>({
  name    : 'editors',
  defaults: {
    filters           : [],
    items             : [],
    homeItems         : [],
    item              : null,
    categories        : [],
    mainCategories    : [],
    selectedAttributes: [],
    selectedCategory  : null,
    globalItems       : null,
    globalItemsTotal  : 0,
    globalItem        : null,
    globalItemExternal: null,
    barcodeLookupItem : null,
    catalogItems       : null,
    catalogItemsTotal  : 0,
    catalogItem        : null,
    ontology: null,
    ontologies: null,
    ontologiesTotal: 0,
    venues: []
  }
})
@Injectable()
export class EditorsState {

  constructor(
      private store: Store,
      private editorsService: EditorsService,
      private dialog: MatDialog,
      private translationService: TranslationService,
      private notificationService: NotificationService,
      private ngZone: NgZone
  ) {
  }

  @Action(SetSelectedCategory)
  setSelectedCategory({ patchState }: StateContext<EditorsStateModel>, { category }: SetSelectedCategory) {
    patchState({
      selectedCategory: category
    });
  }

  @Action(SetSelectedAttributes)
  setSelectedAttributes({ patchState }: StateContext<EditorsStateModel>, { attributes }: SetSelectedAttributes) {
    patchState({
      selectedAttributes: attributes
    });
  }

  @Action(OpenEditorsTagDialog)
  openEditorsTagDialog({ patchState }: StateContext<EditorsStateModel>, { type, selectedTags }: OpenEditorsTagDialog) {
    let resultObservable: Observable<Tag[]>;
    const popupData: TagsPopupData = {};

    this.store.dispatch(new SetSpinnerVisibility(true));

    switch (type) {
      case EditorsTagPopupTypes.attributes:
        resultObservable    = this.editorsService.getAttributes();
        popupData.caption   = 'dialogs.attributes';
        popupData.multiple  = true;
        popupData.title     = 'editors.select-attributes-or';
        popupData.popupType = EditorsTagPopupTypes.attributes;

        break;
      case EditorsTagPopupTypes.categories:
        resultObservable    = this.editorsService.getCategories();
        popupData.caption   = 'dialogs.categories';
        popupData.multiple  = false;
        popupData.title     = 'editors.select-food-subcategory-or';
        popupData.popupType = EditorsTagPopupTypes.categories;

        break;
      default:
        console.warn('Unknown type of tag');
        resultObservable    = this.editorsService.getCategories();
        popupData.caption   = 'dialogs.categories';
        popupData.multiple  = false;
        popupData.popupType = EditorsTagPopupTypes.categories;
    }

    return resultObservable
        .pipe(
            tap(response => {
              this.store.dispatch(new SetSpinnerVisibility(false));
              this.openPopup(TagPopupComponent, { ...popupData, tags: response, selectedTags }, APP.dialogs.tags);
            })
        );
  }

  @Action(SaveItem)
  saveItem({ patchState }: StateContext<EditorsStateModel>, { editorsItem }: SaveItem) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.editorsService.saveItem(editorsItem)
        .pipe(
            tap(() => {
              this.finishRequestWithMessage('notification-titles.edit-item', 'success-message.edit-item');
            })
        );
  }

  @Action(DeleteItem)
  deleteItem({ patchState, getState }: StateContext<EditorsStateModel>, { itemId }: DeleteItem) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.editorsService.deleteItem(itemId)
        .pipe(
            tap(() => {
              const items     = [...getState().items];
              const itemIndex = items.findIndex(item => item.id === itemId);

              items.splice(itemIndex, 1);
              patchState({
                items
              });
              this.finishRequestWithMessage('notification-titles.delete-item', 'success-message.delete-item');
            })
        );
  }

  @Action(OpenItemForm)
  getItemById({ patchState }: StateContext<EditorsStateModel>, { itemId }: OpenItemForm) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return forkJoin([
      this.editorsService.getItemById(itemId),
      this.editorsService.getMainCategories()
    ])
        .pipe(
            tap(responses => {
              patchState({
                item          : responses[0],
                mainCategories: responses[1]

              });
              this.store.dispatch(new SetSpinnerVisibility(false));
              this.openPopup(EditorsFormComponent, { itemId }, APP.dialogs.editorsForm);
            })
        );
  }

  @Action(GetEditorsHomeInitData)
  getEditorsHomeInitData({ patchState }: StateContext<EditorsStateModel>) {
    this.store.dispatch(new SetSpinnerVisibility(true));
    return forkJoin([
      this.editorsService.getQuantities(),
      this.editorsService.getGlobalItemsCount(),
      this.editorsService.getCatalogItemsCount()
    ])
        .pipe(
          tap(responses => {
            console.log(responses)
            responses[0][0].quantity = responses[1] ? responses[1].total : 'N/A';
            responses[0][2].quantity = responses[1] ? responses[2].total : 'N/A';
              patchState({
                homeItems: responses[0]
              });
              this.store.dispatch(new SetSpinnerVisibility(false));
            })
        );
  }

  @Action(GetEditorsInitData)
  getEditorsInitData({ patchState }: StateContext<EditorsStateModel>) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return forkJoin([
      this.editorsService.getFilters(),
      this.editorsService.getItems()
    ])
        .pipe(
            tap(responses => {
              patchState({
                filters: responses[0],
                items  : responses[1]
              });
              this.store.dispatch(new SetSpinnerVisibility(false));
            })
        );
  }

  private finishRequestWithMessage(title: string, content: string): void {
    forkJoin([
      this.translationService.translate(title),
      this.translationService.translate(content)
    ])
        .pipe(
            first()
        )
        .subscribe(translatedResults => {
          this.store.dispatch(new SetSpinnerVisibility(false));
          this.notificationService.showSuccessMessage(translatedResults[0], translatedResults[1]);
          this.dialog.closeAll();
        });
  }

  private openPopup(component: any, data: any, id: string) {
    this.ngZone.run(() => {
      this.dialog.open(component, {
        width : '100vw',
        height: '100vh',
        data,
        id
      });
    });
  }

  // TODO: Temporary Global Items, Remove later
  @Action(GetEditorsTempItemsData)
  getEditorsTempItemData({ patchState }: StateContext<EditorsStateModel>, { page, pageSize, filter, q, source}: GetEditorsTempItemsData) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return forkJoin([
      this.editorsService.getGlobalItems(page, pageSize, filter, q, source)
    ])
        .pipe(
            tap(responses => {
              patchState({
                globalItems     : responses[0],
                globalItemsTotal: responses[0].total
              });
              this.store.dispatch(new SetSpinnerVisibility(false));
            })
        );
  }

  // TODO: Temporary Global Items, Remove later
  @Action(NewOntology)
  newOntology({ patchState }: StateContext<EditorsStateModel>, { ontology }: NewOntology) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.editorsService.newOntology(ontology)
        .pipe(
            tap(() => {
              this.finishRequestWithMessage('notification-titles.new-item', 'success-message.new-item');
            })
        );
    }

  @Action(GetEditorsOntologiesData)
  getEditorsOntologyData({ patchState }: StateContext<EditorsStateModel>, { page, pageSize, filter, q }: GetEditorsOntologiesData) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return forkJoin([
      this.editorsService.getOntologies(page, pageSize, filter, q)
    ])
        .pipe(
            tap(responses => {
              patchState({
                ontologies     : responses[0],
                ontologiesTotal: responses[0].total
              });
              this.store.dispatch(new SetSpinnerVisibility(false));
            })
        );
  }

     // TODO: Temporary Global Items, Remove later
     @Action(OpenAddOntologyForm)
     openAddOntologyForm({ patchState }: StateContext<EditorsStateModel>, { }: OpenAddOntologyForm) {
       this.openPopup(AddOntologyEditorsFormComponent, {}, APP.dialogs.addOntologyEditorsForm);
   }


   @Action(OpenOntologyForm)
   openOntologyForm({ patchState }: StateContext<EditorsStateModel>, { itemId, name, build }: OpenOntologyForm) {
     this.store.dispatch(new SetSpinnerVisibility(true));
       return this.editorsService.getOntologyByIDorName(itemId, name, build).pipe(
               tap(responses => {
                 patchState({
                   ontology        : responses,
                 });
                 this.store.dispatch(new SetSpinnerVisibility(false));
                //  this.openPopup(OntologyEditorsFormComponent, { itemId }, APP.dialogs.editorsForm);
               })
           );
     }

  @Action(SaveOntology)
  saveOntology({ patchState }: StateContext<EditorsStateModel>, { ontology, itemId, itemName }: SaveOntology) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.editorsService.saveOntology(ontology, itemId, itemName)
        .pipe(
            tap(() => {
              this.finishRequestWithMessage('notification-titles.edit-item', 'success-message.edit-item');
            })
        );
  }
  @Action(OpenGlobalItemForm)
  openGlobalItemForm({ patchState }: StateContext<EditorsStateModel>, { itemId, barcode }: OpenGlobalItemForm) {
    this.store.dispatch(new SetSpinnerVisibility(true));
    if (itemId && !barcode) {
      return this.editorsService.getGlobalItemById(itemId).pipe(
              tap(responses => {
                patchState({
                  globalItem        : responses,
                });
                this.store.dispatch(new SetSpinnerVisibility(false));
                this.openPopup(TempAddItemEditorsFormComponent, { itemId }, APP.dialogs.addItemEditorsForm);
              })
          );
    }

    return forkJoin([
      itemId ? this.editorsService.getGlobalItemById(itemId) : of(null),
      barcode ? this.editorsService.getUPCItemByBarcode(barcode).pipe(catchError(err => { return Array.of([]) })) : of(null),
      barcode ? this.editorsService.getBarcodeLookupItemByBarcode(barcode.length == 14? barcode.substr(1) : barcode).pipe(catchError(err => { return Array.of([]) })) : of(null)
    ])
      .pipe(
        tap(responses => {
              patchState({
                globalItem        : responses[0],
                globalItemExternal: !Array.isArray(responses[1]) && responses[1] && responses[1].items[0] ? responses[1].items[0] : {},
                barcodeLookupItem : !Array.isArray(responses[2]) && responses[2] && responses[2].products[0] ? responses[2].products[0] : {}
              });
              this.store.dispatch(new SetSpinnerVisibility(false));
              this.openPopup(TempAddItemEditorsFormComponent, { itemId }, APP.dialogs.addItemEditorsForm);
            })
        );
  }

  @Action(OpenAddGlobalItemForm)
  openAddGlobalItemForm({ patchState }: StateContext<EditorsStateModel>, { }: OpenAddGlobalItemForm) {
    patchState({
      globalItem        : null,
    });
    this.openPopup(TempAddItemEditorsFormComponent, {}, APP.dialogs.addItemEditorsForm);
  }

  @Action(OpenApproveItems)
  openApproveItem({ patchState }: StateContext<EditorsStateModel>, { }: OpenApproveItems) {
      this.openPopup(TempApproveItemsComponent, {}, APP.dialogs.approveItemEditorsForm);
  }

  // TODO: Temporary Global Items, Remove later
  @Action(SaveGlobalItem)
  saveGlobalItem({ patchState }: StateContext<EditorsStateModel>, { globalItem, itemId }: SaveGlobalItem) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.editorsService.saveGlobalItem(globalItem, itemId)
        .pipe(
            tap(() => {
              this.finishRequestWithMessage('notification-titles.edit-item', 'success-message.edit-item');
            })
        );
  }

  // TODO: Temporary Global Items, Remove later
  @Action(NewGlobalItem)
  newGlobalItem({ patchState }: StateContext<EditorsStateModel>, { globalItem }: NewGlobalItem) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.editorsService.newGlobalItem(globalItem)
        .pipe(
            tap(() => {
              this.finishRequestWithMessage('notification-titles.new-item', 'success-message.new-item');
            })
        );
    }

  // TODO: Temporary Global Items, Remove later
  @Action(DeleteGlobalItem)
  deleteGlobalItem({ patchState, getState }: StateContext<EditorsStateModel>, { itemId }: DeleteGlobalItem) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.editorsService.deleteGlobalItem(itemId)
        .pipe(
            tap(() => {
              const items     = [...getState().globalItems.response];
              const itemIndex = items.findIndex(item => item.id === itemId);
              let itemsTotal  = getState().globalItemsTotal;
              itemsTotal      = itemsTotal - 1;
              items.splice(itemIndex, 1);
              patchState({
                ...getState(),
                globalItems     : {
                  ...getState().globalItems,
                  response: items
                },
                globalItemsTotal: itemsTotal
              });
              this.finishRequestWithMessage('notification-titles.delete-item', 'success-message.delete-item');
            })
        );
  }

  // TODO: Temporary Global Items, Remove later
  @Action(GetGlobalItemById)
  getGlobalItemById({ patchState }: StateContext<EditorsStateModel>, { itemId, barcode }: OpenGlobalItemForm) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return forkJoin([
      itemId ? this.editorsService.getGlobalItemById(itemId) : of(null),
      barcode ? this.editorsService.getUPCItemByBarcode(barcode).pipe(catchError(err => { return Array.of([]) })) : of(null),
      barcode ? this.editorsService.getBarcodeLookupItemByBarcode(barcode.length == 14? barcode.substr(1) : barcode).pipe(catchError(err => { return Array.of([]) })) : of(null)
    ])
        .pipe(
            tap(responses => {
              patchState({
                globalItem        : responses[0],
                globalItemExternal: !Array.isArray(responses[1]) && responses[1] && responses[1].items[0] ? responses[1].items[0] : {},
                barcodeLookupItem : !Array.isArray(responses[2]) && responses[2] && responses[2].products[0] ? responses[2].products[0] : {}
              });
              this.store.dispatch(new SetSpinnerVisibility(false));
              // this.openPopup(TempEditorsFormComponent, { itemId }, APP.dialogs.editorsForm);
            })
        );
  }

  // TODO: Temporary Global Items, Remove later
  @Action(GetNextItem)
  getNextItem({ patchState, getState }: StateContext<EditorsStateModel>) {
    const items     = [...getState().globalItems.response];
    const item      = getState().globalItem;
    const itemIndex = items.findIndex(i => i.id === item.id);

    if (items[itemIndex + 1]) {
      this.store.dispatch(new GetGlobalItemById(items[itemIndex + 1].id, items[itemIndex + 1].barcode));
    } else {
      var st = getState()
      this.store.dispatch(new GetEditorsTempItemsData(parseInt(st.globalItems.page, 10) + 1, parseInt(st.globalItems.pageSize, 10), st.globalItems.filter, st.globalItems.q, st.globalItems.source)).subscribe(() => {
        const currentItems = [...getState().globalItems.response];
        this.store.dispatch(new GetGlobalItemById(currentItems[0].id, currentItems[0].barcode));
      });
    }
  }

  // TODO: Temporary Global Items, Remove later
  @Action(GetPreviousItem)
  getPreviousItem({ patchState, getState }: StateContext<EditorsStateModel>) {
    const items     = [...getState().globalItems.response];
    const item      = getState().globalItem;
    const itemIndex = items.findIndex(i => i.id === item.id);

    if (items[itemIndex - 1]) {
      this.store.dispatch(new GetGlobalItemById(items[itemIndex - 1].id, items[itemIndex - 1].barcode));
    } else {
      var st = getState()
      this.store.dispatch(new GetEditorsTempItemsData(parseInt(st.globalItems.page, 10) - 1, parseInt(st.globalItems.pageSize, 10), st.globalItems.filter, st.globalItems.q, st.globalItems.source)).subscribe(() => {
        const currentItems = [...getState().globalItems.response];

        this.store.dispatch(new GetGlobalItemById(currentItems[currentItems.length - 1].id, currentItems[currentItems.length - 1].barcode));
      });
    }

  }

    @Action(GetCatalogItemsPendingApprovalData)
    getTempItemsPendingApprovalData({ patchState }: StateContext<EditorsStateModel>, { pageSize, next, previous, sort}: GetCatalogItemsPendingApprovalData) {
      this.store.dispatch(new SetSpinnerVisibility(true));

      return forkJoin([
        this.editorsService.getPendingItems(pageSize, next, previous, sort)
      ])
          .pipe(
              tap(responses => {
                patchState({
                  catalogItems     : responses[0],
                  catalogItemsTotal: responses[0].total
                });
                this.store.dispatch(new SetSpinnerVisibility(false));
              })
          );
    }

    @Action(IgnoreCatalogItemVisibility)
    changeItemVisibility({ patchState, getState  }: StateContext<EditorsStateModel>, {itemId, visibility}: IgnoreCatalogItemVisibility) {
      this.store.dispatch(new SetSpinnerVisibility(true));

      return this.editorsService.changeItemVisibility(itemId, visibility)
      .pipe(
        tap(() => {
          const items     = [...getState().catalogItems.response];
          const itemIndex = items.findIndex(item => item.id === itemId);
          let itemsTotal  = getState().catalogItemsTotal;
          itemsTotal      = itemsTotal - 1;
          items.splice(itemIndex, 1);
          patchState({
            ...getState(),
            catalogItems     : {
              ...getState().catalogItems,
              response: items
            },
            catalogItemsTotal: itemsTotal
          });
          this.finishRequestWithMessage('notification-titles.ignore-item', 'success-message.ignore-item');
        })
      );
    }

  // TODO: Temporary Global Items, Remove later
  @Action(NewGlobalItemFromCatalog)
  newGlobalItemFromCatalog({ patchState, getState }: StateContext<EditorsStateModel>, { globalItem }: NewGlobalItemFromCatalog) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.editorsService.newGlobalItem(globalItem)
    .pipe(
      tap(() => {
        const items     = [...getState().catalogItems.response];
        const itemIndex = items.findIndex(item => item.id === globalItem.itemId);
        let itemsTotal  = getState().catalogItemsTotal;
        itemsTotal      = itemsTotal - 1;
        items.splice(itemIndex, 1);
        patchState({
          ...getState(),
          catalogItems     : {
            ...getState().catalogItems,
            response: items
          },
          catalogItemsTotal: itemsTotal
        });
        this.finishRequestWithMessage('notification-titles.new-item', 'success-message.new-item');
      })
    );
    }

  @Action(GetVenues)
  getVenues({patchState}: StateContext<EditorsStateModel>) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.editorsService.getVenues()
        .pipe(
            tap((response) => {
              const venues = [];
              patchState({
                venues: response
              });
              this.store.dispatch(new SetSpinnerVisibility(false));
            })
        );
  }

}
