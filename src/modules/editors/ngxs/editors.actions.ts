import { EditorsTagPopupTypes } from 'src/modules/shared/enums';
import { Tag } from 'src/modules/shared/models/tag.model';
import { Product } from 'src/modules/shared/models/products/product.model';
import { GlobalItem } from '../components/temp-global-items/items/items.component';
import { Ontology } from '../components/ontology/ontologies/ontologies.component';

export class GetEditorsInitData {
  static readonly type = '[Editors Init Data] Get';
}

export class GetEditorsHomeInitData {
  static readonly type = '[Editors Home Init Data] Get';
}

export class OpenItemForm {
  static readonly type = '[Item Form] Open';

  constructor(
      public itemId: number
  ) {
  }
}

export class DeleteItem {
  static readonly type = '[Item] Delete';

  constructor(
      public itemId: number
  ) {
  }
}

export class SaveItem {
  static readonly type = '[Item] Save';

  constructor(
      public editorsItem: Product
  ) {
  }
}

export class OpenEditorsTagDialog {
  static readonly type = '[Editors Tag Dialog] Open';

  constructor(
      public type: EditorsTagPopupTypes,
      public selectedTags: Tag[]
  ) {
  }
}

export class CreateCategory {
  static readonly type = '[Category] Create';

  constructor(
      public id: number
  ) {
  }
}

export class SetSelectedAttributes {
  static readonly type = '[Selected Attributes] Set';

  constructor(
      public attributes: Tag[]
  ) {
  }
}

export class SetSelectedCategory {
  static readonly type = '[Selected Category] Set';

  constructor(
      public category: Tag
  ) {
  }
}

// TODO: Temporary Global Items, Remove later
export class GetEditorsTempItemsData {
  static readonly type = '[Editors Temp Items Data] Get';

  constructor(
      public page: number,
      public pageSize: number,
      public filter: string[],
      public q: string,
      public source: string[]
  ) {
  }
}

// TODO: Temporary Global Items, Remove later
export class NewOntology {
  static readonly type = '[Ontology] New';

  constructor(
      public ontology: Ontology
  ) {
  }
}

// TODO: Temporary Global Items, Remove later
export class OpenGlobalItemForm {
  static readonly type = '[Global Item Form] Open';

  constructor(
      public itemId: string,
      public barcode: string
  ) {
  }
}

// TODO: Temporary Global Items, Remove later
export class OpenAddGlobalItemForm {
  static readonly type = '[Add Global Item Form] Open';

  constructor() {
  }
}

// TODO: Temporary Global Items, Remove later
export class OpenApproveItems {
  static readonly type = '[Approve Item] Open';

  constructor() {
  }
}

// TODO: Temporary Global Items, Remove later
export class DeleteGlobalItem {
  static readonly type = '[Global Item] Delete';

  constructor(
      public itemId: string
  ) {
  }
}

// TODO: Temporary Global Items, Remove later
export class SaveGlobalItem {
  static readonly type = '[Global Item] Save';

  constructor(
      public globalItem: GlobalItem,
      public itemId: string
  ) {
  }
}

// TODO: Temporary Global Items, Remove later
export class NewGlobalItem {
  static readonly type = '[Global Item] New';

  constructor(
      public globalItem: any
  ) {}
}

// TODO: Temporary Global Items, Remove later
export class GetGlobalItemById {
  static readonly type = '[Global Item] GET';

  constructor(
      public itemId: string,
      public barcode: string
  ) {
  }
}

// TODO: Temporary Global Items, Remove later
export class GetNextItem {
  static readonly type = '[Global Item] Next';
}

// TODO: Temporary Global Items, Remove later
export class GetPreviousItem {
  static readonly type = '[Global Item] Previous';
}

export class GetEditorsOntologiesData {
  static readonly type = '[Editors Ontologies Data] Get';

  constructor(
      public page: number,
      public pageSize: number,
      public filter: string[],
      public q: string
  ) {
  }
}

export class OpenAddOntologyForm {
  static readonly type = '[Add Ontology Form] Open';

  constructor() {
  }
}

// TODO: Temporary Global Items, Remove later
export class OpenOntologyForm {
  static readonly type = '[Ontology Form] Open';

  constructor(
      public itemId: string,
      public name: string,
      public build?: boolean,
  ) {
  }
}

export class SaveOntology {
  static readonly type = '[Ontology] Save';

  constructor(
      public ontology: Ontology,
      public itemId: string,
      public itemName: string
  ) {
  }
}

// TODO: Temporary Global Items, Remove later
export class GetCatalogItemsPendingApprovalData {
  static readonly type = '[Editors Items Approval Data] Get';

  constructor(
      public pageSize: number,
      public next?: string,
      public previous?: string,
      public sort?: string
  ) {
  }
}

// TODO: Temporary Global Items, Remove later
export class IgnoreCatalogItemVisibility {
  static readonly type = '[Editors Ignore Items approval] Patch';

  constructor(
      public itemId: string,
      public visibility: boolean,
  ) {
  }
}

export class NewGlobalItemFromCatalog {
  static readonly type = '[Global Item] New from catalog';

  constructor(
      public globalItem: GlobalItem
  ) {
  }
}

export class GetVenues {
  static readonly type = '[Editors Venues] Get all Venues';

  constructor() {
  }
}
