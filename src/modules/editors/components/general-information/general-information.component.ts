import { Component, Input, ViewChild, ElementRef } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { APP } from 'src/modules/shared/constants';
import { Product } from 'src/modules/shared/models/products/product.model';
import { ProductCategory } from 'src/modules/shared/enums';
import { ResolutionService } from 'src/modules/shared/services/resolution.service';

@Component({
  selector: 'app-general-information',
  templateUrl: 'general-information.component.html',
  styleUrls: ['general-information.component.scss']
})
export class GeneralInformationComponent {

  @ViewChild('newNameVariation') newNameVariation: ElementRef<HTMLInputElement>;

  @Input() item: Product;
  @Input() generalInformationForm: FormGroup;
  @Input() productCategory: ProductCategory;

  constructor(
    public resolutionService: ResolutionService
  ) {}

  public measures = APP.measures;
  public soldBy = APP.soldBy;
  public beerTypes = APP.beerTypes;
  public beerSubtypes = APP.beerSubtypes;
  public stoutSubtypes = APP.stoutSubtypes;
  public ginTypes = APP.ginTypes;
  public sakeTypes = APP.sakeTypes;
  public tequilaTypes = APP.tequilaTypes;
  public vodkaTypes = APP.vodkaTypes;
  public whiskyTypes = APP.whiskyTypes;
  public whiskySubtypes = APP.whiskySubtypes;
  public flavorProfiles = APP.flavorProfiles;
  public wineTypes = APP.wineTypes;
  public wineSubtypes = APP.wineSubtypes;
  public grapeVarieties = APP.grapeVarieties;
  public acidityScales = APP.acidityScales;
  public bodyScales = APP.bodyScales;
  public tanninScales = APP.tanninScales;

  public isDefault(): boolean {
    return this.productCategory === ProductCategory.default;
  }

  public isBerries(): boolean {
    return this.productCategory === ProductCategory.berries;
  }

  public isBeer(): boolean {
    return this.productCategory === ProductCategory.beer;
  }

  public isGin(): boolean {
    return this.productCategory === ProductCategory.gin;
  }

  public isSake(): boolean {
    return this.productCategory === ProductCategory.sake;
  }

  public isTequila(): boolean {
    return this.productCategory === ProductCategory.tequila;
  }

  public isVodka(): boolean {
    return this.productCategory === ProductCategory.vodka;
  }

  public isWhisky(): boolean {
    return this.productCategory === ProductCategory.whisky;
  }

  public isWine(): boolean {
    return this.productCategory === ProductCategory.wine;
  }

  public deleteNameVariation(index: number): void {
    const productNameVariations = [...this.generalInformationForm.controls.productNameVariations.value];

    productNameVariations.splice(index, 1);
    this.resetVariations(productNameVariations);
  }

  public reactOnAddNameVariation(): void {
    if (this.newNameVariation.nativeElement.value) {
      const productNameVariations = [...this.generalInformationForm.controls.productNameVariations.value, this.newNameVariation.nativeElement.value];

      this.resetVariations(productNameVariations);
      this.newNameVariation.nativeElement.value = '';
    }
  }

  private resetVariations(variations: string[]): void {
    this.generalInformationForm.controls.productNameVariations.reset(variations);
    this.item.productNameVariations = variations;
  }

}
