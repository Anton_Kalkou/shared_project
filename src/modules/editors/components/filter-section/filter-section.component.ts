import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Filter } from 'src/modules/shared/models/filter.model';
import { FilterEvent } from 'src/modules/shared/models/filter-event.model';

@Component({
  selector: 'app-filter-section',
  templateUrl: 'filter-section.component.html',
  styleUrls: ['filter-section.component.scss']
})
export class FilterSectionComponent {

  @Output() applyFilter = new EventEmitter<FilterEvent>();

  @Input() filter: Filter;

  public selectFilter(filterValue: string, filterIsActive: boolean): void {
    this.applyFilter.emit({
      filterValue,
      filterIsActive
    });
  }

}
