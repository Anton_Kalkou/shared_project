import { Component, OnInit } from '@angular/core';

import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { GetEditorsHomeInitData } from '../../ngxs/editors.actions';
import { NavItem } from 'src/modules/shared/models/nav-item.model';

@Component({
  selector: 'app-editors-home',
  templateUrl: 'editors-home.component.html',
  styleUrls: ['editors-home.component.scss']
})
export class EditorsHomeComponent extends Unsubscribe implements OnInit {

  @Select(state => state.editors.homeItems) homeItems$: Observable<NavItem[]>;

  public menuItems: NavItem[];

  constructor(
    private store: Store
  ) {
    super();
  }

  ngOnInit() {
    this.initSubscriptions();
    this.dispatchActions();
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.homeItems$
      .subscribe(menuItems => this.menuItems = menuItems);
  }

  private dispatchActions(): void {
    this.store.dispatch(new GetEditorsHomeInitData());
  }

}
