import { Component, Input } from '@angular/core';

import { Product } from 'src/modules/shared/models/products/product.model';

@Component({
  selector: 'app-item-info',
  templateUrl: 'item-info.component.html',
  styleUrls: ['item-info.component.scss']
})
export class ItemInfoComponent {

  @Input() item: Product;

}
