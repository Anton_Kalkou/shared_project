import { Component } from '@angular/core';

@Component({
  selector: 'app-mobile-items-filter',
  templateUrl: 'mobile-items-filter.component.html',
  styleUrls: ['mobile-items-filter.component.scss']
})
export class MobileItemsFilterComponent {}
