import { Component, Input, ViewChild, OnChanges } from '@angular/core';

import { MatSort } from '@angular/material/sort';
import { Store } from '@ngxs/store';
import { MatTableDataSource } from '@angular/material/table';

import { EditorsItem } from 'src/modules/shared/models/editors-item.model';
import { OpenItemForm } from '../../ngxs/editors.actions';

@Component({
  selector: 'app-items-table',
  templateUrl: 'items-table.component.html',
  styleUrls: ['items-table.component.scss']
})
export class ItemsTableComponent implements OnChanges {

  @Input() items: EditorsItem[];

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public displayedColumns = [
    'name',
    'category',
    'frequency',
    'modified',
    'lastEditedBy',
    'status'
  ];
  public dataSource: MatTableDataSource<EditorsItem>;

  constructor(
    private store: Store
  ) {}

  ngOnChanges() {
    this.dataSource = new MatTableDataSource<EditorsItem>(this.items);
    this.dataSource.sort = this.sort;
  }

  public openItem(itemId: number): void {
    this.store.dispatch(new OpenItemForm(itemId));
  }

}
