import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { GetEditorsInitData, OpenItemForm } from '../../ngxs/editors.actions';
import { EditorsItem } from 'src/modules/shared/models/editors-item.model';
import { ResolutionService } from 'src/modules/shared/services/resolution.service';

@Component({
  selector: 'app-items',
  templateUrl: 'items.component.html',
  styleUrls: ['items.component.scss']
})
export class ItemsComponent extends Unsubscribe implements OnInit {

  @Select(state => state.editors.items) items$: Observable<EditorsItem[]>;

  public packagedItems: EditorsItem[];
  public unpackagedItems: EditorsItem[];

  private items: EditorsItem[];

  constructor(
    private store: Store,
    private resolutionService: ResolutionService
  ) {
    super();
  }

  ngOnInit() {
    this.dispatchActions();
    this.initSubscriptions();
  }

  public openItem(itemId: number): void {
    this.store.dispatch(new OpenItemForm(itemId));
  }

  public mobileFilterVisibility(): boolean {
    return this.resolutionService.mobileOnly();
  }

  public filtersVisibility(): boolean {
    return this.resolutionService.tabletAndMore();
  }

  public tablesVisibility(): boolean {
    return this.resolutionService.desktopAndMore();
  }

  public cardsVisibility(): boolean {
    return this.resolutionService.laptopAndLess();
  }

  private dispatchActions(): void {
    this.store.dispatch(new GetEditorsInitData());
  }

  private splitToPackagedAndUnpackaged(): void {
    this.packagedItems = this.items.filter(item => item.packaged);
    this.unpackagedItems = this.items.filter(item => !item.packaged);
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.items$
      .subscribe(items => {
        this.items = items;
        this.splitToPackagedAndUnpackaged();
      });
  }

}
