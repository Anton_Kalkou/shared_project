import { Component, OnInit, ViewChild,AfterViewInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators, ReactiveFormsModule} from '@angular/forms';
import { JsonEditorComponent, JsonEditorOptions } from 'ang-jsoneditor';

import { Select, Store } from '@ngxs/store';
import { Observable, Subject } from 'rxjs';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { SaveOntology, OpenOntologyForm } from '../../../ngxs/editors.actions';
import {Ontology} from '../ontologies/ontologies.component';
import { JsonValidator } from 'src/modules/shared/helpers';

@Component({
  selector   : 'app-ontology-editors-form',
  templateUrl: 'edit-ontology-form.component.html',
  styleUrls  : ['edit-ontology-form.component.scss']
})
export class OntologyEditorsFormComponent extends Unsubscribe implements OnInit {
  @Select(state => state.editors.ontology) ontology$: Observable<Ontology>;
  @ViewChild('editor') editor: JsonEditorComponent;

  public ontology: Ontology;
  options = new JsonEditorOptions;

  updatedOntologyForm = new FormGroup({
    'json': new FormControl(),
  });
  constructor(
      private formBuilder: FormBuilder,
      private store: Store
  ) {
    super();
      this.options.language = 'en';
      this.options.mode = 'tree';
      this.options.modes = ['code', 'text', 'tree', 'view'];
      this.options.statusBar = false;
      this.updatedOntologyForm.get('json').setValidators([
        Validators.required,
        JsonValidator(),
      ]);
  }

  ngOnInit() {
    this.dispatchActions();
    this.initSubscriptions();
  }

  public save(): void {
    this.store.dispatch(new SaveOntology(this.updatedOntologyForm.controls.json.value, this.ontology.id, this.ontology.name));
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.ontology$
        .subscribe(ontology => {
          this.ontology     = { ...ontology };
          this.updatedOntologyForm.controls.json.setValue(this.ontology)
        });
  }

  // TODO: Once we enhance Ontology editor, change this to fetch specific nodes
  private dispatchActions(): void {
    this.store.dispatch(new OpenOntologyForm('', 'root.metadata', true));
  }

}

