import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { Ontology } from '../ontologies/ontologies.component';

@Component({
  selector: 'app-ontology-mobile-item',
  templateUrl: 'ontology-mobile-item.component.html',
  styleUrls: ['ontology-mobile-item.component.scss']
})
export class OntologyMobileItemComponent {

  @Output() openPreviewEvent = new EventEmitter<void>();

  @Input() ontology: Ontology;

  public openPreview(event: MouseEvent): void {
    event.stopPropagation();
    this.openPreviewEvent.emit();
  }

}
