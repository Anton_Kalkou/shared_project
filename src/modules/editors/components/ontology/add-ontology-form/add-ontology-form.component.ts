import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NewOntology } from '../../../ngxs/editors.actions';
import { JsonValidator } from 'src/modules/shared/helpers';

import { Select, Store } from '@ngxs/store';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';

@Component({
  selector   : 'app-add-item-editors-form',
  templateUrl: 'add-ontology-form.component.html',
  styleUrls  : ['add-ontology-form.component.scss']
})
export class AddOntologyEditorsFormComponent extends Unsubscribe implements OnInit {

  public newOntologyForm: FormGroup;

  constructor(
      private formBuilder: FormBuilder,
      private store: Store
  ) {
    super();
  }

  ngOnInit() {
    this.newOntologyForm = new FormGroup({
      'json': new FormControl(),
    });

    this.newOntologyForm.get('json').setValidators([
      Validators.required,
      JsonValidator(),
    ]);
  }

  public save(): void {
    this.store.dispatch(new NewOntology(this.newOntologyForm.controls.json.value));
  }
}
