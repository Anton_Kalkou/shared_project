import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { GetEditorsOntologiesData, OpenOntologyForm, OpenAddOntologyForm } from 'src/modules/editors/ngxs/editors.actions';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ResolutionService } from 'src/modules/shared/services/resolution.service';
import { debounceTime, distinctUntilChanged, mergeMap } from 'rxjs/operators';

import { DisplaySettings } from '../../temp-global-items/components/model';
import { EditorsService } from 'src/modules/editors/services/editors.service';
import { FiltersService } from 'src/modules/shared/components/filters/services/filters.service';

export interface Ontologies {
  page: string;
  pageSize: string;
  response: Ontology[];
  total: number;
  filter: string[];
  q: string;
}

export interface Ontology {
    id: string;
    name: string;
    type: string;
    keys: Ontology[];
    solvable: boolean;
    possibleValues: GProp[];
    displaySettings: DisplaySettings;
    mandatory: boolean;
  }

export interface GProp{
    name: string;
    type: string;
    value: any;
    values: GProp[];
}

@Component({
  selector   : 'app-ontologies',
  templateUrl: 'ontologies.component.html',
  styleUrls  : ['ontologies.component.scss']
})
export class OntologiesComponent extends Unsubscribe implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  @Select(state => state.editors.ontologies) ontologies$: Observable<Ontologies>;
  @Select(state => state.editors.ontologiesTotal) ontologiesTotal$: Observable<number>;

  public dataSource: MatTableDataSource<Ontology>;
  public ontologiesTotal       = 0;
  public noData: any[]    = [{} as any];
  public displayedColumns = [
    'name',
    'id',
    'actions'
  ];

  public selectedFilterValue: string[] = [];
  public items: Ontology[];
  public displayedOntologies: Ontology[];

  private nameTokenSearch: string = '';
  private selectedFilter: string;
  private modelChanged: Subject<string> = new Subject<string>();

  constructor(
    private store: Store,
    private resolutionService: ResolutionService,
    private editorsService: EditorsService,
    private filtersService: FiltersService
  ) {
    super();
    this.modelChanged.pipe(
      debounceTime(1000),
      distinctUntilChanged()
    ).subscribe(token => {
      this.nameTokenSearch = token;
      // this.selectFilter(token);
      this.searchOntologies();
    });
  }

  ngOnInit() {}

  ngAfterViewInit() {
    this.dispatchActions();
    this.initSubscriptions();
  }

  public openOntology(itemId: string, name: string): void {
    this.store.dispatch(new OpenOntologyForm(itemId, name, true));
  }

  public openAddOntology(): void {
    this.store.dispatch(new OpenAddOntologyForm())
  }

  private dispatchActions(page = 1, pageSize = 10, filter = [], q = ''): void {
    this.store.dispatch(new GetEditorsOntologiesData(page, pageSize, filter, q));
  }

  public selectFilter(): void {
    this.store.dispatch(new GetEditorsOntologiesData(this.paginator.pageIndex, this.paginator.pageSize, this.selectedFilterValue, this.nameTokenSearch));
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.ontologiesTotal$
        .subscribe(ontologiesTotal => {
          this.ontologiesTotal  = ontologiesTotal;
          this.subscribeTo = this.ontologies$
              .subscribe(items => {
                this.items               = items?.response;
                this.paginator.pageIndex = parseInt(items?.page, 10) - 1;
                this.dataSource          = new MatTableDataSource(this.ontologiesTotal > 0 ? items.response : this.noData);
              });
        });

    this.subscribeTo = this.paginator.page.subscribe(() =>
      this.dispatchActions(this.paginator.pageIndex + 1, this.paginator.pageSize, this.selectedFilterValue));

      this.subscribeTo = this.filtersService.emitter.subscribe(filter => {
        if (filter) {
           filter.event?
             this.selectedFilterValue.push(filter.value.toLowerCase()) :
             this.selectedFilterValue = this.selectedFilterValue.filter(ft => ft != filter.value.toLowerCase())

          this.store.dispatch(new GetEditorsOntologiesData(this.paginator.pageIndex, this.paginator.pageSize, this.selectedFilterValue, this.nameTokenSearch));
        }
      })
  }

  public filterOntologies(key: string) {
    this.modelChanged.next(key);
  }

  public searchOntologies(): void {
    this.store.dispatch(new GetEditorsOntologiesData(this.paginator.pageIndex, this.paginator.pageSize, this.selectedFilterValue, this.nameTokenSearch));
  }

  public tableVisibility(): boolean {
    return this.resolutionService.laptopAndMore();
  }

  public mobileItemsVisibility(): boolean {
    return this.resolutionService.tabletAndLess();
  }
}
