import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-ratings',
  templateUrl: 'ratings.component.html',
  styleUrls: ['ratings.component.scss']
})
export class RatingsComponent {

  @Input() ratings: FormControl;

  public newRatingBy: string;
  public newRatingPoints: number;

  public deleteRating(index: number): void {
    const ratingsValue = [...this.ratings.value];

    ratingsValue.splice(index, 1);
    this.ratings.reset(ratingsValue);
  }

  public addNewRating(): void {
    if (this.newRatingBy && this.newRatingPoints) {
      this.ratings.reset([...this.ratings.value, {by: this.newRatingBy, points: this.newRatingPoints}]);
      this.newRatingPoints = null;
      this.newRatingBy = '';
    }
  }

}
