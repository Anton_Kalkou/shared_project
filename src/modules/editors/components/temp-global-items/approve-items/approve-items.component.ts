import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';

import { Observable, Subject } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import {
  GetCatalogItemsPendingApprovalData,
  GetVenues,
  IgnoreCatalogItemVisibility,
  NewGlobalItemFromCatalog
} from 'src/modules/editors/ngxs/editors.actions';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ResolutionService } from 'src/modules/shared/services/resolution.service';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { GlobalItem } from '../items/items.component';
import { ShortNumberPipe } from 'src/modules/shared/pipes/short-number.pipe';
import { MatSort } from '@angular/material/sort';

export interface CatalogItems {
  next: string;
  previous: string;
  pageSize: string;
  response: CatalogItem[];
  total: number;
}

export interface CatalogItem {
  id: string;
  name: string;
  title: string;
  category: string;
  venueID: string;
}

@Component({
  selector   : 'app-approve-items',
  templateUrl: 'approve-items.component.html',
  styleUrls  : ['approve-items.component.scss']
})
export class TempApproveItemsComponent extends Unsubscribe implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  @Select(state => state.editors.catalogItems) items$: Observable<CatalogItems>;
  @Select(state => state.editors.catalogItemsTotal) itemsTotal$: Observable<number>;
  @Select(state => state.editors.venues) venues$: Observable<any[]>;

  public dataSource: MatTableDataSource<CatalogItem>;
  public itemsTotal       = 0;
  public noData: any[]    = [{} as any];
  public displayedColumns = [
    'name',
    'category',
    'venue',
    'actions'
  ];

  public selectedFilterValue: string[] = [];
  public items: CatalogItem[];
  public displayedCatalogItems: CatalogItem[];
  public nextPageIndex: string;
  public previousPageIndex: string;
  public venues                        = {};

  private nameTokenSearch: string       = '';
  private modelChanged: Subject<string> = new Subject<string>();

  constructor(
      private store: Store,
      private resolutionService: ResolutionService,
  ) {
    super();
    this.modelChanged.pipe(
        debounceTime(1000),
        distinctUntilChanged()
    ).subscribe(token => {
      this.nameTokenSearch = token;
      this.searchCatalogItems();
    });
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.dispatchActions();
    this.initSubscriptions();
    this.paginator._intl.getRangeLabel = this.getRangeLabel
  }

  private dispatchActions(pageSize = 10, sort?): void {
    this.store.dispatch(new GetVenues()).subscribe(() => this.store.dispatch(new GetCatalogItemsPendingApprovalData(pageSize, null, null, sort)));
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.itemsTotal$
        .subscribe(itemsTotal => {
          this.itemsTotal  = itemsTotal;
          this.subscribeTo = this.venues$
              .subscribe(venues => {
                venues.forEach(venue => this.venues[venue.id] = venue.name);

                this.subscribeTo = this.items$
                    .subscribe(items => {
                      this.items             = items?.response;
                      this.nextPageIndex     = items?.next;
                      this.previousPageIndex = items?.previous;

                      this.dataSource = new MatTableDataSource(this.itemsTotal > 0 ? items.response : this.noData);
                    });
              });
        });

    this.subscribeTo = this.sort.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.store.dispatch(new GetCatalogItemsPendingApprovalData(this.paginator.pageSize, null, null, this.sort.active));
    });

    this.subscribeTo = this.paginator.page.subscribe((p) => {
      if (p.pageIndex === p.previousPageIndex) {
        this.store.dispatch(new GetCatalogItemsPendingApprovalData(this.paginator.pageSize, '', '', this.sort.active));
      } else if (p.pageIndex > p.previousPageIndex) {
        this.store.dispatch(new GetCatalogItemsPendingApprovalData(this.paginator.pageSize, this.nextPageIndex, '', this.sort.active));
      } else {
        this.store.dispatch(new GetCatalogItemsPendingApprovalData(this.paginator.pageSize, '', this.previousPageIndex, this.sort.active));
      }
    });
    // TODO: uncomment line below when we have a filter to the items waitiing for approval
    // this.subscribeTo = this.filtersService.emitter.subscribe(filter => {
    //   if (filter) {
    //     let value = filter.value
    //     filter.event?
    //        this.selectedFilterValue.push(value) :
    //        this.selectedFilterValue = this.selectedFilterValue.filter(ft => ft != value)
    //     this.store.dispatch(new GetCatalogItemsPendingApprovalData(this.nextPageIndex, this.paginator.pageSize));
    //   }
    // })
  }

  public filterCatalogItems(key: string) {
    this.modelChanged.next(key);
  }

  public searchCatalogItems(): void {
    this.store.dispatch(new GetCatalogItemsPendingApprovalData(this.paginator.pageSize));
  }

  public filterGlobalItems(key: string) {
    this.modelChanged.next(key);
  }

  public tableVisibility(): boolean {
    return this.resolutionService.laptopAndMore();
  }

  public mobileItemsVisibility(): boolean {
    return this.resolutionService.tabletAndLess();
  }

  // public getCategory(globalItem: CatalogItem): string {
  //   let category = globalItem.tags ?
  //     globalItem.tags.find(tag => tag.includes("category>")) : null;
  //   return category ? category.replace("category>", "") : '-';
  // }

  private getRangeLabel(page: number, pageSize: number, length: number): any {
    if (length == 0 || pageSize == 0)
      return `0 of ${length}`;

    const startIndex = page * pageSize;
    const endIndex   = startIndex < length ?
        Math.min(startIndex + pageSize, length) :
        startIndex + pageSize;

    const shortNumber = new ShortNumberPipe;
    return shortNumber.transform(startIndex + 1) + ' - ' + shortNumber.transform(endIndex) + ' of  ' + shortNumber.transform(length);
  }

  approveItem(item: CatalogItem): void {
    let catalogAsGlobal         = {} as GlobalItem;
    catalogAsGlobal.rockspoonID = item.id;
    catalogAsGlobal.name        = item.title;
    catalogAsGlobal.itemId      = item.id;
    catalogAsGlobal.dataSource  = "rockspoon";
    this.store.dispatch(new NewGlobalItemFromCatalog(catalogAsGlobal));
  }

  ignoreItem(item: CatalogItem): void {
    this.store.dispatch(new IgnoreCatalogItemVisibility(item.id, true));
  }
}
