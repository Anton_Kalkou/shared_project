import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { DeleteGlobalItem, GetNextItem, GetPreviousItem, SaveGlobalItem } from '../../../ngxs/editors.actions';

@Component({
  selector   : 'app-temp-editors-form',
  templateUrl: 'editors-form.component.html',
  styleUrls  : ['editors-form.component.scss']
})
export class TempEditorsFormComponent extends Unsubscribe implements OnInit {

  @Select(state => state.editors.globalItem) item$: Observable<any>;
  @Select(state => state.editors.globalItemExternal) itemExternal$: Observable<any>;
  @Select(state => state.editors.barcodeLookupItem) barcodeLookupItem$: Observable<any>;

  public item: any;
  public itemExternal: any;
  public barcodeLookupItem: any;

  public itemForm: FormGroup;

  constructor(
      private formBuilder: FormBuilder,
      private store: Store
  ) {
    super();
  }

  ngOnInit() {
    this.initSubscriptions();
  }

  public save(): void {
    // this.store.dispatch(new SaveGlobalItem(this.itemForm.getRawValue(), this.item.id));
  }

  public delete(): void {
    this.store.dispatch(new DeleteGlobalItem(this.item.id));
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.item$
        .subscribe(item => {
          this.item     = { ...item };
          this.itemForm = this.formBuilder.group({
            separatedIngredients: this.formBuilder.array(this.item.separatedIngredients || []),
            separatedModifiers  : this.formBuilder.array(this.item.separatedModifiers || []),
            separatedAllergens  : this.formBuilder.array(this.item.separatedAllergens || [])
          });
        });
    this.subscribeTo = this.itemExternal$
        .subscribe(itemExternal => {
          this.itemExternal    = { ...itemExternal };
        });
    this.subscribeTo = this.barcodeLookupItem$
        .subscribe(item => {
          this.barcodeLookupItem    = { ...item };
        });
  }

  get separatedIngredients() {
    return this.itemForm.get('separatedIngredients') as FormArray;
  }

  get separatedModifiers() {
    return this.itemForm.get('separatedModifiers') as FormArray;
  }

  get separatedAllergens() {
    return this.itemForm.get('separatedAllergens') as FormArray;
  }

  addItem(type) {
    switch (type) {
      case 'ingredient':
        this.separatedIngredients.push(new FormControl('', Validators.required));
        break;
      case 'modifier':
        this.separatedModifiers.push(new FormControl('', Validators.required));
        break;
      case 'allergen':
        this.separatedAllergens.push(new FormControl('', Validators.required));

        break;
    }
  }

  removeItem(type, index) {
    switch (type) {
      case 'ingredient':
        this.separatedIngredients.removeAt(index);
        break;
      case 'modifier':
        this.separatedModifiers.removeAt(index);
        break;
      case 'allergen':
        this.separatedAllergens.removeAt(index);
        break;
    }
  }

  isObject(value) {
    return typeof value === 'object' && value !== null;
  }

  next() {
    this.store.dispatch(new GetNextItem());
  }

  previous() {
    this.store.dispatch(new GetPreviousItem());
  }
}
