import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

import { GlobalItem } from '../items/items.component';

@Component({
  selector: 'app-global-item-mobile-item',
  templateUrl: 'global-item-mobile-item.component.html',
  styleUrls: ['global-item-mobile-item.component.scss']
})
export class GlobalItemMobileItemComponent {

  @Output() openPreviewEvent = new EventEmitter<void>();

  @Input() item: GlobalItem;

  public openPreview(event: MouseEvent): void {
    event.stopPropagation();
    this.openPreviewEvent.emit();
  }

}
