import { AfterViewInit, Component, OnInit, ViewChild, ComponentFactoryResolver } from '@angular/core';

import { Observable, Subject } from 'rxjs';
import { Select, Store } from '@ngxs/store';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { GetEditorsTempItemsData, OpenGlobalItemForm, OpenAddGlobalItemForm, OpenOntologyForm} from 'src/modules/editors/ngxs/editors.actions';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { ResolutionService } from 'src/modules/shared/services/resolution.service';
import { debounceTime, distinctUntilChanged, mergeMap, filter } from 'rxjs/operators';
import { EditorsService } from 'src/modules/editors/services/editors.service';
import { FiltersService } from 'src/modules/shared/components/filters/services/filters.service';
import { first, tap, catchError, map } from 'rxjs/operators';
import {Ontology} from 'src/modules/editors/components/ontology/ontologies/ontologies.component';
import { ShortNumberPipe } from 'src/modules/shared/pipes/short-number.pipe';
import { FilterEvent, Filter } from 'src/modules/shared/models/filter.model';
import { Gprop } from '../components/model';

export interface GlobalItems {
  page: string;
  pageSize: string;
  response: GlobalItem[];
  total: number;
  filter: string[];
  q: string;
  source: string[];
}

export interface GlobalItem {
  barcode: string;
  cholesterol: number;
  courses?: any;
  dietaryFiber: number;
  facets: string[];
  genericNames?: any;
  id: string;
  images?: any;
  ingredients?: any;
  menuIDs?: any;
  name: string;
  nameAsKey: string;
  numIngredients: number;
  prices?: any;
  protein: number;
  rockspoonID: string;
  saturatedFat: number;
  sectionIDs?: any;
  separatedAllergens?: any;
  separatedIngredients?: any;
  separatedModifiers?: any;
  metadata: Gprop[];
  sodium: number;
  tags?: string[];
  totalCarbs: number;
  totalFat: number;
  totalSugar: number;
  transFat: number;
  venueID: string;
  itemId: string;
  dataSource: string;
}

@Component({
  selector   : 'app-temp-items',
  templateUrl: 'items.component.html',
  styleUrls  : ['items.component.scss']
})
export class TempItemsComponent extends Unsubscribe implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  @Select(state => state.editors.globalItems) items$: Observable<GlobalItems>;
  @Select(state => state.editors.globalItemsTotal) itemsTotal$: Observable<number>;
  @Select(state => state.editors.ontology) ontology$: Observable<Ontology>;

  public dataSource: MatTableDataSource<GlobalItem>;
  public itemsTotal       = 0;
  public noData: any[]    = [{} as any];
  public displayedColumns = [
    'name',
    'category',
    'actions'
  ];


  public selectedFilterValue: string[] = [];
  public selectedSources: string[] = [];
  public items: GlobalItem[];
  public displayedGlobalItems: GlobalItem[];

  private nameTokenSearch = '';
  private modelChanged: Subject<string> = new Subject<string>();

  constructor(
    private store: Store,
    private resolutionService: ResolutionService,
    private editorsService: EditorsService,
    private filtersService: FiltersService
  ) {
    super();
    this.modelChanged.pipe(
      debounceTime(1000),
      distinctUntilChanged()
    ).subscribe(token => {
      this.nameTokenSearch = token;
      // this.selectFilter(token);
      this.searchGlobalItems();
    });
  }

  ngOnInit() {}

  ngAfterViewInit() {
    this.dispatchActions();
    this.initSubscriptions();
    this.paginator._intl.getRangeLabel = this.getRangeLabel;
  }

  public openItem(itemId: string, barcode: string): void {
    this.store.dispatch(new OpenGlobalItemForm(itemId, barcode));
  }

  public openAddItem(): void {
    this.store.dispatch(new OpenAddGlobalItemForm());
  }

  private dispatchActions(page = 1, pageSize = 10, filter = [], q = '', source = []): void {
    this.store.dispatch(new OpenOntologyForm('', 'root.metadata', true));
    this.store.dispatch(new GetEditorsTempItemsData(page, pageSize, filter, q, source));
  }

  public selectFilter(): void {
    this.store.dispatch(new GetEditorsTempItemsData(this.paginator.pageIndex, this.paginator.pageSize, this.selectedFilterValue, this.nameTokenSearch, this.selectedSources));
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.itemsTotal$
        .subscribe(itemsTotal => {
          this.itemsTotal  = itemsTotal;
          this.subscribeTo = this.items$
              .subscribe(items => {
                this.items               = items && items.response;
                this.paginator.pageIndex = parseInt(items && items.page, 10) - 1;
                this.dataSource          = new MatTableDataSource(this.itemsTotal > 0 ? items.response : this.noData);
              });
        });

    this.subscribeTo = this.ontology$
      .subscribe(ontology => {
        this.filtersService.ontologies = { ...ontology };
    });

    this.subscribeTo = this.paginator.page.subscribe(() =>
      this.dispatchActions(this.paginator.pageIndex + 1, this.paginator.pageSize, this.selectedFilterValue));
    this.subscribeTo = this.filtersService.emitter.subscribe(filter => {
        if (filter) {
          let value = this.getOntologyPath(filter)

          if (filter.event) {
            filter.filter.paramName == "source"?
              this.selectedSources.push(value):
              this.selectedFilterValue.push(value)
          } else {
            filter.filter.paramName == "source"?
              this.selectedSources = this.selectedSources.filter(ft => ft != value):
              this.selectedFilterValue = this.selectedFilterValue.filter(ft => ft != value)
          }
          this.store.dispatch(new GetEditorsTempItemsData(this.paginator.pageIndex, this.paginator.pageSize, this.selectedFilterValue, this.nameTokenSearch, this.selectedSources));
        }
      });
  }

  private getOntologyPath(event: FilterEvent): string {
    let value = event.value.substring(event.value.lastIndexOf('.') + 1); // this basically remove everyything before the first "." to remove the path to the dictionary
    if (event.filter.ontologyPath) {
      value = event.filter.ontologyPath + '.' + value;
    }
    return value;
  }

  public filterGlobalItems(key: string) {
    this.modelChanged.next(key);
  }

  public searchGlobalItems(): void {
    this.store.dispatch(new GetEditorsTempItemsData(this.paginator.pageIndex, this.paginator.pageSize, this.selectedFilterValue, this.nameTokenSearch, this.selectedSources));
  }

  public tableVisibility(): boolean {
    return this.resolutionService.laptopAndMore();
  }

  public mobileItemsVisibility(): boolean {
    return this.resolutionService.tabletAndLess();
  }

  public getCategory(globalItem: GlobalItem): string {
    let category = '';
    const catProp = globalItem.metadata && globalItem.metadata.find(key => key.name === 'mainCategory');
    category += catProp && catProp.values && catProp.values[0] && catProp.values[0].name || '';
    const subCatProp = catProp && catProp.values[0] && catProp.values[0].values && catProp.values[0].values.find(key => key.name === 'subCategory' || key.name === 'drinkType');
    if (subCatProp) {
      category += '>';
      category += subCatProp.values && subCatProp.values[0] && subCatProp.values[0].name || subCatProp.value;
    }
    return category !== '' && category  || '-';
  }

  private getRangeLabel(page: number, pageSize: number, length: number): any {
    if (length == 0 || pageSize == 0) {
      return `0 of ${length}`;
    }

    const startIndex = page * pageSize;
    const endIndex = startIndex < length ?
      Math.min(startIndex + pageSize, length) :
      startIndex + pageSize;

    const shortNumber = new  ShortNumberPipe;
    return shortNumber.transform(startIndex + 1) + ' - ' + shortNumber.transform(endIndex) + ' of  ' + shortNumber.transform(length);
  }

}
