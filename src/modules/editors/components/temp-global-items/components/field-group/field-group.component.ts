import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, FormControl, Validators, AbstractControlOptions, FormArray } from '@angular/forms';
import { Descriptor, getOtherData } from '../model';
import { IWrappableComponent } from '../property/property.component';
import { GProp } from '../../../ontology/ontologies/ontologies.component';


export class IFieldGroup {
  path: string[];
  key: Descriptor;
  form: AbstractControl;
  tab: string;
  activatedKeys: string[];
  children: {[key: string]: IFieldGroup };
  parent: IFieldGroup;
  enabled: boolean;

  constructor() {
    this.children = {};
    this.enabled = false;
  }
  activateValue(path: string[]) {
    throw new Error('Must Override activateValue');
  }
  deactivateValue(path: string[]) {
    throw new Error('Must Override deactivateValue');
  }
  private _toggleEnable(to: boolean, onlySelf: boolean) {
    this.enabled = to;
    if (!onlySelf) {
      Object.keys(this.children).forEach(c => {
        this.children[c]._toggleEnable(false, onlySelf);
      });
    }
  }

  disable(onlySelf: boolean) {
    this._toggleEnable(false, onlySelf);
    this.form.disable({ onlySelf: true });
  }
  enable(onlySelf: boolean) {
    this._toggleEnable(true, onlySelf);
    this.form.enable({ onlySelf: true });
  }

  getValue() {
    if (!this.enabled) { return null; }
    const isObj = this.key && (this.key.type === 'object' || this.key.type === 'array:object');
    
    if (!isObj) {
      return this.form.value;
    }else if (this.key && this.key.type === 'array:object') {
      return this.form.value.reduce((acc, v) => {
        if (!v) return acc
        return [...acc, v]
      }, []);
    }


    return Object.keys(this.children).reduce((acc, current) => {
      return this.children[current].enabled && {...acc, [current]: this.children[current].getValue()} || acc;
    }, {});
  }

  getCleanValue() {
    if (!this.enabled) { return null; }
    const isObj = this.key && (this.key.type === 'object' || this.key.type === 'array:object');
    if (!isObj) {
      return this.form.value;
    }
    return Object.keys(this.children).reduce((acc, current) => {
      const childValue = this.children[current].enabled && this.children[current].getValue();
      const childIsEmpty = childValue && Object.keys(childValue).length > 0;
      return childValue && !childIsEmpty && {...acc, [current]: childValue} || acc;
    }, {});
  }

  getInvalidKeys() {
    if (!this.enabled) { return []; }
    const isObj = this.key && (this.key.type === 'object' || this.key.type === 'array:object');
    if (!isObj) {
      const isFormInvalid  = this.form.invalid  || ((this.form.value === null) && this.key.mandatory);
      return (isFormInvalid && this.key.name && [this.key.name]) || [];
    }
    let allChildrenIsDisabled = true;
    const invalidChildrenKeys = Object.keys(this.children).reduce((acc, current) => {
      allChildrenIsDisabled = allChildrenIsDisabled && !this.children[current].enabled;
      const childKeys = this.children[current].enabled && this.children[current].getInvalidKeys() || [];
      return [...acc, ...childKeys];
    }, []);
    if (allChildrenIsDisabled && this.key.mandatory) {
      return [this.key.name];
    }
    return invalidChildrenKeys;
  }

  getNumberOfEnabledFields(incoming: number): number {
    if (!this.enabled) { return 0; }
    const isObj = this.key && (this.key.type === 'object' || this.key.type === 'array:object');
    if (!isObj) {
      return 1;
    }
    const childrenKeys = Object.keys(this.children).reduce((acc, current) => {
      const childKeys = this.children[current].enabled && this.children[current].getNumberOfEnabledFields(incoming) || 0;
      return acc + childKeys;
    }, incoming);
    return childrenKeys;
  }

  getNumberOfEmptyFields(incoming: number): number {
    if (!this.enabled) { return 0; }
    const isObj = this.key && (this.key.type === 'object' || this.key.type === 'array:object');
    if (!isObj) {
      const isEmpty = (this.form.value == null || this.form.value == undefined || this.form.value == '' || (Array.isArray(this.form.value) && !this.form.value.length));
      return isEmpty && 1 || 0;
    }
    const childrenKeys = Object.keys(this.children).reduce((acc, current) => {
      const childKeys = this.children[current].enabled && this.children[current].getNumberOfEmptyFields(incoming) || 0;
      return acc + childKeys;
    }, incoming);
    return childrenKeys;
  }
}


@Component({
  selector: 'app-global-field-group',
  templateUrl: 'field-group.component.html',
  styleUrls: ['field-group.component.scss']
})

// The globalitemFieldComponent will be responsible to sort/show/hide the components
export class GlobalItemFieldGroupComponent implements OnInit, IWrappableComponent {
  @Input() key: Descriptor;
  @Input() index: number;
  @Input() form: FormGroup;
  @Input() data: any;
  @Input() onSelect: any;
  @Input() fieldGroup: IFieldGroup;
  @Input() activeTab: string;

  public reducedForm = null;

  public activeForms = {};
  public completeForm: FormGroup;
  public groups = {
    'Additional Info': [],
    _invisible: []
  };
  private sortedGroupList = {};

  constructor(private formBuilder: FormBuilder) {
    this.generateGroups = this.generateGroups.bind(this);
    this.onChangeTab = this.onChangeTab.bind(this);
  }
  ngOnInit() {
    this.fieldGroup = this.fieldGroup || new IFieldGroup();
    this.addKeyToControl(this.key, this.form);
    this.fieldGroup.form =  this.form;
    this.fieldGroup.key = this.key; // TODO: remove this receiving key and form. We only need the fieldGroup
    this.generateGroups(this.key, true, [], this.fieldGroup, this.activeTab);
    this.fieldGroup.enable(true);
  }

  printTree(form: FormGroup, prepend= '') {
    if (!form.controls) {
      return;
    }
    Object.keys(form.controls).forEach(entry => {
      if (form.controls[entry]) {
        this.printTree((form.controls[entry] as FormGroup), prepend + ' ');
      }
    });
  }

  generateGroups(key: Descriptor, show: boolean, path: string[], parentControl: IFieldGroup, parentTab: string): IFieldGroup {
    const fieldGroup = new IFieldGroup();
    if (key.displaySettings && key.displaySettings.group) {
      this.groups[key.displaySettings.group] = this.groups[key.displaySettings.group] || [];
    }
    fieldGroup.key = key;
    fieldGroup.path = [...path, key.name];
    const tab = getOtherData(key, 'tab-name');
    fieldGroup.tab = tab && tab[0].value || parentTab;
    if (parentControl && !parentControl.form) {
      fieldGroup.form = this.formBuilder.group({disabled: true, required: parentControl.key.mandatory});
    } else {
      if (parentControl.form) {
        fieldGroup.form = (parentControl.form as FormGroup).controls[key.name];
        parentControl.children[key.name] = fieldGroup; // Add itself as children of the parent
      }
    }
    if (show) {
      this.activeForms[this.pathToString(fieldGroup.path)] = true;
      if (fieldGroup.form) {
        fieldGroup.enable(true);
      }
    }
    fieldGroup.activateValue = this.activateValue.bind(this);
    fieldGroup.deactivateValue = this.deactivateValue.bind(this);
    if (key.name !== 'root.metadata' && key.displaySettings && key.displaySettings.group) {
      this.groups[key.displaySettings.group || 'Additional Info'].push(fieldGroup);
    } else {
      this.groups._invisible.push(fieldGroup);
    }
    if (key && Array.isArray(key.keys)) {
      key.keys.forEach((sub) => {
        this.generateGroups(
          sub,
          show,
          [...path, key.name],
          parentControl.children[key.name],
          fieldGroup.tab || parentTab
        ); // this makes sure that we always show nested that are inside keys and hide nested in possibleValues
      });
    }
    if (key.possibleValues && (key.type === 'object' || key.type === 'array:object')) {
      key.possibleValues.forEach((pv) => {
        if (pv.value) {
          this.generateGroups(
            pv.value,
            false,
            [...path, key.name],
            parentControl.children[key.name],
            fieldGroup.tab || parentTab
          ); // this makes sure that we always show nested that are inside keys and hide nested in possibleValues
        }
      });
    }
    return fieldGroup;
  }

  addKeyToControl(key: Descriptor, controlGroup: FormGroup) {
    if (key.type === 'array:object' || key.type === 'object' || (key.keys && key.keys.length > 0)) {
      const group = this.formBuilder.group({}, {disabled: !key.mandatory, validators: key.mandatory ? [Validators.required] : undefined});
      const obj = key;
      if (obj.keys) {
        obj.keys.forEach(element => {
          this.addKeyToControl(element, group);
        });
      } else {
        obj.possibleValues.forEach(pv => {
          this.addKeyToControl(pv.value, group);
        });
      }

      key.type === 'array:object' ?
        controlGroup.addControl(key.name, this.formBuilder.array([], ({disabled: true} as AbstractControlOptions))) :
        controlGroup.addControl(key.name, group);
    } else {
      const defaultValue = key.type === 'bool' ? false : null;
      let fc: AbstractControl = new FormControl({value: defaultValue, disabled: true}, key.mandatory ? [Validators.required] : []);
      if (key.type.includes('array:')) {
        fc = new FormArray([], {disabled: true} as AbstractControlOptions);
      }
      if (controlGroup.addControl) {
        controlGroup.addControl(key.name, fc);
      }
    }
  }

  activateValue(path: string[]) {
    const pathAsString = this.pathToString(path);
    Object.keys(this.groups).forEach(entry => {
      this.groups[entry].forEach(fieldGroup => {
        if (this.pathToString(fieldGroup.path) === this.pathToString(path.slice(0, path.length - 1))) {
          // Set that the fieldGroup activated a key
          if (!fieldGroup.activatedKeys) {
            fieldGroup.activatedKeys = [];
          }
          fieldGroup.activatedKeys.push(path[path.length - 1]);
        }
        if (pathAsString === this.pathToString(fieldGroup.path)) {
          fieldGroup.enable(true);
        }
        if (fieldGroup.path.length === path.length + 1) {
          if (pathAsString === this.pathToString(fieldGroup.path.slice(0, path.length))) {
            this.activeForms[this.pathToString(fieldGroup.path)] = true;
            if (fieldGroup.key.type === 'object' || fieldGroup.key.type === 'array:object') {
              if (!fieldGroup.key.displaySettings) {
                // If we have an object that is not displayed and have properties we propagate the activation
                this.activateValue(fieldGroup.path);
              }
            }
            fieldGroup.enable(true);
          }
        }
      });
    }
    );
    this.activeForms[pathAsString] = true;
  }

  deactivateValue(path: string[]) {
    const pathAsString = this.pathToString(path);
    Object.keys(this.groups).forEach(entry =>
      this.groups[entry].forEach(fieldGroup => {
        if (this.pathToString(fieldGroup.path) === this.pathToString(path.slice(0, path.length - 1))) {
          // Remove that the fieldGroup activated a key
          fieldGroup.activatedKeys = fieldGroup.activatedKeys && fieldGroup.activatedKeys.filter(k => k !== path[path.length - 1]) || null;
        }
        if (fieldGroup.path.length >= path.length) {
          if (pathAsString === this.pathToString(fieldGroup.path.slice(0, path.length))) {
            this.activeForms[this.pathToString(fieldGroup.path)] = false;
            fieldGroup.disable();
          }
        }
      }));
    this.activeForms[pathAsString] = false;
  }

  pathToString(path: string[]) {
    return path.reduce((acc, cur) => acc + '.' + cur, '');
  }

  getSortedGroupNames() {
    const groupNames = Object.keys(this.groups);
    const tabDefinitions = getOtherData(this.key, 'tab-definition');
    if (!tabDefinitions) {
      return groupNames;
    }
    const currentTab = tabDefinitions.findIndex(t => t.value.name === this.activeTab);
    return (currentTab !== -1 && tabDefinitions[currentTab].value.groupsOrder) || groupNames;
  }

  showField(fg: IFieldGroup) {
    return fg.tab === this.activeTab && this.activeForms[this.pathToString(fg.path)] && true;
  }

  getSortedFieldGroupedByPriority(g) {
    return getSortedFieldByPriority(g, this.sortedGroupList, this.groups)
  }
  
  onChangeTab(newTab: string) {
    this.activeTab = newTab;
  }

  mustShowSomeFieldOfTheGroup(g) {
    if (g === '_invisible') {
      return false;
    }
    return this.groups[g] && -1 !== this.groups[g].findIndex(field => (field.tab === this.activeTab) && this.activeForms[this.pathToString(field.path)]);
  }

  numberOfFieldsToShow(fieldGroups: IFieldGroup[]) {
    let fieldsToShow = 0;
    fieldGroups.forEach(fg => {
      if ((fg.tab === this.activeTab) && this.activeForms[this.pathToString(fg.path)]) {
        fieldsToShow++;
      }
    });
    return fieldsToShow;
  }

  getKeyClass(subkey: Descriptor) {
    const temp = getOtherData(subkey, 'class');
    return temp && temp[0].value;
  }

  isTabFreeForm() {
    const tab = getOtherData(this.key, 'tab-definition').filter(a=>{return a.value.name === this.activeTab})
    return tab && tab[0].value && tab[0].value.freeForm
  }

  getData(fg: IFieldGroup): any {
    return getValueInGProp(fg.path.slice(1), this.data)
  }
}

export function getSortedFieldByPriority(g, groupList, groups): any {
  if (g === '_invisible') {
    return [];
  }
  if (groupList[g]) {
    return groupList[g];
  }
  groupList[g] = groups[g].sort((fd1, fd2) => {
    const key1Sort = getOtherData(fd1.key, 'priority');
    if (key1Sort === null) {
      return 1;
    }
    const key2Sort = getOtherData(fd2.key, 'priority');
    if (key2Sort === null) {
      return -1;
    }
    return key2Sort[0].value - key1Sort[0].value;
  }).reduce((acc, cfg) => {
    const priorityProp = getOtherData(cfg.key, 'priority');
    let priorityValue = priorityProp && priorityProp[0] && priorityProp[0].value;
    if (priorityValue === null) {
      priorityValue = 99;
    }
    priorityValue = Math.round(priorityValue); // This makes sure that we can organize fields in the same row
    let oldArray = acc.get(`${priorityValue}`);
    if (!oldArray) {
      oldArray = [];
    }
    oldArray.push(cfg);
    acc.set(`${priorityValue}`, oldArray);
    return acc;
  }, new Map());
  return groupList[g];
}

function getValueInGProp(path: string[], obj: GProp[]): any{
  if (!obj || obj.length == 0) return null
  if (path.length > 1){
    const temp = obj.find(prop => prop.name === path[0])
    if (temp && temp.values){
      return getValueInGProp(path.slice(1), temp.values)
    }
    return null
  }
  const temp = obj.find(prop => prop.name === path[0])
  return temp
}