import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Descriptor } from '../model';
import { IWrappableComponent } from '../property/property.component';
import { IFieldGroup } from '../field-group/field-group.component';


@Component({
  selector: 'app-global-generic-radio',
  templateUrl: 'radio.component.html',
  styleUrls: ['radio.component.scss']
})
export class GlobalItemRadioComponent implements OnInit, IWrappableComponent {
  @Input() key: Descriptor;
  @Input() parentRef: string;
  @Input() index: number;
  @Input() data: any;
  @Input() fieldGroup: IFieldGroup;

  constructor(private formBuilder: FormBuilder) {
    this.innerForm = this.formBuilder.group({});
  }

  public selectedOption = '';
  public lastSelected = '';
  public selectedKey = null;
  public innerForm = null;
  public options = [];

  ngOnInit() {
    const form = (this.fieldGroup.form as FormGroup);
    console.log(this.key.name, this.data)
    let initialCase = null;
    const comingFromData = this.data && this.data.values && this.data.values[0] && this.data.values[0].name;
    this.innerForm = form;
    // TODO: Check if there is a better way
    const responsibleControl = form;
    if ( responsibleControl ) {
      const allControlNames = Object.keys((responsibleControl as FormGroup).controls);
      if ( allControlNames.length ) {
        allControlNames.forEach(entry => {
          const currentControl = (responsibleControl as FormGroup).controls[entry]
          if (currentControl.enabled && initialCase == null){
            initialCase = entry;
          }else if(initialCase !== null){
            // We have more than 1 enabled control. That means we are in the first screen loading, try to use the data coming from the server if possible
            if (currentControl.enabled && comingFromData){
              initialCase = comingFromData;
            }
          }
        })
      }
    }
    this.populateOptions(initialCase);
  }

  populateOptions(initialCase: string|null) {
    let clicked = null;
    if (this.key.possibleValues) {
      this.options = this.key.possibleValues.map(cat => {
        const categoryName = cat.value && cat.value.displaySettings && cat.value.displaySettings.name || cat.value && cat.value.name || 'Category not Found';
        // This populates the selected in case we have the controls full and are rebuilding the component;
        if ( initialCase && cat.value.name ===  initialCase ) {
          clicked = categoryName;
        }
        return categoryName;
      });
    } else {
      throw new Error('No options in the item JSON received!!!');
    }
    if ( clicked ) {
      this.onClick(clicked);
    }
  }

  onClick(clicked: any) {
    // don't change if clicking the same place
    if (this.selectedKey && clicked === this.lastSelected) {
      return;
    }
    if (this.selectedKey) {
      this.fieldGroup.deactivateValue([...this.fieldGroup.path, this.selectedKey.name]);
    }
    this.selectedKey = this.key.possibleValues.find((v, i) => {
      const value  = v.value && v.value.displaySettings && v.value.displaySettings.name || v.value.name || v.value;
      return value === clicked;
    }).value;
    this.lastSelected = clicked;
    if ( this.selectedOption !== clicked ) {
      this.selectedOption = clicked;
    }
    this.fieldGroup.activateValue([...this.fieldGroup.path, this.selectedKey.name]);
  }

  public getInnerForm() {
    return this.innerForm;
  }

  public getSelectedKey() {
    return this.selectedKey;
  }
  public getSelectedOption() {
     return this.selectedKey.name;
  }
}
