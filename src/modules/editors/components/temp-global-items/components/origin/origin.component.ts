import { Component, OnInit, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators  } from '@angular/forms';
import { Descriptor } from '../model';
import { IWrappableComponent } from '../property/property.component';
import { IFieldGroup } from '../field-group/field-group.component';

@Component({
  selector: 'app-global-item-origin',
  templateUrl: 'origin.component.html',
  styleUrls: ['origin.component.scss']
})
export class GlobalItemOriginComponent implements OnInit, IWrappableComponent {

  constructor() {}
  @Input() key: Descriptor;
  @Input() parentRef: string;
  @Input() index: number;
  @Input() data: any;
  @Input() fieldGroup: IFieldGroup;

  public countries = [
    {
      name: 'USA',
      value: 'usa'
    },
    {
      name: 'Brazil',
      value: 'brazil'
    },
  ];

  ngOnInit() {
    const countryControl = this.fieldGroup.form.get('country');
    const regionControl = this.fieldGroup.form.get('region');
    const subRegions = this.fieldGroup.form.get('subregions');
    if (!countryControl) {
      return;
    }
    // get previous selected country.
    let dataCountry = null
    if ( this.data ) {
      const temp = this.data.values.find(v => v.name === 'country')
      dataCountry = temp && temp.value
      this.countries.push({name: dataCountry, value: dataCountry})
      dataCountry && countryControl.setValue(dataCountry)

      const region = this.data.values.find(v => v.name === 'region')
      regionControl && region && region.value && regionControl.setValue(region.value)

      const subregion = this.data.values.find(v => v.name === 'subregions')
      subRegions && subregion && subregion.value && subRegions.setValue(subregion.value)
    }
    const selectedCountry = this.fieldGroup.form.value.country || dataCountry;
    if (!selectedCountry) {
      return;
    }
    const countryIndex = this.countries.findIndex((country) => country.value === selectedCountry);
    if (countryIndex !== -1) {
      countryControl.setValue(this.countries[countryIndex].value);
    }
  }
  @Output() shouldShowKey(key: string) {
    return this.key && this.key.keys.filter(k => k.name === key).length > 0;
  }
  getArrayData () {
    return this.data && this.data.values && this.data.values.find(v => v.name === 'subregions') || null
  }

  getArrayFieldForm () {
    return this.fieldGroup.children.subregions
  }

  getArrayKey () {
    return this.fieldGroup.children.subregions.key
  }
}
