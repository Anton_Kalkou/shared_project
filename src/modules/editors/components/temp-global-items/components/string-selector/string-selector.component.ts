import { Component, OnInit, Input, Output } from '@angular/core';
import { FormGroup, FormControl} from '@angular/forms';
import { Descriptor, getOtherData } from '../model';
import { IWrappableComponent } from '../property/property.component';
import { IFieldGroup } from '../field-group/field-group.component';


@Component({
  selector: 'app-global-string-selector',
  templateUrl: 'string-selector.component.html',
  styleUrls: ['string-selector.component.scss']
})
export class StringSelectorComponent implements OnInit, IWrappableComponent {
  @Input() key: Descriptor;
  @Input() parentRef: string;
  @Input() index: number;
  @Input() data: any;
  @Input() onSelect: any;
  @Input() fieldGroup: IFieldGroup;

  constructor() {}

  public selectedString = '';
  public strings = [];

  ngOnInit() {
    const defaultValue = this.getDefaultValue()
    this.populateStrings();
    if (defaultValue) {
      const form = (this.fieldGroup.form as FormControl);
      form.setValue(defaultValue);
    }
  }

  getDefaultValue() {
    const temp = getOtherData(this.key, 'default');
    return (this.data && this.data.value) || temp && temp[0].value;
  }

  populateStrings() {
    const form = (this.fieldGroup.form as FormControl);
    if (this.key.possibleValues) {
      this.strings = this.key.possibleValues.map(cat => {
        return cat.value;
      });
    }
    this.selectedString = form.value || this.getDefaultValue();
  }

  onClick(clicked: any) {
    const form = (this.fieldGroup.form as FormControl);
    this.selectedString = clicked;
    form.setValue(clicked);
    if ( this.onSelect ) {
      this.onSelect(clicked);
    }
  }
}
