import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, OnInit, Input, Output } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, Form } from '@angular/forms';
import { Descriptor, getOtherData } from '../model';
import { IWrappableComponent } from '../property/property.component';
import { IFieldGroup } from '../field-group/field-group.component';

@Component({
	selector: 'app-global-item-nutritional-value',
	templateUrl: 'nutritional-value.component.html',
	styleUrls: ['nutritional-value.component.scss']
})
export class GlobalItemNutritionalValueComponent implements OnInit, IWrappableComponent {
	constructor() { }
	@Input() key: Descriptor;
	@Input() parentRef: string;
	@Input() index: number;
	@Input() data: any;
	@Input() fieldGroup: IFieldGroup;

	public arrayInputStorer = [];
	public newNutritionalInfo = {
		'name': '',
		'value': null,
	};
	public calculatedDV = '';

	ngOnInit() {
		const formValue = (this.fieldGroup.form as FormControl).value;
		(this.fieldGroup.form as FormControl).valueChanges.subscribe(a => this.calculateDV(a))
		if (formValue === null && this.data && this.data.value !== null) {
			(this.fieldGroup.form as FormControl).setValue(this.data.value);

		}
		if (this.isArray(this.key.type)) {
			const incomingValue = [];
			if (this.data) {
				this.data.values.forEach((v, index) => {
					if (v.values.length){
						incomingValue.push({name: v.values[0].value, value: v.values[1].value}) 
					}
				})
			}
			this.arrayInputStorer = (this.fieldGroup.form as FormArray).controls.map((c, index) => {

				return ({ name: c.value.name || (incomingValue[index] && incomingValue[index].name), value: c.value.value || (incomingValue[index] && incomingValue[index].value) });
			}).filter(k => k.name);
			if (!this.arrayInputStorer.length) {
				incomingValue.forEach(d => this.addNewNutritionalInfo(d.name, d.value))
			}
		}
		if (formValue) {
			this.calculateDV(formValue);
		}
	}

	calculateDV(g: number) {
		const data = getOtherData(this.key, 'daily-intake');
		if (data && typeof data[0].value === "number") {
			let percentage: string = (100 * (g / data[0].value)).toFixed(2)
			this.calculatedDV = percentage + "%"
		}
	}

	isMainGroup(key: Descriptor) {
		return (key.displaySettings.name === key.displaySettings.group)
	}

	containsDV() {
		const data = getOtherData(this.key, 'daily-intake')
		return (data !== null)
	}

	isArray(type: string) {
		return type && type.includes('array:');
	}

	isValidNutrient(value: number) {
		if (value && value >= 0) {
			return true
		}
		return false
	}

	addNewNutritionalInfo(name: string, value: number) {
		if ((name || '').trim()) {
			this.arrayInputStorer.push({ name: name.trim(), value: value });
			const formArray = (this.fieldGroup.form as FormArray)
			formArray.insert(formArray.length, new FormGroup({ name: new FormControl(name.trim(), null), value: new FormControl(value, null) }));
		}
		// Reset the input value
		if (this.newNutritionalInfo) {
			this.newNutritionalInfo.name = '';
			this.newNutritionalInfo.value = null;
		}
	}
}
