import { Component, OnInit, Input } from "@angular/core";
import { Descriptor } from '../model';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-global-item-picture',
  templateUrl: 'picture.component.html',
  styleUrls: ['picture.component.scss']
})
export class GlobalItemPictureComponent implements OnInit {
  @Input("form") childForm: FormGroup;

  constructor() { }
  
  public pictures: File[] = [];
  public picturesPreview: any[] = [];

  ngOnInit() { }
  
  onSelectPicture(event) {
    let files: FileList = event.files;

    for (let i = 0; i < files.length; i++) {
      this.pictures.push(files.item(i));

      const reader = new FileReader();

      reader.onload = (e) => {
        this.picturesPreview.push(reader.result);
      }

      reader.readAsDataURL(files.item(i));

    }

    console.log(this.pictures);
  }
}