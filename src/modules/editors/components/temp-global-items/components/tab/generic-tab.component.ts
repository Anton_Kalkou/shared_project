import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup} from '@angular/forms';
import { Descriptor } from '../model';
import { IWrappableComponent } from '../property/property.component';
import { IFieldGroup } from '../field-group/field-group.component';
import { MatTabGroup } from '@angular/material/tabs';


@Component({
  selector: 'app-global-generic-tab',
  templateUrl: 'generic-tab.component.html',
  styleUrls: ['generic-tab.component.scss']
})
export class GenericTabComponent implements OnInit, IWrappableComponent {
  @Input() key: Descriptor;
  @Input() parentRef: string;
  @Input() index: number;
  @Input() data: any;
  @Input() onSelect: any;
  @Input() fieldGroup: IFieldGroup;
  @ViewChild("globalTabs", { static: false }) matTab: MatTabGroup;
  constructor() { }

  public selectedTab = '';
  public tabs = [];

  ngOnInit() {
    this.onSelect = this.onSelect.bind(this)
    this.populateTabs();
    this.selectedTab = this.tabs[0];
    this.onSelect(this.tabs[0]);
  }

  ngAfterViewInit() {
    // This is NECESSARY since we create the form as we show the views
    // this makes sure all tabs are shown at least once and therefore all the values are instantiated
    const tabCount = this.matTab._tabs.length;
    this.tabs.forEach((t, index) => setTimeout(t => {
      this.matTab.selectedIndex = (this.matTab.selectedIndex + 1) % tabCount;
    },10*index, t));
    setTimeout(this.onSelect(this.tabs[0]),10*this.tabs.length + 10);
  }

  populateTabs() {
    this.tabs = [];
    this.key.displaySettings.otherData.forEach(ds => {
      if (ds.name === 'tab-definition') {
        this.tabs.push(ds.value.name);
      }
    });
  }

  getTabKeys() {
    const tabKeys = this.key.keys.reduce( (acc, sub) => {
      const keyTabName = this.checkTabOfKey(sub);
      if ( (keyTabName === this.selectedTab) || (this.selectedTab === this.tabs[0] && keyTabName === '') ) {
        return [...acc, sub];
      }
      return acc;
    }, []);
    return tabKeys;
  }

  onClick(clicked: any) {
    this.selectedTab = this.tabs[clicked.index];
    if ( this.onSelect ) {
      this.onSelect(this.tabs[clicked.index]);
    }
  }

  checkTabOfKey(key: Descriptor): string {
    let value = '';


    if (key && key.displaySettings && key.displaySettings.otherData) {
      key.displaySettings.otherData.forEach(ds => {
        if (ds.name === 'tab-name') {
          value = ds.value;
        }
      });
    }
    return value;
  }
}
