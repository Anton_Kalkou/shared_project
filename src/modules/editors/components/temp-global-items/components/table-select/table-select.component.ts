import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, FormArray} from '@angular/forms';
import { Descriptor } from '../model';
import { IWrappableComponent, Storer } from '../property/property.component';
import { IFieldGroup } from '../field-group/field-group.component';
import { MatChipInputEvent } from '@angular/material/chips';
import { ENTER, COMMA } from '@angular/cdk/keycodes';


@Component({
  selector: 'app-global-table-select',
  templateUrl: 'table-select.component.html',
  styleUrls: ['table-select.component.scss']
})
export class TableSelectComponent implements OnInit, IWrappableComponent {
  @Input() key: Descriptor;
  @Input() index: number;
  @Input() data: any;
  @Input() onSelect: any;
  @Input() fieldGroup: IFieldGroup;

  constructor() { }

  public currentNewAllergen = '';
  public columns = [];
  public arrayInputStorer = [];
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  ngOnInit() {
    this.populate();
  }

  populate() {
    this.columns = [];
    this.key.displaySettings.otherData.forEach(ds => {
      if (ds.name === 'table-define-column') {
        this.columns.push(ds.value);
      }
    });
    this.arrayInputStorer = ((this.fieldGroup.form as FormGroup).controls.Other as FormArray).controls.map((c, index) => {
      return ({name: c.value.name }); 
    }).filter(k => k.name);

    // Normal table Setup
    if (this.data && this.data.values){
      Object.keys((this.fieldGroup.form as FormGroup).controls).map( key => {
        const c = (this.fieldGroup.form as FormGroup).controls[key];
        const index = this.data.values.findIndex(data => data.name === key);
        if (index !== -1 && !c.value && !Array.isArray(c.value)) {
          c.setValue(this.data.values[index].value);
        }
        if (Array.isArray(c.value) && index !== -1 && !c.value.length){
          this.data.values[index].values && this.data.values[index].values.forEach(dataInArray => {
            const formArray = (c as FormArray);
            const arrayValues = dataInArray.values;
            if (arrayValues.length > 1) {
              formArray.insert(formArray.length, new FormGroup({name: new FormControl(arrayValues[0].value, null), value: new FormControl(arrayValues[1].value, null)}));
              this.arrayInputStorer.push({name: arrayValues[0].value})
            }
          })
        }
      })
    }
  }

  isArray(type: string): boolean {
    return type && type.includes('array:');
  }

  remove(key): void {
    this.arrayInputStorer = this.arrayInputStorer.filter(s => s.name !== key.name);
    const index = ((this.fieldGroup.form as FormGroup).controls.Other as FormArray).controls.findIndex(c => c.value.name === key.name);
    if (index !== -1) {
      ((this.fieldGroup.form as FormGroup).controls.Other as FormArray).removeAt(index);
    }
  }

  getControlForKey(key: Descriptor) {
    if (!key.type) {
      const a = ((this.fieldGroup.form as FormGroup).controls.Other as FormArray).controls.find(c => c.value.name === key.name);
      return a && (a as FormGroup).controls.value;
    }
    return (this.fieldGroup.form as FormGroup).controls[key.name];
  }

  onBlurMethod() {
    console.log(this.currentNewAllergen);
    if ((this.currentNewAllergen || '').trim()) {
      this.arrayInputStorer.push({name: this.currentNewAllergen.trim()});
      console.log((this.fieldGroup.form as FormGroup).controls.Other);
      const formArray = ((this.fieldGroup.form as FormGroup).controls.Other as FormArray);
      formArray.insert(formArray.length, new FormGroup({name: new FormControl(this.currentNewAllergen.trim(), null), value: new FormControl('', null)}));
    }
    console.log(this.arrayInputStorer);
    // Reset the input value
    if (this.currentNewAllergen) {
      this.currentNewAllergen = '';
    }
  }

  getKeys() {
    const keys = [...this.fieldGroup.key.keys.slice(0, this.fieldGroup.key.keys.length - 1), ...this.arrayInputStorer, this.fieldGroup.key.keys[this.fieldGroup.key.keys.length - 1]];
    return keys;
  }
}
