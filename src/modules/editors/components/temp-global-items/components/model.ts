export interface Descriptor {
  name: string;
  type: string;
  mandatory?: boolean;
  solvable?: boolean;
  possibleValues?: Gprop[];
  keys?: Descriptor[];
  displaySettings?: DisplaySettings;
}

export interface DisplaySettings {
  name?: string;
  component?: string;
  group?: string;
  otherData?: Gprop[];
}

export interface Gprop {
  name?: string;
  type?: string;
  value?: any;
  values?: Gprop[];
}

export function getOtherData(d: Descriptor, dataName: string): Gprop[] {
  if ( d.displaySettings && d.displaySettings.otherData ) {
    const temp =  d.displaySettings.otherData.filter(k => {
      return k.name === dataName;
    });
    return temp.length && temp || null;
  }
  return null;
}
