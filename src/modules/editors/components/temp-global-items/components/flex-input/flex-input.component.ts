import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Descriptor, getOtherData } from '../model';
import { IWrappableComponent } from '../property/property.component';
import { IFieldGroup } from '../field-group/field-group.component';


@Component({
  selector: 'app-global-item-flex-input',
  templateUrl: 'flex-input.component.html',
  styleUrls: ['flex-input.component.scss']
})
export class GlobalItemFlexInputComponent implements OnInit, IWrappableComponent {

  constructor(private formBuilder: FormBuilder) { }
  @Input() key: Descriptor;
  @Input() parentRef: string;
  @Input() index: number;
  @Input() data: any;
  @Input() fieldGroup: IFieldGroup;

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  ngOnInit() {
  }

  getKeyClass(subkey: Descriptor) {
    const temp = getOtherData(subkey, 'class');
    return temp && temp[0].value;
  }
}


export interface Storer {
  name: string;
}
