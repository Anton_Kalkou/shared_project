import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { Descriptor, getOtherData } from '../model';
import { IWrappableComponent } from '../property/property.component';
import { IFieldGroup } from '../field-group/field-group.component';


@Component({
  selector: 'app-global-item-generic-input',
  templateUrl: 'generic-input.component.html',
  styleUrls: ['generic-input.component.scss']
})
export class GlobalItemGenericInputComponent implements OnInit, IWrappableComponent {

  constructor(private formBuilder: FormBuilder) { }
  @Input() key: Descriptor;
  @Input() parentRef: string;
  @Input() index: number;
  @Input() data: any;
  @Input() fieldGroup: IFieldGroup;

  public arrayInputStorer = [];
  public arrayObjectStorer = {};
  public currentForm = null;

  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  ngOnInit() {
    if (this.isArrayObject(this.key.type)) {
      const form = (this.fieldGroup.form as FormGroup);
      this.arrayObjectStorer[this.key.name] = [];
      const controls = form.controls[this.key.name];
      if ( controls ) {
        for (const _ of (controls as FormArray).controls) {
          this.arrayObjectStorer[this.key.name].push({});
        }
      }
    } else if (this.isArrayBasic(this.key.type)) {
      const form = (this.fieldGroup.form as FormArray);
      this.arrayInputStorer = form.controls && form.controls.reduce( (acc, cur) => (cur.value && [...acc, cur.value]) || acc, []) || [];
      this.currentForm = new FormControl();
      //test for data initiation
      const values = [];
      if (this.data && Array.isArray(this.data.values)) {
        this.data.values.forEach( v => {
          this.add({
            value: v.value
          });
          values.push(v.value);
        });
        this.fieldGroup.form.setValue(values);
      }
    }
    if(this.data && this.data.value && this.fieldGroup.form.untouched){
      this.fieldGroup.form.setValue(this.data.value);
    }
  }

  getInnerForm(index = null) {
    const form = (this.fieldGroup.form as FormGroup);
    if (index != null) {
      return (form.controls[this.key.name] as FormArray).at(index);
    }
    return form.controls[this.key.name];
  }

  isArrayObject(type: string): boolean {
    const value = type;
    return this.isArray(type) && this.isObject(value.replace('array:', ''));
  }

  isBasicType(type: string): boolean {
    return type === 'int' || type === 'float' || type === 'string';
  }

  isBoolType(type: string): boolean {
    return type === 'bool';
  }

  isObject(type: string): boolean {
    return type === 'object';
  }

  isArray(type: string): boolean {
    return type.includes('array:');
  }

  isArrayBasic(type: string): boolean {
    const value = type;
    return this.isArray(type) && this.isBasicType(value.replace('array:', ''));
  }

  add(event: any): void {
    const input = event.input;
    const value = event.value;
    // Add our fruit
    if ((value || '').trim()) {
      this.arrayInputStorer ?
        this.arrayInputStorer.push(value.trim()) :
        this.arrayInputStorer = [ value.trim() ];
      const form = (this.fieldGroup.form as FormArray);
      form.insert(form.length, this.currentForm);
      this.currentForm = new FormControl();
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(value: Storer): void {
    const index = this.arrayInputStorer.indexOf(value);

    if (index >= 0) {
      this.arrayInputStorer.splice(index, 1);
    }
    const form = (this.fieldGroup.form as FormArray);
    form.removeAt(index);
  }

  appendNewObject(field): void {
    // Add the control
    const form = (this.fieldGroup.form as FormGroup);
    (form.controls[field] as FormArray).insert(this.arrayObjectStorer[field] - 1, this.formBuilder.group({}));
    if (field) {
      this.arrayObjectStorer[field] = this.arrayObjectStorer[field] ?
        [...this.arrayObjectStorer[field], {}] : [{}];
    }
  }

  removeObject(field, index): void {
    if (index >= 0 && this.arrayObjectStorer[field]) {
      const form = (this.fieldGroup.form as FormGroup);
      (form.controls[field] as FormArray).removeAt(index);
      this.arrayObjectStorer[field].splice(index, 1);
    }
  }

  getKeyClass(subkey: Descriptor) {
    const temp = getOtherData(subkey, 'class');
    return temp && temp[0].value;
  }
}

export interface Storer {
  name: string;
}
