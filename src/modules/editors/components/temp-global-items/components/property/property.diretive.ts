import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[adHost]',
})
export class PropertyWrapperDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
