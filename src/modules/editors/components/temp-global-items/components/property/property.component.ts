import { Component, OnInit, Input, ViewChild, ComponentFactoryResolver, OnChanges } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Descriptor } from '../model';
import { PropertyWrapperDirective} from './property.diretive';
import { GlobalItemCategoryComponent } from '../category/category.component';
import { GlobalItemOriginComponent } from '../origin/origin.component';
import { GlobalItemGenericInputComponent } from '../generic-input/generic-input.component';
import { StringSelectorComponent } from '../string-selector/string-selector.component';
import { GenericTabComponent } from '../tab/generic-tab.component';
import { GlobalItemRadioComponent } from '../generic-radio/radio.component';
import { GlobalItemFlexInputComponent } from '../flex-input/flex-input.component';
import { IFieldGroup } from '../field-group/field-group.component';
import { TableSelectComponent } from '../table-select/table-select.component';
import {GlobalItemNutritionalValueComponent} from '../nutritional-value/nutritional-value.component';

export interface IWrappableComponent {
  data: any;
  // form: FormGroup;
  key: Descriptor;
  // parentRef: string;
  index: number;
  fieldGroup: IFieldGroup;
}
@Component({
  selector: 'app-global-property-wrapper',
  templateUrl: 'property.component.html',
  styleUrls: ['property.component.scss']
})
export class PropertyWrapperComponent implements IWrappableComponent, OnInit, OnChanges {
  @Input() key: Descriptor;
  @Input() data: any;
  @Input() parentRef: string;
  @Input() index: number;
  @Input() fieldGroup: IFieldGroup;
  @ViewChild(PropertyWrapperDirective, {static: true}) adHost: PropertyWrapperDirective;
  component: any;

  constructor(private componentFactoryResolver: ComponentFactoryResolver,  private formBuilder: FormBuilder) { }

  public arrayInputStorer = {};
  public arrayObjectStorer = {};

  ngOnInit() {
    this.loadFace();
  }
  ngOnChanges() {
    this.loadFace();
  }

  getComponentBasedOnComponentName(): any {
    const name = this.key.displaySettings && this.key.displaySettings.component || this.key.displaySettings && this.key.displaySettings.name || this.key.name;
    switch (name) {
      case 'Origin':
        return GlobalItemOriginComponent;
      case 'Category':
        return GlobalItemCategoryComponent;
      case 'mat-select':
        return StringSelectorComponent;
      case 'tab-component':
        return GenericTabComponent;
      case 'radio-component':
        return GlobalItemRadioComponent;
      case 'flex-input':
        return GlobalItemFlexInputComponent;
      case 'table-select':
          return TableSelectComponent;
      case 'Nutritional Info':
        return GlobalItemNutritionalValueComponent;
      default:
        return GlobalItemGenericInputComponent;
    }
  }

  loadFace() {
    if (!this.fieldGroup) {
      this.fieldGroup = new IFieldGroup();
    }
    if (!this.fieldGroup.form) {
      this.fieldGroup.form = this.formBuilder.group({});
    }
    const comp = this.getComponentBasedOnComponentName();

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(comp);

    const viewContainerRef = this.adHost.viewContainerRef;
    viewContainerRef.clear();
    const componentRef = viewContainerRef.createComponent(componentFactory);

    ( componentRef.instance as IWrappableComponent).key = this.key;
    ( componentRef.instance as IWrappableComponent).fieldGroup = this.fieldGroup;
    ( componentRef.instance as IWrappableComponent).index = this.index;
    ( componentRef.instance as IWrappableComponent).data = this.data;
  }
}

export interface Storer {
  name: string;
}
