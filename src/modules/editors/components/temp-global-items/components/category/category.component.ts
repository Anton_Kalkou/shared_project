import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Descriptor } from '../model';
import { IWrappableComponent } from '../property/property.component';
import { IFieldGroup } from '../field-group/field-group.component';


@Component({
  selector: 'app-global-item-category',
  templateUrl: 'category.component.html',
  styleUrls: ['category.component.scss']
})
export class GlobalItemCategoryComponent implements OnInit, IWrappableComponent {
  @Input() key: Descriptor;
  @Input() parentRef: string;
  @Input() index: number;
  @Input() data: any;
  @Input() fieldGroup: IFieldGroup;

  public selectedCategory = '';
  public lastSelected = '';
  public selectedKey = null;
  public categories = [];

  ngOnInit() {
    const form = (this.fieldGroup.form as FormGroup);
    console.log(this.key.name, this.data)
    let initialCase = null;
    const extractedFromData = this.data && this.data.values && this.data.values[0] && this.data.values[0].name
    const lastActivatedKey = this.fieldGroup.activatedKeys && this.fieldGroup.activatedKeys[0] || extractedFromData;
    if ( lastActivatedKey ) {
        initialCase = lastActivatedKey;
    }
    this.populateCategories(initialCase);
  }

  populateCategories(initialCase: string|null) {
    let clicked = null;
    if (this.key.possibleValues) {
      this.categories = this.key.possibleValues.map(cat => {
        const categoryName = cat.value && cat.value.displaySettings && cat.value.displaySettings.name || cat.value && cat.value.name || 'Category not Found';
        // This populates the selected in case we have the controls full and are rebuilding the component;
        if ( initialCase && cat.value.name ===  initialCase ) {
          clicked = categoryName;
        }
        return categoryName;
      });
    } else {
      throw new Error('No categories in the item JSON received!!!');
    }
    if ( clicked ) {
      this.onClick(clicked);
    }
  }

  onClick(clicked: any) {
    // don't change if clicking the same place
    const form = (this.fieldGroup.form as FormGroup);
    if (this.selectedKey && clicked === this.lastSelected) {
      return;
    }
    if (this.selectedKey) {
      this.fieldGroup.deactivateValue([...this.fieldGroup.path , this.selectedKey.name]);
    }
    this.selectedKey = this.key.possibleValues.find((v, i) => {
      const value  = v.value && v.value.displaySettings && v.value.displaySettings.name || v.value.name;
      return value === clicked;
    }).value;
    this.lastSelected = clicked;
    if ( this.selectedCategory !== clicked ) {
      this.selectedCategory = clicked;
    }
    this.fieldGroup.activateValue([...this.fieldGroup.path , this.selectedKey.name]);
  }

  public getSelectedKey() {
    return this.selectedKey;
  }

  public getSelectedCategory() {
     return this.selectedKey.name;
  }

  shouldShowTitle() {
    return true;
  }
}
