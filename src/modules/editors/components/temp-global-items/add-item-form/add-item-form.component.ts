import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators  } from '@angular/forms';

import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { NewGlobalItem, SaveGlobalItem } from '../../../ngxs/editors.actions'
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { Descriptor } from '../components/model';
import { IFieldGroup } from '../components/field-group/field-group.component';
import { Ontology } from '../../ontology/ontologies/ontologies.component';

@Component({
  selector   : 'app-add-item-editors-form',
  templateUrl: 'add-item-form.component.html',
  styleUrls  : ['add-item-form.component.scss']
})

export class TempAddItemEditorsFormComponent extends Unsubscribe implements OnInit {
  @Select(state => state.editors.ontology) ontology$: Observable<Ontology>;
  @Select(state => state.editors.globalItem) item$: Observable<any>;

  public _itemDescriptor: Descriptor;
  public newGlobalItemForm: FormGroup;
  public components ;
  public fieldGroup: IFieldGroup;
  public status: number = 50;
  public item: any;

  constructor(
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
    private store: Store) {
      super();
      this.newGlobalItemForm = this.formBuilder.group({
        name: new FormControl(null, [Validators.required])
      });
      this.fieldGroup = new IFieldGroup();
      this.getFormProg = this.getFormProg.bind(this);
  }

  public get itemDescriptor() : Descriptor {
    return this._itemDescriptor;
  }

  ngOnInit() {
    this.fieldGroup.form = this.newGlobalItemForm;
    this.fieldGroup.form.statusChanges.subscribe(()=> this.getFormProg());
    this.subscribeTo = this.item$
        .subscribe(item => {
          this.item     = { ...item };
        });
  }

  sameLevelFindInGprop(g, name) {
    const temp = g.values.filter(v => v.name === name);
    return temp[0];
  }
  findInGProp(g, path: string[]) {
    if ( path.length > 1) {
      const temp = this.sameLevelFindInGprop(g, path[0]);
      return this.findInGProp(temp, path.slice(1));
    }
    return this.sameLevelFindInGprop(g, path[0]);
  }

  getFormProg() {
    this.status=Math.round(100*(1 - this.fieldGroup.getNumberOfEmptyFields(0)/this.fieldGroup.getNumberOfEnabledFields(0)))
  }

  save(): void {
    const item = this.fieldGroup.getValue();
    console.log({item})
    const metadata = this.transformToBackItem(item, 'root').values[0];
    const inputData = {
      name: this.findInGProp(metadata, ['name']).value,
      description: this.findInGProp(metadata, ['description']) && this.findInGProp(metadata, ['description']).value,
      brand: this.findInGProp(metadata, ['brand']).value,
      metadata: metadata.values,
    };
   
    const transformedItem = this.item && this.item.id ? {...this.item, ...inputData} : inputData;
    console.log(transformedItem)
    const saveAction = this.item && this.item.id ? new SaveGlobalItem(transformedItem, transformedItem.id) : new NewGlobalItem(inputData);
    this.store.dispatch(saveAction);
  }

  transformToBackItem(item: any, name: string): any {
    const init: any = {
      name,
    };
    if (!item) {
      return item;
    }

    
      Object.keys(item).forEach(field => {
        const it = typeof item[field] === 'object' ? this.transformToBackItem(item[field], field) : {name: field, value: item[field]};
        if (!init.values) {
            init.values = [];
          }
          if (it != null){
            init.values = [...init.values, it];
          }
      });
    

    return init;

  }

  ngAfterContentInit() {
    this.subscribeTo = this.ontology$.subscribe(ontology => {
      this._itemDescriptor = { ...ontology };
      this.fieldGroup.key = this._itemDescriptor;
      this.fieldGroup.form = this.newGlobalItemForm;
      this.cdr.detectChanges();
    });
  }
}
