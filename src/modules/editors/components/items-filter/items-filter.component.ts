import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { Filter } from 'src/modules/shared/models/filter.model';

@Component({
  selector: 'app-items-filter',
  templateUrl: 'items-filter.component.html',
  styleUrls: ['items-filter.component.scss']
})
export class ItemsFilterComponent extends Unsubscribe implements OnInit {

  @Select(state => state.editors.filters) filters$: Observable<Filter[]>;

  public filters: Filter[];

  ngOnInit() {
    this.initSubscriptions();
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.filters$
      .subscribe(filters => this.filters = filters);
  }

}
