import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { APP } from 'src/modules/shared/constants';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { DeleteItem, SaveItem, OpenEditorsTagDialog, SetSelectedAttributes, SetSelectedCategory } from '../../ngxs/editors.actions';
import { EditorsTagPopupTypes } from 'src/modules/shared/enums';
import { Tag } from 'src/modules/shared/models/tag.model';
import { Product } from 'src/modules/shared/models/products/product.model';
import { ProductCategory } from 'src/modules/shared/enums';
import { Beer } from 'src/modules/shared/models/products/beer.model';
import { Berries } from 'src/modules/shared/models/products/berries.model';
import { Gin } from 'src/modules/shared/models/products/gin.model';
import { Sake } from 'src/modules/shared/models/products/sake.model';
import { Vodka } from 'src/modules/shared/models/products/vodka.model';
import { Tequila } from 'src/modules/shared/models/products/tequila.model';
import { Whisky } from 'src/modules/shared/models/products/whisky.model';
import { Wine } from 'src/modules/shared/models/products/wine.model';
import { Alcohol } from 'src/modules/shared/models/products/alcohol.model';

@Component({
  selector: 'app-editors-form',
  templateUrl: 'editors-form.component.html',
  styleUrls: ['editors-form.component.scss']
})
export class EditorsFormComponent extends Unsubscribe implements OnInit {

  @Select(state => state.editors.item) item$: Observable<Product>;
  @Select(state => state.editors.mainCategories) mainCategories$: Observable<Tag[]>;
  @Select(state => state.editors.selectedAttributes) selectedAttributes$: Observable<Tag[]>;
  @Select(state => state.editors.selectedCategory) selectedCategory$: Observable<Tag>;

  public item: any;
  public mainCategories: Tag[];
  public itemForm: FormGroup;
  public productCategory: ProductCategory;

  private generalInformation = {};
  private additionalInformation = {};
  private mainInformation = {};

  constructor(
    private formBuilder: FormBuilder,
    private store: Store
  ) {
    super();
  }

  ngOnInit() {
    this.initSubscriptions();
  }

  public isWine(): boolean {
    return this.productCategory === ProductCategory.wine;
  }

  public reactOnDeleteAttribute(attribute: Tag): void {
    this.store.dispatch(
      new SetSelectedAttributes(this.item.attributes.filter(currentAttribute => currentAttribute.id !== attribute.id))
    );
  }

  public openCategoryPopup(): void {
    this.store.dispatch(new OpenEditorsTagDialog(EditorsTagPopupTypes.categories, [this.item.category]));
  }

  public openAttributesPopup(): void {
    this.store.dispatch(new OpenEditorsTagDialog(EditorsTagPopupTypes.attributes, this.item.attributes));
  }

  public save(): void {
    this.store.dispatch(new SaveItem(this.item));
  }

  public delete(): void {
    this.store.dispatch(new DeleteItem(this.item.id));
  }

  private detectProductType(subcategory: Tag): void {
    switch (subcategory.name) {
      case APP.subcategories.beer:
        const beer = this.item as Beer;

        this.initAlcoholForm(beer);
        this.initBeerForm(beer);
        this.productCategory = ProductCategory.beer;

        break;
      case APP.subcategories.berries:
        const berries = this.item as Berries;

        this.initBerriesForm(berries);
        this.productCategory = ProductCategory.berries;

        break;
      case APP.subcategories.gin:
        const gin = this.item as Gin;

        this.initAlcoholForm(gin);
        this.initGinForm(gin);
        this.productCategory = ProductCategory.gin;

        break;
      case APP.subcategories.sake:
        const sake = this.item as Sake;

        this.initAlcoholForm(sake);
        this.initSakeForm(sake);
        this.productCategory = ProductCategory.sake;

        break;
      case APP.subcategories.tequila:
        const tequila = this.item as Tequila;

        this.initAlcoholForm(tequila);
        this.initTequilaForm(tequila);
        this.productCategory = ProductCategory.tequila;

        break;
      case APP.subcategories.vodka:
        const vodka = this.item as Vodka;

        this.initAlcoholForm(vodka);
        this.initVodkaForm(vodka);
        this.productCategory = ProductCategory.vodka;

        break;
      case APP.subcategories.whisky:
        const whisky = this.item as Whisky;

        this.initAlcoholForm(whisky);
        this.initWhiskyForm(whisky);
        this.productCategory = ProductCategory.whisky;

        break;
      case APP.subcategories.wine:
        const wine = this.item as Wine;

        this.initAlcoholForm(wine);
        this.initWineForm(wine);
        this.productCategory = ProductCategory.wine;

        break;
      default:
        this.initForm();
        this.productCategory = ProductCategory.default;
    }

    this.initForm();
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.item$
      .subscribe(item => {
        this.item = {...item};
        this.setDefaultSelectedAttributes();
        this.detectProductType(this.item.category);
      });
    this.subscribeTo = this.mainCategories$
      .subscribe(mainCategories => {
        this.mainCategories = mainCategories;
        this.initForm();
      });
    this.subscribeTo = this.selectedAttributes$
      .subscribe(attributes => {
        this.item.attributes = attributes;
      });
    this.subscribeTo = this.selectedCategory$
      .subscribe(category => {
        this.item.category = category;
        this.detectProductType(category);
      });
  }

  private initForm(): void {
    if (this.item && this.mainCategories) {
      this.itemForm = this.formBuilder.group({
        mainCategory: [this.mainCategories.find(category => this.item.mainCategory === category.name)],
        ...this.mainInformation,
        generalInformation: this.formBuilder.group({
          packaged: [this.item.packaged],
          productName: [this.item.name],
          productNameVariations: [this.item.productNameVariations],
          ...this.generalInformation
        }),
        additionalInformation: this.formBuilder.group({
          extraNotes: [this.item.extraNotes],
          ...this.additionalInformation
        })
      });
    }
  }

  private initAlcoholForm(item: Alcohol): void {
    this.generalInformation = {
      ...this.generalInformation,
      brand: item.brand,
      countryOrRegion: item.countryOrRegion,
      quantityPerUnit: item.quantityPerUnit,
      measure: item.measure,
      barCodeNumber: item.barCodeNumber,
      alcohol: item.alcohol
    };
    this.additionalInformation = {
      ...this.additionalInformation,
      recommendedGlassware: item.recommendedGlassware,
      pairing: item.pairing
    };
  }

  private initBerriesForm(item: Berries): void {
    this.generalInformation = {
      ...this.generalInformation,
      producer: [item.producer],
      soldBy: [item.soldBy]
    };
  }

  private initBeerForm(item: Beer): void {
    this.generalInformation = {
      ...this.generalInformation,
      manufacturer: [item.manufacturer],
      beerType: [item.beerType],
      beerSubtype: [item.beerSubtype],
      stoutSubtype: [item.stoutSubtype],
      aged: [item.aged]
    };
    this.additionalInformation = {
      ...this.additionalInformation,
      EBC: [item.EBC],
      IBU: [item.IBU],
      lupus: [item.lupus]
    };
  }

  private initGinForm(item: Gin): void {
    this.generalInformation = {
      ...this.generalInformation,
      manufacturer: [item.manufacturer],
      ginType: [item.ginType],
      aged: [item.aged]
    };
  }

  private initSakeForm(item: Sake): void {
    this.generalInformation = {
      ...this.generalInformation,
      manufacturer: [item.manufacturer],
      sakeType: [item.sakeType],
      aged: [item.aged]
    };
    this.additionalInformation = {
      ...this.additionalInformation,
      servingStyle: [item.servingStyle]
    };
  }

  private initVodkaForm(item: Vodka): void {
    this.generalInformation = {
      ...this.generalInformation,
      producer: [item.producer],
      vodkaType: [item.vodkaType],
      aged: [item.aged]
    };
  }

  private initTequilaForm(item: Tequila): void {
    this.generalInformation = {
      ...this.generalInformation,
      producer: [item.producer],
      tequilaType: [item.tequilaType],
      aged: [item.aged]
    };
  }

  private initWhiskyForm(item: Whisky): void {
    this.generalInformation = {
      ...this.generalInformation,
      producer: [item.producer],
      whiskyType: [item.whiskyType],
      whiskySubtype: [item.whiskySubtype],
      flavorProfile: [item.flavorProfile],
      aged: [item.aged]
    };
  }

  private initWineForm(item: Wine): void {
    this.generalInformation = {
      ...this.generalInformation,
      producer: [item.producer],
      wineType: [item.wineType],
      wineSubtype: [item.wineSubtype],
      flavorProfile: [item.flavorProfile],
      grapes: [item.grapes],
      acidityScale: [item.acidityScale],
      bodyScale: [item.bodyScale],
      tanninScale: [item.tanninScale],
      vintage: [item.vintage]
    };
    this.additionalInformation = {
      ...this.additionalInformation,
      letWineBreathIn: [item.letWineBreathIn]
    };
    this.mainInformation = {
      ...this.mainInformation,
      ratings: [item.ratings]
    };
  }

  private setDefaultSelectedAttributes(): void {
    this.store.dispatch(new SetSelectedAttributes(this.item.attributes));
    this.store.dispatch(new SetSelectedCategory(this.item.category));
  }

}
