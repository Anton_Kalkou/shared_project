import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { ProductCategory } from 'src/modules/shared/enums';
import { APP } from 'src/modules/shared/constants';

@Component({
  selector: 'app-additional-information',
  templateUrl: 'additional-information.component.html',
  styleUrls: ['additional-information.component.scss']
})
export class AdditionalInformationComponent {

  @Input() additionalInformationForm: FormGroup;
  @Input() productCategory: ProductCategory;

  public EBC = APP.EBC;
  public IBU = APP.IBU;
  public servingStyles = APP.servingStyles;
  public letWineBreathIn = APP.letWineBreathIn;

  public isDefault(): boolean {
    return this.productCategory === ProductCategory.default;
  }

  public isBerries(): boolean {
    return this.productCategory === ProductCategory.berries;
  }

  public isBeer(): boolean {
    return this.productCategory === ProductCategory.beer;
  }

  public isGin(): boolean {
    return this.productCategory === ProductCategory.gin;
  }

  public isSake(): boolean {
    return this.productCategory === ProductCategory.sake;
  }

  public isTequila(): boolean {
    return this.productCategory === ProductCategory.tequila;
  }

  public isVodka(): boolean {
    return this.productCategory === ProductCategory.vodka;
  }

  public isWhisky(): boolean {
    return this.productCategory === ProductCategory.whisky;
  }

  public isWine(): boolean {
    return this.productCategory === ProductCategory.wine;
  }

}
