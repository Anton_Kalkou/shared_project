import { Component, Input } from '@angular/core';
import { EditorsItem } from 'src/modules/shared/models/editors-item.model';

@Component({
  selector: 'app-mobile-item',
  templateUrl: 'mobile-item.component.html',
  styleUrls: ['mobile-item.component.scss']
})
export class MobileItemComponent {

  @Input() item: EditorsItem;

}
