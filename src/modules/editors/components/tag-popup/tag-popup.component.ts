import { Component, Inject, ViewChild, ElementRef } from '@angular/core';

import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import * as deepEqual from 'fast-deep-equal';
import { Store } from '@ngxs/store';
import { Subject } from 'rxjs';

import { TagsPopupData } from 'src/modules/shared/models/popups-data/tags.model';
import { Tag } from 'src/modules/shared/models/tag.model';
import { APP } from 'src/modules/shared/constants';
import { EditorsTagPopupTypes } from 'src/modules/shared/enums';
import { SetSelectedAttributes, SetSelectedCategory } from '../../ngxs/editors.actions';
import { TranslationService } from 'src/modules/shared/services/translation.service';

@Component({
  selector: 'app-tag-popup',
  templateUrl: 'tag-popup.component.html',
  styleUrls: ['tag-popup.component.scss']
})
export class TagPopupComponent {

  @ViewChild('newTagInput') newTagInput: ElementRef<HTMLInputElement>;

  public tags: Tag[];
  public applyIsDisabled = true;
  public isAdding = false;
  public newTag: string;
  public unselectSubject = new Subject<void>();

  private selectedTags: Tag[];
  private sourceSelectedTags: Tag[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData: TagsPopupData,
    private dialog: MatDialog,
    private store: Store,
    public translationService: TranslationService
  ) {
    this.tags = [...dialogData.tags];
    this.selectedTags = [...dialogData.selectedTags];
    this.sourceSelectedTags = [...dialogData.selectedTags];
  }

  public apply(): void {
    switch (this.dialogData.popupType) {
      case EditorsTagPopupTypes.attributes:
        this.store.dispatch(new SetSelectedAttributes(this.selectedTags));
        break;
      case EditorsTagPopupTypes.categories:
        this.store.dispatch(new SetSelectedCategory(this.selectedTags[0]));
        break;
      default:
        console.warn('Unknown popup type');
    }

    this.dialog.getDialogById(APP.dialogs.tags).close();
  }

  public reactOnDeleteMyTag(tag: Tag): void {
    this.tags = this.tags.filter(currentTag => currentTag !== tag);
    this.selectedTags = this.selectedTags.filter(currentTag => currentTag !== tag);
    this.detectApplyButtonState();
  }

  public openAdding(): void {
    this.isAdding = true;
    setTimeout(() => {
      this.newTagInput.nativeElement.focus();
    });
  }

  public closeAdding(): void {
    this.isAdding = false;
    this.newTag = '';
  }

  public addNewTag(): void {
    if (this.newTag) {
      this.tags.push({name: this.newTag, addedByMe: true});
      this.isAdding = false;
      this.newTag = '';
    }
  }

  public reactOnSelectTag(value: boolean, tag: Tag): void {
    if (this.dialogData.multiple) {
      if (value) {
        this.selectedTags.push(tag);
      } else {
        this.selectedTags = this.selectedTags.filter(currentTag => currentTag.id !== tag.id);
      }
    } else {
      this.unselectSubject.next();
      this.selectedTags = [tag];
    }

    this.detectApplyButtonState();
  }

  public tagIsSelected(tag: Tag): boolean {
    return !!this.dialogData.selectedTags.find(currentTag => currentTag.id === tag.id);
  }

  private detectApplyButtonState(): void {
    this.applyIsDisabled = deepEqual(this.sourceSelectedTags, this.selectedTags);
  }

}
