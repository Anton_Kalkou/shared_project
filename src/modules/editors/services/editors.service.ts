import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { delay } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

import { ApiService } from 'src/modules/shared/services/api.service';
import { Filter } from 'src/modules/shared/models/filter.model';
import { APP } from 'src/modules/shared/constants';
import { EditorsItem } from 'src/modules/shared/models/editors-item.model';
import { Tag } from 'src/modules/shared/models/tag.model';
import { NavItem } from 'src/modules/shared/models/nav-item.model';
import { Product } from 'src/modules/shared/models/products/product.model';
import { GlobalItems } from '../components/temp-global-items/items/items.component';
import { CatalogItems } from '../components/temp-global-items/approve-items/approve-items.component';
import { Ontologies, Ontology } from '../components/ontology/ontologies/ontologies.component';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EditorsService {

  private products = {
    beer   : {
      id                   : 1,
      created              : '1 day ago',
      name                 : 'Guinness Draught 11.2 fl oz',
      addedBy              : {
        name    : 'Ray Burger',
        number  : 5,
        contacts: 'Restaurant A 95 Weiss St, Palo Alto CA restauranta@rayburger.com +1 650 894 4351'
      },
      status               : 50,
      mainCategory         : 'Alcoholic Beverage',
      category             : { id: 2, name: 'Beer' },
      barCodePhotos        : [
        '../../../../assets/images/item.jpg',
        '../../../../assets/images/item.jpg',
        '../../../../assets/images/item.jpg'
      ],
      packaged             : true,
      productNameVariations: [
        'Guinness Draught Stout'
      ],
      brand                : 'Guinness',
      manufacturer         : 'Guiness Brewery',
      beerType             : 'Ale',
      alcohol              : '4%',
      beerSubtype          : 'Stout',
      stoutSubtype         : 'Dry / Irish Stout',
      countryOrRegion      : 'Dublin, Ireland',
      aged                 : '2 years',
      quantityPerUnit      : 11.2,
      measure              : 'fl oz',
      barCodeNumber        : '1 234567 890123',
      attributes           : [
        { id: 2, name: 'Fair Trade' },
      ],
      extraNotes           : ''
    },
    berries: {
      id                   : 1,
      created              : '1 day ago',
      name                 : 'Driscoll’s Blueberry',
      addedBy              : {
        name    : 'Ray Burger',
        number  : 5,
        contacts: 'Restaurant A 95 Weiss St, Palo Alto CA restauranta@rayburger.com +1 650 894 4351'
      },
      status               : 50,
      mainCategory         : 'Food',
      category             : { id: 1, name: 'Berries' },
      barCodePhotos        : [
        '../../../../assets/images/item.jpg',
        '../../../../assets/images/item.jpg',
        '../../../../assets/images/item.jpg'
      ],
      packaged             : true,
      productNameVariations: [
        'Driscoll’s Natural Blueberry',
        'Driscoll’s Local Blueberry'
      ],
      producer             : 'Driscoll’s Berries',
      soldBy               : 'Kg',
      attributes           : [
        { id: 2, name: 'Fair Trade' },
        { id: 6, name: 'Vegan' }
      ],
      extraNotes           : ''
    },
    gin    : {
      id                   : 1,
      created              : '1 day ago',
      name                 : 'Bombay Sapphire London Dry Gin 750 ml',
      addedBy              : {
        name    : 'Ray Burger',
        number  : 5,
        contacts: 'Restaurant A 95 Weiss St, Palo Alto CA restauranta@rayburger.com +1 650 894 4351'
      },
      status               : 50,
      mainCategory         : 'Alcoholic Beverage',
      category             : { id: 3, name: 'Gin' },
      barCodePhotos        : [
        '../../../../assets/images/item.jpg',
        '../../../../assets/images/item.jpg',
        '../../../../assets/images/item.jpg'
      ],
      packaged             : true,
      productNameVariations: [
        'Bombay Sapphire Distilled London Dry Gin'
      ],
      brand                : 'Bombay Sapphire',
      manufacturer         : 'Bombay Sapphire Distillery',
      ginType              : 'London Dry',
      alcohol              : '4%',
      countryOrRegion      : 'London, England',
      aged                 : '2 years',
      quantityPerUnit      : 750,
      measure              : 'ml',
      barCodeNumber        : '1 234567 890123',
      attributes           : [],
      extraNotes           : ''
    },
    sake   : {
      id                   : 1,
      created              : '1 day ago',
      name                 : 'Sho Chiku Bai Nigori Silky Mild Unfiltered Sake 750 ml',
      addedBy              : {
        name    : 'Ray Burger',
        number  : 5,
        contacts: 'Restaurant A 95 Weiss St, Palo Alto CA restauranta@rayburger.com +1 650 894 4351'
      },
      status               : 50,
      mainCategory         : 'Alcoholic Beverage',
      category             : { id: 4, name: 'Sake' },
      barCodePhotos        : [
        '../../../../assets/images/item.jpg',
        '../../../../assets/images/item.jpg',
        '../../../../assets/images/item.jpg'
      ],
      packaged             : true,
      productNameVariations: [
        'Sho Chiku Bai Nigori Sake'
      ],
      brand                : 'Sho Chiku Bai',
      manufacturer         : 'Takara Sake',
      sakeType             : 'Ginjo',
      alcohol              : '4%',
      countryOrRegion      : 'Akita, Japan',
      aged                 : '2 years',
      quantityPerUnit      : 750,
      measure              : 'ml',
      barCodeNumber        : '1 234567 890123',
      attributes           : [],
      extraNotes           : '',
      servingStyle         : 'Chilled'
    },
    tequila: {
      id                   : 1,
      created              : '1 day ago',
      name                 : 'Jose Cuervo Especial Silver Tequila 750 ml',
      addedBy              : {
        name    : 'Ray Burger',
        number  : 5,
        contacts: 'Restaurant A 95 Weiss St, Palo Alto CA restauranta@rayburger.com +1 650 894 4351'
      },
      status               : 50,
      mainCategory         : 'Alcoholic Beverage',
      category             : { id: 5, name: 'Tequila' },
      barCodePhotos        : [
        '../../../../assets/images/item.jpg',
        '../../../../assets/images/item.jpg',
        '../../../../assets/images/item.jpg'
      ],
      packaged             : true,
      productNameVariations: [
        'Jose Cuervo Especial Silver'
      ],
      brand                : 'Jose Cuervo',
      producer             : 'Jose Cuervo Distillery',
      tequilaType          : 'Silver',
      alcohol              : '4%',
      countryOrRegion      : 'Tequila, Mexico',
      aged                 : '2 years',
      quantityPerUnit      : 750,
      measure              : 'ml',
      barCodeNumber        : '1 234567 890123',
      attributes           : [],
      extraNotes           : ''
    },
    vodka  : {
      id                   : 1,
      created              : '1 day ago',
      name                 : 'Absolut Lime Flavored Vodka 750 ml',
      addedBy              : {
        name    : 'Ray Burger',
        number  : 5,
        contacts: 'Restaurant A 95 Weiss St, Palo Alto CA restauranta@rayburger.com +1 650 894 4351'
      },
      status               : 50,
      mainCategory         : 'Alcoholic Beverage',
      category             : { id: 6, name: 'Vodka' },
      barCodePhotos        : [
        '../../../../assets/images/item.jpg',
        '../../../../assets/images/item.jpg',
        '../../../../assets/images/item.jpg'
      ],
      packaged             : true,
      productNameVariations: [
        'Absolut Lime Vodka'
      ],
      brand                : 'Absolut',
      producer             : 'Absolut Swedish Vodka',
      vodkaType            : 'Flavored',
      alcohol              : '4%',
      countryOrRegion      : 'Ahus, Sweden',
      aged                 : '2 years',
      quantityPerUnit      : 750,
      measure              : 'ml',
      barCodeNumber        : '1 234567 890123',
      attributes           : [],
      extraNotes           : ''
    },
    whisky : {
      id                   : 1,
      created              : '1 day ago',
      name                 : 'Jack Daniel`s Old No. 7 Tennessee Whiskey 750 ml',
      addedBy              : {
        name    : 'Ray Burger',
        number  : 5,
        contacts: 'Restaurant A 95 Weiss St, Palo Alto CA restauranta@rayburger.com +1 650 894 4351'
      },
      status               : 50,
      mainCategory         : 'Alcoholic Beverage',
      category             : { id: 7, name: 'Whisky' },
      barCodePhotos        : [
        '../../../../assets/images/item.jpg',
        '../../../../assets/images/item.jpg',
        '../../../../assets/images/item.jpg'
      ],
      packaged             : true,
      productNameVariations: [
        'Jack Daniel`s Old No. 7 Tennessee Whiskey 750 ml'
      ],
      brand                : 'Jack Daniel’s',
      producer             : 'Jack Daniel Distillery',
      whiskyType           : 'American',
      alcohol              : '4%',
      whiskySubtype        : 'Tenessee',
      flavorProfile        : 'Full-bodied',
      countryOrRegion      : 'Medoc, Bordeaux, France',
      aged                 : '8 years',
      quantityPerUnit      : 750,
      measure              : 'ml',
      barCodeNumber        : '1 234567 890123',
      attributes           : [],
      extraNotes           : ''
    },
    wine   : {
      id                   : 1,
      created              : '1 day ago',
      name                 : 'Chateau Red Wine 1942 750 ml',
      addedBy              : {
        name    : 'Ray Burger',
        number  : 5,
        contacts: 'Restaurant A 95 Weiss St, Palo Alto CA restauranta@rayburger.com +1 650 894 4351'
      },
      status               : 50,
      mainCategory         : 'Alcoholic Beverage',
      category             : { id: 8, name: 'Wine' },
      barCodePhotos        : [
        '../../../../assets/images/item.jpg',
        '../../../../assets/images/item.jpg',
        '../../../../assets/images/item.jpg'
      ],
      packaged             : true,
      productNameVariations: [
        'Chateau Red Wine'
      ],
      brand                : 'Chateau',
      producer             : 'Chateau Winery',
      wineType             : 'Red',
      alcohol              : '4%',
      wineSubtype          : 'Tenessee',
      flavorProfile        : 'Sweet',
      grapes               : [
        { grape: '50%', grapeVariety: 'Pinot Noir' },
        { grape: '25%', grapeVariety: 'Merlot' },
        { grape: '25%', grapeVariety: 'Malbec' }
      ],
      acidityScale         : 1,
      bodyScale            : 1,
      tanninScale          : 1,
      countryOrRegion      : 'Medoc, Bordeaux, France',
      vintage              : '1942',
      quantityPerUnit      : 750,
      measure              : 'ml',
      barCodeNumber        : '1 234567 890123',
      attributes           : [],
      extraNotes           : '',
      letWineBreathIn      : 'Decanter',
      recommendedGlassware : 'Burgundy',
      ratings              : [
        { by: 'WS Wine Spectator', points: 99 }
      ]
    }
  };

  private coldstart = {
    "id"             : "5f104d42a77b4dfb399d54e5",
    "name"           : "root.metadata",
    "type"           : "object",
    "solvable"       : true,
    "mandatory"      : true,
    "displaySettings": null,
    "possibleValues" : null,
    "keys"           : [],
  }

  constructor(
      private apiService: ApiService, private http: HttpClient
  ) {
  }

  public getCategories(): Observable<Tag[]> {
    // return this.apiService.get(APP.endpoints.categories);
    return of([
      { id: 1, name: 'Berries' },
      { id: 2, name: 'Beer' },
      { id: 3, name: 'Gin' },
      { id: 4, name: 'Sake' },
      { id: 5, name: 'Tequila' },
      { id: 6, name: 'Vodka' },
      { id: 7, name: 'Whisky' },
      { id: 8, name: 'Wine' }
    ])
        .pipe(
            delay(500)
        );
  }

  public getMainCategories(): Observable<Tag[]> {
    // return this.apiService.get(APP.endpoints.categories);
    return of([
      { id: 1, name: 'Food' },
      { id: 1, name: 'Alcoholic Beverage' }
    ])
        .pipe(
            delay(500)
        );
  }

  public getAttributes(): Observable<Tag[]> {
    // return this.apiService.get(APP.endpoints.attributes);
    return of([
      { id: 1, name: 'Biodynamic' },
      { id: 2, name: 'Fair Trade' },
      { id: 3, name: 'Local' },
      { id: 4, name: 'Organic' },
      { id: 5, name: 'Sustainable' },
      { id: 6, name: 'Vegan' }
    ])
        .pipe(
            delay(500)
        );
  }

  public saveItem(item: Product): Observable<void> {
    // return this.apiService.put(`${APP.endpoints.editorsItems}`, item);
    return of(null)
        .pipe(
            delay(500)
        );
  }

  public deleteItem(itemId: number): Observable<void> {
    // return this.apiService.delete(`${APP.endpoints.editorsItems}/${itemId}`);
    return of(null)
        .pipe(
            delay(500)
        );
  }

  public getItemById(itemId: number): Observable<Product> {
    // return this.apiService.get(`${APP.endpoints.editorsItems}/${itemId}`);
    return of(this.products.wine)
        .pipe(
            delay(500)
        );
  }

  public getItems(): Observable<EditorsItem[]> {
    // return this.apiService.get(APP.endpoints.editorsItems);
    return of([
      {
        id          : 1,
        name        : 'Driscoll’s Blueberry 8 oz',
        category    : 'Fruit',
        frequency   : 2,
        modified    : '1 day ago',
        lastEditedBy: 'John Muller',
        status      : 50,
        packaged    : true
      },
      {
        id          : 2,
        name        : 'Chateau Red Wine 1942 750 ml',
        category    : 'Wine',
        frequency   : 1,
        modified    : '1 day ago',
        lastEditedBy: 'Ray Johnson',
        status      : 10,
        packaged    : true
      },
      {
        id          : 3,
        name        : 'Guinness Draught Stout 11.2 fl oz',
        category    : 'Beer',
        frequency   : 4,
        modified    : '2 day ago',
        lastEditedBy: 'Ryan Taylor',
        status      : 70,
        packaged    : true
      },
      {
        id          : 4,
        name        : 'Guinness Weiss 11.2 fl oz',
        category    : 'Beer',
        frequency   : 3,
        modified    : '2 day ago',
        lastEditedBy: 'Taylor Shawn',
        status      : 25,
        packaged    : true
      },
      {
        id          : 5,
        name        : 'Driscoll’s Strawberry 8 oz',
        category    : 'Fruit',
        frequency   : 8,
        modified    : '4 day ago',
        lastEditedBy: 'John Muller',
        status      : 50,
        packaged    : true
      },
      {
        id          : 6,
        name        : 'Chateau White Wine 1942 750 ml',
        category    : 'Wine',
        frequency   : 12,
        modified    : '5 day ago',
        lastEditedBy: 'Ray Johnson',
        status      : 85,
        packaged    : true
      },
      {
        id          : 7,
        name        : 'Heineken Lager Bottle 11.2 fl oz',
        category    : 'Beer',
        frequency   : 34,
        modified    : '10 day ago',
        lastEditedBy: 'Ryan Taylor',
        status      : 100,
        packaged    : true
      },
      {
        id          : 8,
        name        : 'Driscoll’s Blackberry 8 oz',
        category    : 'Fruit',
        frequency   : 55,
        modified    : '12 day ago',
        lastEditedBy: 'Taylor Shawn',
        status      : 30,
        packaged    : true
      },
      {
        id          : 9,
        name        : 'Casablanca Red Wine 750 ml',
        category    : 'Wine',
        frequency   : 33,
        modified    : '30 day ago',
        lastEditedBy: 'Ray Johnson',
        status      : 25,
        packaged    : true
      },
      {
        id          : 10,
        name        : 'Casablanca Rose Wine 750 ml',
        category    : 'Wine',
        frequency   : 87,
        modified    : '35 day ago',
        lastEditedBy: 'Ryan Taylor',
        status      : 85,
        packaged    : true
      },
      {
        id          : 11,
        name        : 'Driscoll’s Blueberry',
        category    : 'Fruit',
        frequency   : 2,
        modified    : '1 day ago',
        lastEditedBy: 'John Muller',
        status      : 50,
        packaged    : false
      },
      {
        id          : 12,
        name        : 'Driscoll’s Blueberry',
        category    : 'Fruit',
        frequency   : 8,
        modified    : '4 day ago',
        lastEditedBy: 'John Muller',
        status      : 50,
        packaged    : false
      },
      {
        id          : 13,
        name        : 'Driscoll’s Blackberry',
        category    : 'Fruit',
        frequency   : 55,
        modified    : '12 day ago',
        lastEditedBy: 'Taylor Shawn',
        status      : 30,
        packaged    : false
      }
    ])
        .pipe(
            delay(500)
        );
  }

  public getFilters(): Observable<Filter[]> {
    // return this.apiService.get(APP.endpoints.filters);
    return of([
      {
        title: 'completion',
        group: [
          {
            title  : '',
            filters: [
              '0-20%',
              '20-40%',
              '40-60%',
              '60-80%',
              '80-100%',
              '100%'
            ]
          }
        ]
      },
      {
        title: 'items categories',
        group: [
          {
            title  : 'Food',
            filters: [
              'Canned goods',
              'Dry Goods',
              'Fruit',
              'Pasta',
              'Seafood',
              'Vegetables'
            ]
          },
          {
            title  : 'Beverage',
            filters: [
              'Iced Coffee',
              'Iced Ted',
              'Juice',
              'Soda',
              'Water'
            ]
          },
          {
            title  : 'Alcoholic Beverage',
            filters: [
              'Beer',
              'Wine'
            ]
          }
        ]
      },
      {
        title: 'last edited by',
        group: [
          {
            title  : '',
            filters: [
              'John Muller',
              'Ray Johnson',
              'Ryan Taylor',
              'Taylor Shawn'
            ]
          }
        ]
      }
    ])
        .pipe(
            delay(500)
        );
  }

  public getQuantities(): Observable<NavItem[]> {
    // return this.apiService.get(APP.endpoints.quantities);
    return of([
      // TODO: Temporary Global Items, Remove later
      { translationKey: 'menu.global-items', quantity: 'N/A', link: '/editors/global-items' },
      { translationKey: 'menu.ontology', link: '/editors/ontology' },
      { translationKey: 'menu.approve-items', quantity: 'N/A', link: '/editors/approve-items' },
      // { translationKey: 'menu.allergens', quantity: 10, link: '/editors/items/allergens', disabled: true },
      // { translationKey: 'menu.categories', quantity: 235, link: '/editors/items/categories', disabled: true },
      // { translationKey: 'menu.cooking-methods', quantity: 32, link: '/editors/items/cooking-methods', disabled: true },
      // { translationKey: 'menu.cuisines', quantity: 10, link: '/editors/items/cuisines', disabled: true },
      // { translationKey: 'menu.diets', quantity: 50, link: '/editors/items/diets', disabled: true },
      // { translationKey: 'menu.food-type', quantity: 235, link: '/editors/items/food-type', disabled: true },
      // { translationKey: 'menu.generic-names', quantity: 235, link: '/editors/items/generic-names', disabled: true },
      // { translationKey: 'menu.items', quantity: 184, link: '/editors/items/items', disabled: true }
    ])
        .pipe(
            delay(500)
        );
  }

  // TODO: Temporary Global Items, Remove later
  public getGlobalItems(page, pageSize, filter, q, source): Observable<GlobalItems> {
    const filters = filter.map(ft => `&filter=${ft}`).join('')
    const sources = source.map(ft => `&source=${ft}`).join('')
    if (q != "") {
      q = `&q=${q}`
    }
    return this.apiService.get(APP.endpoints.globalItems + `?page=${page}&pageSize=${pageSize}${filters}${sources}${q}`);
  }

  // TODO: Temporary Global Items, Remove later
  public getPendingItems(pageSize, nextPage?, previousPage?, sort?): Observable<CatalogItems> {
    let sortKey  = '';
    let next     = '';
    let previous = '';

    if (nextPage && nextPage != "") {
      next = `&next=${nextPage}`
    }

    if (previousPage && previousPage != "") {
      previous = `&previous=${previousPage}`
    }

    if (sort && sort != "") {
      sortKey = `&sort=${sort}`
    }

    return this.apiService.get(APP.endpoints.catalogPendingItems + `?pageSize=${pageSize}${next}${previous}${sortKey}`);
  }

  // TODO: Temporary Global Items, Remove later
  public changeItemVisibility(itemId: string, visibility: boolean): Observable<GlobalItems> {
    return this.apiService.patch(APP.endpoints.catalogPendingItems + `/${itemId}/visibility`, { "isHidden": visibility });
  }

  // TODO: Temporary Global Items Counter, Remove Later
  public getGlobalItemsCount(): Observable<GlobalItems> {
    return this.apiService.get(APP.endpoints.globalItems + `?count=true`);
  }

  // TODO: Temporary Global Items Counter, Remove Later
  public getCatalogItemsCount(): Observable<GlobalItems> {
    return this.apiService.get(APP.endpoints.catalogPendingItems);
  }

  // TODO: Temporary Global Items, Remove later
  public getGlobalItemById(itemId: string): Observable<any> {
    return this.apiService.get(APP.endpoints.globalItems + `/${itemId}`);
  }

  // TODO: Temporary Global Items, Remove later
  public newGlobalItem(item: any): Observable<void> {
    return this.apiService.post(APP.endpoints.globalItems, item);
  }

  // TODO: Temporary Global Items, Remove later
  public saveGlobalItem(item: any, itemId: string): Observable<void> {
    return this.apiService.put(APP.endpoints.globalItems + `/${itemId}`, item);
  }

  // TODO: Temporary Global Items, Remove later
  public deleteGlobalItem(itemId: string): Observable<void> {
    return this.apiService.delete(`${APP.endpoints.globalItems}/${itemId}`);
  }

  public newOntology(item: any): Observable<Ontology> {
    return this.apiService.post(APP.endpoints.ontologies, item);
  }

  public saveOntology(item: any, itemId: string, itemName: string): Observable<void> {
    if (itemId !== "") {
      return this.apiService.put(APP.endpoints.ontologies + `?id=${itemId}`, item);
    }
    return this.apiService.put(APP.endpoints.ontologies + `?name=${itemName}`, item);
  }

  public getOntologies(page, pageSize, filter, q): Observable<Ontologies> {
    const dataSources = filter.map(ft => `&filter=${ft}`).join('')
    return this.apiService.get(APP.endpoints.ontologies + `?page=${page}&pageSize=${pageSize}${dataSources}&q=${q}`);
  }

  public countOntologies(): Observable<number> {
    return this.apiService.get(APP.endpoints.ontologies + `?count=true`);
  }

  public getOntologyByIDorName(id: string, name: string, build: boolean): Observable<Ontology> {
    if (id !== "") {
      return this.apiService.get(APP.endpoints.ontologies + `?id=${id}` + `&build=${build}`);
    }
    // TODO: Update this once editor is more developed and we are getting specific nodes
    return this.apiService.get(APP.endpoints.ontologies + `?name=${name}` + `&build=${build}`);
    // return of({
    //   "id": "5f104d42a77b4dfb399d54e5",
    // "name": "root.metadata",
    // "type": "object",
    // "solvable": true,
    // "mandatory": true,
    // "displaySettings": {},
    // "possibleValues": [],
    // "keys": [],
    // })
    // .pipe(
    //   delay(500)
    // );
  }

  // TODO: Temporary Global Items, Remove later
  public getDescriptor(dID: string): Observable<Ontology> {
    return this.apiService.get(APP.endpoints.ontologies + `/${dID}`);
  }

  // TODO: Temporary Global Items, Remove later
  public getUPCItemByBarcode(barcode: string): Observable<any> {
    const proxyUrl = APP.corsAnywhereURL;
    try {
      return this.http.get(`${proxyUrl}${APP.upcItemDBURL}${barcode}`);
    } catch (e) {
      console.log(e);
      return null;
    }
  }

  public getBarcodeLookupItemByBarcode(barcode: string): Observable<any> {
    // return of (
    //     {
    //       "products": [
    //         {
    //           "barcode_number": "013562001224",
    //           "barcode_type": "UPC",
    //           "barcode_formats": "UPC 013562001224, EAN 0013562001224",
    //           "mpn": "0001356200122",
    //           "model": "",
    //           "asin": "",
    //           "product_name": "Annie's Homegrown Cheddar Squares Baked Snack Crackers - Case of 12 - 10 Oz.",
    //           "title": "",
    //           "category": "Food, Beverages & Tobacco > Food Items > Snack Foods > Fruit Snacks",
    //           "manufacturer": "General Mills",
    //           "brand": "Annie's",
    //           "label": "",
    //           "author": "",
    //           "publisher": "",
    //           "artist": "",
    //           "actor": "",
    //           "director": "",
    //           "studio": "",
    //           "genre": "",
    //           "audience_rating": "",
    //           "ingredients": "",
    //           "nutrition_facts": "",
    //           "color": "",
    //           "format": "",
    //           "package_quantity": "",
    //           "size": "",
    //           "length": "",
    //           "width": "",
    //           "height": "",
    //           "weight": "",
    //           "release_date": "",
    //           "description": "This is an original Annie's Homegrown Cheddar Squares, Organic, Shrng . , California . No Artificial Ingredients, Does not Contain Refined Sugar, 70%+ Organic.",
    //           "features": [],
    //           "images": [
    //             "https://images.barcodelookup.com/2686/26865905-1.jpg"
    //           ],
    //           "stores": [
    //             {
    //               "store_name": "Walmart",
    //               "store_price": "6.80",
    //               "product_url": "https://www.walmart.com/ip/Annie-s-Homegrown-Cheddar-Squares-Baked-Snack-Crackers-Case-of-12-10-oz/35055494",
    //               "currency_code": "USD",
    //               "currency_symbol": "$"
    //             },
    //             {
    //               "store_name": "UnbeatableSale.com",
    //               "store_price": "62.42",
    //               "product_url": "http://www.gourmet-foodshop.com/gnfi150.html",
    //               "currency_code": "USD",
    //               "currency_symbol": "$"
    //             },
    //             {
    //               "store_name": "Rakuten.com",
    //               "store_price": "64.22",
    //               "product_url": "https://www.rakuten.com/shop/unbeatablesale/product/GNFI150/?scid=af_feed",
    //               "currency_code": "USD",
    //               "currency_symbol": "$"
    //             }
    //           ],
    //           "reviews": []
    //         }
    //       ]
    //     }
    // ).pipe(delay(150))
    const proxyUrl = APP.corsAnywhereURL;
    return this.http.get(`${proxyUrl}${APP.barcodeLookupURL}?barcode=${barcode}&key=${environment.barcodeLookupKey}`);
  }

  public getVenues(): Observable<any[]> {
    return this.apiService.get(APP.endpoints.venues);
  }

}
