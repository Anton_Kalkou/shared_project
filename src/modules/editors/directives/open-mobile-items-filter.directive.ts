import { Directive, HostListener } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';

import { MobileItemsFilterComponent } from '../components/mobile-items-filer/mobile-items-filter.component';


@Directive({
  selector: '[appOpenMobileItemsFilter]'
})
export class OpenMobileItemsFilterDirective {

  constructor(
    private dialog: MatDialog
  ) {}

  @HostListener('click')
  openMobileItemsFilterDirective(): void {
    this.dialog.open(MobileItemsFilterComponent, {
      height: '100vh',
      width: '100vw'
    });
  }

}
