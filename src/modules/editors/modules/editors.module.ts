import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NgJsonEditorModule, JsonEditorComponent } from 'ang-jsoneditor';

import { NgxsModule } from '@ngxs/store';

import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { EditorsComponent } from '../components/editors/editors.component';
import { ItemsComponent } from '../components/items/items.component';
import { LabelModule } from 'src/modules/shared/components/label/label.module';
import { EditorsState } from '../ngxs/editors.state';
import { FilterSectionComponent } from '../components/filter-section/filter-section.component';
import { SearchModule } from 'src/modules/shared/components/search/search.module';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { ItemsTableComponent } from '../components/items-table/items-table.component';
import { StatusModule } from 'src/modules/shared/components/status/status.module';
import { AddButtonModule } from 'src/modules/shared/components/add-button/add-button.module';
import { EditorsHomeComponent } from '../components/home/editors-home.component';
import { NavItemModule } from 'src/modules/shared/components/nav-item/nav-item.module';
import { PopupModule } from 'src/modules/shared/components/popup/popup.module';
import { EditorsFormComponent } from '../components/form/editors-form.component';
import { ItemInfoComponent } from '../components/item-info/item-info.component';
import { ConfirmationModule } from 'src/modules/shared/components/confirmation/confirmation.module';
import { TagPopupComponent } from '../components/tag-popup/tag-popup.component';
import { ItemsFilterComponent } from '../components/items-filter/items-filter.component';
import { MobileItemComponent } from '../components/mobile-item/mobile-item.component';
import { OpenMobileItemsFilterDirective } from '../directives/open-mobile-items-filter.directive';
import { RatingsComponent } from '../components/ratings/ratings.component';
import { DirectivesModule } from 'src/modules/shared/directives/directives.module';
// TODO: Temporary Global Items, Remove later
import { TempItemsComponent } from '../components/temp-global-items/items/items.component';
import { TempEditorsFormComponent } from '../components/temp-global-items/form/editors-form.component';
import { PipesModule } from '../../shared/pipes/pipes.module';
import { AdditionalInformationComponent } from '../components/additional-information/additional-information.component';
import { GeneralInformationComponent } from '../components/general-information/general-information.component';
import { MobileItemsFilterComponent } from '../components/mobile-items-filer/mobile-items-filter.component';
import { ModuleHeaderModule } from 'src/modules/shared/components/module-header/module-header.module';
import { SquareButtonModule } from 'src/modules/shared/components/square-button/square-button.module';
import { GlobalItemMobileItemComponent } from '../components/temp-global-items/mobile-item/global-item-mobile-item.component';
import { TempAddItemEditorsFormComponent } from '../components/temp-global-items/add-item-form/add-item-form.component';
import { TempApproveItemsComponent } from '../components/temp-global-items/approve-items/approve-items.component';
import { GlobalItemOriginComponent } from '../components/temp-global-items/components/origin/origin.component';
import { GlobalItemPictureComponent } from '../components/temp-global-items/components/picture/picture.component';
import {GlobalItemCategoryComponent} from '../components/temp-global-items/components/category/category.component';
import {StringSelectorComponent} from '../components/temp-global-items/components/string-selector/string-selector.component';
import {AddOntologyEditorsFormComponent} from '../components/ontology/add-ontology-form/add-ontology-form.component';
import {OntologiesComponent} from '../components/ontology/ontologies/ontologies.component';
import {OntologyMobileItemComponent} from '../components/ontology/mobile-item/ontology-mobile-item.component';
import {OntologyEditorsFormComponent} from '../components/ontology/form/edit-ontology-form.component';
import { GlobalItemGenericInputComponent } from '../components/temp-global-items/components/generic-input/generic-input.component';
import { PropertyWrapperDirective } from '../components/temp-global-items/components/property/property.diretive';
import { PropertyWrapperComponent } from '../components/temp-global-items/components/property/property.component';
import { GenericTabComponent } from '../components/temp-global-items/components/tab/generic-tab.component';
import { MatChipsModule } from '@angular/material/chips';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { CommonModule } from '@angular/common';
import { GlobalItemRadioComponent } from '../components/temp-global-items/components/generic-radio/radio.component';
import { GlobalItemFlexInputComponent } from '../components/temp-global-items/components/flex-input/flex-input.component';
import { GlobalItemFieldGroupComponent } from '../components/temp-global-items/components/field-group/field-group.component';
import { TableSelectComponent } from '../components/temp-global-items/components/table-select/table-select.component';
import {GlobalItemNutritionalValueComponent} from '../components/temp-global-items/components/nutritional-value/nutritional-value.component';
@NgModule({
  imports : [
    CommonModule,
    SharedModule,
    RouterModule,
    LabelModule,
    NgxsModule.forFeature([EditorsState]),
    NavItemModule,
    PopupModule,
    SearchModule,
    MaterialModule,
    StatusModule,
    AddButtonModule,
    ConfirmationModule,
    DirectivesModule,
    PipesModule,
    ModuleHeaderModule,
    SquareButtonModule,
    NgJsonEditorModule,
    MatChipsModule,
    MatInputModule,
    MatFormFieldModule
  ],
  declarations: [
    EditorsComponent,
    ItemsComponent,
    // TODO: Temporary Global Items, Remove later
    TempItemsComponent,
    TempEditorsFormComponent,
    TempAddItemEditorsFormComponent,
    TempApproveItemsComponent,
    GlobalItemMobileItemComponent,
    FilterSectionComponent,
    ItemsTableComponent,
    EditorsHomeComponent,
    EditorsFormComponent,
    GeneralInformationComponent,
    ItemInfoComponent,
    TagPopupComponent,
    ItemsFilterComponent,
    MobileItemComponent,
    MobileItemsFilterComponent,
    OpenMobileItemsFilterDirective,
    AdditionalInformationComponent,
    RatingsComponent,
    GlobalItemOriginComponent,
    GlobalItemPictureComponent,
    GlobalItemCategoryComponent,
    AddOntologyEditorsFormComponent,
    OntologiesComponent,
    OntologyMobileItemComponent,
    OntologyEditorsFormComponent,
    GlobalItemGenericInputComponent,
    StringSelectorComponent,
    PropertyWrapperDirective,
    PropertyWrapperComponent,
    AddOntologyEditorsFormComponent,
    GenericTabComponent,
    GlobalItemRadioComponent,
    GlobalItemFlexInputComponent,
    GlobalItemFieldGroupComponent,
    TableSelectComponent,
    GlobalItemNutritionalValueComponent
  ],
  exports: [
    EditorsComponent,
    ItemsComponent,
    // TODO: Temporary Global Items, Remove later
    TempItemsComponent,
    TempEditorsFormComponent,
    TempAddItemEditorsFormComponent,
    TempApproveItemsComponent,
    GlobalItemMobileItemComponent,
    GlobalItemOriginComponent,
    GlobalItemCategoryComponent,
    AddOntologyEditorsFormComponent,
    GlobalItemNutritionalValueComponent,
    OntologiesComponent,
    OntologyMobileItemComponent,
    OntologyEditorsFormComponent,
    JsonEditorComponent
  ]
})
export class EditorsModule {
}
