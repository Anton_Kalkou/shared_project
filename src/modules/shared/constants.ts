import { environment } from '../../environments/environment';

export const APP = {
  apiUrl: environment.apiUrl,
  payApiURL: environment.payApiURL,
  corsAnywhereURL: 'https://cors-anywhere.herokuapp.com/',
  upcItemDBURL: 'https://api.upcitemdb.com/prod/trial/lookup?upc=',
  barcodeLookupURL: 'https://api.barcodelookup.com/v2/products',
  microServices: {
    search: 'search',
    payment: 'payment'
  },
  filters: {
    staffShortName: ['Mark C.', 'Henry F.', 'Cythnia H.'],
    accountNumber: ['***5693', '***1267', '***9869'],
    device: ['PT5310I', 'PT5890', 'PT2345'],
    paymentResult: ['Success', 'Fail'],
    card: ['transactions.american-express-platinum', 'transactions.master-black', 'transactions.visa'],
    cardHolderName: ['Fabio Messi', 'John Doe'],
    paymentStatus: ['transactions.authorization', 'transactions.incremental-authorization', 'transactions.captured'],
    staffFullName: ['Fabio Vale', 'Gustavo Gamino', 'Leo Messi'],
    roles: ['Waiter', 'Role'],
    accessRightsStatus: ['filters.no-access', 'filters.read', 'filters.write'],
    dates: ['filters.date.now', 'filters.date.last-day', 'filters.date.last-seven-days', 'filters.date.choose-dates'],
    paymentTransactionsChooseDates: ['filters.date.last-day', 'filters.date.last-seven-days', 'filters.date.choose-dates'],
    chooseDatesFormat: {
      parse: {
        dateInput: 'LL',
      },
      display: {
        dateInput: 'MMM DD',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM YYYY',
      },
    },
    orchestrator: {
      orchestratorB2B: {
        tags: [
          'Early',
          'Mid',
          'Late',
          'USA'
        ],
        products: [
          'Basics',
          'Consumers',
          'Delivery',
          'Reservation'
        ],
        features: [
          'Basics',
          'Consumers',
          'Delivery',
          'Reservation'
        ]
      }
    }
  },
  endpoints: {
    login: 'authentication/user/login',
    reauthenticate: 'authentication/user/login/refresh',
    userProfileData: 'user-profile/profile/me',
    logout: 'logout',
    updates: 'updates',
    users: 'users',
    filters: 'filters',
    editorsItems: 'editors-items',
    catalogPendingItems: 'catalog/item/pending',
    globalItems: 'global-catalog/global',
    ontologies: 'global-catalog/ontology',
    approval: 'global-catalog/approve-items',
    quantities: 'quantities',
    categories: 'categories',
    attributes: 'attributes',
    merchant: 'merchant',
    merchants: 'merchant-profile/admin/business-entity',
    merchantsFilters: 'merchants-filters',
    verificationFilters: 'verification-filters',
    provisioningFilters: 'provisioning-filters',
    transactionsFilters: 'transactions-filters',
    profitabilityFilters: 'profitability-filters',
    devicesFilters: 'devices-filters',
    globalItemsFilters: 'global-items-filter',
    globalItemsApprovalFilters: 'global-items-approval-filter',
    ontologiesFilters: 'ontologies-filter',
    bankingInformation: 'banking-info',
    payment: 'payment',
    individual: 'principal/individual',
    salesInformation: 'sales-information',
    admin: 'admin',
    device: 'device',
    deviceProfile: 'device-profile',
    rockspoonFee: 'rockspoon-fee',
    verification: 'verification',
    venues: 'merchant-profile/venue',
    customerSupport: 'customer-support/current',
    devicesStatus: 'devices',
    customerSupportRequestsFilters: 'customer-support-requests-filters',
    paymentTransactionsFilters: 'support-reuqest-payment-transactions-filters',
    paymentBatchesFilters: 'support-reuqest-payment-batches-filters',
    paymentBatchFilters: 'support-reuqest-payment-batch-filters',
    accessRightsFilters: 'support-request-access-rights-filters',
    accessRightsDetailsFilters: 'support-request-access-rights-details-filters',
    tenantsCategoriesFilters: 'tenants-categories-filters',
    tenantsFilters: 'tenants-filters',
    paymentTransactions: 'payment/admin',
    paymentBatches: 'payment/settlement',
    accessRightsDetails: 'user-profile/admin/profile/{profileId}/access-rights',
    adminRole: 'user-profile/admin/role',
    adminProfile: 'user-profile/admin/profile',
    tags: 'tenant-management/tags'
  },
  requestHeaders: {
    accessToken: 'access_token',
    key: 'key'
  },
  cache: {
    userData: 'userData',
    authentication: 'authenticationData',
    prevRoute: 'prevRoute'
  },
  pages: {
    authentication: 'authentication',
    home: 'home',
    dashboard: 'dashboard',
    staff: 'staff',
    sales: 'sales',
    merchants: 'merchants',
    editors: 'editors',
    support: 'support',
    list: 'list',
    items: 'items',
    profile: 'profile',
    addMerchant: 'add-merchant',
    customerSupport: 'customer-support',
    orchestratorB2B: 'orchestrator-b2b',
    tenantsCategories: 'tenants-categories'
  },
  customerSupportPages: {
    rootRoute: 'requests',
    homeRoute: 'customer-support/requests/{requestId}/navigation',
    routesBackToHome: [
      'payment',
      'systems-health',
      'access-rights'
    ],
    routesWithClose: [
      'batches',
      'details'
    ],
    routesWithNothing: [
      'navigation'
    ]
  },
  merchantInformationTypes: {
    legalEntity: 'legal-entity',
    contact: 'contact',
    leadership: 'leadership',
    individualOwners: 'individual-owners',
    companyOwners: 'company-owners',
    sales: 'sales'
  },
  notificationTitles: {
    addMerchantTermOfUse: 'add-merchant.term-of-use',
    errorMessageTermOfUse: 'error-message.term-of-use',
    valueCopied: 'notification-titles.value-copied'
  },
  menuItems: [
    {
      translationKey: 'menu.my-dashboard',
      iconClass: 'icon-dashboard',
      link: '/dashboard',
      subMenu: [
        { translationKey: 'menu.need-my-attention', link: '/need-attention' },
        { translationKey: 'menu.open-tickets', link: '/open-tickets' }
      ]
    },
    {
      translationKey: 'menu.sales-portal',
      iconClass: 'icon-sales',
      link: '/sales',
      subMenu: [
        { translationKey: 'menu.need-my-attention', link: '/need-attention' },
        { translationKey: 'menu.open-tickets', link: '/open-tickets' }
      ]
    },
    {
      translationKey: 'menu.merchants',
      iconClass: 'icon-merchants',
      link: '/merchants',
      subMenu: [
        { translationKey: 'menu.merchants-list', link: '/list' },
        { translationKey: 'menu.transactions', link: '/transactions' },
        { translationKey: 'menu.rockspoon-fees', link: '/fees' },
        { translationKey: 'menu.profitability', link: '/profitability' }
      ]
    },
    {
      translationKey: 'menu.support-tools',
      iconClass: 'icon-support',
      link: '/support',
      subMenu: [
        { translationKey: 'menu.need-my-attention', link: '/need-attention' },
        { translationKey: 'menu.open-tickets', link: '/open-tickets' }
      ]
    },
    {
      translationKey: 'menu.staff-management',
      iconClass: 'icon-staff',
      link: '/staff',
      subMenu: [
        { translationKey: 'menu.staff', link: '/list' },
        { translationKey: 'menu.roles', link: '/roles' },
        { translationKey: 'menu.settings', link: '/settings' }
      ]
    },
    {
      translationKey: 'menu.editors',
      iconClass: 'icon-global-items',
      link: '/editors',
      subMenu: [
        { translationKey: 'menu.items', link: '/global-items' },
        { translationKey: 'menu.ontology', link: '/ontology' },
        { translationKey: 'menu.approve-items', link: '/approve-items' }
      ]
    },
    {
      iconClass: 'icon-editors',
      link: '/editors'
    },
    {
      translationKey: 'menu.customer-support',
      iconClass: 'icon-support',
      link: '/customer-support',
      hasNotification: false
    },
    {
      translationKey: 'menu.orchestrator-b2b',
      iconClass: 'icon-orchestrator-b2b',
      link: '/orchestrator-b2b',
      subMenu: [
        { translationKey: 'menu.products', link: '/products' },
        { translationKey: 'menu.features', link: '/features' },
        { translationKey: 'menu.permissions', link: '/permissions' },
        { translationKey: 'menu.tenants-categories', link: '/tenants-categories' },
        { translationKey: 'menu.tenants', link: '/tenants' },
        { translationKey: 'menu.default-roles', link: '/default-roles' },
      ]
    }
  ],
  customerSupportTicketNavItems: [
    {
      translationKey: 'menu.payment',
      iconClass: 'icon-payment',
      link: '/customer-support/requests/:id/payment',
      hasNotification: false
    },
    {
      translationKey: 'menu.systems-health',
      iconClass: 'icon-systems-health',
      link: '/customer-support/requests/:id/systems-health',
      hasNotification: false
    },
    {
      translationKey: 'menu.access-rights',
      iconClass: 'icon-access-rights',
      link: '/customer-support/requests/:id/access-rights',
      hasNotification: false
    },
  ],
  homeNavItems: [
    {
      translationKey: 'menu.my-dashboard',
      iconClass: 'icon-dashboard',
      link: '/dashboard',
      hasNotification: false,
      subMenu: [
        { translationKey: 'menu.need-my-attention', link: '/need-attention' },
        { translationKey: 'menu.open-tickets', link: '/open-tickets' }
      ]
    },
    {
      translationKey: 'menu.sales-portal',
      iconClass: 'icon-sales',
      link: '/sales',
      hasNotification: true,
      subMenu: [
        { translationKey: 'menu.need-my-attention', link: '/need-attention' },
        { translationKey: 'menu.open-tickets', link: '/open-tickets' }
      ]
    },
    {
      translationKey: 'menu.merchants',
      iconClass: 'icon-merchants',
      link: '/merchants',
      hasNotification: false,
      subMenu: [
        { translationKey: 'menu.merchants-list', link: '/list' },
        { translationKey: 'menu.transactions', link: '/transactions' },
        { translationKey: 'menu.rockspoon-fees', link: '/fees' },
        { translationKey: 'menu.profitability', link: '/profitability' }
      ]
    },
    {
      translationKey: 'menu.editors',
      iconClass: 'icon-editors',
      link: '/editors',
      hasNotification: false
    },
    {
      translationKey: 'menu.staff-management',
      iconClass: 'icon-staff',
      link: '/staff',
      hasNotification: false,
      subMenu: [
        { translationKey: 'menu.staff', link: '/list' },
        { translationKey: 'menu.roles', link: '/roles' },
        { translationKey: 'menu.settings', link: '/settings' }
      ]
    },
    {
      translationKey: 'menu.customer-support',
      iconClass: 'icon-support',
      link: '/customer-support',
      hasNotification: false
    },
    {
      translationKey: 'menu.orchestrator-b2b',
      iconClass: 'icon-orchestrator-b2b',
      link: '/orchestrator-b2b'
    },
  ],
  responseStatusCodes: {
    unauthorized: 401,
    invalidRefreshToken: 400,
    invalidEnvironment: 4701014
  },
  languages: {
    en: 'en',
    ru: 'ru'
  },
  breakpoints: {
    hugeDesktop: {
      title: 'isHugeDesktop',
      value: 1200
    },
    desktop: {
      title: 'isDesktop',
      value: 992
    },
    laptop: {
      title: 'isLaptop',
      value: 768
    },
    tablet: {
      title: 'isTablet',
      value: 576
    },
    mobile: {
      title: 'isMobile'
    }
  },
  roles: [
    'Admin',
    'Editor',
    'Viewer'
  ],
  dialogs: {
    addStaff: 'add-staff',
    cropper: 'cropper',
    takePhoto: 'take-photo',
    tags: 'tags',
    editorsForm: 'editors-form',
    addItemEditorsForm: `add-item-form`,
    approveItemEditorsForm: `approve-item-form`,
    addOntologyEditorsForm: `add-ontology-form`,
    addVerification: 'add-verification',
    requestRefound: 'request-refound',
    confirmEditFee: 'confirm-edit-fee'
  },
  cropperAspectRatios: {
    square: '1:1',
    rectangle: '4:3',
    rectangleWide: '16:9',
    circle: 'circle',
    free: 'free'
  },
  colors: {
    red: '#d0021b',
    red1: '#e02020',
    yellow: '#f5a623',
    green: '#7ed321',
    green1: '#92c16f',
    orange: '#f7b500'
  },
  routesWithSidebar: [
    'merchants/',
    // '/merchants/list',
    // '/merchants/transactions',
    // '/merchants/fees',
    // '/merchants/profitability',
    '/dashboard',
    '/sales',
    '/staff/list',
    '/support',
    '/editors/global-items',
    '/editors/ontology',
    '/editors/approve-items',
  ],
  routesBackToHome: [
    '/merchants/list',
    '/merchants/transactions',
    '/merchants/fees',
    '/dashboard',
    '/sales',
    '/staff/list',
    '/support',
    '/editors/global-items',
    '/editors/ontology',
    '/editors/home',
    '/editors/approve-items',
    '/customer-support/requests'
  ],
  routesWithNothing: [
    '/customer-support/requests/'
  ],
  businessCategories: [
    'Fine dining',
    'Casual dining',
    'Fast casual',
    'Quick Service',
    'Full Service Restaurant',
    'Bar and lounges',
    'Cafe and Bakery'
  ],
  measures: [
    'oz',
    'fl oz',
    'gallon',
    'gill',
    'quart',
    'firkin',
    'ml'
  ],
  categories: {
    food: 'Food',
    alcoholicBeverage: 'Alcoholic Beverage'
  },
  subcategories: {
    whisky: 'Whisky',
    wine: 'Wine',
    tequila: 'Tequila',
    vodka: 'Vodka',
    gin: 'Gin',
    sake: 'Sake',
    berries: 'Berries',
    beer: 'Beer'
  },
  soldBy: [
    'Kg',
    'Mg'
  ],
  main_categories:[
    'Food',
    'Alcoholic Beverage'
  ],
  alcohol_subcategories:[
    'Whisky',
    'Wine',
    'Tequila',
    'Gin',
    'Sake',
    'Beer',
    'Vodka'
  ],
  beerTypes: [
    'Ale',
    'Alo'
  ],
  beerSubtypes: [
    'Stout',
    'quis nostrud',
    'exercitation ullamco'
  ],
  stoutSubtypes: [
    'Dry / Irish Stout',
    'lorem ipsum',
    'dolor sit amet'
  ],
  ginTypes: [
    'London Dry',
    'consectetur adipiscing',
    'elit, sed do',
    'eiusmod tempor'
  ],
  sakeTypes: [
    'Ginjo',
    'incididunt ut',
    'labore et'
  ],
  tequilaTypes: [
    'Silver',
    'dolore magna',
    'aliqua. Ut'
  ],
  vodkaTypes: [
    'Flavored',
    'enim ad',
    'minim veniam'
  ],
  whiskyTypes: [
    'American',
    'laboris nisi',
    'ut aliquip'
  ],
  whiskySubtypes: [
    'Tenessee',
    'ex ea commodo',
    'consequat. Duis'
  ],
  flavorProfiles: [
    'Full-bodied',
    'Sweet',
    'aute irure',
    'dolor in'
  ],
  wineTypes: [
    'Red',
    'reprehenderit in',
    'voluptate velit'
  ],
  wineSubtypes: [
    'Tenessee',
    'ex ea commodo',
    'consequat. Duis'
  ],
  grapeVarieties: [
    'Pinot Noir',
    'Merlot',
    'Malbec'
  ],
  acidityScales: [
    1,
    2,
    3
  ],
  bodyScales: [
    1,
    2,
    3
  ],
  tanninScales: [
    1,
    2,
    3
  ],
  EBC: [
    'EBC',
    'EBC2',
    'EBC3'
  ],
  IBU: [
    'IBU',
    'IBU2',
    'IBU3'
  ],
  servingStyles: [
    'Chilled',
    'Chilled2',
    'Chilled3'
  ],
  letWineBreathIn: [
    'Decanter',
    'Decanter2',
    'Decanter3'
  ],
  accountTypes: [
    'first',
    'second',
    'third'
  ],
  verificationTypes: [
    'Account',
    'KYC',
    'Account, KYC, OFAC',
    'Credit'
  ],
  verificationReasons: [
    'Auto Revalidation',
    'Validation'
  ],
  transactionsOptions: [
    'All Transactions',
    'Payouts',
    'Refunded',
    'Succeeded',
    'Uncaptured'
  ],
  accessRightsDetailsOptions: [
    'All'
  ],
  subjects: {
    editLegalEntityInformation: 'edit-legal-entity-information',
    saveLegalEntityInformation: 'save-legal-entity-information',
    editContactInformation: 'edit-contact-information',
    saveContactInformation: 'save-contact-information',
    editLeadershipInformation: 'edit-leadership-information',
    saveLeadershipInformation: 'save-leadership-information',
    editIndividualOwnersInformation: 'edit-individual-owners-information',
    saveIndividualOwnersInformation: 'save-individual-owners-information',
    editCompanyOwnersInformation: 'edit-company-owners-information',
    saveCompanyOwnersInformation: 'save-company-owners-information',
    editSalesInformation: 'edit-sales-information',
    saveSalesInformation: 'save-sales-information',
  },
  wifiSignals: {
    minRSSI: -100,
    maxRSSI: -55
  },
  cellularSignals: {
    minRSSI: -100,
    maxRSSI: -55
  },
  batteryLevel: {
    good: 50,
    bad: 20
  },
  weeksDays: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
  paymentsMethods: {
    withoutCard: ['cash', 'external', 'loyalty'],
    withCard: ['cardpresent', 'cardnotpresent']
  },
  insideFiltersClasses: [
    'mat-focus-indicator',
    'mat-calendar-body-cell-content',
    'mat-calendar-content',
    'mat-calendar-body-label',
    'mat-calendar-table-header-divider',
    'ng-star-inserted',
    'mat-calendar',
    'mat-calendar-header',
    'mat-button-wrapper',
    'mat-calendar-spacer',
    'cdk-overlay-backdrop',
    'mat-calendar-table'
  ],
  parentDatesRangeLabels: ['filters.date.choose-dates']
};
