import { NgModule } from '@angular/core';

import { NgJsonEditorModule } from 'ang-jsoneditor' 
import {OntologyEditorsFormComponent} from 'src/modules/editors/components/ontology/form/edit-ontology-form.component';

@NgModule({
  declarations: [
    OntologyEditorsFormComponent
  ],
  imports: [
    NgJsonEditorModule
  ],
  providers: [],
  bootstrap: [OntologyEditorsFormComponent]
})
export class AppModule { }