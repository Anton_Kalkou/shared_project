import { FormGroup } from '@angular/forms';

export const requiredOneField = (...controlNames) => (form: FormGroup) => {
  if (controlNames.every(controlName => !form.controls[controlName].value)) {
    form.controls[controlNames[0]].setErrors({
      match: 'Should be filled'
    });
  }

  return;
}
