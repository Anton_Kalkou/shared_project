export const ENDPOINTS = {
  SIGN_IN: 'authentication/user/login',
  BUSINESS_ENTITY: 'merchant-profile/business-entity',
  REFRESH_TOKEN: 'authentication/user/login/refresh',
  USER_PROFILE_DATA: 'user-profile/profile/me',
};
