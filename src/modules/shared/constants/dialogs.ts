export const DIALOGS = {
  ids: {
    provisioning: 'provisioning',
  },
  widths: {
    provisioning: '80%'
  },
  height: {
    provisioning: '80vh'
  }
};
