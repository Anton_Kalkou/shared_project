export interface Loyalty {
  date: string;
  id: string;
  transactionAmount: number;
  orderAmount: number;
  totalDiscount: number;
  merchantDiscount: number;
  rockSpoonDiscount: number;  
}

export interface LoyaltyStatus {
  value: boolean;
  date: string;
  signedBy: string;
}
