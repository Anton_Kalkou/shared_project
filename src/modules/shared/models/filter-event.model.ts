export interface FilterEvent {
  filterValue: string;
  filterIsActive: boolean;
}
