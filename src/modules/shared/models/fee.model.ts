export class Fee {
  title: string;
  items: FeeItem[];
}

export interface FeeItem {
  selected: boolean;
  type: string;
  percent: number;
  price: number;
}
