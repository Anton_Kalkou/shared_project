export interface Environment {
  production: boolean;
  barcodeLookupKey: string;
  apiUrl: string,
  payApiURL: string,
  tenantKey: string
}
