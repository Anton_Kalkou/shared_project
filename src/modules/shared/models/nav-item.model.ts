export interface NavItem {
  translationKey: string;
  iconClass?: string;
  link: string;
  hasNotification?: boolean;
  quantity?: any;
  disabled?: boolean;
}
