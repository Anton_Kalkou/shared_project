export interface BankingInformation {
  merchantId: string;
  id: string;
  accountNumber: string;
  routingNumber: string;
  accountType: string;
  status: string;
}
