export interface Update {
  id: string;
  date: string;
  content: string;
  actor?: string;
}
