export interface DateResponse {
  createdAt?: string;
  updatedAt?: string;
  deletedAt?: string;
  publishedAt?: string;
}
