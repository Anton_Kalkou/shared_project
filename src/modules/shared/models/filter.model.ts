export interface Filter {
  title: string;
  group: FilterGroup[];
}

export interface FilterGroup {
  title: string;
  filters: string[];
  ontologyPath?: string;
  paramName?: string;
  searchPlaceholder?: string;
  more?: boolean;
  from?: number;
  to?: number;
  parent?: ParentFilter[];
  enabled?: boolean;
}

export interface FilterEvent {
  filter: FilterGroup;
  value: string;
  event: boolean;
}

export interface ParentFilter {
  parentFilterTitle: string;
  parentParamName: string;
  parentFilterOption: string;
  parentFilterDatesRange?: any;
}

export interface ChosenDatesRange {
  begin: string;
  end: string;
}

export interface FilterData {
  title: string;
  key: string;
}
