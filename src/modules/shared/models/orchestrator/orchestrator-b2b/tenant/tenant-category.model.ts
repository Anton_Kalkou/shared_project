export interface TenantCategory {
  id: string;
  title: string;
  tags: string;
}
