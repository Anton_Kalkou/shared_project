import { Observable } from 'rxjs';

export interface Tenant {
  id?: string;
  code?: string;
  accessKeys?: string[];
  tags?: string;
  description?: string;
  name: string;
  category: string;
  products: string[];
}

export interface TenantResponse {
  _id: string;
  applications: Application[];
  code: string;
  description: string;
  name: string;
  categoryId?: string;
}

export interface TenantProductsDialogOptions {
  width: string;
  data: TenantProductsDialogData;
}

export interface TenantProductsDialogData {
  selectedProductsNames$: Observable<ProductName[]>;
  productNames$: Observable<ProductName[]>;
}

export interface ProductName {
  id: string;
  name: string;
}

export interface Application {
  id: string;
  product: string;
  description: string;
}
