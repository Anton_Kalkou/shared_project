import { DateResponse } from '../../../api.model';
import { Version } from '../orchestrator-b2b.model';
import { Permission } from '../permissions/permissions.model';

export interface DeafultRoleResponse extends DateResponse {
  id: string;
  code?: string;
  name: string;
  versions: Version[];
  permissions: Permission[];
  packages: PackageResponse[];
}

export interface DefaultRole extends DeafultRoleResponse {
  description: string;
}

export interface PackageResponse {
  code: string;
  name: string;
  tags: TagResponse[];
}

export interface TagResponse {
  code: string;
  name: string;
  tags: string[];
}
