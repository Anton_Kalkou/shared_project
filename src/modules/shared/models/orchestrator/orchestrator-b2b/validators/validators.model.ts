import { Version } from '../orchestrator-b2b.model';

export interface VersionExistsConstraint {
  versions?: Version[];
}
