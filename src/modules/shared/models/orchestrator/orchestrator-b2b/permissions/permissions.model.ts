import { Feature } from '../features/features.model';
import { ApiResponse, FeatureResponse, PermissionResponse } from '../orchestrator-b2b.model';

export interface Permission extends PermissionResponse {
  relatedFeatures?: Feature[];
}

export interface RelatedFeature extends FeatureResponse {}

export interface Api extends ApiResponse {}
