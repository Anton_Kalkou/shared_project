import { FeatureResponse, ProductResponse } from '../orchestrator-b2b.model';

export interface Feature extends FeatureResponse {}

export interface RelatedProduct extends ProductResponse {}
