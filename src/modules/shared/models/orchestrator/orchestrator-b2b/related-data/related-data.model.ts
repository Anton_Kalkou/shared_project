import { Version } from '../orchestrator-b2b.model';

export interface RelatedDataConstraint {
  id?: string;
  versions?: Version[];
}
