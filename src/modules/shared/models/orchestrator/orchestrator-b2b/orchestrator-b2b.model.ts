import { Observable } from 'rxjs';

import { PermissionType } from 'src/modules/orchestrator/components/orchestrator-b2b/enum/orchestrator-b2b.enum';

import { DateResponse } from '../../api.model';
import { Feature } from './features/features.model';
import { Permission } from './permissions/permissions.model';

export interface ProductResponse extends DateResponse {
  id?: string;
  code?: string;
  description?: string;
  name: string;
  features?: FeatureResponse[];
  versions?: VersionResponse[];
}

export interface FeatureResponse extends DateResponse {
  id?: string;
  code?: string;
  description?: string;
  name: string;
  versions?: VersionResponse[];
}

export interface PermissionResponse extends DateResponse {
  name: string;
  id?: string;
  code?: string;
  description?: string;
  permissionType?: PermissionType;
  versions?: VersionResponse[];
}

export interface ApiResponse extends DateResponse {
  id?: string;
  service?: string;
  method?: string;
  path?: string;
  version?: string;
  versions?: VersionResponse[];
}

export interface VersionResponse extends DateResponse {
  id: string;
  code: string;
  name: string;
  description: string;
  isDisabled: boolean;
  version: string;
  features?: FeatureResponse[] | string[];
  apis?: ApiResponse[] | string[];
  permissions?: PermissionResponse[] | string[];
}

export interface Version extends VersionResponse {};

export interface VersionData {
  version: string;
  name: string;
  description: string;
  isDisabled: boolean;
  features?: FeatureResponse[] | string[];
  apis?: ApiResponse[] | string[];
  permissions?: PermissionResponse[] | string[];
}

export interface CreatedData {
  id: string;
}

export interface VersionDialogData {
  selectedItem$: Observable<any>;
  isDuplicate?: boolean;
}

export interface VersionDialogOptions {
  width: string;
  data: VersionDialogData;
}

export interface InitialVersion {
  isDisabled: boolean;
  version: string;
  description: string;
  features?: Feature[];
  permissions?: Permission[];
}
