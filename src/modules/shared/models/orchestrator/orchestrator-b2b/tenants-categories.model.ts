export interface TenantCategoryResponse {
  code: string;
  description: string;
  id: string;
  name: string;
  acl: Acl[];
  pos_versions: PosVersion[];
  tags: Tag[];
  tenants: string[];
  angular_versions: any[];
  vmwareId: number;
}

export interface TenantCategory {
  id?: string;
  vmwareId?: number;
  name: string;
  description: string;
  tags?: Tag[];
  resources?: PosVersion[];
  acls?: Acl[];
}

export interface Acl {
  id: string;
  feature: string;
}

export interface PosVersion {
  isLatest: false;
  name: string;
  version: string;
  vmwareId: number;
}

export interface Tag {
  id?: string;
  name: string;
  vmwareID?: string;
}
export interface EditTextData {
  isUpdated: boolean;
  tenantCategory: TenantCategory;
}

export interface EditTagsData {
  isUpdated: boolean;
  tags: EditTags;
}

export interface EditTags {
  updatedTags: Tag[];
  deletedTags: Tag[];
  addedTags: Tag[];
}
