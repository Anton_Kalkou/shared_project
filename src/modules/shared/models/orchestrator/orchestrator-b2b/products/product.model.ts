import { ProductResponse } from '../orchestrator-b2b.model';

export interface Product extends ProductResponse {}
