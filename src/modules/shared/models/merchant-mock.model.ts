import { Changelog } from './changelog.model';

export enum MerchantCreateResult {
  init,
  success,
  failure,
  pending
}

export interface Merchant {
  id: string;
  name: string;
  location: string;
  status: string;
  plan: string;
  businessCategory: string;
  expiration: string;
  digits: number;
  verification: string;
  processing: string;
  contactName: string;
  contactEmail: string;
  contactNumber: string;
  cardPresent: number;
}

export interface MerchantExtended extends Merchant {
  service: string;
  changelog: Changelog[];
  restaurantInformation: Restaurant;
  legalAddress: LegalAddress;
  leadershipContact: Contact;
  billingContact: Contact;
  businessContact: Contact;
  chargebackContact: Contact;
  leadershipInformation: Contact;
  individualOwnersInformation: Owner[];
  companyOwnersInformation: Owner[];
  salesInformation: Sales;
}

export interface Restaurant {
  DBAName: string;
  legalEntityName: string;
  restaurantURL: string;
  SEIN: string;
  TIN: string;
  category: string;
}

export interface LegalAddress {
  address: string;
  postCode: string;
  country: string;
  state: string;
  city: string;
  differentAddress: boolean;
}

export interface Contact {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  address: string;
  postCode: string;
  country: string;
  state: string;
  city: string;
}

export interface Owner extends Contact {
  nationality: string;
  secondaryNationality: string;
  residingCountry: string;
  document: string;
  number: string;
  birthDate: string;
  share: string;
  legalAddress: string;
  legalPostCode: string;
  legalCountry: string;
  legalState: string;
  legalCity: string;
}

export interface Sales {
  monthlySales: string;
  valuePerCard: string;
  percentageCardPresent: string;
  percentageMailOrder: string;
  percentageInternetTransactions: string;
  highestCardTransaction: string;
  restaurantTables: number;
  serversAmount: number;
  kitchenAmount: number;
  newRestaurant: boolean;
}
