import { Option } from './dropdown.model';

export interface Column {
  title?: string;
  translationKey?: string;
  valueType?: string;
  textType?: string;
  actionType?: string;
  icon?: string;
  path?: string;
  parentInterractionAction?: boolean;
  actionDataKey?: string;
  headerType?: string;
  iconClass?: string;
  isSortableDisabled?: boolean;
  isDelete?: boolean;
  dropdownOptions?: Option[];
  dropdownSelectedOption?: string;
  popupTitle?: string;
  popupCaption?: string;
  popupAcceptTitle?: string;
  popupDeclineTitle?: string;
  arrayItemTitle?: string;
  radioButtons?: any[];
  descriptiveColumnKey?: string;
}

export interface ActionEvent {
  item: any;
  column: Column;
}
