import { Tag } from '../tag.model';
import { EditorsTagPopupTypes } from '../../enums';

export interface TagsPopupData {
  caption?: string;
  title?: string;
  tags?: Tag[];
  selectedTags?: Tag[];
  multiple?: boolean;
  popupType?: EditorsTagPopupTypes;
}
