export interface CropperPopupData {
  caption: string;
  imgUrl: string | ArrayBuffer;
}
