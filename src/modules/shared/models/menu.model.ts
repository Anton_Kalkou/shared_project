export interface Menu {
  translationKey?: string;
  iconClass: string;
  link: string;
  subMenu?: SubMenu[];
}

export interface SubMenu {
  translationKey: string;
  link: string;
}
