export interface History {
  date: string;
  company: string;
  type: string;
  reason: string;
  status: string;
}
