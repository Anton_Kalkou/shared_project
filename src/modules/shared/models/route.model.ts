export interface Route {
  route: string;
  prevPage: string;
  exceptions?: string[];
}
