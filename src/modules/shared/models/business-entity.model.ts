export interface BusinessEntity {
  id: string;
  bankingInformation: BankingInformation;
  businessInformation: BusinessInformation;
  createdAt: string;
  hubspot: Hubspot;
  leaderContact: Contact;
  name: string;
  paymentInAdvance: boolean;
  phoneNumber: string;
  principals: Principals;
  salesInformation: SalesInformation;
  servicePackage: ServicePackage;
  status: string;
  url: string;
}

export interface BusinessInformation {
  businessAddress: Address;
  legalAddress: Address;
  category: string;
  legalEntityName: string;
  sein: string;
  tin: string;
}

export interface Hubspot {
  dealFields: any;
  dealStage: string;
}

export interface Contact {
  email: string;
  firstName: string;
  jobPosition: string;
  lastName: string;
  legalAddress: Address;
  password: string;
  phone: string;
}

export interface Principals {
  individual: IndividualOwner[];
  companies: CompanyOwner[];
}

export interface ServicePackage {
  baseFees: any;
  cost: string;
  hasMenuRSGo: boolean;
  includesAnalytics: boolean;
  includesDeliveryAndTakeout: boolean;
  includesHumanResources: boolean;
  includesInventoryManagement: boolean;
  includesPOSSolution: boolean;
  includesPayments: boolean;
  includesRSGoOrders: boolean;
  includesReservationsWaitlist: boolean;
  includesSalesReport: boolean;
  isVisibleRSGo: boolean;
  name: string;
  products: any;
}

export interface Address {
  address1: string;
  address2: string;
  city: string;
  country: string;
  state: string;
  zipcode: string;
}

export interface BankingInformation {
  accountName: string;
  accountNumber: string;
  accountType: string;
  routingNumber: string;
  bankName: string;
}

export interface CompanyOwner {
  businessName: string;
  email: string;
  firstName: string;
  lastName: string;
  hubSpotContactID: string;
  phone: string;
  share: number;
  tin: string;
  address: Address;
}

export interface IndividualOwner {
  dateOfBirth: string;
  documentDetails: DocumentDetails;
  email: string;
  firstName: string;
  lastName: string;
  legalAddress: Address;
  nationality: string;
  phoneNumber: string;
  residingCountry: string;
  secondaryNationality: string;
  share: number;
  socialSecurityNumber: string;
  socialSecurityType: string;
}

export interface DocumentDetails {
  documentIssuer: string;
  documentType: string;
  expireDate: string;
  idNumber: string;
  issueDate: string;
  issuingState: string;
}

export interface SalesInformation {
  monthlyTotalCardRevenue: number;
  monthlyTotalRevenue: number;
  cardPresentTransactions: number;
  floorServersAmount: number;
  highestCardTransactionYear: number;
  internetTransactions: number;
  kitchenStationsCount: number;
  mailPhoneTransactions: number;
  tablesCount: number;
  averageCheck: number;
}
