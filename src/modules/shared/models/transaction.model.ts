export interface Transaction {
  amount: Amount;
  cardInfo: CardInfo;
  changeFor: Amount;
  checkId: string;
  createdAt: string;
  deviceProfileId: string;
  fee: Amount;
  id: string;
  isCashTips: boolean;
  merchantId: string;
  method: string;
  orderId: string;
  orderShortID: string;
  profileId: string;
  serviceCharge: Amount;
  shortId: string;
  status: string;
  tax: Amount;
  tip: Amount;
  venueId: string;
}

export interface Amount {
  amount: number;
  symbol: string;
}

export interface CardInfo {
  type: string;
  tokenId: string;
  cardHolderName: string;
  cardBrand: string;
  redactedCardNumber: string;
}
