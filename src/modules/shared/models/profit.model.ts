export interface Profit {
  merchant?: string;
  transactionAmount: number;
  processingCoast: number;
  rockSpoonRevenue: number;
  bonusFeeMGM: number;
  loyaltyGrant: number;
  chargeBacks: number;
  grossProfit: number;
}

export interface MerchantProfit extends Profit {
  transactionID: string;
  date: string;
  card: Card;
  transactionType: string;
}

export interface Card {
  type: string;
  number: string;
}
