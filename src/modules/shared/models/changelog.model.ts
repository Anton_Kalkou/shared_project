export interface Changelog {
  date: string;
  time: string;
  madeBy: string;
  change: string;
  action: string;
  changes: Change[];
}

export interface Change {
  field: string;
  from: string;
  to: string;
}
