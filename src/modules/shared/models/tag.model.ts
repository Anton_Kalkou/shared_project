export interface Tag {
  id?: number;
  name: string;
  addedByMe?: boolean;
}
