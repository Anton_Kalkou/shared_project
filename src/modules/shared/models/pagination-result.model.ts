export interface PaginationResult<T> {
  next: string;
  previous: string;
  results: T[];
  total: number;
}
