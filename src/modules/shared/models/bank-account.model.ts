export interface BankAccount {
  merchantId?: string;
  id?: string;
  accountNumber: string;
  routingNumber: string;
  accountType: string;
  status: string;
  selected: boolean;
  accountName: string;
  bankName: string;
}
