export interface MerchantBackend {
  businessInformation?: BusinessInformation;
  contact?: Contact;
  createdAt?: string;
  name?: string;
  paymentInAdvance?: boolean;
  phoneNumber?: string;
  principals?: Principals;
  salesInformation?: SalesInformation;
  servicePackage?: ServicePackage;
  status?: string;
  url?: string;
}

export interface BusinessInformation {
  businessAddress?: Address;
  category?: string;
  legalAddress?: Address;
  legalEntityName?: string;
  sein?: string;
  tin?: string;
  billingContact?: Contact;
  businessContact?: Contact;
  chargebackContact?: Contact;
}

export interface Contact {
  address?: Address;
  contactID?: number;
  email?: string;
  firstName?: string;
  jobPosition?: string;
  lastName?: string;
  phone?: string;
  profileID?: string;
  password?: string;
}

export interface Principals {
  companies?: CompanyOwner[];
  individual?: IndividualOwner[];
}

export interface CompanyOwner {
  address: Address;
  businessName: string;
  email: string;
  firstName: string;
  hubSpotContactID: number;
  lastName: string;
  phone: string;
  share: number;
  tin: string;
}

export interface IndividualOwner {
  creditReport?: any;
  documentDetails?: Document;
  email?: string;
  firstName?: string;
  hubSpotContactID?: number;
  kyc?: any;
  lastName?: string;
  legalAddress?: Address;
  nationality?: string;
  secondaryNationality?: string;
  phoneNumber?: string;
  residingCountry?: string;
  share?: number;
  socialSecurityNumber?: string;
  socialSecurityType?: string;
  dateOfBirth?: string;
}

export interface Document {
  documentIssuer?: string;
  expireDate?: string;
  issueDate?: string;
  documentType?: string;
  idNumber?: string;
}

export interface SalesInformation {
  monthlyTotalCardRevenue: number;
  monthlyTotalRevenue: number;
  cardPresentTransactions: number;
  floorServersAmount: number;
  highestCardTransactionYear: number;
  internetTransactions: number;
  kitchenStationsCount: number;
  mailPhoneTransactions: number;
  tablesCount: number;
  averageCheck: number;
}

export interface ServicePackage {
  baseFees: string;
  cost: string;
  hasMenuRSGo: boolean;
  includesAnalytics: boolean;
  includesDeliveryAndTakeout: boolean;
  includesHumanResources: boolean;
  includesInventoryManagement: boolean;
  includesPOSSolution: boolean;
  includesPayments: boolean;
  includesRSGoOrders: boolean;
  includesReservationsWaitlist: boolean;
  includesSalesReport: boolean;
  isVisibleRSGo: boolean;
  name: string;
  products: Product[];
}

export interface Product {
  code: string;
  packages: string[];
}

export interface Address {
  address1?: string;
  address2?: string;
  city?: string;
  country?: string;
  id?: string;
  instructions?: string;
  isDefault?: boolean;
  isValidated?: boolean;
  latitude?: number;
  longitude?: number;
  name?: string;
  region?: string;
  state?: string;
  status?: string;
  timezone?: string;
  zipcode?: string;
}
