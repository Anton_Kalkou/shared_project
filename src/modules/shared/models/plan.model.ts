export interface Plan {
  title: string;
  available: boolean;
  cost: string;
  visibleOnRockspoonGo: boolean;
  menuOnRockspoonGo: boolean;
  reservationsWaitlist: boolean;
  deliveryTakeout: boolean;
  inventoryManagement: boolean;
  payments: boolean;
  ordersFromRockspoonGo: boolean;
  posSolution: boolean;
  analytics: boolean;
  humanResources: boolean;
  salesReports: boolean;
}
