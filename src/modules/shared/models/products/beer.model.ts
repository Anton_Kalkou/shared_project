import { Alcohol } from './alcohol.model';

export class Beer extends Alcohol {
  public manufacturer: string;
  public beerType: string;
  public beerSubtype: string;
  public stoutSubtype: string;
  public aged: string;
  public EBC: string;
  public IBU: string;
  public lupus: string;
}
