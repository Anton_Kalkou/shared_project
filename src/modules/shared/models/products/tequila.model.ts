import { Alcohol } from './alcohol.model';

export class Tequila extends Alcohol {
  public producer: string;
  public tequilaType: string;
  public aged: string;
}
