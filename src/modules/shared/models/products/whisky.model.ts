import { Alcohol } from './alcohol.model';

export class Whisky extends Alcohol {
  public producer: string;
  public whiskyType: string;
  public whiskySubtype: string;
  public flavorProfile: string;
  public aged: string;
}
