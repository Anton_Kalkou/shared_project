import { Product } from './product.model';
import { Tag } from '../tag.model';

export class Fruit extends Product {
  public manufacturer: string;
  public quantityPerUnit: number;
  public measure: string;
  public barCodeNumber: string;
  public attributes: Tag[];
}
