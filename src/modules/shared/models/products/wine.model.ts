import { Alcohol } from './alcohol.model';

export class Wine extends Alcohol {
  public producer: string;
  public wineType: string;
  public wineSubtype: string;
  public flavorProfile: string;
  public grapes: Grape[];
  public acidityScale: number;
  public bodyScale: number;
  public tanninScale: number;
  public vintage: string;
  public letWineBreathIn: string;
  public ratings: Rate[];
}

export interface Grape {
  grape: string;
  grapeVariety: string;
}

export interface Rate {
  by: string;
  points: number;
}
