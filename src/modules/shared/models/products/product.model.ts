import { Tag } from '../tag.model';

export class Product {
  public id: number;
  public created: string;
  public name: string;
  public addedBy: {
    name: string;
    number: number;
    contacts: string;
  };
  public status: number;
  public mainCategory: string;
  public category: Tag;
  public barCodePhotos: string[];
  public packaged: boolean;
  public productNameVariations: string[];
  public attributes: Tag[];
  public extraNotes: string;
}
