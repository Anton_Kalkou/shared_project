import { Product } from './product.model';

export class Berries extends Product {
  public producer: string;
  public soldBy: string;
}
