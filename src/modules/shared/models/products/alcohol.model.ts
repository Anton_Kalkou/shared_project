import { Product } from './product.model';

export class Alcohol extends Product {
  public brand: string;
  public countryOrRegion: string;
  public quantityPerUnit: number;
  public measure: string;
  public barCodeNumber: string;
  public alcohol: string;
  public recommendedGlassware: string;
  public pairing: string;
}
