import { Alcohol } from './alcohol.model';

export class Sake extends Alcohol {
  public manufacturer: string;
  public sakeType: string;
  public aged: string;
  public servingStyle: string;
}
