import { Alcohol } from './alcohol.model';

export class Vodka extends Alcohol {
  public producer: string;
  public vodkaType: string;
  public aged: string;
}
