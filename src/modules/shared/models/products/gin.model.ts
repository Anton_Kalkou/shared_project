import { Alcohol } from './alcohol.model';

export class Gin extends Alcohol {
  public manufacturer: string;
  public ginType: string;
  public aged: string;
}
