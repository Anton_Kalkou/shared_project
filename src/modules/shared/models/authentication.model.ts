export interface Authentication {
  accessToken: string;
  expiresIn: number;
  refreshToken: string;
  tokenType: string;
}
