export interface EditorsFilter {
  completion: string[];
  category: string[];
  lastEditedBy: string[];
}

export const emptyEditorsFilter: EditorsFilter = {
  category: [],
  completion: [],
  lastEditedBy: []
};
