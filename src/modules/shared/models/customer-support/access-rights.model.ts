export namespace AccessRights {
  export interface Data {
    id: string;
    employee: string;
    roles: string;
    dateLastModified: string;
  }
  
  export interface AdminProfileResponse {
    results: AdminProfileEmployeeResponse[];
  }
  
  export interface AdminRoleResponse {
    id: string;
    name: string;
  }
  
  export interface AdminProfileEmployeeResponse {
    id: string;
    firstName: string;
    lastName: string;
    venues: Venue[];
  }

  export interface Venue {
    id: string;
    firstName: string;
    lastName: string;
    roles: Role[];
  }
  export interface Role {
    id: string;
    startDate: string;
  }
  
}

export namespace AccessRightsDetails {

  export interface Data {
    categoryTitle: string;
    items: AccessRightsDetailsItem[];
  }
  export interface AccessRightsDetailsSettings {
    userProfile: UserProfile;
    venue: Venue;
  }
  
  export interface AccessRightsDetailsItem {
    id: string;
    name: string,
    status: string;
  }

  export interface DataResponse {
    id: string;
    name: string;
    accessRights: AccessRightsResponse[];
  }
  
  export interface AccessRightsResponse {
    id: string;
    name: string;
    category: string;
    level: string;
  }

  export interface UserProfile {
    id: string;
  }

  export interface Venue {
    id: string;
    code: string;
  }

  export interface Filter {
    type: string;
    values: FilterValues;
  }
  
  export interface FilterValues {
    searchValue: string;
    filtersValues: string[];
  }


}
