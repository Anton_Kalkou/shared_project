import { Chart, DevicesStatus } from 'src/modules/support/components/systems-health/enum/systems-health.enum';

export interface SystemsHealthData {
  devicesAvailabilityChart: DevicesAvailabilityChart;
  devicesStatus: DeviceStatus[];
}

export interface DevicesAvailabilityChart {
  labelFormat: string;
  valuesRange: ValueRange;
  devicesAvailability: DeviceAvailability[];
  latestReport: DeviceAvailability;
}
export interface DeviceAvailability {
  id: string;
  availability: Chart.BarStatus;
  type: string;
  start: Date;
  end: Date;
}

export interface ValueRange {
  startValue: Date;
  endValue: Date;
}

export interface DateFilterValues {
  from: Date;
  to: Date;
}

export interface DeviceStatus {
  datesRange: ChosenDatesRange;
  deviceId: string;
  status: DevicesStatus.Status;
  wifi: Wifi;
  battery: Battery;
  version: Version;
  function: string;
  bluetooth: boolean;
  cellularSignal: number;
}

export interface DeviceStatusResponse {
  deviceId: string;
  report: DeviceStatusReport[];
}
export interface DateFilter {
  key: string;
  value: Date[] | undefined;
}

export interface DeviceStatusReport {
  _id: string;
  androidVersionRelease: string,
  batteryHealth: string,
  batteryLevel: 0,
  bluetoothError: string;
  bluetoothIsOk: false,
  cellularSignal: 0,
  deviceType: DevicesStatus.Function,
  powerStatus: string,
  sdkVersion: 0,
  updateTime: 0,
  version: string,
  wifiNetwork: string,
  wirelessSignal: 0,
  createdAt: string;
  deviceId: string;
}

export interface Battery {
  health: string; 
  level: number;
  chargingStatus: string;
}

export interface Version {
  android?: string;
  app?: string;
}

export interface Wifi {
  network: string;
  signal: number;
}

export interface DeviceStatusConstraint {
  deviceId: string;
  report: DeviceStatusReport[]
}

export interface ChosenDatesRange {
  start: Date;
  end: Date;
}

export interface FilterData {
  day: string;
  availability: Chart.BarStatus;
}

export interface PointData extends FilterData {}
