export interface ButtonFilter {
  translationKey: string;
  key: string;
  icon?: string;
}

export interface ButtonFilterEvent {
  key: string;
  value: any[] | undefined;
}
