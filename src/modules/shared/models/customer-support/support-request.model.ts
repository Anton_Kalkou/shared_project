export interface SupportRequest {
  time: string,
  restaurantName: string,
  callerName: string,
  venueId: string;
  merchantId: string,
  merchantStatus: string,
  rockspoonPlan: string
}

export interface SupportRequestResponse {
  venueId: string;
  venueCode: string;
  contact: Contact;
  createdAt: string;
  venueName: string;
  merchantId: string;
  merchantStatus: string;
  rockspoonPlan: string;
  venueAddress: VenueAddress;
  venueAvailability: VenueAvailability;
}

export interface VenueAddress {
  address1: string;
  timezone: string;
}

export interface VenueAvailability {
  openingHours: OpeningHoursItem[];
}

export interface OpeningHoursItem {
  daysOfWeek: string[];
  periods: OpeningHoursPeriod[];
}

export interface OpeningHoursPeriod {
  from: string;
  to: string;
}

export interface DateFilter {
  key: string;
  value: Date[] | undefined;
}

interface Contact {
  email: string;
  job_title: string;
  name: string;
  profile_id: string;
}

export interface OpenningHoursData {
  time: OpeningHoursPeriod;
  weekDays: string | string[];
}

export interface SelectedSupportRequest {
  id: string;
  caller: Caller;
  restaurant: Restaurant;
  createdTime: string;
  venue: Venue;
  merchantId: string;
}

export interface Caller {
  id: string;
  letters: string;
  name: string;
  position: string;
}
export interface Restaurant {
  name: string;
  type: string;
  address: string;
  localTime: LocalTime;
  openningHours: OpenHours[],
  timezone: string;
}

export interface Venue {
  id: string;
  code: string;
}

export interface OpenHours {
  daysOfWeek: string[],
  periods: Period[];
}

export interface Period {
  from: string;
  to: string
}

export interface LocalTime {
  time: string;
  offset: number;
}
