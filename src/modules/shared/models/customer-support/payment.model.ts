import { Amount, Transaction as TransactionResponse } from 'src/modules/shared/models/transaction.model';

export namespace Transactions {

  export interface TransactionsResponse {
    next: string;
    previous: string;
    total: number;
    results: TransactionItemResponse[];
  }

  export interface TransactionItemResponse extends TransactionResponse {
    total: Amount;
    employeeProfileId: string;
  }

  export interface Transaction {
    date: string;
    orderId: string;
    paymentId: string;
    cardHolderName: string | null
    card: Card | null;
    paymentAmount: string;
    tipAmount: string;
    staff: string;
    stage?: string;
    method?: string;
  }

  export interface Card {
    number: string;
    brand: string;
  }

  export interface AdminProfileEmployeeData {
    firstName: string;
    lastName: string;
    id: string;
  }
}

export namespace Batches {
  export interface BatchResponse {
    createdAt: string;
    destinationCode: string;
    id: string;
    originCode: string;
    referenceCode: string;
    result: string;
    settlementType: string;
    totalAmount: Amount;
    accountNumber: string;
    paymentCount: number;
  }

  export interface Query {
    merchantId: string;
    startDate: string;
    endDate?: string;
  }

  export interface Batch {
    date: string;
    batchId: string;
    accountNumber: string
    numberOfPayments: number;
    amount: string
  }
}

export interface AdminProfileResponse {
  results: AdminProfileEmployeeResponse[];
}

export interface AdminProfileEmployeeResponse {
  id: string;
  venues: Venue[];
}


export interface Venue {
  firstName: string;
  lastName: string;
}

export interface DateQuery {
  fromDate: string;
  toDate?: string;
}
