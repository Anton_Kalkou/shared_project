export interface User {
  birthDate: string;
  email: string;
  firstName: string;
  id: string;
  lastName: string;
  address: Address;
  phone: Phone;
  customerPicture: Picture;
  employeePicture: Picture;
}

export interface Phone {
  countryCode: string;
  phoneNumber: string;
}

export interface Address {
  address1: string;
  address2: string;
  city: string;
  country: string;
  region: string;
  timezone: string;
}

export interface Picture {
  hiResolution: Resolution;
  loResolution: Resolution;
  noResolution: Resolution;
}

export interface Resolution {
  width: number;
  height: number;
  url: string;
}
