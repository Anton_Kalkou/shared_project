export interface Device {
  terminalID: string;
  locationAddresses: string;
  venue: string;
  brand: string;
  model: string;
  serialNumber: string;
}
