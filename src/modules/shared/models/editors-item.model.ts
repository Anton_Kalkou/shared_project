export interface EditorsItem {
  id: number;
  name: string;
  category: string;
  frequency: number;
  modified: string;
  lastEditedBy: string;
  status: number;
  packaged: boolean;
}
