import { Directive, Input, HostListener } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';

import { ResolutionService } from 'src/modules/shared/services/resolution.service';
import { History } from 'src/modules/shared/models/history.model';
import { HistoryResultComponent } from '../components/history-result/history-result.component';

@Directive({
  selector: '[appOpenHistoryResult]',
})
export class OpenHistoryResultDirective {

  @Input() verification: History;
  @Input() isProvisioning: boolean;

  constructor(
    private dialog: MatDialog,
    private resolutionService: ResolutionService
  ) {}

  @HostListener('click')
  openVerificationResult(): void {
    this.dialog.open(HistoryResultComponent, {
      data: {
        isProvisioning: this.isProvisioning
      },
      width: '1240px'
    });
  }

}
