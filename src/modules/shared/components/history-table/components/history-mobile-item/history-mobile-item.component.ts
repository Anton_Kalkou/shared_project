import { Component, Input } from '@angular/core';

import { History } from 'src/modules/shared/models/history.model';

@Component({
  selector: 'app-history-mobile-item',
  templateUrl: 'history-mobile-item.component.html',
  styleUrls: ['history-mobile-item.component.scss']
})
export class HistoryMobileItemComponent {

  @Input() historyItem: History;
  @Input() isProvisioning: boolean;

}
