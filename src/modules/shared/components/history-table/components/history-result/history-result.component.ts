import { Component, Input, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { History } from 'src/modules/shared/models/history.model';

@Component({
  selector: 'app-history-result',
  templateUrl: 'history-result.component.html',
  styleUrls: ['history-result.component.scss']
})
export class HistoryResultComponent implements OnInit {

  @Input() history: History;

  public verificationForm: FormGroup;
  public isProvisioning: boolean;

  constructor(
    @Inject(MAT_DIALOG_DATA) dialogData
  ) {
    this.isProvisioning = dialogData.isProvisioning;
  }

  ngOnInit() {
    this.initAndDisableForm();
  }

  private initAndDisableForm(): void {
    this.verificationForm = new FormGroup({
      date: new FormControl('05/03/2020'),
      time:  new FormControl('10:10 AM'),
      company:  new FormControl('Giact'),
      type:  new FormControl('Account, KYC, OFAC'),
      startedBy:  new FormControl('Davon Philips'),
      reason:  new FormControl('Validation'),
      reasonDetails:  new FormControl('Reason details text'),
      status:  new FormControl('Denied'),
      fullReason: new FormControl('Full response text')
    });
    this.verificationForm.disable();
  }


}
