import { Component, Input, ViewChild, OnInit, OnChanges } from '@angular/core';

import { Store } from '@ngxs/store';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { History } from 'src/modules/shared/models/history.model';
import { ResolutionService } from 'src/modules/shared/services/resolution.service';
import { OpenFilters } from '../../../filters/ngxs/filters.actions';
import { APP } from 'src/modules/shared/constants';

@Component({
  selector: 'app-history-table',
  templateUrl: 'history-table.component.html',
  styleUrls: ['history-table.component.scss']
})
export class HistoryTableComponent implements OnInit, OnChanges {

  @Input() historyItems: History[];
  @Input() isProvisioning: boolean;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public displayedColumns = [
    'date',
    'company',
    'type',
    'reason',
    'status',
    'actions'
  ];
  public dataSource: MatTableDataSource<History>;

  constructor(
    private store: Store,
    private resolutionService: ResolutionService
  ) {}

  ngOnInit() {
    this.iniTable();
  }

  ngOnChanges() {
    this.iniTable();
  }

  public mobileItemsVisibility(): boolean {
    return this.resolutionService.mobileOnly();
  }

  public tableVisibility(): boolean {
    return this.resolutionService.tabletAndMore();
  }

  public openFilters(): void {
    this.store.dispatch(new OpenFilters(this.isProvisioning ? APP.endpoints.provisioningFilters : APP.endpoints.verificationFilters));
  }

  private iniTable(): void {
    this.dataSource = new MatTableDataSource(this.historyItems);
    this.dataSource.sort = this.sort;
  }

}
