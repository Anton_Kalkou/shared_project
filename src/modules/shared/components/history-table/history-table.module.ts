import { NgModule } from '@angular/core';

import { SharedModule } from '../../modules/shared.module';
import { HistoryMobileItemComponent } from './components/history-mobile-item/history-mobile-item.component';
import { HistoryTableComponent } from './components/history-table/history-table.component';
import { MaterialModule } from '../../modules/material.module';
import { SquareButtonModule } from '../square-button/square-button.module';
import { OpenHistoryResultDirective } from './directives/open-history-result.directive';
import { PopupModule } from '../popup/popup.module';
import { HistoryResultComponent } from './components/history-result/history-result.component';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    SquareButtonModule,
    PopupModule
  ],
  declarations: [
    HistoryTableComponent,
    HistoryMobileItemComponent,
    OpenHistoryResultDirective,
    HistoryResultComponent
  ],
  exports: [
    HistoryTableComponent
  ]
})
export class HistoryTableModule {}
