import { NgModule } from '@angular/core';

import { SharedModule } from '../../modules/shared.module';
import { SelectReasonComponent } from './select-reason.component';
import { MaterialModule } from '../../modules/material.module';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule
  ],
  declarations: [
    SelectReasonComponent
  ],
  exports: [
    SelectReasonComponent
  ]
})
export class SelectReasonModule {}
