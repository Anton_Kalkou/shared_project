import { Component, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { APP } from 'src/modules/shared/constants';

@Component({
  selector: 'app-select-reason',
  templateUrl: 'select-reason.component.html',
  styleUrls: ['select-reason.component.scss']
})
export class SelectReasonComponent {

  @Input() caption: string;

  public reasonForm = new FormGroup({
    reason: new FormControl('', Validators.required),
    reasonDetails: new FormControl('')
  });
  public reasons = APP.verificationReasons;

}
