import { NgModule } from '@angular/core';

import { SharedModule } from '../../modules/shared.module';
import { PlusMinusComponent } from './plus-minus.component';
import { MaterialModule } from '../../modules/material.module';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule
  ],
  declarations: [
    PlusMinusComponent
  ],
  exports: [
    PlusMinusComponent
  ]
})
export class PlusMinusModule {}
