import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-plus-minus',
  templateUrl: 'plus-minus.component.html',
  styleUrls: ['plus-minus.component.scss']
})
export class PlusMinusComponent {

  @Input() value: boolean;

}
