import { Component, Inject, ViewChild, ElementRef, Renderer2, OnDestroy, OnInit } from '@angular/core';

import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';

import { NotificationService } from '../../services/notification.service';
import { APP } from '../../constants';
import { CustomCropperComponent } from '../cropper/cropper.component';

@Component({
  selector: 'app-take-photo',
  templateUrl: 'take-photo.component.html',
  styleUrls: ['take-photo.component.scss']
})
export class TakePhotoComponent implements OnInit, OnDestroy {

  @ViewChild('video') video: ElementRef<HTMLVideoElement>;
  @ViewChild('canvas') canvas: ElementRef<HTMLCanvasElement>;

  public isTaken: boolean;

  private stream: MediaStream;
  private videoWidth = 0;
  private videoHeight = 0;

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData,
    private renderer: Renderer2,
    private notificationService: NotificationService,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.startCamera();
  }

  public takePhoto(): void {
    this.renderer.setProperty(this.canvas.nativeElement, 'width', this.videoWidth);
    this.renderer.setProperty(this.canvas.nativeElement, 'height', this.videoHeight);
    this.canvas.nativeElement.getContext('2d').drawImage(this.video.nativeElement, 0, 0);
    this.isTaken = true;
  }

  public use(): void {
    this.dialog.open(CustomCropperComponent, {
      width: '800px',
      id: APP.dialogs.cropper,
      data: {
        caption: 'dialogs.crop-your-photo',
        imgUrl: this.canvas.nativeElement.toDataURL()
      }
    });
    this.close();
  }

  private async startCamera(): Promise<void> {
    try {
      this.stream = await navigator.mediaDevices.getUserMedia({
        video: {
            facingMode: 'environment',
            width: { ideal: 4096 },
            height: { ideal: 2160 }
        }
      });

      this.attachVideo();
    } catch (error) {
      this.handleError(error);
    }
  }

  private handleError(error: any): void {
    this.notificationService.showErrorMessage('Webcam Error', error);
    this.close();
  }

  private close(): void {
    this.dialog.getDialogById(APP.dialogs.takePhoto).close();
  }

  private attachVideo(): void {
    this.renderer.setProperty(this.video.nativeElement, 'srcObject', this.stream);
    this.renderer.listen(this.video.nativeElement, 'play', (event) => {
        this.videoHeight = this.video.nativeElement.videoHeight;
        this.videoWidth = this.video.nativeElement.videoWidth;
    });
  }

  ngOnDestroy() {
    if (this.stream) {
      this.stream.getTracks().forEach(track => track.stop());
    }
  }

}
