import { NgModule } from '@angular/core';

import { SharedModule } from '../../modules/shared.module';
import { TakePhotoComponent } from './take-photo.component';
import { PopupModule } from '../popup/popup.module';
import { MaterialModule } from '../../modules/material.module';
import { CropperModule } from '../cropper/cropper.module';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    PopupModule,
    CropperModule
  ],
  declarations: [
    TakePhotoComponent
  ],
  exports: [
    TakePhotoComponent
  ]
})
export class TakePhotoModule {}
