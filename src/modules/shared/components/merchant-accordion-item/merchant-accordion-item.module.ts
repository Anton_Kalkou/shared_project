import { NgModule } from '@angular/core';

import { SharedModule } from '../../modules/shared.module';
import { MaterialModule } from '../../modules/material.module';
import { MerchantAccordionItemComponent } from './merchant-accordion-item.component';
import { SquareButtonModule } from '../square-button/square-button.module';
import { ConfirmationModule } from '../confirmation/confirmation.module';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    SquareButtonModule,
    ConfirmationModule
  ],
  declarations: [
    MerchantAccordionItemComponent
  ],
  exports: [
    MerchantAccordionItemComponent
  ]
})
export class MerchantAccordionItemModule {}
