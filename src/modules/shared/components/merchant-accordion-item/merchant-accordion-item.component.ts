import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-merchant-accordion-item',
  templateUrl: 'merchant-accordion-item.component.html',
  styleUrls: ['merchant-accordion-item.component.scss']
})
export class MerchantAccordionItemComponent {

  @Input() caption: string;

}
