import { NgModule } from '@angular/core';

import { SharedModule } from '../../modules/shared.module';
import { MaterialModule } from '../../modules/material.module';
import { AutoSaveNotificationTemplateComponent } from './auto-save-notification-template.component';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule
  ],
  declarations: [
    AutoSaveNotificationTemplateComponent
  ],
  exports: [
    AutoSaveNotificationTemplateComponent
  ]
})
export class AutoSaveNotificationTemplateModule {}
