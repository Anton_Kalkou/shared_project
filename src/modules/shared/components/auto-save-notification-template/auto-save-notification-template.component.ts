import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-auto-save-notification-template',
  templateUrl: 'auto-save-notification-template.component.html',
  styleUrls: ['auto-save-notification-template.component.scss']
})
export class AutoSaveNotificationTemplateComponent {
  @Input() title: string;
}
