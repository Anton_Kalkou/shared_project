import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../../modules/material.module';
import { SpinnerComponent } from './spinner.component';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    MaterialModule
  ],
  declarations: [
    SpinnerComponent
  ],
  exports: [
    SpinnerComponent
  ]
})
export class SpinnerModule {}
