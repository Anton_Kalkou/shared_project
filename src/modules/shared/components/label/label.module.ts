import { NgModule } from '@angular/core';

import { SharedModule } from '../../modules/shared.module';
import { LabelComponent } from './label.component';
import { MaterialModule } from '../../modules/material.module';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule
  ],
  declarations: [
    LabelComponent
  ],
  exports: [
    LabelComponent
  ]
})
export class LabelModule {}
