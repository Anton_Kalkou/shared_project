import { Component, Input, OnInit, Output, EventEmitter, ChangeDetectorRef } from '@angular/core';

import { Subject } from 'rxjs';

import { Unsubscribe } from '../../classes/unsubscribe.class';

@Component({
  selector: 'app-label',
  templateUrl: 'label.component.html',
  styleUrls: ['label.component.scss']
})
export class LabelComponent extends Unsubscribe implements OnInit {

  @Output() changeEvent = new EventEmitter<boolean>();
  @Output() delete = new EventEmitter<void>();

  @Input() caption: string;
  @Input() default: boolean;
  @Input() isBig: boolean;
  @Input() disabled: boolean;
  @Input() isDeletable: boolean;
  @Input() unselectSubject: Subject<void>;
  @Input() icon: string;

  public active: boolean;

  constructor(
    private changeDetectorRef: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit() {
    this.setDefaultLabel();
    this.subscribeToUnselect();
  }

  public deleteLabel(event: MouseEvent): void {
    event.stopPropagation();
    this.delete.emit();
  }

  public toggleLabel(): void {
    if (!this.disabled) {
      this.changeEvent.emit(!this.active);

      setTimeout(() => {
        this.active = !this.active;
        this.changeDetectorRef.markForCheck();
      });
    }
  }

  private setDefaultLabel(): void {
    this.active = this.default;
  }

  private subscribeToUnselect(): void {
    if (this.unselectSubject) {
      this.subscribeTo = this.unselectSubject
        .subscribe(() => {
          this.active = false;
        });
    }
  }

}
