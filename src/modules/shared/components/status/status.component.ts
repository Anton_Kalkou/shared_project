import { Component, Input } from '@angular/core';

import { APP } from '../../../shared/constants';

@Component({
  selector: 'app-status',
  templateUrl: 'status.component.html',
  styleUrls: ['status.component.scss']
})
export class StatusComponent {

  @Input() value: number;

  public getColor(): string {
    if (this.value > 70) {
      return APP.colors.green;
    } else
    if (this.value > 40) {
      return APP.colors.yellow;
    } else {
      return APP.colors.red;
    }
  }

}
