import { FilterGroup } from 'src/modules/shared/models/filter.model';

export class OpenFilters {
  static readonly type = '[Filters] Open';

  constructor(public filtersEndpoint: string) {}
}

export class SetFilters {
  static readonly type = '[Filters] Set';

  constructor(public filters: FilterGroup[]) {}
}

export class HideFilters {
  static readonly type = '[Filters] Hide';
}
export class ClearFilters {
  static readonly type = '[Filters] Clear';
}
