import { Injectable } from '@angular/core';
import { State, Action, StateContext, Store } from '@ngxs/store';
import { of } from 'rxjs';
import { tap, delay } from 'rxjs/operators';

import { OpenFilters, HideFilters, SetFilters, ClearFilters } from './filters.actions';
import { FilterGroup } from 'src/modules/shared/models/filter.model';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';

import { FiltersService } from '../services/filters.service';

interface FiltersStateModel {
  filters: FilterGroup[];
  filtersVisibility: boolean;
}

@State<FiltersStateModel>({
  name: 'filters',
  defaults: {
    filters: [],
    filtersVisibility: false
  }
})
@Injectable()
export class FiltersState {
  constructor(
    private readonly store: Store,
    private readonly filtersService: FiltersService
  ) {}

  @Action(OpenFilters)
  setFiltersVisibility({ patchState, getState }: StateContext<FiltersStateModel>, { filtersEndpoint}: OpenFilters) {
    const currentFilters = getState().filters;

    this.store.dispatch(new SetSpinnerVisibility(true));

    if (currentFilters && currentFilters.length) {
      return of({}).pipe(
        delay(500),
        tap(() => {
          patchState( { filtersVisibility: true });
          this.store.dispatch(new SetSpinnerVisibility(false));
        })
      );
    }

    return this.filtersService.getFilters(filtersEndpoint)
      .pipe(
        tap(filters => {
          filters
            ? patchState({ filters, filtersVisibility: true })
            : patchState({ filtersVisibility: true });

          this.store.dispatch(new SetSpinnerVisibility(false));
        })
      );
  }

  @Action(SetFilters)
  setFilters({ patchState }: StateContext<FiltersStateModel>, { filters }: SetFilters) {
    patchState({ filters });
  }

  @Action(HideFilters)
  hideFilters({ patchState }: StateContext<FiltersStateModel>) {
    patchState({
      filtersVisibility: false
    });
  }

  @Action(ClearFilters)
  clearFilters({ patchState }: StateContext<FiltersStateModel>) {
    patchState({ filters: [] });
  }
}
