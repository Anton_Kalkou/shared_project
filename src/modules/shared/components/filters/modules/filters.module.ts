import { NgModule } from '@angular/core';

import { ClickOutsideModule } from 'ng-click-outside';
import { NgxsModule } from '@ngxs/store';
import { Ng5SliderModule } from 'ng5-slider';
import { SatDatepickerModule, SatNativeDateModule } from 'saturn-datepicker';

import { SharedModule } from '../../../modules/shared.module';
import { FiltersComponent } from '../component/filters.component';
import { FiltersState } from '../ngxs/filters.state';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { LabelModule } from '../../label/label.module';
import { SearchModule } from '../../search/search.module';
import { FiltersClickOutside } from '../directives/click-outside.directive';

@NgModule({
  imports: [
    SharedModule,
    NgxsModule.forFeature([FiltersState]),
    MaterialModule,
    ClickOutsideModule,
    LabelModule,
    SearchModule,
    Ng5SliderModule,
    SatDatepickerModule,
    SatNativeDateModule
  ],
  declarations: [
    FiltersComponent,
    FiltersClickOutside
  ],
  exports: [
    FiltersComponent
  ]
})
export class FiltersModule {}
