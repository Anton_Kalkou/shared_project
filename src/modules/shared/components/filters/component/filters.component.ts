import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Options } from 'ng5-slider';

import { FilterGroup, ParentFilter } from 'src/modules/shared/models/filter.model';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { darkness, fromRightToLeft, height } from 'src/modules/shared/animations';
import { HideFilters } from '../ngxs/filters.actions';
import { copy } from 'src/modules/shared/helpers';
import { FiltersService } from '../services/filters.service';
import { Translation as FiltersTranslation } from '../enum/filters.enum';
import { APP } from 'src/modules/shared/constants';

@Component({
  selector: 'app-filters',
  templateUrl: 'filters.component.html',
  styleUrls: ['filters.component.scss'],
  animations: [darkness, fromRightToLeft, height]
})
export class FiltersComponent extends Unsubscribe implements OnInit {

  @Select(state => state.filters.filtersVisibility) filtersVisibility$: Observable<boolean>;
  @Select(state => state.filters.filters) filters$: Observable<FilterGroup[]>;

  public formGroup: FormGroup;
  public value = 20;
  public highValue = 100;
  public sliderOptions: Options = { floor: 0, ceil: 250 };
  public filtersVisibility: boolean;
  public displayedFilters: FilterGroup[];
  public activatedDatesFilter: string;

  private filters: FilterGroup[];

  constructor(private store: Store, private filtersService: FiltersService, private formBuilder: FormBuilder) {
    super();
  }

  ngOnInit() {
    this.initSubscriptions();
    this.formGroup = this.formBuilder.group({
      datesRange: new FormControl({ value: { begin: null, end: null }, disabled: true })
    });
  }

  public isFilterEnabled(filterTitle: string, filterValue: string): boolean {
    return this.filtersService
      .getActivatedFilters()
      .some(activatedFilter => activatedFilter.parentFilterOption === filterValue && activatedFilter.parentFilterTitle === filterTitle);
  }

  public isDates(filterGroup: FilterGroup): boolean {
    return filterGroup.filters ? filterGroup.filters.some(filter => APP.filters.dates.includes(filter)) : false;
  }

  public isSlider(filterGroup: FilterGroup): boolean {
    return typeof filterGroup.from !== 'undefined';
  }

  public isParentSelected(filterGroup: FilterGroup): boolean {
    if (filterGroup.parent.length == 0) {
      return true;
    }
    var visible = false;
    filterGroup.parent.forEach(parent => {
      this.filtersService.getActivatedFilters().forEach(activated => {
        if (parent.parentParamName == activated.parentParamName && parent.parentFilterOption == activated.parentFilterOption) {
          visible = true;
        }
      });
    });

    return visible;
  }
  
  public isChooseDatesFilter(filterTranslationKey: string): boolean {
    return filterTranslationKey === FiltersTranslation.Date.ChooseDates;
  }

  public filtersRemainder(filterGroupTitle: string, number: number): string[] {
    const displayedFilter = this.displayedFilters.find(filter => filter.title === filterGroupTitle);

    return displayedFilter.filters.slice(number);
  }

  public setFilterGroupMore(filterGroupTitle: string, value: boolean): void {
    const displayedFilter = this.displayedFilters.find(filter => filter.title === filterGroupTitle);
    displayedFilter.more = value;
  }

  public searchFilters(filterGroupTitle: string, key: string): void {
    const displayedFilter = this.displayedFilters.find(currentFilter => currentFilter.title === filterGroupTitle);
    const filter = this.filters.find(currentFilter => currentFilter.title === filterGroupTitle);

    displayedFilter.filters = filter.filters.filter(value => value.toLowerCase().includes(key.toLowerCase()));
  }

  public hideFilters(): void {
    this.store.dispatch(new HideFilters());
  }

  public handleChange(filter: FilterGroup, value: string, event: boolean): void {
    if (event) {
      this.filtersService.addActivatedFilter({ parentFilterTitle: filter.title, parentParamName: filter.paramName, parentFilterOption: value });
    } else {
      const activatedFilters = this.filtersService.getActivatedFilters().filter(ft => !(ft.parentParamName == filter.paramName && ft.parentFilterOption == value));

      this.filtersService.setActivatedFilters(this.removeUnusedDatesFilters(activatedFilters));
    }

    this.filtersService.setActivatedFilters(this.removeUnusedDatesFilters(this.filtersService.getActivatedFilters()));
    this.filtersService.emitFilter(filter, value, event);

    if (this.isDates(filter)) {
      this.activatedDatesFilter = value;
      this.setFilterChosenDates(filter, value);
    }
  }

  public handleChangeDatesRange(filter: FilterGroup, value: string): void {
    this.setFilterChosenDates(filter, value);
  }

  public handleDatepickerToggle(): void {
    this.activatedDatesFilter = FiltersTranslation.Date.ChooseDates;
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.filtersVisibility$.subscribe(filtersVisibility => this.filtersVisibility = filtersVisibility);
    this.subscribeTo = this.filters$.subscribe(filters => {
      this.filters = filters;
      this.displayedFilters = copy(filters);
    });
  }

  private setFilterChosenDates(filter: FilterGroup, value: string): void {
    const datesRange = this.formGroup.get('datesRange').value;
    const chooseDatesFilter = this.filtersService.getActivatedFilters().find(filter => filter.parentFilterOption === FiltersTranslation.Date.ChooseDates);

    if (!chooseDatesFilter) {
      this.filtersService.addActivatedFilter({
        parentFilterTitle: filter.title,
        parentParamName: filter.paramName,
        parentFilterOption: value,
        parentFilterDatesRange: datesRange
      });
    } else {
      chooseDatesFilter.parentFilterDatesRange = datesRange;
    }
  }

  private removeUnusedDatesFilters(activatedFilters: ParentFilter[]): ParentFilter[] {
    const flters = activatedFilters.filter(activatedFilter => {
      if (activatedFilter.parentFilterOption.startsWith(FiltersTranslation.Date.StartsWith) && activatedFilter.parentFilterOption !== this.activatedDatesFilter) {
        return false;
      } else {
        return true;
      }
    });

    return flters;
  }
}
