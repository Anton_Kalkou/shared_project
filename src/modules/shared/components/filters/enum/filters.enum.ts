export namespace Translation {
  export enum Date {
    StartsWith = 'filters.date',
    Now = 'filters.date.now',
    LastDay = 'filters.date.last-day',
    Last7Days = 'filters.date.last-seven-days',
    ChooseDates = 'filters.date.choose-dates'
  }

  export enum Common {
    Staff = 'filters.staff',
    Roles = 'filters.roles',
    Status = 'filters.status',
    NoAccess = 'filters.no-access',
    Read = 'filters.read',
    Write = 'filters.write',
    Tag = 'filters.tag',
    Product = 'filters.product',
    Products = 'filters.products',
    Feature = 'filters.feature',
    Category = 'filters.category'
  }
}

export namespace FilterKey {
  export enum Date {
    Now = 'now',
    LastDay = 'day',
    Last7Days = 'week',
    ChooseDates = 'chooseDates'
  }
}
