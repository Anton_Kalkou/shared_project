import { Directive, ElementRef, HostListener } from '@angular/core';
import { Store } from '@ngxs/store';
import { APP } from 'src/modules/shared/constants';
import { HideFilters } from '../ngxs/filters.actions';

@Directive({
  selector: '[filtersClickOutside]'
})
export class FiltersClickOutside {

  constructor(private el: ElementRef, private store: Store) {}

  @HostListener('document:click', ['$event.target'])
  public onClickOutside(targetElement: any): void {
    if (this.isInsideFilters(targetElement)) {
      return;
    } else if (this.isDatePicker(targetElement)) {
      return;
    }

    this.store.dispatch(new HideFilters());
  }

  private isInsideFilters(targetElement: any): boolean {
    return this.el.nativeElement.contains(targetElement);
  }

  private isDatePicker(targetElement: any): boolean {
    return APP.insideFiltersClasses.some(filterInsideClass => filterInsideClass === targetElement.classList[0]);
  }

}
