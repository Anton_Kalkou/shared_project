import { Injectable } from '@angular/core';

import { delay } from 'rxjs/operators';
import { Observable, of, BehaviorSubject } from 'rxjs';

import { FilterGroup, FilterEvent, ParentFilter, FilterData } from 'src/modules/shared/models/filter.model';
import { APP } from 'src/modules/shared/constants';
import { Ontology } from 'src/modules/editors/components/ontology/ontologies/ontologies.component';
import { Translation as FiltersTranslation } from '../enum/filters.enum';
import { Translation as SearchTranslation } from '../../search/enum/search.enum';
import { Translation as TransactionsTranslation } from '../../transactions/enum/transactions.enum';

@Injectable({
  providedIn: 'root'
})
export class FiltersService {
  private activatedFilters: ParentFilter[] = [];

  public emitter = new BehaviorSubject<FilterEvent>(null);
  public ontologies: Ontology;

  public prepareFilters<T>(filtersData: FilterData[], data: T[]): FilterGroup[] {
    return filtersData.map(filterData => {
      const filter: FilterGroup = {
        title: filterData.title,
        filters: [...new Set(data.map(item => item[filterData.key]))]
      };

      return filter;
    });
  }

  public getFilters(filtersEndpoint: string): Observable<FilterGroup[]> {
    let filters;

    switch (filtersEndpoint) {
      case APP.endpoints.merchantsFilters:
        filters = [
          { title: 'Merchant Status', filters: ['Active', 'Inactive', 'Inactive'] },
          { title: 'RockSpoon Plan', filters: ['Full', 'Delivery & Takeout', 'Light'] },
          { title: 'Merchant Location - State', filters: ['Arizona', 'California', 'Nevada', 'Oregon'], searchPlaceholder: 'Search States' },
          { title: 'Merchant Location - City', filters: ['Las Vegas', 'Palo Alto', 'Phoenix', 'Salem', 'San Francisco', 'San Jose'], searchPlaceholder: 'Search Cities' }
        ];
        break;
      case APP.endpoints.verificationFilters:
        filters = [
          { title: 'Status', filters: ['Active', 'Denied', 'Pending'] },
          { title: 'Company', filters: ['Experian', 'Giact'] },
          { title: 'Type', filters: ['Account, KYC, OFAC', 'Account', 'Credit', 'KYC', 'OFAC'] },
          { title: 'Date', filters: ['Last Day', 'Last 12 Months', 'Select Date Range'] }
        ];
        break;
      case APP.endpoints.provisioningFilters:
        filters = [
          { title: 'Status', filters: ['Active', 'Denied', 'Pending'] },
          { title: 'Company', filters: ['Elavon', 'Ingenico'] },
          { title: 'Type', filters: ['Create', 'Update'] },
          { title: 'Date', filters: ['Last Day', 'Last 12 Months', 'Select Date Range'] }
        ];
        break;
      case APP.endpoints.transactionsFilters:
        filters = [
          { title: 'Status', filters: ['Active', 'Denied', 'Pending'] },
          { title: 'Card Type', filters: ['Credit', 'Debit'] },
          { title: 'Card', filters: ['American Express Platinum', 'Mastercard Black'], searchPlaceholder: 'Search Cards' },
          { title: 'Transaction Date', filters: ['Last Day', 'Last 12 Months', 'Select Date Range'] },
          { title: 'Transaction Amount', from: 0, to: 200 },
          { title: 'Processing Cost', from: 0, to: 200 },
          { title: 'Net Amount', from: 0, to: 200 }
        ];
        break;
      case APP.endpoints.profitabilityFilters:
        filters = [
          { title: 'Merchant', filters: ['Merchant 001', 'Merchant 002', 'Merchant 003', 'Merchant 004'], searchPlaceholder: 'Search Merchant' },
          { title: 'Transaction Amount', from: 0, to: 200 },
          { title: 'Processing Cost', from: 0, to: 200 },
          { title: 'Net Amount', from: 0, to: 200 }
        ];
        break;
      case APP.endpoints.devicesFilters:
        filters = [
          { title: 'Brand', filters: ['Ingenico', 'Motorola', 'Samsung'] },
          { title: 'Model', filters: ['APOS A8', 'Galaxy Tab A10', 'Ing M70', 'Moto G6'] }
        ];
        break;
      case APP.endpoints.globalItemsFilters:
        filters = [
          { title: 'Source', filters: ['filters.rockspoon', 'filters.vivino', 'filters.usda'], paramName: 'source' }
        ]
        filters = filters.concat(this.ontologyToFilter())
        break;
      case APP.endpoints.ontologiesFilters:
        filters = [
          { title: 'Mandatory', filters: ['Mandatory', 'Optional'], paramName: 'filter' }
        ];
        break;
      case APP.endpoints.globalItemsApprovalFilters:
        filters = [
          { title: 'Some filter', filters: ['A', 'B', 'C'] },
          { title: 'Another Filter', filters: ['1', '2', '3', '4'] }
        ];
        break;
      case APP.endpoints.paymentTransactionsFilters:
        filters = [
          { title: TransactionsTranslation.Staff, filters: APP.filters.staffShortName, searchPlaceholder: SearchTranslation.SearchStaff },
          { title: TransactionsTranslation.Status, filters: APP.filters.paymentStatus },
          { title: TransactionsTranslation.Card, filters: APP.filters.card, searchPlaceholder: SearchTranslation.SearchCards },
          { title: FiltersTranslation.Date.ChooseDates, filters: APP.filters.paymentTransactionsChooseDates }
        ];
        break;
      case APP.endpoints.paymentBatchesFilters:
        filters = [
          { title: TransactionsTranslation.AccountNumber, filters: APP.filters.accountNumber }
        ];
        break;
      case APP.endpoints.paymentBatchFilters:
        filters = [
          { title: TransactionsTranslation.CardHolderName, filters: APP.filters.cardHolderName },
          { title: TransactionsTranslation.Card, filters: APP.filters.card, searchPlaceholder: SearchTranslation.SearchCards },
          { title: TransactionsTranslation.Source, filters: APP.filters.device },
          { title: TransactionsTranslation.Staff, filters: APP.filters.staffShortName, searchPlaceholder: SearchTranslation.SearchStaff },
        ];
        break;
      case APP.endpoints.accessRightsFilters:
        filters = [
          { title: FiltersTranslation.Common.Staff, filters: APP.filters.staffFullName, searchPlaceholder: SearchTranslation.SearchStaff },
          { title: FiltersTranslation.Common.Roles, filters: APP.filters.roles }
        ];
        break;
      case APP.endpoints.accessRightsDetailsFilters:
        filters = [
          { title: FiltersTranslation.Common.Status, filters: APP.filters.accessRightsStatus }
        ];
        break;
      case APP.endpoints.tenantsCategoriesFilters:
        filters = [
          { title: FiltersTranslation.Common.Tag, filters: APP.filters.orchestrator.orchestratorB2B.tags },
          { title: FiltersTranslation.Common.Product, filters: APP.filters.orchestrator.orchestratorB2B.products },
          { title: FiltersTranslation.Common.Feature, filters: APP.filters.orchestrator.orchestratorB2B.features }
        ];
        break;
      case APP.endpoints.tenantsFilters:
        filters = [
          { title: FiltersTranslation.Common.Category, filters: [] }
        ];
        break;
    }

    return of(filters).pipe(delay(500));
  }

  public emitFilter(filter, value, event): void {
    this.emitter.next({ filter, value, event });
  }

  public getActivatedFilters(): ParentFilter[] {
    return this.activatedFilters;
  }

  public setActivatedFilters(activatedFilters: ParentFilter[]): void {
    this.activatedFilters = activatedFilters;
  }

  public addActivatedFilter(activatedFilter: ParentFilter): void {
    this.activatedFilters.push(activatedFilter);
  }

  public resetActivatedFilters(): void {
    this.setActivatedFilters([]);
  }

  public removeActivatedDateFilter(): void {
    const filters = this.activatedFilters.filter(activatedFilter => !activatedFilter.parentFilterOption.startsWith(FiltersTranslation.Date.StartsWith));

    this.setActivatedFilters(filters);
  }

  public isOntology(obj: any): boolean {
    if (typeof obj !== 'object') {
      return false;
    }

    return ('keys' in obj || 'possibleValues' in obj) &&
      'name' in obj &&
      'type' in obj;
  }

  private recursiveFilterBuild(cNode: Ontology, parent: ParentFilter, filters: FilterGroup[], ontologyPath: string): FilterGroup[] {
    var currentFilter: FilterGroup = { title: '', ontologyPath: '', filters: [], parent: [] };

    currentFilter.paramName = cNode.name
    currentFilter.title = this.addTranslationPath(cNode.name)
    currentFilter.ontologyPath = cNode.name
    if (ontologyPath != '')
      currentFilter.ontologyPath = ontologyPath + '.' + cNode.name

    if (parent) {
      currentFilter.parent = currentFilter.parent.concat(parent)
    }

    cNode.possibleValues?.forEach(possibleValue => {
      if (this.isOntology(possibleValue.value)) {
        var name = this.addTranslationPath(possibleValue.value.name)
        currentFilter.filters = currentFilter.filters.concat(name)
        filters = this.recursiveFilterBuild(possibleValue.value, { parentFilterTitle: currentFilter.title, parentParamName: currentFilter.paramName, parentFilterOption: name }, filters, currentFilter.ontologyPath)
      } else if (typeof possibleValue.value == 'string') {
        var name = this.addTranslationPath(possibleValue.value)
        currentFilter.filters = currentFilter.filters.concat(name)
      } else if (typeof possibleValue.value == 'number') {
        var name = String(possibleValue.value)
        currentFilter.filters = currentFilter.filters.concat(name)
      }
    });

    if (currentFilter.filters?.length > 0) {
      filters = filters.concat(currentFilter)
    }

    cNode.keys?.forEach(key => {
      filters = this.recursiveFilterBuild(key, parent, filters, currentFilter.ontologyPath)
    })

    return filters
  }

  private ontologyToFilter(): FilterGroup[] {
    return this.recursiveFilterBuild(this.ontologies, null, [], '').reverse()
  }

  private addTranslationPath(path: string): string {
    return 'filters.' + path;
  }
}
