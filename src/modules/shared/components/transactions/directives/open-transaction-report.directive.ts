import { Directive, HostListener } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';

import { TransactionReportComponent } from '../../../../merchants/merchant-profile/profile-transactions/components/transaction-report/transaction-report.component';
import { ResolutionService } from 'src/modules/shared/services/resolution.service';

@Directive({
  selector: '[appOpenTransactionReport]'
})
export class OpenTransactionReportDirective {

  constructor(
    private dialog: MatDialog,
    private resolutionService: ResolutionService
  ) {}

  @HostListener('click')
  openTransactionReport(): void {
    this.dialog.open(TransactionReportComponent, {
      width: '1240px',
      maxHeight: this.resolutionService.tabletAndLess() ? '100vh' : '80vh'
    });
  }

}
