import { NgModule } from '@angular/core';

import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { TransactionsComponent } from '../components/transactions/transactions.component';
import { TransactionMobileItemComponent } from '../components/mobile-transaction-item/transaction-mobile-item.component';
import { SquareButtonModule } from '../../square-button/square-button.module';
import { OpenTransactionReportDirective } from '../directives/open-transaction-report.directive';
import { DirectivesModule } from 'src/modules/shared/directives/directives.module';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    SquareButtonModule,
    DirectivesModule
  ],
  declarations: [
    TransactionsComponent,
    TransactionMobileItemComponent,
    OpenTransactionReportDirective
  ],
  exports: [
    TransactionsComponent
  ]
})
export class TransactionsModule {}
