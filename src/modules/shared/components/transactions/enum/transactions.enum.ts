export enum Translation {
  Date = 'transactions.date',
  Method = 'transactions.method',
  Amount = 'transactions.amount',
  Stage = 'transactions.stage',
  Status = 'transactions.status',
  Staff = 'transactions.staff',
  Card = 'transactions.card',
  CardHolderName = 'transactions.card-holder-name',
  PaymentAmount = 'transactions.payment-amount',
  TipAmount = 'transactions.tip-amount',
  AccountNumber = 'transactions.account-number',
  Source = 'transactions.source',
  PaymentId = 'transactions.payment-id',
  NumberOfPayments = 'transactions.number-of-payments',
  Captured = 'transactions.captured',
  Authorization = 'transactions.authorization',
  IncrementalAuthorization = 'transactions.incremental-authorization',
  Visa = 'transactions.visa',
  MasterBlack = 'transactions.master-black',
  AmericanExpressPlatinum = 'transactions.american-express-platinum',
  BatchId = 'transactions.batch-id',
  Cash = 'transactions.cash'
}

export enum Stage {
  PendingProcessor = 'pendingProcessor',
  PendingSettlement = 'pendingSettlement',
  Rejected = 'rejected',
  Authorized = 'authorized',
  Captured = 'captured',
  Settled = 'settled',
  Voided = 'voided',
  FastFunded = 'fastfunded'
}

export enum PaymentMethod {
  Cash = 'cash',
  CardNotPresent = 'cardnotpresent',
  External = 'external',
  Loyalty = 'loyalty'
}
