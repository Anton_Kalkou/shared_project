import { Component, Input } from '@angular/core';

import { Transaction } from 'src/modules/shared/models/transaction.model';

@Component({
  selector: 'app-transaction-mobile-item',
  templateUrl: 'transaction-mobile-item.component.html',
  styleUrls: ['transaction-mobile-item.component.scss']
})
export class TransactionMobileItemComponent {

  @Input() transaction: Transaction;

}
