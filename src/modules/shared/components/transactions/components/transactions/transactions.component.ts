import { Component, OnInit, ViewChild, Input, ChangeDetectorRef, OnChanges } from '@angular/core';

import { Store } from '@ngxs/store';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';

import { APP } from 'src/modules/shared/constants';
import { Transaction } from 'src/modules/shared/models/transaction.model';
import { SetExportVisibility } from 'src/modules/shared/components/export/ngxs/export.actions';
import { OpenFilters } from 'src/modules/shared/components/filters/ngxs/filters.actions';
import { ResolutionService } from 'src/modules/shared/services/resolution.service';
import { RenderOnResize } from 'src/modules/shared/classes/render-on-resize.class';


@Component({
  selector: 'app-transactions',
  templateUrl: 'transactions.component.html',
  styleUrls: ['transactions.component.scss']
})
export class TransactionsComponent extends RenderOnResize implements OnInit, OnChanges {

  @Input() transactions: Transaction[];
  @Input() merchantProfile: boolean;

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public transactionsOptions = APP.transactionsOptions;
  public sourceData: MatTableDataSource<Transaction>;
  public displayedColumns = [
    'type',
    'merchant',
    'id',
    'date',
    'amount',
    'cost',
    'netAmount',
    'card',
    'cardType',
    'batchId',
    'status',
    'actions'
  ];

  constructor(
    private store: Store,
    private resolutionService: ResolutionService,
    protected changeDetectorRef: ChangeDetectorRef
  ) {
    super(changeDetectorRef);
  }

  ngOnInit() {
    super.ngOnInit();
    this.removeUnnecessaryColumns();
    this.initTable();
  }

  ngOnChanges() {
    this.initTable();
  }

  public tableVisibility(): boolean {
    return this.resolutionService.laptopAndMore();
  }

  public mobileItemsVisibility(): boolean {
    return this.resolutionService.tabletAndLess();
  }

  public openFilters(): void {
    this.store.dispatch(new OpenFilters(APP.endpoints.transactionsFilters));
  }

  public openExport(): void {
    this.store.dispatch(new SetExportVisibility(true, 'transactions.transactions'));
  }

  private initTable(): void {
    this.sourceData = new MatTableDataSource(this.transactions);
    this.sourceData.sort = this.sort;
  }

  private removeUnnecessaryColumns(): void {
    if (this.merchantProfile) {
      this.displayedColumns.splice(1, 1);
    }
  }

}
