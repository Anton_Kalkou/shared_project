import { ChangeDetectionStrategy, ChangeDetectorRef, Component, ElementRef, EventEmitter, HostBinding, Input, OnDestroy, OnInit, Optional, Output, Self } from '@angular/core';
import { ENTER } from '@angular/cdk/keycodes';
import { MatFormFieldControl } from '@angular/material/form-field';
import { FormControl, NgControl } from '@angular/forms';
import { FocusMonitor } from '@angular/cdk/a11y';

import { debounceTime } from 'rxjs/operators';

import { BaseControlValueAccessor } from '../../../classes/base/forms/base-control-value-accessor.class';

@Component({
  selector: 'app-input-with-chips',
  templateUrl: './input-with-chips.component.html',
  styleUrls: ['./input-with-chips.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [{ provide: MatFormFieldControl, useExisting: InputWithChipsComponent }]
})
export class InputWithChipsComponent extends BaseControlValueAccessor<object[]> implements OnInit, OnDestroy {
  @HostBinding('class.floating')
  get shouldLabelFloat() {
    return this.focused || !this.empty;
  }

  @Input() inputPlaceholder;
  @Input() autocompleteShowPropertyName = 'name';
  @Input() isRemovableChips = true;
  @Input() isMatChipInputAddOnBlur = true;
  @Input() matChipInputSeparatorKeyCodes: number[] = [ENTER];

  @Input() set autocompleteData(values: object[]) {
    if (!(values && values.length)) {
      this.autocompleteOptions = [];
      return;
    }

    this.autocompleteOptions = values;
    this.changeDetectorRef.markForCheck();
  }

  get autocompleteData(): object[] {
    return this.autocompleteOptions;
  }

  @Output() inputValueIsChanged = new EventEmitter<string>();

  get empty(): boolean {
    return !(this.value && this.value.length);
  }

  get errorState(): boolean {
    return this.ngControl.touched && this.ngControl.invalid;
  }

  public addChipsControl = new FormControl();
  public focused = false;
  public controlType = 'app-input-with-chips';
  public autocompleteOptions: object[] = [];

  constructor(
    @Optional() @Self() public ngControl: NgControl,
    private readonly changeDetectorRef: ChangeDetectorRef,
    private readonly focusMonitor: FocusMonitor,
    private readonly elementRef: ElementRef<HTMLElement>
  ) {
    super();

    this.initNgControl();
    this.initFocusHandler();
  }

  ngOnInit(): void {
    this.handleInputChanges();
  }

  public writeValue(newValue: object[]): void {
    super.writeValue(newValue);
    this.changeDetectorRef.markForCheck();
  }

  public onAddTag(input: HTMLInputElement, value: string): void {
    if (!value.trim()) {
      return;
    }

    const chips = [...this.value];
    const chip = { name: value.trim() };

    if (chip.name) {
      chips.push(chip);
    }

    input.value = '';

    this.updateChips(chips);
  }

  public onRemoveTag(chip: object): void {
    const chips = [...this.value];
    const index = chips.indexOf(chip);

    chips.splice(index, 1);

    this.updateChips(chips);
  }

  public selectedAutocompleteOption(input: HTMLInputElement, selectedOption: object) {
    if (!selectedOption) {
      return;
    }

    const chips = [...this.value, { ...selectedOption }];

    input.value = '';

    this.updateChips(chips);
    this.changeDetectorRef.detectChanges();
  }

  private updateChips(chips: object[]): void {
    this.clearAutocomplete();
    this.writeValue(chips);
    this.stateChanges.next();
    this.onChange(chips);
  }

  private initNgControl(): void {
    if (this.ngControl !== null) {
      // Setting the value accessor directly (instead of using the providers) to avoid running into a circular import.
      this.ngControl.valueAccessor = this;
    }
  }

  private initFocusHandler(): void {
    this.focusMonitor.monitor(this.elementRef, true).subscribe(origin => {
      if (this.disabled) {
        return;
      }

      if (this.focused && !origin) {
        this.onTouched();
        this.stateChanges.next();
      }

      this.focused = !!origin;
    });
  }

  private handleInputChanges(): void {
    if (this.addChipsControl) {
      this.addChipsControl.valueChanges
        .pipe(
          debounceTime(400)
        )
        .subscribe((value: string) => {
          if (!value) {
            this.autocompleteOptions = [];
            this.changeDetectorRef.detectChanges();
            return;
          }

          this.inputValueIsChanged.emit(value);
        });
    }
  }

  private clearAutocomplete(): void {
    if (!this.autocompleteData && !this.autocompleteData.length) {
      return;
    }

    this.addChipsControl.setValue(null);
  }

  ngOnDestroy(): void {
    this.focusMonitor.stopMonitoring(this.elementRef);
    this.stateChanges.complete();
  }
}
