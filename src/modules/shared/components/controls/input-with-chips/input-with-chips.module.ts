import { NgModule } from '@angular/core';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { MaterialModule } from '../../../modules/material.module';
import { SharedModule } from '../../../modules/shared.module';
import { InputWithChipsComponent } from './input-with-chips.component';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    MatAutocompleteModule
  ],
  declarations: [
    InputWithChipsComponent
  ],
  exports: [
    InputWithChipsComponent
  ]
})
export class InputWithChipsModule {}
