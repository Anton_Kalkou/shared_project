import { NgModule } from '@angular/core';

import { NgxMaskModule } from 'ngx-mask';

import { MaterialModule } from '../../modules/material.module';
import { SharedModule } from '../../modules/shared.module';
import { ControlWithMaskComponent } from './control-with-mask.component';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    NgxMaskModule.forRoot()
  ],
  declarations: [
    ControlWithMaskComponent
  ],
  exports: [
    ControlWithMaskComponent
  ]
})
export class ControlWithMaskModule {}
