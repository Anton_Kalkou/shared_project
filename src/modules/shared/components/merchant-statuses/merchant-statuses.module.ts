import { NgModule } from '@angular/core';

import { SharedModule } from '../../modules/shared.module';
import { MerchantStatusesComponent } from './merchant-statuses.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    MerchantStatusesComponent
  ],
  exports: [
    MerchantStatusesComponent
  ]
})
export class MerchantStatusesModule {

}
