import { Component, Input } from '@angular/core';

import { Merchant } from 'src/modules/shared/models/merchant-mock.model';

@Component({
  selector: 'app-merchant-statuses',
  templateUrl: 'merchant-statuses.component.html',
  styleUrls: ['merchant-statuses.component.scss']
})
export class MerchantStatusesComponent {

  @Input() merchant: Merchant;

}
