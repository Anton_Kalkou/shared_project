import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-square-button',
  templateUrl: 'square-button.component.html',
  styleUrls: ['square-button.component.scss']
})
export class SquareButtonComponent {

  @Input() color: string;
  @Input() icon: string;
  @Input() disabled: boolean;

  public reactOnClick(clickEvent: MouseEvent): void {
    if (this.disabled) {
      clickEvent.stopPropagation();
    }
  }

}
