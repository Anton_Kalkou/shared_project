import { NgModule } from '@angular/core';

import { SharedModule } from '../../modules/shared.module';
import { MaterialModule } from '../../modules/material.module';
import { SquareButtonComponent } from './square-button.component';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule
  ],
  declarations: [
    SquareButtonComponent
  ],
  exports: [
    SquareButtonComponent
  ]
})
export class SquareButtonModule {}
