import { Component, Input } from '@angular/core';

import { TranslationService } from '../../services/translation.service';

@Component({
  selector: 'app-popup',
  templateUrl: 'popup.component.html',
  styleUrls: ['popup.component.scss']
})
export class PopupComponent {

  @Input() caption?: string;
  @Input() black: boolean;

  constructor(
    public translationService: TranslationService
  ) {}

}
