import { Component, Input, OnInit } from '@angular/core';

import { NavItem } from 'src/modules/shared/models/nav-item.model';
import { TranslationService } from 'src/modules/shared/services/translation.service';

@Component({
  selector: 'app-nav-item',
  templateUrl: 'nav-item.component.html',
  styleUrls: ['nav-item.component.scss']
})
export class NavItemComponent {

  @Input() navItem: NavItem;

  constructor(
    public translationService: TranslationService
  ) { }

}
