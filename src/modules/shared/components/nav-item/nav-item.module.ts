import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from '../../modules/shared.module';
import { NavItemComponent } from './nav-item.component';

@NgModule({
  imports: [
    SharedModule,
    RouterModule
  ],
  declarations: [
    NavItemComponent
  ],
  exports: [
    NavItemComponent
  ]
})
export class NavItemModule { }
