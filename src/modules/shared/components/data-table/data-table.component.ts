import { Component, EventEmitter, Input, OnChanges, Output, ViewChild } from '@angular/core';
import { SelectionModel } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Select } from '@ngxs/store';

import { Observable } from 'rxjs';
import { ActionEvent, Column } from 'src/modules/shared/models/data-table.model';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnChanges {

  @Select(state => state.root.spinnerVisibility) spinnerVisibility$: Observable<boolean>;
  @Select(state => state.filters.filtersVisibility) filtersVisibility$: Observable<boolean>;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  @Output() select: EventEmitter<any> = new EventEmitter<any>();
  @Output() action: EventEmitter<ActionEvent> = new EventEmitter<ActionEvent>();
  @Output() delete: EventEmitter<any> = new EventEmitter<any>();

  @Input() tableName: string;
  @Input() items: any[];
  @Input() selectedItems: any[];
  @Input() columns: Column[];
  @Input() totalItems: number;
  @Input() pageSize: number;
  @Input() pageSizeOptions: number[];
  @Input() isPaginator = true;
  @Input() isTableDisabled: boolean;

  public dataSource: MatTableDataSource<any>;
  public selection = new SelectionModel<any>(true, []);
  public columnTitles: string[];
  public selectedItem: any;

  constructor() {}

  ngOnChanges(): void {
    this.initializeValues();
  }

  public setPagination(): void {}

  public onChange(item: any): void {
    this.selection.toggle(item);
    this.select.emit(this.selection.selected);
  }

  public onDelete(item: any): void {
    this.delete.emit(item);
  }

  public isAllSelected(): boolean {
    const selectedItemsCount = this.selection.selected.length;
    const sourceItemsCount = this.dataSource.data.length;

    return selectedItemsCount === sourceItemsCount;
  }

  public masterToggle(): void {
    this.isAllSelected() ? this.selection.clear() : this.dataSource.data.forEach(row => this.selection.select(row));
    this.select.emit(this.selection.selected);
  }

  public onActionWithParentInterraction(item: any, column: Column): void {
    this.action.emit({ item, column });
  }

  public getBehaviourType(tableName: string, columnName: string): string {
    return `${tableName}-${columnName}`;
  }

  private initializeValues(): void {
    this.dataSource = new MatTableDataSource(this.items);
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.columnTitles = this.columns.map(column => column.title);

    this.selectedItems && this.handleSelectedItems();
  }

  private handleSelectedItems(): void {
    this.items.forEach(item => {
      const isItemToSelect = !!this.selectedItems.find(selectedItem => selectedItem.id === item.id);

      if (isItemToSelect) {
        this.selection.select(item);
      } else {
        this.selection.isSelected(item) && this.selection.deselect(item);
      }
    });
  }
}
