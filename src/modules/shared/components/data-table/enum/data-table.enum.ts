export enum ValueType {
  Action = 'action',
  Text = 'text',
  Link = 'link',
  TextWithIcon = 'textWithIcon',
  Select = 'select',
  Dropdown = 'dropdown',
  RadioButton = 'radioButton'
}

export enum TextType {
  Array = 'array',
  Simple = 'simple',
  DateTime = 'datetime',
  BankCard = 'bank-card',
  HttpMethod = 'http-method'
}

export enum ActionType {
  SquareButton = 'squareButton',
  CustomIcon = 'customIcon'
}

export enum Icon {
  Visibility = 'visibility',
  Pencil = 'rs-pencil-icon'
}

export enum HeaderType {
  Icon = 'icon'
}
