import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'arrayToString' })
export class ArrayToStringPipe implements PipeTransform {

  transform(data: any[], itemTitle: string): string {
    return data.map(item => item[itemTitle]).join(', ');
  }
}
