import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({ name: 'datetime' })
export class DateTimePipe implements PipeTransform {
  transform(value: string, datetimeFormat: string): string {
    return moment(value).format(datetimeFormat);
  }
}
