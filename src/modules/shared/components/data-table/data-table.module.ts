import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { SharedModule } from 'src/modules/shared/modules/shared.module';

import { DirectivesModule } from '../../directives/directives.module';
import { ConfirmationModule } from '../confirmation/confirmation.module';
import { SquareButtonModule } from '../square-button/square-button.module';
import { DataTableComponent } from './data-table.component';
import { ArrayToStringPipe } from './pipes/array-to-string.pipe';
import { DateTimePipe } from './pipes/datetime.pipe';

@NgModule({
  imports: [
    RouterModule,
    SharedModule,
    MaterialModule,
    SquareButtonModule,
    DirectivesModule,
    ConfirmationModule
  ],
  declarations: [
    DataTableComponent,
    DateTimePipe,
    ArrayToStringPipe
  ],
  exports: [
    DataTableComponent
  ]
})
export class DataTableModule {}
