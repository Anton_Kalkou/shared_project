import { NgModule } from '@angular/core';

import { SharedModule } from '../../modules/shared.module';
import { ModuleHeaderComponent } from './module-header.component';
import { SearchModule } from '../search/search.module';
import { IconButtonModule } from '../icon-button/icon-button.module';
import { SquareButtonModule } from '../square-button/square-button.module';

@NgModule({
  imports: [
    SharedModule,
    SearchModule,
    IconButtonModule,
    SquareButtonModule
  ],
  declarations: [
    ModuleHeaderComponent
  ],
  exports: [
    ModuleHeaderComponent
  ]
})
export class ModuleHeaderModule {}
