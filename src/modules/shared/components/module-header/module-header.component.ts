import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Store } from '@ngxs/store';

import { SetExportVisibility } from '../export/ngxs/export.actions';
import { OpenFilters } from '../filters/ngxs/filters.actions';

@Component({
  selector: 'rs-module-header',
  templateUrl: 'module-header.component.html',
  styleUrls: ['module-header.component.scss']
})
export class ModuleHeaderComponent {

  @Output() search = new EventEmitter<string>();

  @Input() caption: string;
  @Input() exportTitle: string;
  @Input() filtersEndpoint: string;
  @Input() placeholder: string;
  @Input() hideFilter: boolean;

  constructor(
    private store: Store
  ) {}

  public reactOnSearch(key: string): void {
    this.search.emit(key);
  }

  public openExport(): void {
    this.store.dispatch(new SetExportVisibility(true, this.exportTitle));
  }

  public openFilters(): void {
    this.store.dispatch(new OpenFilters(this.filtersEndpoint));
  }

}
