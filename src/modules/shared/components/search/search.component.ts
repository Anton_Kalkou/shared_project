import { Component, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: 'search.component.html',
  styleUrls: ['search.component.scss']
})
export class SearchComponent {

  @Output() search = new EventEmitter<string>();

  @Input() placeholder = 'shared.search';
  @Input() isBig: boolean;

  public reactOnInput(key: string): void {
    this.search.emit(key);
  }

}
