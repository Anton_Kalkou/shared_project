import { NgModule } from '@angular/core';

import { SharedModule } from '../../modules/shared.module';
import { SearchComponent } from './search.component';
import { MaterialModule } from '../../modules/material.module';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule
  ],
  declarations: [
    SearchComponent
  ],
  exports: [
    SearchComponent
  ]
})
export class SearchModule {}
