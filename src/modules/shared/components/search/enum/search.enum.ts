export enum Translation {
  SearchStaff = 'placeholders.search-staff',
  SearchId = 'placeholders.search-id',
  SearchCards = 'placeholders.search-cards'
}
