export class SetExportVisibility {
  static readonly type = '[Export Visibility] Set';

  constructor(
    public exportVisibility: boolean,
    public caption?: string
  ) {}
}
