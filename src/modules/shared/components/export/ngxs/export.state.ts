import { Injectable } from '@angular/core';

import { State, Action, StateContext } from '@ngxs/store';

import { SetExportVisibility } from './export.actions';

interface ExportStateModel {
  exportVisibility: boolean;
  caption: string;
}

@State<ExportStateModel>({
  name: 'export',
  defaults: {
    exportVisibility: false,
    caption: ''
  }
})
@Injectable()
export class ExportState {

  @Action(SetExportVisibility)
  setExportVisibility({patchState}: StateContext<ExportStateModel>, {exportVisibility, caption}: SetExportVisibility) {
    patchState({
      exportVisibility,
      caption
    });
  }
}
