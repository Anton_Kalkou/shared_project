import { Component, OnInit } from '@angular/core';

import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { fromRightToLeft, darkness } from '../../../animations';
import { SetExportVisibility } from '../ngxs/export.actions';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { TranslationService } from 'src/modules/shared/services/translation.service';

@Component({
  selector: 'app-export',
  templateUrl: 'export.component.html',
  styleUrls: ['export.component.scss'],
  animations: [
    fromRightToLeft,
    darkness
  ]
})
export class ExportComponent extends Unsubscribe implements OnInit {

  @Select(state => state.export.exportVisibility) exportVisibility$: Observable<boolean>;
  @Select(state => state.export.caption) caption$: Observable<string>;

  public exportVisibility: boolean;
  public caption: string;

  constructor(
    private store: Store,
    public translationService: TranslationService
  ) {
    super();
  }

  ngOnInit() {
    this.initSubscriptions();
  }

  public closeExport(): void {
    this.store.dispatch(new SetExportVisibility(false));
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.exportVisibility$
      .subscribe(exportVisibility => this.exportVisibility = exportVisibility);
    this.subscribeTo = this.caption$
      .subscribe(caption => this.caption = caption);
  }

}
