import { NgModule } from '@angular/core';

import { NgxsModule } from '@ngxs/store';
import { ClickOutsideModule } from 'ng-click-outside';

import { SharedModule } from '../../../modules/shared.module';
import { ExportComponent } from '../component/export.component';
import { ExportState } from '../ngxs/export.state';
import { MaterialModule } from 'src/modules/shared/modules/material.module';

@NgModule({
  imports: [
    SharedModule,
    ClickOutsideModule,
    NgxsModule.forFeature([ExportState]),
    MaterialModule
  ],
  declarations: [
    ExportComponent
  ],
  exports: [
    ExportComponent
  ]
})
export class ExportModule {}
