import { NgModule } from '@angular/core';

import { AngularCropperjsModule } from 'angular-cropperjs';

import { SharedModule } from '../../modules/shared.module';
import { CustomCropperComponent } from './cropper.component';
import { PopupModule } from '../popup/popup.module';
import { MaterialModule } from '../../modules/material.module';

@NgModule({
  imports: [
    SharedModule,
    PopupModule,
    AngularCropperjsModule,
    MaterialModule
  ],
  declarations: [
    CustomCropperComponent
  ],
  exports: [
    CustomCropperComponent
  ]
})
export class CropperModule {}
