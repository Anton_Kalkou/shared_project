import { Component, Inject, OnInit, ViewChild } from '@angular/core';

import { MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { CropperComponent } from 'angular-cropperjs';
import { MatButtonToggleChange } from '@angular/material/button-toggle';
import { Store } from '@ngxs/store';

import { CropperPopupData } from '../../models/popups-data/cropper.model';
import { APP } from '../../constants';
import { SetPhoto } from 'src/modules/staff/ngxs/staff.actions';

@Component({
  selector: 'app-cropper',
  templateUrl: 'cropper.component.html',
  styleUrls: ['cropper.component.scss']
})
export class CustomCropperComponent implements OnInit {

  @ViewChild('angularCropper') public angularCropper: CropperComponent;

  public imgUrl: string | ArrayBuffer;
  public cropperConfig: any = {
    viewMode: 1,
    dragMode: 'crop',
    preview: '.rs-cropper__content-preview',
    zoomable: true
  };
  public resultImage: string | ArrayBuffer;
  public isCircle = true;

  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData: CropperPopupData,
    private dialog: MatDialog,
    private store: Store
  ) {
    this.imgUrl = this.dialogData.imgUrl;
  }

  ngOnInit() {
    this.setDefaultAspectRation();
  }

  public crop(): void {
    this.dialog.getDialogById(APP.dialogs.cropper).close();
    this.store.dispatch(new SetPhoto(this.angularCropper.cropper.getCroppedCanvas().toDataURL()));
  }

  public reactOnChangeAspectRatio(event: MatButtonToggleChange): void {
    switch (event.value) {
      case APP.cropperAspectRatios.circle:
        this.angularCropper.cropper.setAspectRatio(1);
        this.isCircle = true;
        break;
      case APP.cropperAspectRatios.square:
        this.angularCropper.cropper.setAspectRatio(1);
        this.isCircle = false;
        break;
      case APP.cropperAspectRatios.rectangle:
        this.angularCropper.cropper.setAspectRatio(4 / 3);
        this.isCircle = false;
        break;
      case APP.cropperAspectRatios.rectangleWide:
        this.angularCropper.cropper.setAspectRatio(16 / 9);
        this.isCircle = false;
        break;
      case APP.cropperAspectRatios.free:
        this.angularCropper.cropper.setAspectRatio(null);
        this.isCircle = false;
        break;
      default:
        this.angularCropper.cropper.setAspectRatio(null);
        this.isCircle = false;
    }
  }

  private setDefaultAspectRation(): void {
    this.cropperConfig.aspectRatio = 1;
  }

}
