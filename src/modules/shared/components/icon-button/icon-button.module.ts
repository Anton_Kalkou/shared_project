import { NgModule } from '@angular/core';

import { SharedModule } from '../../modules/shared.module';
import { MaterialModule } from '../../modules/material.module';
import { IconButtonComponent } from './icon-button.component';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule
  ],
  declarations: [
    IconButtonComponent
  ],
  exports: [
    IconButtonComponent
  ]
})
export class IconButtonModule {}
