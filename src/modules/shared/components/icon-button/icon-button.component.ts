import { Component, Input } from '@angular/core';

import { TranslationService } from '../../services/translation.service';

@Component({
  selector: 'app-icon-button',
  templateUrl: 'icon-button.component.html',
  styleUrls: ['icon-button.component.scss']
})
export class IconButtonComponent {

  @Input() icon: string;
  @Input() caption: string;

  constructor(
    public translationService: TranslationService
  ) {}

}
