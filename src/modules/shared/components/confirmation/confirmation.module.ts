import { NgModule } from '@angular/core';

import { SharedModule } from '../../modules/shared.module';
import { ConfirmationComponent } from './confirmation.component';
import { OpenConfirmationDirective } from './open-confirmation.directive';
import { MaterialModule } from '../../modules/material.module';
import { PopupModule } from '../popup/popup.module';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    PopupModule
  ],
  declarations: [
    ConfirmationComponent,
    OpenConfirmationDirective
  ],
  exports: [
    ConfirmationComponent,
    OpenConfirmationDirective
  ]
})
export class ConfirmationModule {}
