import { Directive, Input, HostListener, Output, EventEmitter } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';

import { ConfirmationComponent } from './confirmation.component';
import { ResolutionService } from '../../services/resolution.service';

@Directive({
  selector: '[appOpenConfirmation]'
})
export class OpenConfirmationDirective {

  @Output() accept = new EventEmitter<void>();
  @Output() decline = new EventEmitter<void>();

  @Input() caption: string;
  @Input() highlighted: string;
  @Input() acceptTitle: string;
  @Input() declineTitle: string;
  @Input() popupTitle: string;

  constructor(
    private dialog: MatDialog,
    private resolutionService: ResolutionService
  ) {}

  @HostListener('click')
  openConfirmationDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationComponent, {
      data: {
        caption: this.caption,
        popupTitle: this.popupTitle,
        highlighted: this.highlighted,
        acceptTitle: this.acceptTitle,
        declineTitle: this.declineTitle
      },
      width: this.resolutionService.tabletAndLess() ? '100%' : '800px'
    });

    dialogRef.afterClosed()
      .subscribe(result => {
        if (result) {
          this.accept.emit();
        } else {
          this.decline.emit();
        }
      });
  }

}
