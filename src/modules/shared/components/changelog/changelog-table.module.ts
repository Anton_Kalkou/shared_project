import { NgModule } from '@angular/core';

import { SharedModule } from '../../modules/shared.module';
import { ChangelogTableComponent } from './components/changelog-table/changelog-table.component';
import { MaterialModule } from '../../modules/material.module';
import { ChangelogMobileItemComponent } from './components/changelog-mobile-item/changelog-mobile-item.component';
import { SquareButtonModule } from '../square-button/square-button.module';
import { OpenChangelogDetailsDirective } from './directives/open-changelog-details.directive';
import { ChangelogDetailsComponent } from './components/changelog-details/changelog-details.component';
import { PopupModule } from '../popup/popup.module';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    SquareButtonModule,
    PopupModule
  ],
  declarations: [
    ChangelogTableComponent,
    ChangelogMobileItemComponent,
    OpenChangelogDetailsDirective,
    ChangelogDetailsComponent
  ],
  exports: [
    ChangelogTableComponent,
    ChangelogMobileItemComponent
  ]
})
export class ChangelogModule {}
