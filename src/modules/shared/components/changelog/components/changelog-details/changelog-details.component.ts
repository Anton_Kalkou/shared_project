import { Component, Inject, OnInit } from '@angular/core';

import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Changelog } from 'src/modules/shared/models/changelog.model';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'rs-changelog-details',
  templateUrl: 'changelog-details.component.html',
  styleUrls: ['changelog-details.component.scss']
})
export class ChangelogDetailsComponent implements OnInit {

  public displayedColumns: string[] = [
    'field',
    'from',
    'to'
  ];
  public changelogForm: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public changelog: Changelog
  ) {}

  ngOnInit() {
    this.initForm();
  }

  private initForm(): void {
    this.changelogForm = new FormGroup({
      date: new FormControl(this.changelog.date),
      time: new FormControl(this.changelog.time),
      change: new FormControl(this.changelog.change),
      action: new FormControl(this.changelog.action),
      madeBy: new FormControl(this.changelog.madeBy)
    });
    this.changelogForm.disable();
  }

}
