import { Component, Input } from '@angular/core';

import { Changelog } from 'src/modules/shared/models/changelog.model';

@Component({
  selector: 'app-changelog-mobile-item',
  templateUrl: 'changelog-mobile-item.component.html',
  styleUrls: ['changelog-mobile-item.component.scss']
})
export class ChangelogMobileItemComponent {

  @Input() changelog: Changelog;

}
