import { Component, Input, OnInit, ViewChild, ChangeDetectorRef, OnChanges } from '@angular/core';

import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { Changelog } from 'src/modules/shared/models/changelog.model';
import { RenderOnResize } from 'src/modules/shared/classes/render-on-resize.class';
import { ResolutionService } from 'src/modules/shared/services/resolution.service';

@Component({
  selector: 'app-changelog-table',
  templateUrl: 'changelog-table.component.html',
  styleUrls: ['changelog-table.component.scss']
})
export class ChangelogTableComponent extends RenderOnResize implements OnInit, OnChanges {

  @Input() changelogItems: Changelog[];

  @ViewChild(MatSort, { static: true }) sort: MatSort;

  public displayedColumns = [
    'date',
    'madeBy',
    'field',
    'action',
    'details'
  ];
  public sourceData: MatTableDataSource<Changelog>;

  constructor(
    private resolutionService: ResolutionService,
    protected changeDetectorRef: ChangeDetectorRef
  ) {
    super(changeDetectorRef);
  }

  ngOnInit() {
    this.initTable();
  }

  ngOnChanges() {
    this.initTable();
  }

  public tableVisibility(): boolean {
    return !this.resolutionService.mobileOnly();
  }

  public mobileItemsVisibility(): boolean {
    return this.resolutionService.mobileOnly();
  }

  private initTable(): void {
    this.sourceData = new MatTableDataSource(this.changelogItems);
    this.sourceData.sort = this.sort;
  }

}
