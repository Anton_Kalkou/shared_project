import { Directive, HostListener, Input } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';

import { ChangelogDetailsComponent } from '../components/changelog-details/changelog-details.component';
import { Changelog } from 'src/modules/shared/models/changelog.model';

@Directive({
  selector: '[rsOpenChangelogDetails]'
})
export class OpenChangelogDetailsDirective {

  @Input() changelog: Changelog;

  constructor(
    private dialog: MatDialog
  ) {}

  @HostListener('click')
  openChangelogDetails(): void {
    this.dialog.open(ChangelogDetailsComponent, {
      data: this.changelog,
      width: '1200px'
    });
  }

}
