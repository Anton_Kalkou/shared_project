import { NgModule } from '@angular/core';

import { SharedModule } from '../../modules/shared.module';
import { UserIconComponent } from './user-icon.component';

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    UserIconComponent
  ],
  exports: [
    UserIconComponent
  ]
})
export class UserIconModule {}
