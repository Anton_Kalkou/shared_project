import { NgModule } from '@angular/core';

import { SharedModule } from '../../modules/shared.module';
import { AddButtonComponent } from './add-button.component';
import { MaterialModule } from '../../modules/material.module';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule
  ],
  declarations: [
    AddButtonComponent
  ],
  exports: [
    AddButtonComponent
  ]
})
export class AddButtonModule {}
