import { FormControl } from '@angular/forms';

import * as moment from 'moment';

export const validateDate = (formControl: FormControl) => {
  const momentDate = moment(formControl.value);

  return momentDate.get('year') < 1900 ? {
    validateDate: {
      valid: false
    }
  } : null;
};
