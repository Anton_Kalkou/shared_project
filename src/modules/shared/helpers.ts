import { AbstractControl, FormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

export const copy = (object: any): any => JSON.parse(JSON.stringify(object));

export const isBetween = <T>(value: T, first: T, second: T, isEqual?: boolean): boolean => {
  return isEqual
    ? value >= first && value <= second
    : value > first && value < second;
};

export const isSubstring = (source: string, substring: string, isCaseSensitive?: boolean): boolean =>
  isCaseSensitive
    ? source.indexOf(substring) !== -1
    : source.toLowerCase().indexOf(substring.toLowerCase()) !== -1;

export const isFormValid = (form: FormGroup) => form.valid;

export const getEntityRouteId = (route: ActivatedRoute, idKey?: string): string => {
  return route.snapshot.paramMap.get(idKey ? idKey : 'id');
};

export function JsonValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const error: ValidationErrors = { jsonInvalid: true };

    try {
      JSON.parse(control.value);
    } catch (e) {
      control.setErrors(error);
      return error;
    }

    control.setErrors(null);
    return null;
  };
}
