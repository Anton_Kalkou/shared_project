import { trigger, transition, style, animate } from '@angular/animations';

export const fromRightToLeft =
trigger('fromRightToLeft', [
  transition(':enter', [
    style({transform: 'translateX(100%)'}),
    animate(150, style({transform: 'translateX(0)'}))
  ]),
  transition(':leave', [
    style({transform: 'translateX(0)'}),
    animate(150, style({transform: 'translateX(100%)'}))
  ])
]);

export const fromLeftToRight =
trigger('fromLeftToRight', [
  transition(':enter', [
    style({transform: 'translateX(-100%)'}),
    animate(150, style({transform: 'translateX(0)'}))
  ]),
  transition(':leave', [
    style({transform: 'translateX(0)'}),
    animate(150, style({transform: 'translateX(-100%)'}))
  ])
]);

export const fade =
trigger('fade', [
  transition(':enter', [
    style({opacity: 0}),
    animate(300, style({opacity: 1}))
  ]),
  transition(':leave', [
    style({opacity: 1}),
    animate(300, style({opacity: 0}))
  ])
]);

export const expansion =
trigger('expansion', [
  transition(':enter', [
    style({width: 0, opacity: 0}),
    animate(200, style({width: '*', opacity: 1}))
  ]),
  transition(':leave', [
    style({width: '*', opacity: 1}),
    animate(200, style({width: 0, opacity: 0}))
  ])
]);

export const scale =
trigger('scale', [
  transition(':enter', [
    style({transform: 'scale(0)', opacity: 0}),
    animate(100, style({transform: 'scale(1)', opacity: 1}))
  ]),
  transition(':leave', [
    style({transform: 'scale(1)', opacity: 1}),
    animate(100, style({transform: 'scale(0)', opacity: 0}))
  ])
]);

export const darkness =
trigger('darkness', [
  transition(':enter', [
    style({backgroundColor: 'transparent'}),
    animate(300, style({backgroundColor: 'rgba(0,0,0,.6)'}))
  ]),
  transition(':leave', [
    style({backgroundColor: 'rgba(0,0,0,.6)'}),
    animate(300, style({backgroundColor: 'transparent'}))
  ])
]);

export const height =
trigger('height', [
  transition(':enter', [
    style({height: 0}),
    animate(100, style({height: '*'}))
  ]),
  transition(':leave', [
    style({height: '*'}),
    animate(100, style({height: 0}))
  ])
]);
