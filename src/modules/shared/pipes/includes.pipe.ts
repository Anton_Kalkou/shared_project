import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'includes'
})
export class IncludesPipe implements PipeTransform {
  transform(input: any[], args: any[]): boolean {
    return args.some(el => input.includes(el));
  }
}
