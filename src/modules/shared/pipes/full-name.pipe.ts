import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fullName'
})
export class FullNamePipe implements PipeTransform {

  public transform(object: any): string {
    return `${object?.firstName ? object?.firstName : ''} ${object?.lastName ? object?.lastName : ''}`;
  }

}
