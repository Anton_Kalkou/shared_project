import { NgModule } from '@angular/core';

import { FullNamePipe } from './full-name.pipe';
import { StartCasePipe } from './start-case.pipe';
import { IncludesPipe } from './includes.pipe';
import { LocationPipe } from './location.pipe';
import { ShortNumberPipe } from './short-number.pipe';

@NgModule({
  declarations: [
    FullNamePipe,
    StartCasePipe,
    IncludesPipe,
    ShortNumberPipe,
    LocationPipe
  ],
  exports: [
    FullNamePipe,
    StartCasePipe,
    IncludesPipe,
    ShortNumberPipe,
    LocationPipe
  ]
})
export class PipesModule {
}
