import { Pipe, PipeTransform } from '@angular/core';

import { Address } from '../models/merchant.model';

@Pipe({
  name: 'location'
})
export class LocationPipe implements PipeTransform {

  transform(object: Address): string {
    return `${object?.city}, ${object?.country}`;
  }

}
