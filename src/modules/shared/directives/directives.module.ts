import { NgModule } from '@angular/core';

import { NumericControlDirective } from './numeric-control.directive';
import { OpenRefundDirective } from './open-refund.directive';
import { PercentDirective } from './percent.directive';
import { PriceDirective } from './price.directive';
import { ClickAndHoldDirective } from './click-and-hold.directive';
import { BehaviourDirective } from './bahaviour.directive';

@NgModule({
  declarations: [
    NumericControlDirective,
    OpenRefundDirective,
    PercentDirective,
    PriceDirective,
    ClickAndHoldDirective,
    BehaviourDirective
  ],
  exports: [
    NumericControlDirective,
    OpenRefundDirective,
    PercentDirective,
    PriceDirective,
    ClickAndHoldDirective,
    BehaviourDirective
  ]
})
export class DirectivesModule {}
