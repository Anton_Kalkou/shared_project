import { Directive, OnInit, ElementRef } from '@angular/core';

import { fromEvent } from 'rxjs';

import { Unsubscribe } from '../classes/unsubscribe.class';

@Directive({
  selector: '[rsPercent]'
})
export class PercentDirective extends Unsubscribe implements OnInit {

  private allowedKeys = ['Backspace', 'Delete', 'ArrowLeft', 'ArrowRight', 'Tab'];
  private input: HTMLInputElement;
  private postfix: HTMLInputElement;

  constructor(
    private matFormFieldRef: ElementRef<HTMLInputElement>
  ) {
    super();
  }

  ngOnInit() {
    this.getInput();
    this.getPostfix();
    this.placeSuffix();
    this.subscribeToInput();
    this.subscribeToBlur();
  }

  private placeSuffix(): void {
    setTimeout(() => {
      this.postfix.style.left = `${this.input.value.length * 10.1}px`;
    });
  }

  private subscribeToInput(): void {
    this.subscribeTo = fromEvent(this.input, 'keydown')
      .subscribe((event: KeyboardEvent) => {
        if (this.input.value.length > 2 && !this.allowedKeys.includes(event.key)) {
          event.preventDefault();
        } else {
          this.placeSuffix();
        }
      });
  }

  private subscribeToBlur(): void {
    this.subscribeTo = fromEvent(this.input, 'blur')
      .subscribe(() => {
        if (!this.input.value) {
          this.input.value = '0';
        }

        this.placeSuffix();
      });
  }

  private getInput(): void {
    this.input = this.matFormFieldRef.nativeElement.querySelector('input');
  }

  private getPostfix(): void {
    this.postfix = this.matFormFieldRef.nativeElement.querySelector('#my-postfix');
  }

}
