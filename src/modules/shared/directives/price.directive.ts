import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[rsPrice]'
})
export class PriceDirective implements OnInit {

  constructor(
    private inputRef: ElementRef
  ) {}

  ngOnInit() {
    this.processValue();
  }

  private processValue(): void {
    const [dollars, penny] = this.inputRef.nativeElement.value.split('.');
    const resultDollars = Number(dollars) > 9 ? dollars : `0${dollars}`;

    if (penny === undefined) {
      this.inputRef.nativeElement.value = `${resultDollars}.00`;
    } else {
      const resultPenny = penny.length > 1 ? penny : `${penny}0`;

      this.inputRef.nativeElement.value = `${resultDollars}.${resultPenny}`;
    }
  }

}
