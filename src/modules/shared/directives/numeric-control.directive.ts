import { Directive, HostListener, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appNumericControl]'
})
export class NumericControlDirective implements OnInit {

  @Input() exceptions: string[] = [];

  private allowedKeys = ['Backspace', 'Delete', 'ArrowLeft', 'ArrowRight', '.', 'Tab'];

  ngOnInit() {
    this.allowedKeys = this.allowedKeys.concat(this.exceptions);
  }

  @HostListener('keydown', ['$event'])
  inputHandler(event: KeyboardEvent): void {
    const value = Number(event.key);

    if (event.key === 'Control') {
      this.allowedKeys.push('v');
    }
    if (value !== value && !this.allowedKeys.includes(event.key)) {
      event.preventDefault();
    }
  }

  @HostListener('keyup', ['$event'])
  keyUp(event: KeyboardEvent): void {
    if (event.key === 'Control') {
      this.allowedKeys.pop();
    }
  }

}
