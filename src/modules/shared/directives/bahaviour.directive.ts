import { Directive, ElementRef, Input } from '@angular/core';

import { Behavior } from '../enums';
import { PaymentBehaviourService } from '../../support/components/payment/services/behaviour.service';
import { SystemsHealthBehaviourService } from '../../support/components/systems-health/services/behaviour.service';
import { HttpMethodService } from '../services/http-method.service';

@Directive({
  selector: '[appBehaviour]'
})
export class BehaviourDirective {
  
  @Input() behaviourType: string;
  @Input() value: any;

  constructor(
    private readonly el: ElementRef,
    private readonly systemsHealthBegaviourService: SystemsHealthBehaviourService,
    private readonly paymentBehaviourService: PaymentBehaviourService,
    private readonly httpMethodService: HttpMethodService
  ) {}

  ngOnInit(): void {
    switch(this.behaviourType) {
      case Behavior.TransactionStatus: {
        return this.paymentBehaviourService.handleTransactionsStatus(this.el, this.value);
      }
      case Behavior.SystemsHealthStatus: {
        return this.systemsHealthBegaviourService.handleStatus(this.el, this.value);
      }
      case Behavior.WifiSignal: {
        return this.systemsHealthBegaviourService.handleWifiSignal(this.el, this.value);
      }
      case Behavior.WifiSignalIcon: {
        return this.systemsHealthBegaviourService.handleWifiSignalIcon(this.el, this.value);
      }
      case Behavior.BatteryChargingIcon: {
        return this.systemsHealthBegaviourService.handleBatteryChargingIcon(this.el, this.value);
      }
      case Behavior.Bluetooth: {
        return this.systemsHealthBegaviourService.handleBluetooth(this.el, this.value);
      }
      case Behavior.CellularSignal: {
        return this.systemsHealthBegaviourService.handleCellularSignal(this.el, this.value);
      }
      case Behavior.HttpMethod: {
        return this.httpMethodService.setHttpMethodClass(this.el, this.value);
      }
    }
  }
}
