import { Directive, HostListener } from '@angular/core';

import { MatDialog } from '@angular/material/dialog';

import { APP } from 'src/modules/shared/constants';

@Directive({
  selector: '[appOpenRefund]'
})
export class OpenRefundDirective {

  constructor(
    private dialog: MatDialog
  ) {}

  @HostListener('click')
  openRefundDialog(): void {
    // this.dialog.open(RefundComponent, {
    //   width: '800px',
    //   id: APP.dialogs.requestRefound
    // });
  }

}
