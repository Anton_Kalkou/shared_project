import { Injectable } from '@angular/core';

import { fromEvent } from 'rxjs';

import { APP } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class ResolutionService {

  private isHugeDesktop: boolean;
  private isDesktop: boolean;
  private isLaptop: boolean;
  private isTablet: boolean;
  private isMobile: boolean;

  constructor() {
    this.subscribeToWindowResize();
    this.setCurrentDeviceType(window.outerWidth);
  }

  public mobileOnly(): boolean {
    return this.isMobile;
  }

  public tabletOnly(): boolean {
    return this.isTablet;
  }

  public laptopOnly(): boolean {
    return this.isLaptop;
  }

  public desktopOnly(): boolean {
    return this.isDesktop;
  }

  public hugeDesktopOnly(): boolean {
    return this.isHugeDesktop;
  }

  public tabletAndLess(): boolean  {
    return this.isTablet || this.isMobile;
  }

  public laptopAndLess(): boolean {
    return this.isLaptop || this.tabletAndLess();
  }

  public desktopAndLess(): boolean {
    return this.isDesktop || this.laptopAndLess();
  }

  public desktopAndMore(): boolean {
    return this.isDesktop || this.isHugeDesktop;
  }

  public laptopAndMore(): boolean {
    return this.isLaptop || this.desktopAndMore();
  }

  public tabletAndMore(): boolean {
    return this.isTablet || this.laptopAndMore();
  }

  private clearDeviceTypes(): void {
    this.isDesktop = false;
    this.isLaptop = false;
    this.isTablet = false;
    this.isMobile = false;
    this.isHugeDesktop = false;
  }

  private subscribeToWindowResize(): void {
    fromEvent(window, 'resize')
      .subscribe((event: Event) => {
        const currentScreenWidth = (event.target as Window).outerWidth;

        this.setCurrentDeviceType(currentScreenWidth);
      });
  }

  private setCurrentDeviceType(currentScreenWidth: number): void {
    if (currentScreenWidth > APP.breakpoints.hugeDesktop.value) {
      this.changeCurrentDeviceType(APP.breakpoints.hugeDesktop.title);
    } else
    if (currentScreenWidth > APP.breakpoints.desktop.value) {
      this.changeCurrentDeviceType(APP.breakpoints.desktop.title);
    } else
    if (currentScreenWidth > APP.breakpoints.laptop.value) {
      this.changeCurrentDeviceType(APP.breakpoints.laptop.title);
    } else
    if (currentScreenWidth > APP.breakpoints.tablet.value) {
      this.changeCurrentDeviceType(APP.breakpoints.tablet.title);
    } else {
      this.changeCurrentDeviceType(APP.breakpoints.mobile.title);
    }
  }

  private changeCurrentDeviceType(currentDevice: string): void {
    this.clearDeviceTypes();
    this[currentDevice] = true;
  }

}
