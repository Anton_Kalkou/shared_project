import { Injectable } from '@angular/core';

import { NotificationsService, NotificationType, Notification } from 'angular2-notifications';
import { ActionNotificationButtonClass } from '../enums';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private notification: Notification;
  private options = {
    timeOut: 5000,
    showProgressBar: true,
    pauseOnHover: true,
    clickToClose: true
  };

  constructor(private notificationsService: NotificationsService) { }

  public showSuccessMessage(title: string, content?: string, options?: any): void {
    this.notificationsService.success(title, content, { ...this.options, ...options });
  }

  public showErrorMessage(title: string, content?: string, options?: any): void {
    this.notificationsService.error(title, content, { ...this.options, ...options });
  }

  public showActionMessage(
    html: any,
    type: NotificationType,
    actionClass: string,
    actionCallback?: Function,
    closeCallback?: Function,
    options?: any,
    icon?: string,
    context?: any
  ): void {
    this.notification = this.notificationsService.html(html, type, { ...this.options, ...options }, icon, context);

    this.notification.click.subscribe((event: MouseEvent) => {
      if ((event.target as HTMLElement).className.indexOf(actionClass) !== -1) {
        if (ActionNotificationButtonClass.Undo) {
          this.notificationsService.remove(this.notification.id);
        }

        if (actionCallback) {
          actionCallback();
        }
      }
    });

    this.notification.timeoutEnd.subscribe(() => {
      if (closeCallback) {
        closeCallback();
      }
    });
  }

  public removeNotification(): void {
    if (this.notification) {
      this.notificationsService.remove(this.notification.id);
    }
  }
}
