import { Injectable } from '@angular/core';

import { LocalStorageService } from './local-storage.service';
import { APP } from '../constants';
import { Authentication } from '../models/authentication.model';

@Injectable({
  providedIn: 'root'
})
export class SharedDataService  {

  public merchantProfileId: string;

  private authenticationData$: Authentication;

  constructor(
    private localStorageService: LocalStorageService
  ) {
    this.authenticationData = this.localStorageService.getCachedData(APP.cache.authentication);
  }

  set authenticationData(authenticationData: Authentication) {
    this.authenticationData$ = authenticationData;
    this.localStorageService.cacheData(APP.cache.authentication, authenticationData);
  }

  get authenticationData(): Authentication {
    return this.authenticationData$;
  }

}
