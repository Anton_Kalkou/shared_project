import { Injectable } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  constructor(
    private readonly translateService: TranslateService
  ) {}

  public translate(key: string): Observable<string> {
    return this.translateService.get(key);
  }
}
