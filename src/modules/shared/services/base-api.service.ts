import { HttpClient, HttpParams } from '@angular/common/http';

import { Observable } from 'rxjs';

import { CustomHttpParamEncoder } from '../http-params.encoder';

export class BaseApiService {

  constructor(
    protected apiUrl: string,
    protected http: HttpClient
  ) {}

  public get(endpoint: string, queryParams?: any): Observable<any> {
    const httpParams = new HttpParams({
      // encoder: new CustomHttpParamEncoder(),
      fromObject: queryParams
    });

    return this.http.get<any>(`${this.apiUrl}/${endpoint}`, {
      params: httpParams
    });
  }

  public post(endpoint: string, body: any): Observable<any> {
    return this.http.post(`${this.apiUrl}/${endpoint}`, body);
  }

  public put(endpoint: string, body: any): Observable<any> {
    return this.http.put(`${this.apiUrl}/${endpoint}`, body);
  }

  public patch(endpoint: string, body: any): Observable<any> {
    return this.http.patch(`${this.apiUrl}/${endpoint}`, body);
  }

  public delete(endpoint: string, queryParams?: any): Observable<any> {
    const httpParams = new HttpParams({
      encoder: new CustomHttpParamEncoder(),
      fromObject: queryParams
    });

    return this.http.delete<any>(`${this.apiUrl}/${endpoint}`, {
      params: httpParams
    });
  }

  public setApiUrl(apiUrl: string): void {
    this.apiUrl = apiUrl;
  }
}
