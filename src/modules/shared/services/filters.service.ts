
import { Injectable } from '@angular/core';
import { isArray, isNull } from 'lodash';

import { FilterType } from 'src/modules/support/enum/support.enum';

import { isSubstring } from '../helpers';
import { FilterGroup, ParentFilter } from '../models/filter.model';

@Injectable({ providedIn: 'root' })
export class FiltersService {
  public handleFilteringDataOnSearch(entity: any, source: any[], filteringDataMethod: Function, key: string, searchColumnKey?: string): void {
    let filteredData: any[];
    let isFilteredSource: boolean;
    let filterType: string;

    if (!entity.filterType) {
      filteredData = filteringDataMethod(source, FilterType.Search, key, searchColumnKey);
      isFilteredSource = true;
      filterType = FilterType.Search;
    } else if (entity.filterType === FilterType.Filters && !key) {
      filteredData = filteringDataMethod(entity.filteredFiltersSource, FilterType.Search, key, searchColumnKey);
      isFilteredSource = true;
    } else if (entity.filterType === FilterType.Search && !key) {
      filteredData = filteringDataMethod(source, FilterType.Search, key, searchColumnKey);
      isFilteredSource = false;
      filterType = null;
    } else if (entity.filterType === FilterType.Filters && entity.filteredFiltersSource && entity.filteredFiltersSource.length) {
      filteredData = filteringDataMethod(entity.filteredFiltersSource, FilterType.Search, key, searchColumnKey);
      isFilteredSource = true;
    } else if (entity.filterType === FilterType.Search || !entity.filteredFiltersSource || !entity.filteredSearchSource) {
      filteredData = filteringDataMethod(source, FilterType.Search, key, searchColumnKey);
      isFilteredSource = true;
      filterType = FilterType.Search;
    } else if (entity.filterType === FilterType.Filters && !entity.filteredFiltersSource && !entity.filteredSearchSource) {
      filteredData = filteringDataMethod(source, FilterType.Search, key, searchColumnKey);
      isFilteredSource = true;
      filterType = FilterType.Search;
    }

    this.setFilteredData(entity, filteredData, FilterType.Search, isFilteredSource, filterType);
  }

  public handleFilteringDataOnFilters(entity: any, source: any[], filteringDataMethod: Function, filtersValues: FilterGroup[]): void {
    let filteredData: any[];
    let isFilteredSource: boolean;
    let filterType: string;

    if (!filtersValues.length && !entity.filterType) {
      entity.filterType = undefined;
      return;
    } else if (!entity.filterType) {
      filteredData = filteringDataMethod(source, FilterType.Filters, filtersValues);
      isFilteredSource = true;
      filterType = FilterType.Filters;
    } else if (entity.filterType !== FilterType.Filters) {
      filteredData = filteringDataMethod(entity.filteredSearchSource, FilterType.Filters, filtersValues);
      isFilteredSource = true;
    } else if (!filtersValues.length) {
      filteredData = source;
      isFilteredSource = false;
    } else {
      if (entity.filteredSearchSource && entity.filteredSearchSource.length) {
        filteredData = filteringDataMethod(entity.filteredSearchSource, FilterType.Filters, filtersValues);
        isFilteredSource = true;
      } else {
        filteredData = filteringDataMethod(source, FilterType.Filters, filtersValues);
        isFilteredSource = true;
        filterType = FilterType.Filters;
      }
    }

    this.setFilteredData(entity, filteredData, FilterType.Filters, isFilteredSource, filterType);
  }

  public filterDataOnSearch(source: any[], filterValue: string, searchColumnKey: string): any[] {
    if (filterValue && source) {
      return source.filter(sourceItem => {
        if (searchColumnKey) {
          return isSubstring(sourceItem[searchColumnKey] ? sourceItem[searchColumnKey].toString() : '', filterValue);
        } else {
          return Object.values(sourceItem).some(value => isSubstring(value ? value.toString() : '', filterValue));
        }
      });
    } else {
      return source;
    }
  }

  public setFilteredData(entity: any, filteredData: any[], filteredSourceType: string, isFilteredSource: boolean, filterType?: string | null): void {
    if (filteredSourceType === FilterType.Search) {
      entity.filteredSearchSource = isFilteredSource ? filteredData : undefined;
    }
    if (filteredSourceType === FilterType.Filters) {
      entity.filteredFiltersSource = isFilteredSource ? filteredData : undefined;
    }
    if (isNull(filterType)) {
      entity.filterType = undefined;
    }
    if (filterType) {
      entity.filterType = filterType;
    }

    entity.filteredSource = filteredData;
  }

  public getFiltersValues(activatedFilters: ParentFilter[]): FilterGroup[] {
    const titles = this.getActivatedFiltersTitles(activatedFilters);
    const filtersValues = titles.map(title => {
      const filters = activatedFilters
        .filter(activatedFilter => activatedFilter.parentFilterTitle === title)
        .map(activatedFilter => activatedFilter.parentFilterOption);

      return { title, filters };
    });

    return filtersValues;
  }

  public getActivatedFiltersTitles(activatedFilters: ParentFilter[]): string[] {
    const notUniqueTitles = activatedFilters.map(activatedFilter => activatedFilter.parentFilterTitle);

    return [...new Set(notUniqueTitles)];
  }

  public isFilterValueMatch(sourceColumnValue: string | string[], filters: string[]): boolean {
    const columnDataValues = isArray(sourceColumnValue)
      ? sourceColumnValue
      : sourceColumnValue.split(', ');

    return filters.some(filter => columnDataValues.includes(filter));
  }
}
