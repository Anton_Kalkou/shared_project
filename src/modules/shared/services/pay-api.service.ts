import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseApiService } from './base-api.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PayApiService extends BaseApiService {

  constructor(protected http: HttpClient) {
    super(environment.payApiURL, http);
  }
}
