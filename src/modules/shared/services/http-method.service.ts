import { ElementRef, Injectable } from '@angular/core';
import { HttpMethod, HttpMethodClass } from '../enums';

@Injectable({ providedIn: 'root' })
export class HttpMethodService {

  public setHttpMethodClass(element: ElementRef, method: string): void {
    let httpMethodClass;

    switch(method) {
      case HttpMethod.Get: {
        httpMethodClass = HttpMethodClass.Get;
        break;
      }
      case HttpMethod.Post: {
        httpMethodClass = HttpMethodClass.Post;
        break;
      }
      case HttpMethod.Put: {
        httpMethodClass = HttpMethodClass.Put;
        break;
      }
      case HttpMethod.Delete: {
        httpMethodClass = HttpMethodClass.Delete;
        break;
      }
    }

    element.nativeElement.classList.add(httpMethodClass);
  }
}
