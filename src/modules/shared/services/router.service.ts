import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RouterService {

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {}

  public navigateToPage(path: string): void {
    this.router.navigate([path]);
  }

  public navigateToPageWithSameQueryParams(path: string): void {
    const queryParams = this.route.snapshot.queryParams;

    this.router.navigate([path], {queryParams});
  }

  public navigateToPageWithQueryParams(path: string, queryParams: any): void {
    this.router.navigate([path], {queryParams});
  }

  public navigateToPageWithState(path: string, stateData: any): void {
    this.router.navigate([path], {state: {stateData}});
  }

}
