import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Fee, FeeItem } from '../models/fee.model';
import { requiredOneField } from '../validators/or.validator';
import { MatRadioChange } from '@angular/material/radio';

export class BaseFee {

  public feeItemsForms: FormGroup[] = [];
  public isEdit: boolean;

  protected fee: Fee;

  constructor(
    protected formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.initForm();
  }

  public changeSelected(change: MatRadioChange, index: number): void {
    this.fee.items.forEach(feeItem => feeItem.selected = false);
    this.fee.items[index].selected = true;
    this.fee.items.forEach((feeItem: FeeItem, index: number) => this.feeItemsForms[index].controls.selected.reset(feeItem.selected));
  }

  public disabledSaveButton(): boolean {
    return this.feeItemsForms.some(form => form.invalid);
  }

  public toggleIsEdit(): void {
    this.isEdit = !this.isEdit;
    this.enableOrDisable();
  }

  private initForm(): void {
    this.fee.items.forEach((feeItem: FeeItem) => {
      this.feeItemsForms.push(this.formBuilder.group({
        selected: {value: feeItem.selected, disabled: true},
        percent: [{value: feeItem.percent, disabled: true}, [Validators.min(0), Validators.max(100)]],
        price: {value: feeItem.price, disabled: true}
      }, {
        validators: requiredOneField('percent', 'price')
      }));
    });
  }

  private enableOrDisable(): void {
    if (this.isEdit) {
      this.feeItemsForms.forEach(feeItemForm => feeItemForm.enable());
    } else {
      this.feeItemsForms.forEach(feeItemForm => feeItemForm.disable());
    }
  }

}
