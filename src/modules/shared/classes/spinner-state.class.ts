import { Store } from '@ngxs/store';

import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';

export class SpinnerState {

  constructor(
    protected store: Store
  ) {}

  protected showSpinner(): void {
    this.store.dispatch(new SetSpinnerVisibility(true));
  }

  protected hideSpinner(): void {
    this.store.dispatch(new SetSpinnerVisibility(false));
  }

}
