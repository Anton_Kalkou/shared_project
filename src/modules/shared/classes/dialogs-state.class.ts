import { Store } from '@ngxs/store';

import { MatDialog } from '@angular/material/dialog';

import { SpinnerState } from './spinner-state.class';


export class DialogsState extends SpinnerState {

  constructor(
    protected store: Store,
    protected dialog: MatDialog
  ) {
    super(store);
  }

  protected hideSpinnerAndClosePopups(): void {
    this.hideSpinner();
    this.dialog.closeAll();
  }

}
