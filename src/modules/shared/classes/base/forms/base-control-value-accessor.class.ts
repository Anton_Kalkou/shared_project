import { ControlValueAccessor, NgControl } from '@angular/forms';
import { MatFormFieldControl } from '@angular/material/form-field';

import { Subject } from 'rxjs';

export abstract class BaseControlValueAccessor<T> implements ControlValueAccessor, MatFormFieldControl<T> {
  public disabled = false;
  public value: T;

  readonly empty: boolean;
  readonly errorState: boolean;
  readonly focused: boolean;
  readonly id: string;
  readonly ngControl: NgControl | null;
  readonly placeholder: string;
  readonly required: boolean;
  readonly shouldLabelFloat: boolean;

  public stateChanges = new Subject<void>();

  /**
   * Call when value has changed programmatically
   */
  public onChange(newValue: T) {}

  public onTouched(_?: any) {}

  /**
   * Model -> View changes
   */
  public writeValue(newValue: T): void {
    this.value = newValue;
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  public setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  onContainerClick(event: MouseEvent): void {}

  setDescribedByIds(ids: string[]): void {}
}
