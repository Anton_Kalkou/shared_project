import { OnInit, ChangeDetectorRef } from '@angular/core';

import { fromEvent } from 'rxjs';

import { Unsubscribe } from './unsubscribe.class';

export class RenderOnResize extends Unsubscribe implements OnInit {

  constructor(
    protected changeDetectorRef: ChangeDetectorRef
  ) {
    super();
  }

  ngOnInit() {
    this.subscribeTo = fromEvent(window, 'resize')
      .subscribe(() => this.changeDetectorRef.markForCheck());
  }

}
