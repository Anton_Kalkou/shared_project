import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

import { APP } from '../constants';

@Injectable()
export class UrlPostfixInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const {url, method, headers, body} = request;

    const urlPieces = url.split('/');
    const microServiceName = urlPieces[3];
    let prefix = '';

    if (microServiceName !== APP.microServices.payment && !microServiceName.includes('.')) {
      if (microServiceName === APP.microServices.search) {
        prefix = 'v2';
      } else {
        prefix = 'v1';
      }
    }
    if (prefix) {
      urlPieces.splice(4, 0, prefix);
    }

    request = request.clone({url: urlPieces.join('/')});

    return next.handle(request);

  }
}
