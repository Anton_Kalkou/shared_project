import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { SharedDataService } from '../services/shared-data.service';
import { APP } from '../constants';

@Injectable()
export class AccessTokenInterceptor implements HttpInterceptor {

  private excludedRoutes = [
    'assets',
    'authentication'
  ];

  constructor(
    private sharedDataService: SharedDataService
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.excludedRoutes.some(route => request.url.includes(route))) {
      request = request.clone({ headers: request.headers.set(APP.requestHeaders.accessToken, this.sharedDataService.authenticationData?.accessToken)});
    }

    return next.handle(request);
  }

}
