import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Store } from '@ngxs/store';

import { APP } from '../constants';
import { SetSpinnerVisibility, Reauthenticate } from '../../root/ngxs/root.actions';
import { NotificationService } from '../services/notification.service';
import { Logout } from 'src/modules/layout/ngxs/layout.actions';
import { Router } from '@angular/router';
import { LocalStorageService } from '../services/local-storage.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerInterceptor implements HttpInterceptor {

  constructor(
    private store: Store,
    private notificationService: NotificationService,
    private localStorageService: LocalStorageService,
    private router: Router,
  ) {}

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        catchError((error: HttpErrorResponse, caught: Observable<HttpEvent<any>>) => {
          this.store.dispatch(new SetSpinnerVisibility(false));
          this.errorProcessing(error);
          throw error;
        })
      );
  }

  private errorProcessing(error: HttpErrorResponse): void {
    let showMessage = true;
    const responseCode = error.error.statusCode ? error.error.statusCode : error.error.code ? error.error.code : error.error.errors?.details[0].code;

    switch (responseCode) {
      case APP.responseStatusCodes.unauthorized:
        showMessage = false;
        this.store.dispatch(new Reauthenticate());
        break;
      case APP.responseStatusCodes.invalidEnvironment:
        this.localStorageService.cacheData('prevRoute', this.router.url);
        this.store.dispatch(new Logout());
        break;
      case APP.responseStatusCodes.invalidRefreshToken:
        showMessage = false;
        this.store.dispatch(new Logout());
        break;
    }

    if (error.url.includes(APP.barcodeLookupURL)) {
      showMessage = false;
    }

    if (error.url.includes(APP.upcItemDBURL)) {
      showMessage = false;
    }

    if (showMessage) {
      this.notificationService.showErrorMessage(error.statusText, error.error.message);
    }
  }

}
