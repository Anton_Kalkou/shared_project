import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { SharedDataService } from '../services/shared-data.service';
import { APP } from '../constants';
import { RouterService } from '../services/router.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {

  constructor(
    private sharedDataService: SharedDataService,
    private routerService: RouterService
  ) {}

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const authentication = this.sharedDataService.authenticationData;

    if (authentication) {
      return true;
    } else {
      this.redirectToAuthenticationPage();

      return false;
    }
  }

  private redirectToAuthenticationPage(): void {
    this.routerService.navigateToPage(APP.pages.authentication);
  }

}
