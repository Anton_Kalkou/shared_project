export enum MerchantProfileTabIndexes {
  generalInformation = 0,
  risk = 1,
  devices = 2,
  provisioning = 3
}

export enum MerchantProfileTabs {
  generalInformation = 'General Info',
  risk = 'Risk/Underwriting',
  devices = 'Devices',
  provisioning = 'Provisioning'
}

export enum EditorsTagPopupTypes {
  attributes,
  categories
}

export enum ProductCategory {
  berries,
  beer,
  gin,
  sake,
  vodka,
  tequila,
  whisky,
  wine,
  default
}

export enum Translation {
  UpdateInfoContent = 'shared.update-info-content',
}

export enum ActionNotificationButtonClass {
  Undo= 'undo'
}

export enum Behavior {
  SystemsHealthStatus = 'systems-health-status',
  CellularSignal = 'cellular-signal',
  BatteryLevel = 'battery-level',
  BatteryChargingIcon = 'battery-charging-icon',
  DevicesStatus = 'status',
  Bluetooth = 'bluetooth',
  WifiSignal = 'wifi-signal',
  WifiSignalIcon = 'wifi-signal-icon',
  TransactionStatus = 'payment-transactions-status',
  HttpMethod = 'http-method'
}

export enum HttpMethod {
  Get = 'GET',
  Post = 'POST',
  Put = 'PUT',
  Delete = 'DELETE'
}

export enum HttpMethodClass {
  Get = 'http-method-get',
  Post = 'http-method-post',
  Put = 'http-method-put',
  Delete = 'http-method-delete'
}
