import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AuthenticationComponent } from 'src/modules/authentication/component/authentication.component';
import { NotAuthenticationGuard } from 'src/modules/shared/guards/not-auth.guard';

@NgModule({
  imports: [
    RouterModule.forRoot([
      { path: '', redirectTo: '/authentication', pathMatch: 'full' },
      { path: 'authentication', component: AuthenticationComponent, canActivate: [NotAuthenticationGuard] }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class RootRountingModule {}
