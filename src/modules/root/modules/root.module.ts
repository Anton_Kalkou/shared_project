import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';

import { NgxsModule } from '@ngxs/store';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgxsRouterPluginModule } from '@ngxs/router-plugin';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';

import { RootComponent } from '../components/root/root.component';
import { RootRountingModule } from './root-routing.module';
import { AuthenticationModule } from 'src/modules/authentication/modules/authentication.module';
import { LayoutModule } from 'src/modules/layout/module/layout.module';
import { environment } from 'src/environments/environment';
import { RootState } from '../ngxs/root.state';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { SpinnerComponent } from '../components/spinner/spinner.component';
import { ErrorHandlerInterceptor } from 'src/modules/shared/interceprots/error-handler.interceptor';
import { APP } from 'src/modules/shared/constants';
import { KeyInterceptor } from 'src/modules/shared/interceprots/key.interceptor';
import { AccessTokenInterceptor } from 'src/modules/shared/interceprots/access-token.interceptor';
import { FiltersModule } from 'src/modules/shared/components/filters/modules/filters.module';
import { ExportModule } from 'src/modules/shared/components/export/modules/export.module';
import { UrlPostfixInterceptor } from 'src/modules/shared/interceprots/url-postfix.interceptor';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RootRountingModule,
    NgxsModule.forRoot([RootState], { developmentMode: !environment.production }),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsRouterPluginModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      defaultLanguage: APP.languages.en
    }),
    MaterialModule,
    SimpleNotificationsModule.forRoot(),
    AuthenticationModule,
    LayoutModule,
    FiltersModule,
    ExportModule
  ],
  declarations: [
    RootComponent,
    SpinnerComponent
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: KeyInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: UrlPostfixInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: AccessTokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorHandlerInterceptor, multi: true }
  ],
  bootstrap: [
    RootComponent
  ]
})
export class RootModule {}
