import { Component, ChangeDetectionStrategy, OnInit, ChangeDetectorRef } from '@angular/core';

import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-root',
  templateUrl: 'root.component.html',
  styleUrls: ['root.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RootComponent extends Unsubscribe implements OnInit {

  @Select(state => state.root.spinnerVisibility) spinnerVisibility$: Observable<boolean>;

  public spinnerVisibility: boolean;

    constructor(
      private changeDetectorRef: ChangeDetectorRef,
      private translateService: TranslateService
    ) {
      super();
    }

  ngOnInit() {
    this.initSubscriptions();
    this.setDefaultBrowserLanguage();
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.spinnerVisibility$
      .subscribe(spinnerVisibility => {
        setTimeout(() => {
          this.spinnerVisibility = spinnerVisibility;
          this.changeDetectorRef.markForCheck();
        });
      });
  }

  private setDefaultBrowserLanguage(): void {
    this.translateService.use(this.translateService.getBrowserLang());
  }

}
