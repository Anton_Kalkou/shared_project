import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { ApiService } from 'src/modules/shared/services/api.service';
import { Authentication } from 'src/modules/shared/models/authentication.model';
import { APP } from 'src/modules/shared/constants';
import { SharedDataService } from 'src/modules/shared/services/shared-data.service';

@Injectable({
  providedIn: 'root'
})
export class RootService {

  constructor(
    private apiService: ApiService,
    private sharedDataService: SharedDataService
  ) {}

  public reauthenticate(): Observable<Authentication> {
    return this.apiService.post(APP.endpoints.reauthenticate, {refreshToken: this.sharedDataService.authenticationData.refreshToken});
  }

}
