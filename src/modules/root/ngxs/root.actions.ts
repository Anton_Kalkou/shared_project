export class SetSpinnerVisibility {
  static readonly type = '[Spinner Visibility] Set';

  constructor(
    public spinnerVisibility: boolean
  ) {}
}

export class Reauthenticate {
  static readonly type = '[User Session] Reauthenticate';
}
