import { Injectable } from '@angular/core';

import { tap } from 'rxjs/operators';
import { State, Action, StateContext, Store } from '@ngxs/store';

import { SetSpinnerVisibility, Reauthenticate } from './root.actions';
import { RootService } from '../services/root.service';
import { LocalStorageService } from 'src/modules/shared/services/local-storage.service';
import { SharedDataService } from 'src/modules/shared/services/shared-data.service';
import { APP } from 'src/modules/shared/constants';
import { GetUserProfileData } from 'src/modules/layout/ngxs/layout.actions';

interface RootStateModel {
  spinnerVisibility: boolean;
}

@State<RootStateModel>({
  name: 'root',
  defaults: {
    spinnerVisibility: false
  }
})
@Injectable()
export class RootState {

  constructor(
    private rootService: RootService,
    private sharedDataService: SharedDataService,
    private localStorageService: LocalStorageService,
    private store: Store
  ) {}

  @Action(SetSpinnerVisibility)
  setSpinnerVisibility({patchState}: StateContext<RootStateModel>, {spinnerVisibility}: SetSpinnerVisibility) {
    patchState({
      spinnerVisibility
    });
  }

  @Action(Reauthenticate)
  reauthenticate() {
    return this.rootService.reauthenticate()
      .pipe(
        tap(authenticationData => {
          this.sharedDataService.authenticationData = authenticationData;
          this.localStorageService.cacheData(APP.cache.authentication, authenticationData);
          this.store.dispatch(new GetUserProfileData());
        })
      );
  }

}
