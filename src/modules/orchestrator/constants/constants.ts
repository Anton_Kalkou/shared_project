export const ORCHESTRATOR = {
  orchestratorB2B: {
    orchestratorB2BPath: 'orchestrator-b2b',
    addPath: 'add',
    editPath: 'edit',
    headerTitles: {
      orchestratorB2B: 'header.orchestrator-b2b'
    },
    navButtonStateTitles: {
      products: 'menu.products',
      features: 'menu.features',
      permissions: 'menu.permissions',
      tenantsCategories: 'menu.tenants-categories',
      tenants: 'menu.tenants',
      defaultRoles: 'menu.default-roles'
    },
    endpoints: {
      products: 'tenant-management/product',
      features: 'tenant-management/feature',
      permissions: 'tenant-management/permission',
      tenantsCategories: 'tenant-management/tenant-category',
      tenantsCategoriesTags: 'tenant-management/tenant-category/{tanantCategoryId}/tags',
      tenants: 'tenant-management/tenant',
      defaultRoles: 'tenant-management/default-role',
      apis: 'tenant-management/api'
    },
    filters: {
      permissionsFilters: 'permissions-filters',
      permissions: 'tenant-management/permission',
      tenants: 'tenant-management/tenant'
    },
    products: {
      productsPath: 'orchestrator-b2b/products'
    },
    features: {
      featuresPath: 'orchestrator-b2b/features'
    },
    permissions: {
      permissionsPath: 'orchestrator-b2b/permissions'
    },
    tenantsCategories: {
      tenantsCategoriesPath: 'orchestrator-b2b/tenants-categories',
      comparePath: 'compare'
    },
    tenants: {
      tenantsPath: 'orchestrator-b2b/tenants'
    },
    defaultRoles: {
      defaultRolesPath: 'orchestrator-b2b/default-roles'
    }
  }
};
