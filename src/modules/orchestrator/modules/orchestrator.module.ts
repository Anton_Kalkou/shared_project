import { NgModule } from '@angular/core';

import { IconButtonModule } from 'src/modules/shared/components/icon-button/icon-button.module';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { SharedModule } from 'src/modules/shared/modules/shared.module';

import { OrchestratorB2BModule } from '../components/orchestrator-b2b/modules/orchestrator-b2b.module';
import {
  CreateVersionDialogComponent
} from '../components/orchestrator-b2b/shared/components/create-version-dialog/create-version-dialog.component';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    IconButtonModule,
    OrchestratorB2BModule
  ],
  declarations: [
    CreateVersionDialogComponent
  ],
  exports: [
    OrchestratorB2BModule
  ]
})
export class OrchestratorModule {}
