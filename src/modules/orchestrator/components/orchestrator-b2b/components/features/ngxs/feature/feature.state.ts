import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import { combineLatest } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { cloneDeep } from 'lodash';

import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { TranslationService } from 'src/modules/shared/services/translation.service';
import { Feature, RelatedProduct } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/features/features.model';
import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { SetNavigationButtonStatesTitle } from 'src/modules/layout/ngxs/layout.actions';
import { ProductResponse } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';

import { FeatureService } from '../../services/feature.service';
import { Translation as FeaturesTranslation } from '../../enum/features.enum';
import { INITIAL_STATE } from '../../constants/feature.constants';
import { SetVersionState } from '../../../../shared/ngxs/version/version.actions';
import { VersionService } from '../../../../shared/services/version.service';
import { getValuesWithoutCode } from '../../../../utils/orchestrator-b2b.utils';
import { DataLabel } from '../../../../enum/orchestrator-b2b.enum';
import { ProductsService } from '../../../products/services/products.service';
import {
  FetchFeature,
  CreateFeature,
  UpdateFeature,
  DeleteFeature,
  ResetFeatureState
} from './feature.actions';

export interface FeatureStateModel {
  isFeatureCreated: boolean;
  selectedFeature: Feature;
  relatedProducts: RelatedProduct[],
}

@State<FeatureStateModel>({
  name: 'feature',
  defaults: INITIAL_STATE
})
@Injectable()
export class FeatureState {

  constructor(
    private readonly router: Router,
    private readonly store: Store,
    private readonly featureService: FeatureService,
    private readonly versionService: VersionService,
    private readonly notificationService: NotificationService,
    private readonly translationService: TranslationService,
    private readonly productsService: ProductsService
  ) {}

  @Selector()
  static getItemId({ selectedFeature }: FeatureStateModel) {
    return selectedFeature && selectedFeature.id;
  }

  @Selector()
  static getItemName({ selectedFeature }: FeatureStateModel) {
    return selectedFeature && selectedFeature.name;
  }

  @Action(FetchFeature)
  fetchFeature({ patchState }: StateContext<FeatureStateModel>, { id }: FetchFeature) {
    const selectedFeature$ = this.featureService.loadFeature(id);
    const products$ = this.productsService.loadProducts();

    this.store.dispatch(new SetSpinnerVisibility(true));

    return combineLatest([selectedFeature$, products$]).subscribe(
      (([selectedFeature, products]) => {
        const { name, versions } = selectedFeature;
        const selectedVersion = this.versionService.getSelectedVersion(versions);
        const relatedProducts = this.versionService.getRelatedData<ProductResponse>(id, DataLabel.Features, products);

        this.store.dispatch([
          new SetVersionState(
            `${ORCHESTRATOR.orchestratorB2B.endpoints.features}/${id}`,
            selectedFeature,
            selectedVersion,
            ORCHESTRATOR.orchestratorB2B.endpoints.permissions,
            DataLabel.Permissions
          ),
          new SetSpinnerVisibility(false),
          new SetNavigationButtonStatesTitle(true, `Edit ${name}`)
        ]);

        patchState({ selectedFeature, relatedProducts });
      }),
      () => this.store.dispatch(new SetSpinnerVisibility(false))
    );
  }

  @Action(CreateFeature)
  createFeature({ patchState }: StateContext<FeatureStateModel>, { feature }: CreateFeature) {
    const featureForSaving = getValuesWithoutCode(feature);
    const createdFeature$ = this.featureService.createFeature(featureForSaving);
    const translatedMessage$ = this.translationService.translate(FeaturesTranslation.CreateInfoContent);

    this.store.dispatch(new SetSpinnerVisibility(true));

    return createdFeature$
      .pipe(flatMap(({ id}) => {
        const getFeatureById$ = this.featureService.loadFeature(id);

        return combineLatest([getFeatureById$, translatedMessage$]);
      }))
      .subscribe(
        ([{ id, code }, translatedMessage]) => {
          const selectedFeature = { id, code, ...feature, versions: [] };

          this.store.dispatch([
            new SetVersionState(
              `${ORCHESTRATOR.orchestratorB2B.endpoints.features}/${selectedFeature.id}`,
              selectedFeature,
              undefined,
              ORCHESTRATOR.orchestratorB2B.endpoints.permissions,
              DataLabel.Permissions
            ),
            new SetSpinnerVisibility(false)
          ]);
          this.notificationService.showSuccessMessage(translatedMessage);

          patchState({ isFeatureCreated: true, selectedFeature });
        },
        () => this.store.dispatch(new SetSpinnerVisibility(false))
      );
  }

  @Action(UpdateFeature)
  updateFeature({ getState, patchState }: StateContext<FeatureStateModel>, { id, feature }: UpdateFeature) {
    const featureForSaving = getValuesWithoutCode(feature);
    const { selectedFeature } = getState();

    return this.featureService.updateFeature(id, featureForSaving).subscribe(() => {
      this.store.dispatch(new SetNavigationButtonStatesTitle(true, `Edit ${name}`));

      patchState({
        selectedFeature: { ...cloneDeep(selectedFeature), id, ...feature }
      });
    });
  }

  @Action(DeleteFeature)
  deleteFeature({}: StateContext<FeatureStateModel>, { id }: DeleteFeature) {
    const deleteFeature$ = this.featureService.deleteFeature(id);
    const translatedMessage$ = this.translationService.translate(FeaturesTranslation.DeleteInfoContent);

    this.store.dispatch(new SetSpinnerVisibility(true));

    return combineLatest([deleteFeature$, translatedMessage$]).subscribe(
      ([, translatedMessage]) => {
        this.store.dispatch([
          new SetSpinnerVisibility(false),
          new ResetFeatureState()
        ]);
        this.router.navigate([`/${ORCHESTRATOR.orchestratorB2B.features.featuresPath}`]);
        this.notificationService.showSuccessMessage(translatedMessage);
      },
      () => this.store.dispatch(new SetSpinnerVisibility(false))
    );
  }

  @Action(ResetFeatureState)
  resetFeatureState({ patchState }: StateContext<FeatureStateModel>) {
    patchState({ ...INITIAL_STATE });
  }
}
