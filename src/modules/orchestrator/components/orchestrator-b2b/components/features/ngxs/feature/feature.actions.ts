import { Feature } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/features/features.model';

export class FetchFeature  {
  static readonly type = '[Feature] Fetch';

  constructor(public id: string) {}
}

export class CreateFeature {
  static readonly type = '[Feature] Create';

  constructor(public feature: Feature) {}
}

export class FetchRelatedProducts {
  static readonly type = '[Related Products] Fetch';

  constructor(public feature: Feature) {}
}

export class UpdateFeature {
  static readonly type = '[Feature] Update';

  constructor(public id: string, public feature: Feature) {}
}

export class DeleteFeature {
  static readonly type = '[Feature] Delete';

  constructor(public id: string) {}
}

export class ResetFeatureState {
  static readonly type = '[Feature] Reset';
}
