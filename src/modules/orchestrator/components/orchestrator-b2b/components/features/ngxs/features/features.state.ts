import { Injectable } from '@angular/core';
import { Action, State, StateContext, Store } from '@ngxs/store';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { Feature } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/features/features.model';
import { FeaturesService } from '../../services/features.service';
import { FeatureState } from '../feature/feature.state';
import { FetchFeatures } from './features.actions';

export interface FeaturesStateModel {
  features: Feature[];
}

@State<FeaturesStateModel>({
  name: 'features',
  defaults: {
    features: []
  },
  children: [FeatureState]
})
@Injectable()
export class FeaturesState {

  constructor(private store: Store, private featuresService: FeaturesService) {}

  @Action(FetchFeatures)
  fetchFeatures({ patchState }: StateContext<FeaturesStateModel>) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.featuresService.loadFeatures().subscribe(
      features => {
        this.store.dispatch(new SetSpinnerVisibility(false));
        patchState({ features });
      },
      () => this.store.dispatch(new SetSpinnerVisibility(false))
    );
  }
}
