import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';

import { Observable } from 'rxjs';
import { getEntityRouteId, isFormValid } from 'src/modules/shared/helpers';
import { Column } from 'src/modules/shared/models/data-table.model';
import { Feature } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/features/features.model';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { TranslationService } from 'src/modules/shared/services/translation.service';
import { TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum';
import { Product } from 'src/modules/shared/models/products/product.model';

import { BaseInfoPage } from '../../../../shared/classes/base/base-info-page.class';
import { FeatureState } from '../../ngxs/feature/feature.state';
import { CreateFeature, DeleteFeature, FetchFeature, UpdateFeature } from '../../ngxs/feature/feature.actions';
import { ColumnDataKey, Translation as FeaturesTranslation } from '../../enum/features.enum';

@Component({
  selector: 'app-feature-info',
  templateUrl: './feature-info.component.html',
  styleUrls: ['./feature-info.component.scss']
})
export class FeatureInfoComponent extends BaseInfoPage<Feature> {

  @Select(state => state.orchestratorB2B.features.feature.isFeatureCreated) isCreated$: Observable<boolean>;
  @Select(state => state.orchestratorB2B.features.feature.selectedFeature) selectedItem$: Observable<Feature>;
  @Select(state => state.orchestratorB2B.features.feature.relatedProducts) relatedProducts$: Observable<Product[]>;

  public columns: Column[];

  protected selectedIdSelector = FeatureState.getItemId;
  protected selectedNameSelector = FeatureState.getItemName;

  constructor(
    private route: ActivatedRoute,
    protected store: Store,
    protected formBuilder: FormBuilder,
    protected notificationService: NotificationService,
    protected translationService: TranslationService
  ) {
    super(store, formBuilder, notificationService, translationService);

    this.initValues();
  }

  public onCreate(): void {
    if (!isFormValid(this.formGroup)) {
      return;
    }

    this.store.dispatch(new CreateFeature(this.formGroup.getRawValue()));
  }

  public onDelete(): void {
    this.store.dispatch(new DeleteFeature(this.currentId));
  }

  protected onEdit(): void {
    this.setInitialFormValues(this.formGroup);
    this.store.dispatch(new UpdateFeature(this.currentId, this.initialFormValues));
  }

  protected initEditData(): void {
    this.currentId = getEntityRouteId(this.route);

    this.initSubscriptions();
    this.store.dispatch(new FetchFeature(this.currentId));
  }

  protected initFormGroup(initialFeature?: Feature): void {
    this.formGroup = this.formBuilder.group({
      name: [initialFeature ? initialFeature.name : '', Validators.required],
      code: [{ value: initialFeature ? initialFeature.code : '', disabled: true }],
      description: initialFeature ? initialFeature.description : ''
    });
  }

  private initValues(): void {
    this.columns = [
      { title: ColumnDataKey.Name, translationKey: FeaturesTranslation.Name, valueType: ValueType.Text, textType: TextType.Simple }
    ];
  }
}
