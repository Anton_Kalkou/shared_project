import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { ActionType, Icon, TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum';
import { Feature } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/features/features.model';
import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';

import { InfoType } from '../../../../enum/orchestrator-b2b.enum';
import { Translation as FeaturesTranslation, ColumnDataKey } from '../../enum/features.enum';
import { FeaturesService } from '../../services/features.service';
import { FetchFeatures } from '../../ngxs/features/features.actions';
import { BaseDataList } from '../../../../shared/classes/base/base-data-list.class';

@Component({
  selector: 'app-features',
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.scss']
})
export class FeaturesComponent extends BaseDataList<Feature, FetchFeatures> {
  
  @Select(state => state.orchestratorB2B.features.features) features$: Observable<Feature[]>;

  constructor(protected store: Store, public featuresService: FeaturesService) {
    super(new FetchFeatures(), ORCHESTRATOR.orchestratorB2B.navButtonStateTitles.features, store, false);
  }

  protected initValues(): void {
    this.displayedColumns = [
      { title: ColumnDataKey.Name, translationKey: FeaturesTranslation.Name, valueType: ValueType.Text, textType: TextType.Simple },
      { title: ColumnDataKey.Code, translationKey: FeaturesTranslation.Code, valueType: ValueType.Text, textType: TextType.Simple },
      { title: ColumnDataKey.Description, translationKey: FeaturesTranslation.Description, valueType: ValueType.Text, textType: TextType.Simple },
      {
        title: ColumnDataKey.Action,
        valueType: ValueType.Action,
        actionType: ActionType.CustomIcon,
        actionDataKey: ColumnDataKey.Id,
        path: InfoType.Edit,
        icon: Icon.Pencil,
        isSortableDisabled: true
      }
    ];
  }
}
