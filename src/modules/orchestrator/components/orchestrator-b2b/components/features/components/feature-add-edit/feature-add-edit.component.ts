import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { HeaderActions, SetHeaderTitle, SetNavigationButtonStatesTitle } from 'src/modules/layout/ngxs/layout.actions';

import { ResetFeatureState } from '../../ngxs/feature/feature.actions';
import { InfoType, Translation as OrchestratorB2bTranslation } from '../../../../enum/orchestrator-b2b.enum';
import { getInfoType } from '../../../../utils/orchestrator-b2b.utils';
import { ResetVersionState } from '../../../../shared/ngxs/version/version.actions';

@Component({
  selector: 'app-feature-add-edit',
  templateUrl: './feature-add-edit.component.html',
  styleUrls: ['./feature-add-edit.component.scss']
})
export class FeatureAddEditComponent implements OnInit, OnDestroy {

  @Select(state => state.orchestratorB2B.features.feature.isFeatureCreated) isFeatureCreated$: Observable<boolean>;

  public infoType: InfoType;

  constructor(
    private readonly router: Router,
    private readonly store: Store
  ) {}

  ngOnInit(): void {
    this.initValues();
    this.dispatchActions();
  }

  ngOnDestroy(): void {
    this.store.dispatch([new ResetFeatureState(), new ResetVersionState()]);
  }

  private getInitialActions(): HeaderActions[] {
    return this.infoType === InfoType.Add
      ? [new SetHeaderTitle(''), new SetNavigationButtonStatesTitle(true, OrchestratorB2bTranslation.AddNavigationButtonStatesTitle)]
      : [new SetHeaderTitle('')];
  }

  private initValues(): void {
    this.infoType = getInfoType(this.router.url);
  }

  private dispatchActions(): void {
   this.store.dispatch(this.getInitialActions());
  }
}
