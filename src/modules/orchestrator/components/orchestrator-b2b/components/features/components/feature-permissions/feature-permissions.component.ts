import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSelectChange } from '@angular/material/select';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { ActionType, TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum';
import { Column } from 'src/modules/shared/models/data-table.model';
import { Feature } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/features/features.model';
import { Version } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';
import { Permission } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/permissions/permissions.model';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { TranslationService } from 'src/modules/shared/services/translation.service';

import { ColumnDataKey, Translation as PermissionsTranslation } from '../../enum/permissions.enum';
import { SelectPermissionsComponent } from '../select-permissions/select-permissions.component';
import { CreateVersionDialogComponent } from '../../../../shared/components/create-version-dialog/create-version-dialog.component';
import { PermissionsService } from '../../../permissions/services/permissions.service';
import { FeatureState } from '../../ngxs/feature/feature.state';
import { BaseVersionPage } from '../../../../shared/classes/base/base-version-page.class';
import { DialogSize, DataLabel } from '../../../../enum/orchestrator-b2b.enum';
import { versionExistsValidator } from '../../../../shared/validators/versionExists.validator';
import {
  ChangeSelectedVersion,
  ChangeVersionState,
  CreateVersion,
  FetchVersionTableData,
  PublishVersion,
  UpdateVersion,
} from '../../../../shared/ngxs/version/version.actions';

@Component({
  selector: 'app-feature-permissions',
  templateUrl: './feature-permissions.component.html',
  styleUrls: ['./feature-permissions.component.scss']
})
export class FeaturePermissionsComponent extends BaseVersionPage<Feature, Permission> {
  @Select(state => state.orchestratorB2B.version.selectedItem) selectedItem$: Observable<Feature>;
  @Select(state => state.orchestratorB2B.version.selectedVersion) selectedVersion$: Observable<Version>;
  @Select(state => state.orchestratorB2B.version.isVersionPuplished) isVersionPuplished$: Observable<boolean>;
  @Select(state => state.orchestratorB2B.version.tableData) tableData$: Observable<Permission[]>;

  constructor(
    public readonly permissionsService: PermissionsService,
    protected readonly formBuilder: FormBuilder,
    protected readonly route: ActivatedRoute,
    protected readonly store: Store,
    protected readonly notificationService: NotificationService,
    protected readonly translationService: TranslationService,
    private readonly dialog: MatDialog
  ) {
    super(
      route,
      store,
      formBuilder,
      notificationService,
      translationService,
      FeatureState
    );
  }

  public onVersionChange({ value }: MatSelectChange): void {
    this.store.dispatch(new ChangeSelectedVersion(value));
  }

  public onPublish(): void {
    this.store.dispatch(new PublishVersion());
  }

  public onVersion(isDuplicate?: boolean): void {
    this.dialog.open(CreateVersionDialogComponent, this.getVersionDialogOptions(isDuplicate))
      .afterClosed()
      .subscribe(version => {
        return version && this.store.dispatch(new CreateVersion(
          { ...version, isDisabled: false },
          isDuplicate
        ));
      });
  }

  public onVersionState(): void {
    this.store.dispatch(new ChangeVersionState(!this.formGroup.get(DataLabel.IsDisabled).value));
  }

  public onSelectTableData(): void {
    this.subscribeTo = this.dialog
      .open(SelectPermissionsComponent, {
        width: DialogSize.AddTableData,
        data: {
          permissions: this.initialFormValues[DataLabel.Permissions]
        }
      })
      .afterClosed()
      .subscribe((selectedPermissions: Permission[]) => {
        if (selectedPermissions) {
          this.store.dispatch(new UpdateVersion({ ...this.formGroup.value, permissions: selectedPermissions }, true))
        }
      });
  }

  public onDeleteTableData(permission: Permission): void {
    const selectedPermissions = this.initialFormValues[DataLabel.Permissions] as Permission[];
    const permissionsAfterDeleting = selectedPermissions.filter(({ id }) => id !== permission.id);

    this.store.dispatch(new UpdateVersion({ ...this.formGroup.value, permissions: permissionsAfterDeleting }, true));
  }

  protected initFormGroup(selectedVersion?: Version): void {
    const { isDisabled = true, name = '', description = '', permissions = [] } = selectedVersion || {};

    this.formGroup = this.formBuilder.group({
      isDisabled,
      name: [
        { value: name, disabled: isDisabled },
        {
          validators: [Validators.required],
          asyncValidators: [versionExistsValidator(this.selectedItem$, this.selectedVersion$)],
        }
      ],
      description: { value: description, disabled: isDisabled }
    });
    
    this.setSelectedDataFormControl(DataLabel.Permissions, permissions as Permission[]);
  }

  protected getColumns(): Column[] {
    return [
      { title: ColumnDataKey.Name, translationKey: PermissionsTranslation.PermissionName, valueType: ValueType.Text, textType: TextType.Simple },
      { title: ColumnDataKey.Description, translationKey: PermissionsTranslation.Description, valueType: ValueType.Text, textType: TextType.Simple },
      {
        title: ColumnDataKey.Action,
        valueType: ValueType.Action,
        actionType: ActionType.SquareButton,
        isSortableDisabled: true,
        isDelete: true,
        popupTitle: PermissionsTranslation.DeleteApiTitle,
        popupCaption: PermissionsTranslation.DeleteApiCaption,
        popupAcceptTitle: PermissionsTranslation.DeleteApiAcceptTitle
      }
    ];
  }

  protected dispatchActions(): void {
    this.store.dispatch(new FetchVersionTableData());
  }
}
