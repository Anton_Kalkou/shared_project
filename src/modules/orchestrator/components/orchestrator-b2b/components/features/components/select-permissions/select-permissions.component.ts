import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';

import { TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum';
import { Column } from 'src/modules/shared/models/data-table.model';
import { Version } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';
import { Permission } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/permissions/permissions.model';
import { FiltersService } from 'src/modules/shared/services/filters.service';

import { ColumnDataKey, Translation as PermissionsTranslation } from '../../enum/permissions.enum';

@Component({
  selector: 'app-select-permissions',
  templateUrl: './select-permissions.component.html',
  styleUrls: ['./select-permissions.component.scss']
})
export class SelectPermissionsComponent extends Unsubscribe implements OnInit {
  @Select(state => state.orchestratorB2B.version.selectedVersion) selectedVersion$: Observable<Version>;
  @Select(state => state.orchestratorB2B.version.tableData) permissions$: Observable<Permission[]>;

  private sourcePermissions: Permission[];

  public filteredPermissions: Permission[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialog: MatDialogRef<SelectPermissionsComponent>,
    private filtersService: FiltersService
  ) {
    super();
  }

  public columns: Column[];
  public selectedItems: Permission[];

  ngOnInit(): void {
    this.initValues();
    this.initSubscriptions();
  }

  public onSearch(key: string): void {
    this.filteredPermissions = this.filtersService.filterDataOnSearch(this.sourcePermissions, key, ColumnDataKey.Name);
  }

  public onSave(): void {
    this.dialog.close(this.selectedItems);
  }

  private initValues(): void {
    this.columns = [
      { title: ColumnDataKey.Id, valueType: ValueType.Select, isSortableDisabled: true },
      { title: ColumnDataKey.Name, translationKey: PermissionsTranslation.PermissionName, valueType: ValueType.Text, textType: TextType.Simple },
      { title: ColumnDataKey.Description, translationKey: PermissionsTranslation.Description, valueType: ValueType.Text, textType: TextType.Simple },
    ];
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.permissions$.subscribe(permissions => {
      this.sourcePermissions = permissions;
      this.filteredPermissions = permissions;
    });
  }
}
