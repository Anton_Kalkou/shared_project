import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { DataTableModule } from 'src/modules/shared/components/data-table/data-table.module';
import { SectionHeaderModule } from 'src/modules/orchestrator/shared/components/section-header/section-header.module';
import { SearchModule } from 'src/modules/shared/components/search/search.module';
import { ConfirmationModule } from 'src/modules/shared/components/confirmation/confirmation.module';
import { IconButtonModule } from 'src/modules/shared/components/icon-button/icon-button.module';
import { AddButtonModule } from 'src/modules/shared/components/add-button/add-button.module';
import { AutoSaveNotificationTemplateModule } from 'src/modules/shared/components/auto-save-notification-template/auto-save-notification-template.module';

import { SelectPermissionsComponent } from '../components/select-permissions/select-permissions.component';
import { DataListModule } from '../../../shared/components/data-list/data-list.module';
import { FeatureAddEditComponent } from '../components/feature-add-edit/feature-add-edit.component';
import { FeatureInfoComponent } from '../components/feature-info/feature-info.component';
import { FeaturePermissionsComponent } from '../components/feature-permissions/feature-permissions.component';
import { FeatureChangelogsComponent } from '../components/feature-changelogs/feature-changelogs.component';
import { FeaturesComponent } from '../components/features/features.component';

@NgModule({
  imports: [
    RouterModule,
    SharedModule,
    MaterialModule,
    DataTableModule,
    DataListModule,
    SectionHeaderModule,
    SearchModule,
    ConfirmationModule,
    IconButtonModule,
    AddButtonModule,
    DataListModule,
    AutoSaveNotificationTemplateModule
  ],
  declarations: [
    FeaturesComponent,
    FeatureAddEditComponent,
    FeatureInfoComponent,
    FeaturePermissionsComponent,
    FeatureChangelogsComponent,
    SelectPermissionsComponent
  ],
  exports: [FeaturesComponent]
})
export class FeaturesModule {}
