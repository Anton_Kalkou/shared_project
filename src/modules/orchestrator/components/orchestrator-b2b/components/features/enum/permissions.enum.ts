export enum Translation {
  PermissionName = 'orchestrator-b2b.features.permission-name',
  Description = 'shared.description',
  DeleteApiTitle = 'orchestrator-b2b.features.delete-api-title',
  DeleteApiCaption = 'orchestrator-b2b.features.delete-api-caption',
  DeleteApiAcceptTitle = 'shared.yes'
}

export enum ColumnDataKey {
  Id = 'id',
  Name = 'name',
  Description = 'description',
  Action = 'action'
}
