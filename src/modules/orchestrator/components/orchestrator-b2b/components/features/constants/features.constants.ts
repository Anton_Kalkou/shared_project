import { Route } from '@angular/router';
import { AuthenticationGuard } from 'src/modules/shared/guards/auth.guard';
import { FeaturesComponent } from '../components/features/features.component';
import { FeatureAddEditComponent } from '../components/feature-add-edit/feature-add-edit.component';

export const FEATURES_ROUTES: Route[] = [
  { path: 'orchestrator-b2b/features', component: FeaturesComponent, canActivate: [AuthenticationGuard] },
  { path: 'orchestrator-b2b/features/add', component: FeatureAddEditComponent, canActivate: [AuthenticationGuard] },
  { path: 'orchestrator-b2b/features/:id/edit', component: FeatureAddEditComponent, canActivate: [AuthenticationGuard] }
];
