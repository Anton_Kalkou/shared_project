import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { Feature } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/features/features.model';
import { ApiService } from 'src/modules/shared/services/api.service';
import { FiltersService } from 'src/modules/shared/services/filters.service';

@Injectable({ providedIn: 'root' })
export class FeaturesService {

  constructor(private apiService: ApiService, private filtersService: FiltersService) {}

  public loadFeatures(): Observable<Feature[]> {
    return this.apiService.get(ORCHESTRATOR.orchestratorB2B.endpoints.features);
  }

  public filterFeatures(features: Feature[], filterType = null, filterValue: string, searchColumnDey: string): Feature[] {
    return this.filtersService.filterDataOnSearch(features, filterValue, searchColumnDey);
  }
}
