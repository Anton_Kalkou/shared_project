import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { Feature } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/features/features.model';
import { ApiService } from 'src/modules/shared/services/api.service';
import { CreatedData } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';

@Injectable({ providedIn: 'root' })
export class FeatureService {

  constructor(private readonly apiService: ApiService) {}

  public loadFeature(id: string): Observable<Feature> {
    return this.apiService.get(`${ORCHESTRATOR.orchestratorB2B.endpoints.features}/${id}`);
  }

  public createFeature(feature: Feature): Observable<CreatedData> {
    return this.apiService.post(ORCHESTRATOR.orchestratorB2B.endpoints.features, feature);
  }

  public updateFeature(id: string, featureInfo: Feature): Observable<CreatedData> {
    return this.apiService.put(`${ORCHESTRATOR.orchestratorB2B.endpoints.features}/${id}`, featureInfo);
  }

  public deleteFeature(id: string): Observable<void> {
    return this.apiService.delete(`${ORCHESTRATOR.orchestratorB2B.endpoints.features}/${id}`);
  }
}
