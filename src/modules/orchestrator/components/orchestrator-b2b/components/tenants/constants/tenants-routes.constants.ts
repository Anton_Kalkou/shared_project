import { Route } from '@angular/router';

import { AuthenticationGuard } from 'src/modules/shared/guards/auth.guard';
import { TenantsComponent } from '../components/tenants/tenants.component';
import { TenantAddEditComponent } from '../components/tenant-add-edit/tenant-add-edit.component';

export const TENANTS_ROUTES: Route[] = [
  { path: 'orchestrator-b2b/tenants', component: TenantsComponent, canActivate: [AuthenticationGuard] },
  { path: 'orchestrator-b2b/tenants/:id/edit', component: TenantAddEditComponent, canActivate: [AuthenticationGuard] },
];
