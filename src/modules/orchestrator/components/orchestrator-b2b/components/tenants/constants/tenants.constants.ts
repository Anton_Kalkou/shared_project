import { ColumnDataKey } from '../enum/tenants.enum';
import { Translation as FiltersTranslation } from '../../../../../../shared/components/filters/enum/filters.enum';

export const FILTER_TRANSLATIONS_WITH_COLUMN_DATA_KEYS = {
  [FiltersTranslation.Common.Category]: ColumnDataKey.Category,
  [FiltersTranslation.Common.Products]: ColumnDataKey.Products
};
