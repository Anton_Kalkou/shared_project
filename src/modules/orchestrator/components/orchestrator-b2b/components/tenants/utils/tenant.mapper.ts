import { Tenant, TenantResponse } from '../../../../../../shared/models/orchestrator/orchestrator-b2b/tenant/tenants.model';
import { TenantCategoryResponse } from '../../../../../../shared/models/orchestrator/orchestrator-b2b/tenants-categories/tenants-categories.model';
import { mapTagsToString } from './tenant-category.mapper';

const mapTenantResponseWithTenantCategories = (tenantsResponse: TenantResponse, tenantCategories: TenantCategoryResponse[]) => {
  const mappedTenantResponseToTenant = mapTenantFromApiToView(tenantsResponse);

  return mapTenantWithTenantCategory(mappedTenantResponseToTenant, tenantCategories);
};

const mapTenantsResponseToView = (tenantsApi: TenantResponse[]): Tenant[] => tenantsApi.map(mapTenantFromApiToView);

const mapTenantFromApiToView = ({  description, code, name, _id: id, categoryId }: TenantResponse): Tenant => {
  return {
    id,
    name,
    code,
    category: categoryId || '',
    description,
    products: [],
  };
};

const mapTenantsWithTenantCategories = (tenants: Tenant[], tenantCategories: TenantCategoryResponse[]): Tenant[] =>
  tenants.map((tenant: Tenant) => mapTenantWithTenantCategory(tenant, tenantCategories));

const mapTenantWithTenantCategory = (tenant: Tenant, tenantCategories: TenantCategoryResponse[]): Tenant  => {
  const findTenantCategory = tenantCategories.find((tenantCategory) => tenantCategory.id === tenant.category);
  const mappedTags = (findTenantCategory && findTenantCategory.tags && findTenantCategory.tags.length)
    ? mapTagsToString(findTenantCategory.tags)
    : '';

  return {
    ...tenant,
    category: findTenantCategory?.id || '',
    tags: mappedTags
  };
};

export {
  mapTenantResponseWithTenantCategories,
  mapTenantsResponseToView,
  mapTenantFromApiToView,
  mapTenantsWithTenantCategories,
  mapTenantWithTenantCategory
};
