import { Tag, TenantCategoryResponse } from '../../../../../../shared/models/orchestrator/orchestrator-b2b/tenants-categories/tenants-categories.model';
import { TenantCategory } from '../../../../../../shared/models/orchestrator/orchestrator-b2b/tenant/tenant-category.model';

const mapTenantsCategoryForDropdown = (tenantsCategoriesResponse: TenantCategoryResponse[]): TenantCategory[] =>
  tenantsCategoriesResponse.map(({ id, name: title, tags }: TenantCategoryResponse) => ({
    id,
    title,
    tags: mapTagsToString(tags)
  }));

const mapTagsToString = (tags: Tag[]): string => {
  const tagsNames = tags.map(({ name }: Tag) => name);

  return tagsNames && tagsNames.length ? tagsNames.join(', ') : null;
};

export {
  mapTenantsCategoryForDropdown,
  mapTagsToString
};
