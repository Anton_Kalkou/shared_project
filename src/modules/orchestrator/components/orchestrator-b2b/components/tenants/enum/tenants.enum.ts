export enum Translation {
  Name = 'shared.name',
  Category = 'orchestrator-b2b.tenants.category',
  Products = 'menu.products',
  CreateInfoContent = 'orchestrator-b2b.tenants.create-info-content',
  DeleteInfoContent = 'orchestrator-b2b.tenants.delete-info-content'
}

export enum ColumnDataKey {
  Id = 'id',
  Name = 'name',
  Category = 'category',
  Products = 'products',
  Action = 'action'
}
