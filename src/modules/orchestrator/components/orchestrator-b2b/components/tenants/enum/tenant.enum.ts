export enum TenantProductsTranslation {
  Name = 'orchestrator-b2b.tenants.product-name'
}

export enum TenantProductsColumnDataKey {
  Id = 'id',
  Name = 'name'
}

export enum SelectTenantProductsTranslation {
  Name = 'menu.products'
}
