import { ProductName, Tenant } from '../../../../../../../shared/models/orchestrator/orchestrator-b2b/tenant/tenants.model';

export class FetchTenant {
  static readonly type = '[Tenant] Fetch';

  constructor(public id: string) {}
}

export class FetchTenantCategories {
  static readonly type = '[Tenant] Fetch Categories';
}

export class FetchTenantProductsNames {
  static readonly type = '[Tenant] Fetch Tenant Products Names';

  constructor(public id: string) {}
}

export class FetchProductsNames {
  static readonly type = '[Tenant] Fetch Product Names';
}

export class UpdateSelectedProductsNames {
  static readonly type = '[Tenant] Update Selected Products Names';

  constructor(public selectedProductsNames: ProductName[]) {}
}

export class UpdateTenant {
  static readonly type = '[Tenant] Update';

  constructor(public id: string, public tenant: Tenant) {}
}

export class DeleteTenant {
  static readonly type = '[Tenant] Delete';

  constructor(public id: string) {}
}

export class ResetTenantState {
  static readonly type = '[Tenant] Reset';
}
