import { Injectable } from '@angular/core';
import { Action, State, StateContext, Store } from '@ngxs/store';
import { Router } from '@angular/router';
import { combineLatest } from 'rxjs';

import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { TranslationService } from 'src/modules/shared/services/translation.service';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { TenantCategory } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/tenant/tenant-category.model';
import { ProductName, Tenant } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/tenant/tenants.model';

import { TenantService } from '../../services/tenant.service';
import { getValuesWithoutCode } from '../../../../utils/orchestrator-b2b.utils';
import { Translation as TenantsTranslation } from '../../enum/tenants.enum';
import { INITIAL_STATE } from '../../constants/tenant.constants';
import {
  DeleteTenant, FetchProductsNames,
  FetchTenant,
  FetchTenantCategories,
  FetchTenantProductsNames,
  UpdateSelectedProductsNames,
  UpdateTenant,
  ResetTenantState
} from './tenant.actions';

interface TenantStateModel {
  selectedTenant: Tenant;
  selectedProductsNames: ProductName[];
  productNames: ProductName[];
  categories: TenantCategory[];
}

@State<TenantStateModel>({
  name: 'tenant',
  defaults: INITIAL_STATE,
  children: []
})
@Injectable()
export class TenantState {
  constructor(
    private readonly tenantService: TenantService,
    private readonly translationService: TranslationService,
    private readonly notificationService: NotificationService,
    private readonly store: Store,
    private readonly router: Router,
  ) {}

  @Action(FetchTenant)
  fetchTenant({ patchState }: StateContext<TenantStateModel>, { id }: FetchTenant) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.tenantService.loadTenantWithCategory(id).subscribe(
      (tenant: Tenant) => {
        this.store.dispatch(new SetSpinnerVisibility(false));

        patchState({ selectedTenant: tenant });
      },
      () => {
        this.store.dispatch(new SetSpinnerVisibility(false));
      });
  }

  @Action(FetchTenantCategories)
  fetchTenantCategories({ patchState }: StateContext<TenantStateModel>) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.tenantService.loadTenantsCategoriesForDropdown().subscribe(
      (categories: TenantCategory[]) => {
        this.store.dispatch(new SetSpinnerVisibility(false));

        patchState({ categories });
      },
      () => {
        this.store.dispatch(new SetSpinnerVisibility(false));
      });
  }

  @Action(FetchProductsNames)
  fetchProductsNames({ patchState }: StateContext<TenantStateModel>) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.tenantService.loadProductsNames().subscribe(
      (productNames: ProductName[]) => {
        this.store.dispatch(new SetSpinnerVisibility(false));

        patchState({ productNames });
      },
      () => {
        this.store.dispatch(new SetSpinnerVisibility(false));
      });
  }

  @Action(FetchTenantProductsNames)
  fetchTenantProductsNames({ patchState }: StateContext<TenantStateModel>, { id }: FetchTenantProductsNames) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.tenantService.loadTenantProductsNames(id).subscribe(
      (selectedProductsNames: ProductName[]) => {
        this.store.dispatch(new SetSpinnerVisibility(false));

        patchState({ selectedProductsNames });
      },
      () => {
        this.store.dispatch(new SetSpinnerVisibility(false));
      });
  }

  @Action(UpdateSelectedProductsNames)
  updateSelectedProductsNames({ patchState }: StateContext<TenantStateModel>, { selectedProductsNames }: UpdateSelectedProductsNames) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.tenantService.saveSelectedProductsNames().subscribe(
      () => {
        this.store.dispatch(new SetSpinnerVisibility(false));

        patchState({ selectedProductsNames });
      },
      () => {
        this.store.dispatch(new SetSpinnerVisibility(false));
      });
  }

  @Action(UpdateTenant)
  updateTenant({ patchState }: StateContext<TenantStateModel>, { id, tenant  }: UpdateTenant) {
    const tenantForSaving = getValuesWithoutCode(tenant);

    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.tenantService.updateTenant(id, tenantForSaving).subscribe(
      () => {
        this.store.dispatch(new SetSpinnerVisibility(false));
        patchState({ selectedTenant: tenant });
      },
      () => this.store.dispatch(new SetSpinnerVisibility(false))
    );
  }

  @Action(DeleteTenant)
  deleteTenant({ patchState }: StateContext<TenantStateModel>, { id }: DeleteTenant) {
    const deleteFeature$ = this.tenantService.deleteTenant(id);
    const translatedMessage$ = this.translationService.translate(TenantsTranslation.DeleteInfoContent);

    this.store.dispatch(new SetSpinnerVisibility(true));

    return combineLatest([deleteFeature$, translatedMessage$]).subscribe(
      ([, translatedMessage]) => {
        this.store.dispatch([
          new SetSpinnerVisibility(false),
          new ResetTenantState(),
        ]);
        this.router.navigate([`/${ORCHESTRATOR.orchestratorB2B.tenants.tenantsPath}`]);
        this.notificationService.showSuccessMessage(translatedMessage);
      },
      () => this.store.dispatch(new SetSpinnerVisibility(false))
    );
  }

  @Action(ResetTenantState)
  resetTenantState({ patchState }: StateContext<TenantStateModel>) {
    patchState({ ...INITIAL_STATE });
  }
}
