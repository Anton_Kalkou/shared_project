import { Injectable } from '@angular/core';
import { Action, State, StateContext, Store } from '@ngxs/store';
import { combineLatest } from 'rxjs';

import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { SetFilters } from 'src/modules/shared/components/filters/ngxs/filters.actions';

import { Tenant } from '../../../../../../../shared/models/orchestrator/orchestrator-b2b/tenant/tenants.model';
import { ProductsService } from '../../../products/services/products.service';
import { TenantsCategoriesService } from '../../../tenants-categories/services/tenants-categories.service';
import { TenantsService } from '../../services/tenants.service';
import { FetchTenants } from './tenants.actions';

interface TenantsStateModel {
  tenants: Tenant[];
}

@State<TenantsStateModel>({
  name: 'tenants',
  defaults: {
    tenants: []
  },
  children: []
})
@Injectable()
export class TenantsState {
  constructor(
    private readonly store: Store,
    private readonly tenantsService: TenantsService,
    private readonly tenantsCategoriesService: TenantsCategoriesService,
    private readonly productsService: ProductsService,
  ) {}

  @Action(FetchTenants)
  fetchTenants({ patchState }: StateContext<TenantsStateModel>) {
    const loadedTenants$ = this.tenantsService.loadTenants();
    const loadedTenantsCategories$ = this.tenantsCategoriesService.loadTenantsCategories();
    const loadedProducts$ = this.productsService.loadProducts();

    this.store.dispatch(new SetSpinnerVisibility(true));

    return combineLatest([loadedTenants$, loadedTenantsCategories$, loadedProducts$])
      .subscribe(([loadedTenants, loadedTenantsCategories, loadedProducts]) => {
        const tenants = this.tenantsService.prepareTenants(loadedTenants, loadedTenantsCategories);
        const tenantsFilters = this.tenantsService.getTenantsFilters(loadedTenantsCategories, loadedProducts);

        this.store.dispatch([
          new SetFilters(tenantsFilters),
          new SetSpinnerVisibility(false),
        ]);

        patchState({ tenants });
      });
  }
}
