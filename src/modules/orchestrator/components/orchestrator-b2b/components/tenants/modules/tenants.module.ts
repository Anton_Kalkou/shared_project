import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { SectionHeaderModule } from 'src/modules/orchestrator/shared/components/section-header/section-header.module';
import { AddButtonModule } from 'src/modules/shared/components/add-button/add-button.module';
import { IconButtonModule } from 'src/modules/shared/components/icon-button/icon-button.module';
import { SearchModule } from 'src/modules/shared/components/search/search.module';
import { SquareButtonModule } from 'src/modules/shared/components/square-button/square-button.module';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { DataTableModule } from 'src/modules/shared/components/data-table/data-table.module';
import { ConfirmationModule } from 'src/modules/shared/components/confirmation/confirmation.module';

import { TenantsComponent } from '../components/tenants/tenants.component';
import { DataListModule } from '../../../shared/components/data-list/data-list.module';
import { TenantAddEditComponent } from '../components/tenant-add-edit/tenant-add-edit.component';
import { TenantInfoComponent } from '../components/tenant-add-edit/tenant-info/tenant-info.component';
import { TenantProductsComponent } from '../components/tenant-add-edit/tenant-products/tenant-products.component';
import { InputWithChipsModule } from '../../../../../../shared/components/controls/input-with-chips/input-with-chips.module';
import { AutoSaveNotificationTemplateModule } from '../../../../../../shared/components/auto-save-notification-template/auto-save-notification-template.module';
import { ChangeSelectedProductsNamesDialogComponent } from '../components/tenant-add-edit/tenant-products/change-selected-products-names-dialog/change-selected-products-names-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    TranslateModule,
    IconButtonModule,
    SquareButtonModule,
    SearchModule,
    SectionHeaderModule,
    DataTableModule,
    MaterialModule,
    AddButtonModule,
    NgxMaterialTimepickerModule,
    ConfirmationModule,
    DataListModule,
    InputWithChipsModule,
    AutoSaveNotificationTemplateModule,
  ],
  declarations: [
    TenantsComponent,
    TenantInfoComponent,
    TenantProductsComponent,
    ChangeSelectedProductsNamesDialogComponent,
    TenantAddEditComponent
  ],
  exports: [
    TenantsComponent
  ]
})
export class TenantsModule {}
