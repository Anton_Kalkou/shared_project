import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';

import { Observable } from 'rxjs';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { TranslationService } from 'src/modules/shared/services/translation.service';
import { SetNavigationButtonStatesTitle } from 'src/modules/layout/ngxs/layout.actions';

import {
  DeleteTenant,
  FetchTenant,
  FetchTenantCategories,
  UpdateTenant
} from '../../../ngxs/tenant/tenant.actions';
import { InfoType } from '../../../../../enum/orchestrator-b2b.enum';
import { Tenant } from '../../../../../../../../shared/models/orchestrator/orchestrator-b2b/tenant/tenants.model';
import { TenantCategory } from '../../../../../../../../shared/models/orchestrator/orchestrator-b2b/tenant/tenant-category.model';
import { BaseAutoSavingPage } from '../../../../../shared/classes/base/base-auto-saving-page.class';

@Component({
  selector: 'app-tenant-info',
  templateUrl: './tenant-info.component.html',
  styleUrls: ['./tenant-info.component.scss']
})
export class TenantInfoComponent extends BaseAutoSavingPage<Tenant> implements OnInit {
  @Select(state => state.orchestratorB2B.tenant.selectedTenant) tenant$: Observable<Tenant>;
  @Select(state => state.orchestratorB2B.tenant.categories) categories$: Observable<TenantCategory[]>;

  @Input() infoType: InfoType;

  private tenantId: string;

  constructor(
    private readonly route: ActivatedRoute,
    store: Store,
    formBuilder: FormBuilder,
    notificationService: NotificationService,
    translationService: TranslationService
  ) {
    super(store, formBuilder, notificationService, translationService);
  }

  ngOnInit(): void {
    this.initEditData();
  }

  public onDelete(): void {
    this.store.dispatch(new DeleteTenant(this.tenantId));
  }

  protected onEdit(): void {
    this.setInitialFormValues(this.formGroup);
    this.store.dispatch(new UpdateTenant(this.tenantId, this.initialFormValues));
  }

  protected initFormGroup(initialValue?: Tenant): void {
    const { name = '', code = '', category = '', tags = '', accessKeys = [], description = '' } = initialValue || {};

    this.formGroup = this.formBuilder.group({
      name: [{ value: name, disabled: true }, Validators.required],
      code: [{ value: code, disabled: true }, Validators.required],
      category: [category],
      tags: [{ value: tags, disabled: true }],
      accessKeys: [accessKeys, Validators.required],
      description: [{ value: description, disabled: true }, Validators.required],
    });

    this.tenantCategoriesChangesHandler();
  }

  private initEditData(): void {
    this.setTenantId();
    this.updateFormGroup();
    this.initSubscriptions();
    this.store.dispatch([new FetchTenant(this.tenantId), new FetchTenantCategories()]);
  }

  private setTenantId(): void {
    this.tenantId = this.route.snapshot.paramMap.get('id');
  }

  private initSubscriptions(): void {
    this.formChangesHandler();
    this.subscribeTo = this.tenant$.subscribe((tenant: Tenant) => {
      if (tenant) {
        this.store.dispatch(new SetNavigationButtonStatesTitle(true, `Edit ${tenant.name}`));
        this.updateFormGroup(tenant);
      }
    });
  }

  private tenantCategoriesChangesHandler(): void {
    this.subscribeTo = this.formGroup.get('category').valueChanges.subscribe(selectedCategoryId => {
      const categories = this.store.selectSnapshot(state => state.orchestratorB2B.tenant.categories);
      const foundCategory = categories.find(({ id }: TenantCategory) => selectedCategoryId === id) || {};
      this.formGroup.patchValue({ tags: foundCategory.tags });
    });
  }
}
