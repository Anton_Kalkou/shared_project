import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';

import { Observable } from 'rxjs';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum';
import { ProductName, TenantProductsDialogOptions } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/tenant/tenants.model';

import { FetchProductsNames, FetchTenantProductsNames, UpdateSelectedProductsNames } from '../../../ngxs/tenant/tenant.actions';
import { TenantProductsColumnDataKey, TenantProductsTranslation } from '../../../enum/tenant.enum';
import { DialogSize, InfoType } from '../../../../../enum/orchestrator-b2b.enum';
import { getInfoType } from '../../../../../utils/orchestrator-b2b.utils';
import {
  ChangeSelectedProductsNamesDialogComponent,
} from './change-selected-products-names-dialog/change-selected-products-names-dialog.component';

@Component({
  selector: 'app-tenant-products',
  templateUrl: './tenant-products.component.html',
  styleUrls: ['./tenant-products.component.scss']
})
export class TenantProductsComponent extends Unsubscribe implements OnInit {
  @Select(state => state.orchestratorB2B.tenant.selectedProductsNames) selectedProductsNames$: Observable<ProductName[]>;
  @Select(state => state.orchestratorB2B.tenant.productNames) productNames$: Observable<ProductName[]>;

  public columns = [
    {
      title: TenantProductsColumnDataKey.Name,
      translationKey: TenantProductsTranslation.Name,
      valueType: ValueType.Text,
      textType: TextType.Simple,
    },
  ];

  public tenantProductsDialogOptions: TenantProductsDialogOptions;

  private infoType: InfoType;
  private tenantId: string;

  constructor(
    private readonly store: Store,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly dialog: MatDialog
  ) {
    super();
  }

  ngOnInit(): void {
    this.initTenantProductsData();
  }

  public onSelectProductsData(): void {
    this.subscribeTo = this.dialog.open(ChangeSelectedProductsNamesDialogComponent, this.tenantProductsDialogOptions)
      .afterClosed()
      .subscribe(newSelectedProductsNames => {
        if (!newSelectedProductsNames) {
          return;
        }

        this.store.dispatch(new UpdateSelectedProductsNames(newSelectedProductsNames));
      });
  }

  private initTenantProductsData(): void {
    this.setInfoType();
    this.setTenantId();
    this.loadTenantProductsNames();
    this.setTenantProductsDialogOptions();
  }

  private setInfoType(): void {
    this.infoType = getInfoType(this.router.url);
  }

  private setTenantId(): void {
    this.tenantId = this.activatedRoute.snapshot.paramMap.get('id');
  }

  private loadTenantProductsNames(): void {
    let dispatchedActions = [new FetchProductsNames()];

    if (this.infoType === InfoType.Edit) {
      dispatchedActions = [...dispatchedActions, new FetchTenantProductsNames(this.tenantId)];
    }

    this.store.dispatch(dispatchedActions);
  }

  private setTenantProductsDialogOptions(): void {
    this.tenantProductsDialogOptions = {
      width: DialogSize.Version,
      data: {
        selectedProductsNames$: this.selectedProductsNames$,
        productNames$: this.productNames$,
      }
    };
  }
}
