import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { FiltersService } from 'src/modules/shared/services/filters.service';
import { TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum'
import { ProductName, TenantProductsDialogData } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/tenant/tenants.model';

import { SelectTenantProductsTranslation, TenantProductsColumnDataKey } from '../../../../enum/tenant.enum';;
import { ColumnDataKey } from '../../../../../features/enum/permissions.enum';

@Component({
  selector: 'app-change-selected-products-names-dialog',
  templateUrl: './change-selected-products-names-dialog.component.html',
  styleUrls: ['./change-selected-products-names-dialog.component.scss']
})
export class ChangeSelectedProductsNamesDialogComponent extends Unsubscribe  implements OnInit {
  public sourceProductNames: ProductName[];
  public filteredProductsNames: ProductName[];
  public selectedProductNames: ProductName[] = [];
  public columns = [
    {
      title: TenantProductsColumnDataKey.Id,
      valueType: ValueType.Select,
      isSortableDisabled: true,
    },
    {
      title: TenantProductsColumnDataKey.Name,
      translationKey: SelectTenantProductsTranslation.Name,
      valueType: ValueType.Text,
      textType: TextType.Simple,
    },
  ];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: TenantProductsDialogData,
    private dialog: MatDialogRef<ChangeSelectedProductsNamesDialogComponent>,
    private filtersService: FiltersService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.initTableValues();
  }

  public onSearch(key: string): void {
    this.filteredProductsNames = this.filtersService.filterDataOnSearch(this.sourceProductNames, key, ColumnDataKey.Name);
  }

  public onSave(): void {
    this.dialog.close(this.selectedProductNames);
  }

  public onTableSelectValues(selectedProductNames: ProductName[]): void {
    this.selectedProductNames = selectedProductNames;
  }

  private initTableValues(): void {
    this.subscribeTo = this.data.selectedProductsNames$.subscribe((selectedProductsNames: ProductName[]) => this.selectedProductNames = selectedProductsNames);
    this.subscribeTo = this.data.productNames$.subscribe((productNames: ProductName[]) => {
      this.sourceProductNames = productNames;
      this.filteredProductsNames = productNames;
    });
  }
}
