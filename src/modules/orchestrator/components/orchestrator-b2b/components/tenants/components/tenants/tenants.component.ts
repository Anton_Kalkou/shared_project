import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';

import { Observable } from 'rxjs';
import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { APP } from 'src/modules/shared/constants';
import { TenantCategory } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/tenants-categories/tenants-categories.model';
import { Tenant } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/tenant/tenants.model';
import {
  ActionType,
  Icon,
  TextType,
  ValueType
} from 'src/modules/shared/components/data-table/enum/data-table.enum';

import { ColumnDataKey, Translation as TenantsTranslation } from '../../enum/tenants.enum';
import { InfoType } from '../../../../enum/orchestrator-b2b.enum';
import { SelectTenantsCategories } from '../../../tenants-categories/ngxs/tenants-categories/tenants-categories.actions';
import { FetchTenants } from '../../ngxs/tenants/tenants.actions';
import { TenantsService } from '../../services/tenants.service';
import { BaseDataList } from '../../../../shared/classes/base/base-data-list.class';

@Component({
  selector: 'app-tenants',
  templateUrl: './tenants.component.html',
  styleUrls: ['./tenants.component.scss']
})
export class TenantsComponent extends BaseDataList<Tenant, FetchTenants> {
  @Select(state => state.orchestratorB2B.tenants.tenants) dataList$: Observable<Tenant[]>;

  constructor(
    public readonly tenantsService: TenantsService,
    protected readonly store: Store,
  ) {
    super(new FetchTenants(), ORCHESTRATOR.orchestratorB2B.navButtonStateTitles.tenants, store, false);
  }

  public onSelect(selectedTenantsCategories: TenantCategory[]): void {
    this.store.dispatch(new SelectTenantsCategories(selectedTenantsCategories));
  }

  protected initValues(): void {
    this.applyCommonFilters = false;
    this.filtersEndpoint = APP.endpoints.tenantsFilters;
    this.displayedColumns = [
      {
        title: ColumnDataKey.Name,
        translationKey: TenantsTranslation.Name,
        valueType: ValueType.Text,
        textType: TextType.Simple,
      },
      {
        title: ColumnDataKey.Category,
        translationKey: TenantsTranslation.Category,
        valueType: ValueType.Text,
        textType: TextType.Simple,
      },
      {
        title: ColumnDataKey.Products,
        translationKey: TenantsTranslation.Products,
        valueType: ValueType.Text,
        textType: TextType.Simple,
      },
      {
        title: ColumnDataKey.Action,
        valueType: ValueType.Action,
        actionType: ActionType.CustomIcon,
        actionDataKey: ColumnDataKey.Id,
        path: InfoType.Edit,
        icon: Icon.Pencil,
        isSortableDisabled: true,
      },
    ];
  }
}
