import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ApiService } from 'src/modules/shared/services/api.service';
import { FiltersService } from 'src/modules/shared/services/filters.service';
import { TenantCategory } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/tenants-categories/tenants-categories.model';
import { Translation as FiltersTranslation } from 'src/modules/shared/components/filters/enum/filters.enum';
import { Tenant, TenantResponse } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/tenant/tenants.model';
import { Product } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/products/product.model';
import { FilterGroup } from 'src/modules/shared/models/filter.model';

import { FilterType } from '../../../../../../support/enum/support.enum';
import { ORCHESTRATOR } from '../../../../../constants/constants';
import { mapTenantsResponseToView } from '../utils/tenant.mapper';
import { FILTER_TRANSLATIONS_WITH_COLUMN_DATA_KEYS } from '../constants/tenants.constants';

@Injectable({ providedIn: 'root' })
export class TenantsService {
  constructor(
    private readonly filtersService: FiltersService,
    private readonly apiService: ApiService,
  ) {}

  public loadTenants(): Observable<Tenant[]> {
    return this.apiService.get(ORCHESTRATOR.orchestratorB2B.endpoints.tenants)
      .pipe(
        map((tenantResponse: TenantResponse[]) => mapTenantsResponseToView(tenantResponse)),
      );
  }

  public getTenantsFilters(tenantsCategories: TenantCategory[], products: Product[]): FilterGroup[] {
    const tenantsCategoriesLabels = tenantsCategories.map(tenantsCategory => tenantsCategory.name);
    const productsLabels = products.map(product => product.name);
    const filters = [
      { title: FiltersTranslation.Common.Category, filters: tenantsCategoriesLabels },
      { title: FiltersTranslation.Common.Product, filters: productsLabels },
    ];

    return filters;
  }

  public filterTenants(
    dataSource: Tenant[],
    filterType: string,
    filterValue: string | FilterGroup[],
    searchColumnKey: string
  ): Tenant[] {
    if (filterType === FilterType.Search) {
      return this.filtersService.filterDataOnSearch(dataSource, filterValue as string, searchColumnKey);
    } else {
      const filter = filterValue as FilterGroup[];

      if (filter.length) {
        return dataSource.filter(dataSourceItem => {
          if (filter.length === 1) {
            return filter.some(filterValueItem =>
              this.filtersService.isFilterValueMatch(
                dataSourceItem[this.getColumnDataKey(this.getFilterTitle(filterValueItem.title))],
                filterValueItem.filters,
              ),
            );
          } else {
            const columnDataKeys = filter.map(filterValueItem =>
              this.getColumnDataKey(this.getFilterTitle(filterValueItem.title)),
            );

            return filter.every((filterValueItem, filterValueItemIndex) =>
              this.filtersService.isFilterValueMatch(
                dataSourceItem[columnDataKeys[filterValueItemIndex]],
                filterValueItem.filters,
              ),
            );
          }
        });
      } else {
        return dataSource;
      }
    }
  }

  public prepareTenants(tenants: Tenant[], tenantsCategories: TenantCategory[]): Tenant[] {
    return tenants.map(tenant => {
      const tenantCategory = this.getTenantCategoryOfTenant(tenant, tenantsCategories);
      const tenantCategoryAcl = tenantCategory ? tenantCategory.acls.find(acl => acl.active) : null;

      return {
        id: tenant.id,
        name: tenant.name,
        category: tenantCategory ? tenantCategory.name : '',
        products: tenantCategoryAcl ? tenantCategoryAcl.productVersions.map(version => version.name) : [],
      };
    });
  }

  private getFilterTitle(filterValueTitle: string): string {
    return filterValueTitle === FiltersTranslation.Common.Product
      ? FiltersTranslation.Common.Products
      : FiltersTranslation.Common.Category;
  }

  private getTenantCategoryOfTenant(tenant: Tenant, tenantsCategories: TenantCategory[]): TenantCategory {
    return tenantsCategories.find(tenantCategory => tenantCategory.id === tenant.category);
  }

  private getColumnDataKey(title: string): string {
    return FILTER_TRANSLATIONS_WITH_COLUMN_DATA_KEYS[title] || null;
  }
}
