import { TenantCategory } from '../../../../../../../shared/models/orchestrator/orchestrator-b2b/tenant/tenant-category.model';

const mockTenantCategories: TenantCategory[] = [
  { id: '1', label: 'Early Adopter - USA', tags: 'Early, USA' },
  { id: '2', label: 'Mid Adopter - USA', tags: 'Mid, USA' },
  { id: '3', label: 'Late Adopter - USA', tags: 'Late, USA' },
];

export {
  mockTenantCategories
};
