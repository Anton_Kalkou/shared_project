import { Injectable } from '@angular/core';

import { map } from 'rxjs/operators';
import { forkJoin, Observable, of } from 'rxjs';
import { ApiService } from 'src/modules/shared/services/api.service';

import { ProductName, Tenant, TenantResponse } from '../../../../../../shared/models/orchestrator/orchestrator-b2b/tenant/tenants.model';
import { TenantCategory } from '../../../../../../shared/models/orchestrator/orchestrator-b2b/tenant/tenant-category.model';
import { ORCHESTRATOR } from '../../../../../constants/constants';
import { mapTenantResponseWithTenantCategories } from '../utils/tenant.mapper';
import { TenantCategoryResponse } from '../../../../../../shared/models/orchestrator/orchestrator-b2b/tenants-categories/tenants-categories.model';
import { mapTenantsCategoryForDropdown } from '../utils/tenant-category.mapper';
import { ProductResponse } from '../../../../../../shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';

@Injectable({ providedIn: 'root' })
export class TenantService {
  constructor(private readonly apiService: ApiService) {}

  public loadTenantWithCategory(id: string): Observable<Tenant> {
    const tenantsCategories$ = this.loadTenantCategories();
    const tenant$ = this.loadTenant(id);

    return forkJoin([tenant$, tenantsCategories$])
      .pipe(
        map(([tenantResponse, tenantCategories]) => mapTenantResponseWithTenantCategories(tenantResponse, tenantCategories))
      );
  }

  public loadTenantProductsNames(tenantId: string): Observable<ProductName[]> {
    return this.loadProductsNames();
  }

  public loadProductsNames(): Observable<ProductName[]> {
    return this.apiService.get(ORCHESTRATOR.orchestratorB2B.endpoints.products)
      .pipe(map((productResponses: ProductResponse[]) => productResponses.map(({ id, name }: ProductResponse) => ({ id, name }))));
  }

  public saveSelectedProductsNames(): Observable<ProductName[]> {
    return of([]);
  }

  public updateTenant(id: string, tenantInfo: Tenant): Observable<[]> {
    return this.apiService.put(`${ORCHESTRATOR.orchestratorB2B.endpoints.tenants}/${id}`, { categoryId: tenantInfo.category });
  }

  public deleteTenant(id: string): Observable<[]> {
    const tenantUrl = `${ORCHESTRATOR.orchestratorB2B.endpoints.tenants}/${id}`;

    return this.apiService.delete(tenantUrl);
  }

  public loadTenantsCategoriesForDropdown(): Observable<TenantCategory[]> {
    return this.loadTenantCategories()
      .pipe(map((tenantCategories: TenantCategoryResponse[]) => mapTenantsCategoryForDropdown(tenantCategories)));
  }

  private loadTenant(id: string): Observable<TenantResponse> {
    const tenantUrl = `${ORCHESTRATOR.orchestratorB2B.endpoints.tenants}/${id}`;

    return this.apiService.get(tenantUrl);
  }

  private loadTenantCategories(): Observable<TenantCategoryResponse[]> {
    return this.apiService.get(ORCHESTRATOR.orchestratorB2B.endpoints.tenantsCategories);
  }
}
