export enum Translation {
  Name = 'shared.name',
  Code = 'shared.code',
  Description = 'shared.description',
  CreateInfoContent = 'orchestrator-b2b.products.create-info-content',
  DeleteInfoContent = 'orchestrator-b2b.products.delete-info-content'
}

export enum ColumnDataKey {
  Id = 'id',
  Name = 'name',
  Code = 'code',
  Description = 'description',
  Action = 'action'
}
