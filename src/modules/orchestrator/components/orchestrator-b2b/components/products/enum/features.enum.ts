export enum Translation {
  Name = 'orchestrator-b2b.products.feature-name',
  Description = 'shared.description'
}

export enum ColumnDataKey {
  Id = 'id',
  Select = 'select',
  Name = 'name',
  Description = 'description'
}
