import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { CreatedData } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';
import { Product } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/products/product.model';
import { ApiService } from 'src/modules/shared/services/api.service';
import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';

@Injectable({ providedIn: 'root' })
export class ProductService {
  constructor(private readonly apiService: ApiService) {}

  public loadProduct(id: string): Observable<Product> {
    const updateProductUrl = `${ORCHESTRATOR.orchestratorB2B.endpoints.products}/${id}`;

    return this.apiService.get(updateProductUrl);
  }

  public updateProduct(id: string, product: Product): Observable<void> {
    const updateProductUrl = `${ORCHESTRATOR.orchestratorB2B.endpoints.products}/${id}`;

    return this.apiService.put(updateProductUrl, product);
  }

  public deleteProduct(id: string): Observable<void> {
    const updateProductUrl = `${ORCHESTRATOR.orchestratorB2B.endpoints.products}/${id}`;

    return this.apiService.delete(updateProductUrl);
  }

  public createProduct(product: Product): Observable<CreatedData> {
    return this.apiService.post(ORCHESTRATOR.orchestratorB2B.endpoints.products, product);
  }
}
