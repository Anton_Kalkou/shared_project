import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { ApiService } from '../../../../../../shared/services/api.service';
import { ORCHESTRATOR } from '../../../../../constants/constants';
import { Product } from '../../../../../../shared/models/orchestrator/orchestrator-b2b/products/product.model';
import { FilterGroup } from '../../../../../../shared/models/filter.model';
import { FilterType } from '../../../../../../support/enum/support.enum';
import { FiltersService } from '../../../../../../shared/services/filters.service';

@Injectable({ providedIn: 'root' })
export class ProductsService {
  constructor(
    private readonly apiService: ApiService,
    private readonly filtersService: FiltersService
    ) {}

  public loadProducts(): Observable<Product[]> {
    return this.apiService.get(ORCHESTRATOR.orchestratorB2B.endpoints.products);
  }

  public filterProducts(
    dataSource: Product[],
    filterType: string,
    filterValue: string | FilterGroup[],
    searchColumnKey: string
  ): Product[] {
    if (filterType === FilterType.Search) {
      return this.filtersService.filterDataOnSearch(dataSource, filterValue as string, searchColumnKey);
    }
  }
}
