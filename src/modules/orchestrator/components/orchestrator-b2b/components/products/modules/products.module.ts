import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SectionHeaderModule } from 'src/modules/orchestrator/shared/components/section-header/section-header.module';
import { AddButtonModule } from 'src/modules/shared/components/add-button/add-button.module';
import { ConfirmationModule } from 'src/modules/shared/components/confirmation/confirmation.module';
import { DataTableModule } from 'src/modules/shared/components/data-table/data-table.module';
import { IconButtonModule } from 'src/modules/shared/components/icon-button/icon-button.module';
import { SearchModule } from 'src/modules/shared/components/search/search.module';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { SharedModule } from 'src/modules/shared/modules/shared.module';

import { ProductChangelogsComponent } from '../components/product-changelogs/product-changelogs.component';
import { ProductAddEditComponent } from '../components/product-add-edit/product-add-edit.component';
import { ProductFeaturesComponent } from '../components/product-features/product-features.component';
import { ProductInfoComponent } from '../components/product-info/product-info.component';
import { ProductsComponent } from '../components/products/products.component';
import { DataListModule } from '../../../shared/components/data-list/data-list.module';
import {
  AutoSaveNotificationTemplateModule
} from '../../../../../../shared/components/auto-save-notification-template/auto-save-notification-template.module';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    SharedModule,
    MaterialModule,
    DataTableModule,
    SectionHeaderModule,
    SearchModule,
    ConfirmationModule,
    IconButtonModule,
    AddButtonModule,
    DataListModule,
    AutoSaveNotificationTemplateModule
  ],
  declarations: [
    ProductsComponent,
    ProductAddEditComponent,
    ProductInfoComponent,
    ProductFeaturesComponent,
    ProductChangelogsComponent
  ],
  exports: [
    ProductsComponent
  ]
})
export class ProductsModule {}
