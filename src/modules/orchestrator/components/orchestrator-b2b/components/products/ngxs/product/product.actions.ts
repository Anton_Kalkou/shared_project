import { Product } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/products/product.model';

export class FetchProduct  {
  static readonly type = '[Product] Fetch';

  constructor(public id: string) {}
}

export class CreateProduct  {
  static readonly type = '[Product] Create';

  constructor(public product: Product) {}
}

export class UpdateProduct  {
  static readonly type = '[Product] Update';

  constructor(public id: string, public product: Product) {}
}

export class DeleteProduct  {
  static readonly type = '[Product] Delete';

  constructor(public id: string) {}
}

export class ResetSelectedProduct {
  static readonly type = '[Product] Reset';
}
