import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import { Router } from '@angular/router';
import { cloneDeep } from 'lodash';
import { combineLatest } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { SetNavigationButtonStatesTitle } from 'src/modules/layout/ngxs/layout.actions';
import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { TranslationService } from 'src/modules/shared/services/translation.service';
import { Product } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/products/product.model';
import { NotificationService } from 'src/modules/shared/services/notification.service';

import { ProductService } from '../../services/product.service';
import { Translation as ProductTranslation } from '../../enum/products.enum';
import { INITIAL_STATE } from '../../constants/product.constants';
import { SetVersionState } from '../../../../shared/ngxs/version/version.actions';
import { VersionService } from '../../../../shared/services/version.service';
import { getValuesWithoutCode } from '../../../../utils/orchestrator-b2b.utils';
import { DataLabel } from '../../../../enum/orchestrator-b2b.enum';
import {
  FetchProduct,
  CreateProduct,
  UpdateProduct,
  DeleteProduct,
  ResetSelectedProduct
} from './product.actions';

export interface ProductStateModel {
  selectedProduct: Product;
  isProductCreated: boolean;
}

@State<ProductStateModel>({
  name: 'product',
  defaults: INITIAL_STATE
})
@Injectable()
export class ProductState {
  constructor(
    private readonly productService: ProductService,
    private readonly versionService: VersionService,
    private readonly translationService: TranslationService,
    private readonly notificationService: NotificationService,
    private readonly router: Router,
    private readonly store: Store
  ) {}

  @Selector()
  static getItemId({ selectedProduct }: ProductStateModel) {
    return selectedProduct && selectedProduct.id;
  }

  @Selector()
  static getItemName({ selectedProduct }: ProductStateModel) {
    return selectedProduct && selectedProduct.name;
  }

  @Action(FetchProduct)
  fetchProduct({ patchState }: StateContext<ProductStateModel>, { id }: FetchProduct) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.productService.loadProduct(id).subscribe(selectedProduct => {
      const { name, versions } = selectedProduct;

      this.store.dispatch([
        new SetVersionState(
          `${ORCHESTRATOR.orchestratorB2B.endpoints.products}/${selectedProduct.id}`,
          selectedProduct,
          this.versionService.getSelectedVersion(versions),
          ORCHESTRATOR.orchestratorB2B.endpoints.features,
          DataLabel.Features
        ),
        new SetSpinnerVisibility(false),
        new SetNavigationButtonStatesTitle(true, `Edit ${name}`)
      ]);

      patchState({ selectedProduct });
    });
  }

  @Action(CreateProduct)
  createProduct({ patchState }: StateContext<ProductStateModel>, { product }: CreateProduct) {
    const productForSaving = getValuesWithoutCode(product);
    const createdProduct$ = this.productService.createProduct(productForSaving);
    const translatedMessage$ = this.translationService.translate(ProductTranslation.CreateInfoContent);

    this.store.dispatch(new SetSpinnerVisibility(true));

    return createdProduct$
      .pipe(flatMap(({ id }) => {
        const getProductById$ = this.productService.loadProduct(id);

        return combineLatest([getProductById$, translatedMessage$]);
      }))
      .subscribe(([{ id, code }, createInfoContent]) => {
        const selectedProduct = { id, code, ...product, versions: [] };

        this.store.dispatch([
          new SetVersionState(
            `${ORCHESTRATOR.orchestratorB2B.endpoints.products}/${selectedProduct.id}`,
            selectedProduct,
            undefined,
            ORCHESTRATOR.orchestratorB2B.endpoints.features,
            DataLabel.Features
          ),
          new SetSpinnerVisibility(false)
        ]);

        this.notificationService.showSuccessMessage(null, createInfoContent);

        patchState({ isProductCreated: true, selectedProduct });
      });
  }

  @Action(UpdateProduct)
  updateProduct({ getState, patchState }: StateContext<ProductStateModel>, { id, product }: UpdateProduct) {
    const productForSaving = getValuesWithoutCode(product);

    return this.productService.updateProduct(id, productForSaving).subscribe(
      () => {
        this.store.dispatch(new SetNavigationButtonStatesTitle(true, `Edit ${product.name}`));

        const { selectedProduct } = getState();

        patchState({ selectedProduct: { ...cloneDeep(selectedProduct), id, ...product } });
      }
    );
  }

  @Action(DeleteProduct)
  deleteProduct({}: StateContext<ProductStateModel>, { id }: DeleteProduct) {
    const deleteProduct$ = this.productService.deleteProduct(id);
    const translatedMessage$ = this.translationService.translate(ProductTranslation.DeleteInfoContent);

    this.store.dispatch(new SetSpinnerVisibility(true));

    return combineLatest([deleteProduct$, translatedMessage$]).subscribe(
      ([, translatedMessage]) => {
        this.store.dispatch([
          new SetSpinnerVisibility(false),
          new ResetSelectedProduct()
        ]);
        this.router.navigate([`/${ORCHESTRATOR.orchestratorB2B.products.productsPath}`]);
        this.notificationService.showSuccessMessage(translatedMessage);
      },
      () => this.store.dispatch(new SetSpinnerVisibility(false))
    );
  }

  @Action(ResetSelectedProduct)
  resetSelectedProduct({ patchState }: StateContext<ProductStateModel>) {
    patchState({ ...INITIAL_STATE });
  }
}
