import { Injectable } from '@angular/core';
import { Action, State, StateContext, Store } from '@ngxs/store';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { ProductsService } from '../../services/products.service';
import { FetchProducts } from './products.actions';
import { ProductState } from '../product/product.state';
import { Product } from '../../../../../../../shared/models/orchestrator/orchestrator-b2b/products/product.model';

export interface ProductsStateModel {
  products: Product[];
}

@State<ProductsStateModel>({
  name: 'products',
  defaults: {
    products: []
  },
  children: [ProductState]
})
@Injectable()
export class ProductsState {

  constructor(private productsService: ProductsService, private store: Store) {}

  @Action(FetchProducts)
  fetchProducts({ patchState }: StateContext<ProductsStateModel>) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.productsService.loadProducts().subscribe(products => {
      this.store.dispatch(new SetSpinnerVisibility(false));
      patchState({ products });
    });
  }
}
