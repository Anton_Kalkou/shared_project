import { Component, OnDestroy, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';

import { Observable } from 'rxjs';

import { SetHeaderTitle, SetNavigationButtonStatesTitle } from 'src/modules/layout/ngxs/layout.actions';
import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { Column } from 'src/modules/shared/models/data-table.model';
import { ActionType, Icon, TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum';
import { FetchProducts } from '../../ngxs/products/products.actions';
import { InfoType } from '../../../../enum/orchestrator-b2b.enum';
import { Translation as ProductsTranslation, ColumnDataKey } from '../../enum/products.enum';
import { ProductsService } from '../../services/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnDestroy {

  @Select(state => state.orchestratorB2B.products.products) products$: Observable<any[]>;

  public displayedColumns: Column[];

  constructor(
    public readonly productsService: ProductsService,
    private readonly store: Store
  ) {}

  ngOnInit(): void {
    this.initializeValues();
    this.dispatchActions();
  }

  ngOnDestroy(): void {
    this.store.dispatch(new SetNavigationButtonStatesTitle(false));
  }

  private initializeValues(): void {
    this.displayedColumns = [
      { title: ColumnDataKey.Name, translationKey: ProductsTranslation.Name, valueType: ValueType.Text, textType: TextType.Simple },
      { title: ColumnDataKey.Code, translationKey: ProductsTranslation.Code, valueType: ValueType.Text, textType: TextType.Simple },
      { title: ColumnDataKey.Description, translationKey: ProductsTranslation.Description, valueType: ValueType.Text, textType: TextType.Simple },
      {
        title: ColumnDataKey.Action,
        valueType: ValueType.Action,
        actionType: ActionType.CustomIcon,
        actionDataKey: ColumnDataKey.Id,
        path: InfoType.Edit,
        icon: Icon.Pencil,
        isSortableDisabled: true
      }
    ];
  }

  private dispatchActions(): void {
    this.store.dispatch([
      new SetHeaderTitle(ORCHESTRATOR.orchestratorB2B.headerTitles.orchestratorB2B),
      new SetNavigationButtonStatesTitle(true, ORCHESTRATOR.orchestratorB2B.navButtonStateTitles.products),
      new FetchProducts()
    ]);
  }
}
