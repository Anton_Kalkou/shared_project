import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { Product } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/products/product.model';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { TranslationService } from 'src/modules/shared/services/translation.service';

import { CreateProduct, DeleteProduct, FetchProduct, UpdateProduct } from '../../ngxs/product/product.actions';
import { ProductState } from '../../ngxs/product/product.state';
import { BaseInfoPage } from '../../../../shared/classes/base/base-info-page.class';

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.scss']
})
export class ProductInfoComponent extends BaseInfoPage<Product> {
  @Select(state => state.orchestratorB2B.products.product.selectedProduct) selectedItem$: Observable<Product>;
  @Select(state => state.orchestratorB2B.products.product.isProductCreated) isCreated$: Observable<boolean>;

  public formGroup: FormGroup;
  public isEdit: boolean;

  protected selectedIdSelector = ProductState.getItemId;
  protected selectedNameSelector = ProductState.getItemName;

  constructor(
    private readonly route: ActivatedRoute,
    store: Store,
    formBuilder: FormBuilder,
    notificationService: NotificationService,
    translationService: TranslationService
  ) {
    super(store, formBuilder, notificationService, translationService);
  }

  public onCreate(): void {
    if (this.formGroup.invalid) {
      return;
    }

    const productForSaving = this.formGroup.getRawValue();

    this.store.dispatch(new CreateProduct(productForSaving));
  }

  public onDelete(): void {
    this.store.dispatch(new DeleteProduct(this.currentId));
  }

  protected onEdit(): void {
    this.setInitialFormValues(this.formGroup);
    this.store.dispatch(new UpdateProduct(this.currentId, this.initialFormValues));
  }

  protected initEditData(): void {
    this.currentId = this.route.snapshot.paramMap.get('id');

    this.initSubscriptions();
    this.store.dispatch(new FetchProduct(this.currentId));
  }

  protected initFormGroup(initialProduct?: Product): void {
    this.formGroup = this.formBuilder.group({
      name: [initialProduct ? initialProduct.name : '', Validators.required],
      code: [{ value: initialProduct ? initialProduct.code : '', disabled: true }],
      description: initialProduct ? initialProduct.description : ''
    });
  }
}
