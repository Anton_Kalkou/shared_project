import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';

import { Observable } from 'rxjs';
import { SetHeaderTitle, SetNavigationButtonStatesTitle } from 'src/modules/layout/ngxs/layout.actions';
import { isSubstring } from 'src/modules/shared/helpers';

import { InfoType } from '../../../../enum/orchestrator-b2b.enum';
import { ResetSelectedProduct } from '../../ngxs/product/product.actions';
import { Translation as OrchestratorB2bTranslation } from '../../../../enum/orchestrator-b2b.enum';
import { ResetVersionState } from '../../../../shared/ngxs/version/version.actions';

@Component({
  selector: 'app-product-add-edit',
  templateUrl: './product-add-edit.component.html',
  styleUrls: ['./product-add-edit.component.scss']
})
export class ProductAddEditComponent implements OnInit, OnDestroy {
  @Select(state => state.orchestratorB2B.products.product.isProductCreated) isCreated$: Observable<boolean>;

  public infoType: string;

  constructor(
    private readonly router: Router,
    private readonly store: Store
  ) {}

  ngOnInit(): void {
    this.initializeValues();
    this.dispatchActions();
  }

  ngOnDestroy(): void {
    this.store.dispatch([new ResetSelectedProduct(), new ResetVersionState()]);
  }

  private initializeValues(): void {
    this.infoType = isSubstring(this.router.url, InfoType.Add) ? InfoType.Add : InfoType.Edit;
  }

  private dispatchActions(): void {
    this.store.dispatch(new SetHeaderTitle(''));

    if (this.infoType === InfoType.Add) {
      this.store.dispatch(new SetNavigationButtonStatesTitle(true, OrchestratorB2bTranslation.AddNavigationButtonStatesTitle));
    }
  }
}
