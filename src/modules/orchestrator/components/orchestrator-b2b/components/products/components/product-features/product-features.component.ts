import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { MatSelectChange } from '@angular/material/select';
import { Observable } from 'rxjs';

import { TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum';
import { Column } from 'src/modules/shared/models/data-table.model';
import { Feature } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/features/features.model';
import { Product } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/products/product.model';
import { Version } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { TranslationService } from 'src/modules/shared/services/translation.service';

import { ColumnDataKey, Translation as FeaturesTranslation } from '../../enum/features.enum';
import { CreateVersionDialogComponent } from '../../../../shared/components/create-version-dialog/create-version-dialog.component';
import { BaseVersionPage } from '../../../../shared/classes/base/base-version-page.class';
import { ProductState } from '../../ngxs/product/product.state';
import { FeaturesService } from '../../../features/services/features.service';
import { DataLabel } from '../../../../enum/orchestrator-b2b.enum';
import { versionExistsValidator } from '../../../../shared/validators/versionExists.validator';
import {
  ChangeSelectedVersion,
  ChangeVersionState,
  CreateVersion,
  FetchVersionTableData,
  PublishVersion,
} from '../../../../shared/ngxs/version/version.actions';

@Component({
  selector: 'app-product-features',
  templateUrl: './product-features.component.html',
  styleUrls: ['./product-features.component.scss']
})
export class ProductFeaturesComponent extends BaseVersionPage<Product, Feature> {
  @Select(state => state.orchestratorB2B.version.selectedItem) selectedItem$: Observable<Product>;
  @Select(state => state.orchestratorB2B.version.selectedVersion) selectedVersion$: Observable<Version>;
  @Select(state => state.orchestratorB2B.version.isVersionPuplished) isVersionPuplished$: Observable<boolean>;
  @Select(state => state.orchestratorB2B.version.tableData) tableData$: Observable<Feature[]>;

  constructor(
    public readonly featuresService: FeaturesService,
    protected readonly formBuilder: FormBuilder,
    protected readonly route: ActivatedRoute,
    protected readonly store: Store,
    protected readonly notificationService: NotificationService,
    protected readonly translationService: TranslationService,
    private readonly dialog: MatDialog
  ) {
    super(
      route,
      store,
      formBuilder,
      notificationService,
      translationService,
      ProductState,
    );
  }

  public onVersionChange({ value }: MatSelectChange): void {
    this.store.dispatch(new ChangeSelectedVersion(value));
  }

  public onPublish(): void {
    this.store.dispatch(new PublishVersion());
  }

  public onVersion(isDuplicate?: boolean): void {
    this.subscribeTo = this.dialog.open(CreateVersionDialogComponent, this.getVersionDialogOptions(isDuplicate))
      .afterClosed()
      .subscribe(version => {
        return version && this.store.dispatch(new CreateVersion(
          { ...version, isDisabled: false },
          isDuplicate
        ));
      });
  }

  public onVersionState(): void {
    this.store.dispatch(new ChangeVersionState(!this.formGroup.get(DataLabel.IsDisabled).value));
  }

  public onSelectTableData(features: Feature[]): void {
    this.setSelectedDataFormControl(DataLabel.Features, features as Feature[]);
  }

  protected initFormGroup(selectedVersion?: Version): void {
    const { isDisabled = true, name = '', description = '', features = [] } = selectedVersion || {};

    this.formGroup = this.formBuilder.group({
      isDisabled,
      name: [
        { value: name, disabled: isDisabled },
        {
          validators: [Validators.required],
          asyncValidators: [versionExistsValidator(this.selectedItem$, this.selectedVersion$)],
        }
      ],
      description: { value: description, disabled: isDisabled}
    });

    this.setSelectedDataFormControl(DataLabel.Features, features as Feature[]);
  }

  protected getColumns(): Column[] {
    return [
      { title: ColumnDataKey.Select, valueType: ValueType.Select, isSortableDisabled: true },
      { title: ColumnDataKey.Name, translationKey: FeaturesTranslation.Name, valueType: ValueType.Text, textType: TextType.Simple },
      { title: ColumnDataKey.Description, translationKey: FeaturesTranslation.Description, valueType: ValueType.Text, textType: TextType.Simple }
    ];
  }

  protected dispatchActions(): void {
    this.store.dispatch(new FetchVersionTableData());
  }
}
