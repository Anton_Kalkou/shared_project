import { Route } from '@angular/router';

import { AuthenticationGuard } from 'src/modules/shared/guards/auth.guard';

import { ProductAddEditComponent } from '../components/product-add-edit/product-add-edit.component';
import { ProductsComponent } from '../components/products/products.component';

export const PRODUCTS_ROUTES: Route[] = [
  { path: 'orchestrator-b2b/products', component: ProductsComponent, canActivate: [AuthenticationGuard] },
  { path: 'orchestrator-b2b/products/add', component: ProductAddEditComponent, canActivate: [AuthenticationGuard] },
  { path: 'orchestrator-b2b/products/:id/edit', component: ProductAddEditComponent, canActivate: [AuthenticationGuard] }
];
