import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { DataTableModule } from 'src/modules/shared/components/data-table/data-table.module';
import { SectionHeaderModule } from 'src/modules/orchestrator/shared/components/section-header/section-header.module';
import { SearchModule } from 'src/modules/shared/components/search/search.module';
import { ConfirmationModule } from 'src/modules/shared/components/confirmation/confirmation.module';
import { AddButtonModule } from 'src/modules/shared/components/add-button/add-button.module';
import { IconButtonModule } from 'src/modules/shared/components/icon-button/icon-button.module';

import { PermissionsComponent } from '../components/permissions/permissions.component';
import { PermissionAddEditComponent } from '../components/permission-add-edit/permission-add-edit.component';
import { PermissionAPIsComponent } from '../components/permission-apis/permission-apis.component';
import { PermissionChangelogsComponent } from '../components/permission-changelogs/permission-changelogs.component';
import { PermissionInfoComponent } from '../components/permission-info/permission-info.component';
import { SelectApisComponent } from '../components/select-apis/select-apis.component';
import { DataListModule } from '../../../shared/components/data-list/data-list.module';
import { AutoSaveNotificationTemplateModule } from '../../../../../../shared/components/auto-save-notification-template/auto-save-notification-template.module';

@NgModule({
  imports: [
    RouterModule,
    SharedModule,
    MaterialModule,
    DataTableModule,
    SectionHeaderModule,
    SearchModule,
    ConfirmationModule,
    AddButtonModule,
    IconButtonModule,
    DataListModule,
    AutoSaveNotificationTemplateModule
  ],
  declarations: [
    PermissionsComponent,
    PermissionAddEditComponent,
    PermissionAPIsComponent,
    PermissionChangelogsComponent,
    PermissionInfoComponent,
    SelectApisComponent
  ],
  exports: [
    PermissionsComponent
  ]
})
export class PermissionsModule {}
