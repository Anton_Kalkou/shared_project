import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Permission } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/permissions/permissions.model';
import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { CreatedData } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';
import { ApiService } from 'src/modules/shared/services/api.service';

@Injectable({ providedIn: 'root' })
export class PermissionService {
  constructor(private readonly apiService: ApiService) {}

  public loadPermission(id: string): Observable<Permission> {
    return this.apiService.get(`${ORCHESTRATOR.orchestratorB2B.endpoints.permissions}/${id}`);
  }

  public updatePermission(id: string, permission: any): Observable<void> {
    const updatePermissionUrl = `${ORCHESTRATOR.orchestratorB2B.endpoints.permissions}/${id}`;

    return this.apiService.put(updatePermissionUrl, permission);
  }

  public deletePermission(id: string): Observable<void> {
    const deletePermissionUrl = `${ORCHESTRATOR.orchestratorB2B.endpoints.permissions}/${id}`;

    return this.apiService.delete(deletePermissionUrl);
  }

  public createPermission(permission: any): Observable<CreatedData> {
    return this.apiService.post(ORCHESTRATOR.orchestratorB2B.endpoints.permissions, permission);
  }
}
