import { Injectable } from '@angular/core';
import { combineLatest, Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { flatMap } from 'lodash';

import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { FilterGroup } from 'src/modules/shared/models/filter.model';
import { Permission } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/permissions/permissions.model';
import { ApiService } from 'src/modules/shared/services/api.service';
import { FiltersService } from 'src/modules/shared/services/filters.service';
import { FilterType } from 'src/modules/support/enum/support.enum';
import { Translation as FiltersTranslation } from 'src/modules/shared/components/filters/enum/filters.enum';
import { PermissionResponse, FeatureResponse } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';

import { DataLabel } from '../../../enum/orchestrator-b2b.enum';
import { VersionService } from '../../../shared/services/version.service';

@Injectable({ providedIn: 'root' })
export class PermissionsService {

  constructor(
    private readonly apiService: ApiService,
    private readonly filtersService: FiltersService,
    private readonly versionService: VersionService
  ) {}

  public loadPermissions(): Observable<Permission[]> {
    const permissionsApiRequest = this.apiService.get(ORCHESTRATOR.orchestratorB2B.endpoints.permissions);
    const featuresApiRequest = this.apiService.get(ORCHESTRATOR.orchestratorB2B.endpoints.features);

    return combineLatest([permissionsApiRequest, featuresApiRequest]).pipe(
      switchMap(([permissionsResponse, featuresResponse]: [PermissionResponse[], FeatureResponse[]]) => {
        return of(this.preparePermissions(permissionsResponse, featuresResponse));
      })
    );
  }

  public filterPermissions(dataSource: Permission[], filterType: string, filterValue: string | FilterGroup[], searchColumnKey: string): Permission[] {
    if (filterType === FilterType.Search) {
      return this.filtersService.filterDataOnSearch(dataSource, filterValue as string, searchColumnKey);
    } else {
      const filter = filterValue as FilterGroup[];

      if (filter.length) {
        return dataSource.filter(({ relatedFeatures }) => {
          const relatedFeaturesNames = relatedFeatures.map(relatedFeature => relatedFeature.name);

          return filter.some(({ filters }) => this.filtersService.isFilterValueMatch(relatedFeaturesNames, filters));
        });
      } else {
        return dataSource;
      }
    }
  }

  public getPermissionsFilters(permissions: Permission[]): FilterGroup[] {
    return [
      {
        title: FiltersTranslation.Common.Feature,
        filters: [...new Set(this.getPermissionsFiltersLabels(permissions))],
      },
    ];
  }

  private preparePermissions(permissionsResponse: PermissionResponse[], featuresResponse: FeatureResponse[]): Permission[] {
    return permissionsResponse.map(({ id, name, description }) => {
      const relatedFeatures = this.versionService.getRelatedData<FeatureResponse>(id, DataLabel.Permissions, featuresResponse);

      return {
        id,
        description,
        name,
        relatedFeatures,
      };
    });
  }

  private getPermissionsFiltersLabels(permissions: Permission[]): string[] {
    return flatMap(
      permissions.map(permission => permission.relatedFeatures.map(relatedFeature => relatedFeature.name)),
    );
  }
}
