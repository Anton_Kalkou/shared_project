export enum Translation {
  PermissionName = 'orchestrator-b2b.permissions.permission-name',
  RelatedFeatures = 'orchestrator-b2b.permissions.related-features',
  CreateInfoContent = 'orchestrator-b2b.permissions.create-info-content',
  DeleteInfoContent = 'orchestrator-b2b.permissions.delete-info-content',
  Description = 'shared.description',
  Name = 'shared.name'
}

export enum ColumnDataKey {
  Id = 'id',
  RelatedFeatures = 'relatedFeatures',
  Description = 'description',
  Name = 'name',
  Action = 'action'
}
