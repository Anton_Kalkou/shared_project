export enum Translation {
  Service = 'orchestrator-b2b.permissions.service',
  Method = 'shared.method',
  Api = 'shared.api',
  Profile = 'orchestrator-b2b.permissions.profile',
  DeleteApiTitle = 'orchestrator-b2b.permissions.delete-api-title',
  DeleteApiCaption = 'orchestrator-b2b.permissions.delete-api-caption',
  DeleteApiAcceptTitle = 'shared.Yes'
}

export enum ColumnDataKey {
  Id = 'id',
  Service = 'service',
  Method = 'method',
  Api = 'api',
  Profile = 'profile',
  Action = 'action'
}
