import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import { combineLatest } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { cloneDeep } from 'lodash';

import { SetNavigationButtonStatesTitle } from 'src/modules/layout/ngxs/layout.actions';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { TranslationService } from 'src/modules/shared/services/translation.service';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { Permission } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/permissions/permissions.model';
import { Feature } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/features/features.model';
import { FeatureResponse } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';

import { FeaturesService } from '../../../features/services/features.service';
import { PermissionService } from '../../services/permission.service';
import { INITIAL_STATE } from '../../constants/permission.constants';
import { Translation as PermissionTranslation } from '../../enum/permissions.enum';
import { getValuesWithoutCode } from '../../../../utils/orchestrator-b2b.utils';
import { SetVersionState } from '../../../../shared/ngxs/version/version.actions';
import { VersionService } from '../../../../shared/services/version.service';
import { DataLabel } from '../../../../enum/orchestrator-b2b.enum';
import {
  FetchPermission,
  CreatePermission,
  UpdatePermission,
  DeletePermission,
  ResetSelectedPermission
} from './permission.actions';

export interface PermissionStateModel {
  isPermissionCreated: boolean;
  selectedPermission: Permission;
  relatedFeatures: Feature[];
}

@State<PermissionStateModel>({
  name: 'permission',
  defaults: INITIAL_STATE
})
@Injectable()
export class PermissionState {
  constructor(
    private readonly versionService: VersionService,
    private readonly permissionService: PermissionService,
    private readonly featuresService: FeaturesService,
    private readonly translationService: TranslationService,
    private readonly notificationService: NotificationService,
    private readonly router: Router,
    private readonly store: Store
  ) {}

  @Selector()
  static getItemId({ selectedPermission }: PermissionStateModel) {
    return selectedPermission && selectedPermission.id;
  }

  @Selector()
  static getItemName({ selectedPermission }: PermissionStateModel) {
    return selectedPermission && selectedPermission.name;
  }

  @Action(FetchPermission)
  fetchPermission({ patchState }: StateContext<PermissionStateModel>, { id }: FetchPermission) {
    const selectedPermission$ = this.permissionService.loadPermission(id);
    const features$ = this.featuresService.loadFeatures();

    this.store.dispatch(new SetSpinnerVisibility(true));

    return combineLatest([selectedPermission$, features$]).subscribe(
      ([selectedPermission, features]) => {
        const { name, versions } = selectedPermission;
        const selectedVersion = this.versionService.getSelectedVersion(versions);
        const relatedFeatures = this.versionService.getRelatedData<FeatureResponse>(id, DataLabel.Permissions, features);

        this.store.dispatch([
          new SetVersionState(
            `${ORCHESTRATOR.orchestratorB2B.endpoints.permissions}/${id}`,
            selectedPermission,
            selectedVersion,
            ORCHESTRATOR.orchestratorB2B.endpoints.apis,
            DataLabel.Apis
          ),
          new SetSpinnerVisibility(false),
          new SetNavigationButtonStatesTitle(true, `Edit ${name}`)
        ]);

        patchState({ selectedPermission, relatedFeatures });
      },
      () => this.store.dispatch(new SetSpinnerVisibility(false))
    );
  }

  @Action(CreatePermission)
  createPermission({ patchState }: StateContext<PermissionStateModel>, { permission }: CreatePermission) {
    const permissionForSaving = getValuesWithoutCode(permission);
    const createdPermission$ = this.permissionService.createPermission(permissionForSaving);
    const translatedMessage$ = this.translationService.translate(PermissionTranslation.CreateInfoContent);

    this.store.dispatch(new SetSpinnerVisibility(true));

    return createdPermission$
      .pipe(
        flatMap(({ id }) => {
          const getPermissionById$ = this.permissionService.loadPermission(id);

          return combineLatest([getPermissionById$, translatedMessage$]);
        })
      )
      .subscribe(([{ id, code }, createInfoContent]) => {
        const selectedPermission = { id, code, ...permission, versions: [] };

        this.store.dispatch([
          new SetVersionState(
            `${ORCHESTRATOR.orchestratorB2B.endpoints.permissions}/${selectedPermission.id}`,
            selectedPermission,
            undefined,
            ORCHESTRATOR.orchestratorB2B.endpoints.apis,
            DataLabel.Apis
          ),
          new SetSpinnerVisibility(false)
        ]);

        this.notificationService.showSuccessMessage(null, createInfoContent);

        patchState({ isPermissionCreated: true, selectedPermission });
      });
  }

  @Action(UpdatePermission)
  updatePermission({ getState, patchState }: StateContext<PermissionStateModel>, { id, permission }: UpdatePermission) {
    const permissionForSaving = getValuesWithoutCode(permission);

    return this.permissionService.updatePermission(id, permissionForSaving).subscribe(
      () => {
        const { selectedPermission } = getState();

        patchState({ selectedPermission: { ...cloneDeep(selectedPermission), id, ...permission } });
      }
    );
  }

  @Action(DeletePermission)
  deletePermission({}: StateContext<PermissionStateModel>, { permissionId }: DeletePermission) {
    const deletePermission$ = this.permissionService.deletePermission(permissionId);
    const translatedMessage$ = this.translationService.translate(PermissionTranslation.DeleteInfoContent);

    this.store.dispatch(new SetSpinnerVisibility(true));

    return combineLatest([deletePermission$, translatedMessage$]).subscribe(
      ([, translatedMessage]) => {
        this.store.dispatch([
          new SetSpinnerVisibility(false),
          new ResetSelectedPermission()
        ]);
        this.router.navigate([`/${ORCHESTRATOR.orchestratorB2B.permissions.permissionsPath}`]);
        this.notificationService.showSuccessMessage(translatedMessage);
      },
      () => this.store.dispatch(new SetSpinnerVisibility(false))
    );
  }

  @Action(ResetSelectedPermission)
  resetSelectedPermission({ patchState }: StateContext<PermissionStateModel>) {
    patchState({ ...INITIAL_STATE });
  }
}
