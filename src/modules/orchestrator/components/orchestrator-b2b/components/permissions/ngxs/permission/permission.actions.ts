import { Permission } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/permissions/permissions.model';

export class FetchPermission {
  static readonly type = '[Permission] Fetch';

  constructor(public readonly id: string) {}
}

export class CreatePermission  {
  static readonly type = '[Permission] Create';

  constructor(public readonly permission: Permission) {}
}

export class UpdatePermission  {
  static readonly type = '[Permission] Update';

  constructor(public readonly id: string, public readonly permission: Permission) {}
}

export class DeletePermission  {
  static readonly type = '[Permission] Delete';

  constructor(public readonly permissionId: string) {}
}

export class FetchAPIs {
  static readonly type = '[APIs] Fetch';

  constructor(public readonly permissionId: string) {}
}

export class ResetSelectedPermission {
  static readonly type = '[Permission] Reset';
}
