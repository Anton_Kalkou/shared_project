import { Injectable } from '@angular/core';
import { Action, State, StateContext, Store } from '@ngxs/store';

import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { SetFilters } from 'src/modules/shared/components/filters/ngxs/filters.actions';
import { Permission } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/permissions/permissions.model';

import { PermissionsService } from '../../services/permissions.service';
import { PermissionState } from '../permission/permission.state';
import { FetchPermissions } from './permissions.actions';

export interface PermissionsStateModel {
  permissions: Permission[];
}

@State<PermissionsStateModel>({
  name: 'permissions',
  defaults: {
    permissions: []
  },
  children: [PermissionState]
})
@Injectable()
export class PermissionsState {
  constructor(
    private readonly store: Store,
    private readonly permissionsService: PermissionsService,
  ) {}

  @Action(FetchPermissions)
  fetchPermissions({ patchState }: StateContext<PermissionsStateModel>) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.permissionsService.loadPermissions().subscribe(
      permissions => {
        const filters = this.permissionsService.getPermissionsFilters(permissions);

        this.store.dispatch([
          new SetFilters(filters),
          new SetSpinnerVisibility(false),
        ]);

        patchState({ permissions });
      },
      () => this.store.dispatch(new SetSpinnerVisibility(false))
    );
  }
}
