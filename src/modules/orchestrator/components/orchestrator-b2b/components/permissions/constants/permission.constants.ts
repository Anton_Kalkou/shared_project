export const INITIAL_STATE = {
  isPermissionCreated: false,
  selectedPermission: undefined,
  relatedFeatures: [],
  apis: []
};
