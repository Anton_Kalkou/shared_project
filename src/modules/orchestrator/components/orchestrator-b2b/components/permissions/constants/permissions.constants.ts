import { Route } from '@angular/router';

import { AuthenticationGuard } from 'src/modules/shared/guards/auth.guard';

import { PermissionAddEditComponent } from '../components/permission-add-edit/permission-add-edit.component';
import { PermissionsComponent } from '../components/permissions/permissions.component';

export const PERMISSIONS_ROUTES: Route[] = [
  { path: 'orchestrator-b2b/permissions', component: PermissionsComponent, canActivate: [AuthenticationGuard] },
  { path: 'orchestrator-b2b/permissions/add', component: PermissionAddEditComponent, canActivate: [AuthenticationGuard] },
  { path: 'orchestrator-b2b/permissions/:id/edit', component: PermissionAddEditComponent, canActivate: [AuthenticationGuard] },
];
