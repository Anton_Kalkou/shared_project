import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { ActionType, Icon, TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum';
import { Permission } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/permissions/permissions.model';
import { FiltersService } from 'src/modules/shared/components/filters/services/filters.service';

import { InfoType } from '../../../../enum/orchestrator-b2b.enum';
import { FetchPermissions } from '../../ngxs/permissions/permissions.actions';
import { ColumnDataKey, Translation as PermissionsTranslation } from '../../enum/permissions.enum';
import { PermissionsService } from '../../services/permissions.service';
import { BaseDataList } from '../../../../shared/classes/base/base-data-list.class';

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.scss']
})
export class PermissionsComponent extends BaseDataList<Permission, FetchPermissions> {
  @Select(state => state.orchestratorB2B.permissions.permissions) dataList$: Observable<Permission[]>;

  constructor(
    public readonly permissionsService: PermissionsService,
    protected readonly store: Store,
    protected readonly filtersService: FiltersService,
  ) {
    super(
      new FetchPermissions(),
      ORCHESTRATOR.orchestratorB2B.navButtonStateTitles.permissions,
      store,
      true,
      filtersService,
    );
  }

  protected initValues(): void {
    this.applyCommonFilters = false;
    this.filtersEndpoint = ORCHESTRATOR.orchestratorB2B.filters.permissionsFilters;
    this.displayedColumns = [
      {
        title: ColumnDataKey.Name,
        translationKey: PermissionsTranslation.PermissionName,
        valueType: ValueType.Text,
        textType: TextType.Simple,
      },
      {
        title: ColumnDataKey.RelatedFeatures,
        translationKey: PermissionsTranslation.RelatedFeatures,
        valueType: ValueType.Text,
        textType: TextType.Array,
        arrayItemTitle: ColumnDataKey.Name,
      },
      {
        title: ColumnDataKey.Description,
        translationKey: PermissionsTranslation.Description,
        valueType: ValueType.Text,
        textType: TextType.Simple,
      },
      {
        title: ColumnDataKey.Action,
        valueType: ValueType.Action,
        actionType: ActionType.CustomIcon,
        actionDataKey: ColumnDataKey.Id,
        path: InfoType.Edit,
        icon: Icon.Pencil,
        isSortableDisabled: true,
      },
    ];
  }
}
