import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';

import { Observable } from 'rxjs';
import { TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum';
import { Column } from 'src/modules/shared/models/data-table.model';
import { Feature } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/features/features.model';
import { Permission } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/permissions/permissions.model';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { TranslationService } from 'src/modules/shared/services/translation.service';

import { ColumnDataKey, Translation as PermissionsTranslation } from '../../enum/permissions.enum';
import { CreatePermission, DeletePermission, FetchPermission, UpdatePermission } from '../../ngxs/permission/permission.actions';
import { BaseInfoPage } from '../../../../shared/classes/base/base-info-page.class';
import { PermissionState } from '../../ngxs/permission/permission.state';
import { PermissionTitleType, PermissionType } from '../../../../enum/orchestrator-b2b.enum';

@Component({
  selector: 'app-permission-info',
  templateUrl: './permission-info.component.html',
  styleUrls: ['./permission-info.component.scss']
})
export class PermissionInfoComponent extends BaseInfoPage<Permission> {
  @Select(state => state.orchestratorB2B.permissions.permission.selectedPermission) selectedItem$: Observable<Permission>;
  @Select(state => state.orchestratorB2B.permissions.permission.relatedFeatures) relatedFeatures$: Observable<any[]>;
  @Select(state => state.orchestratorB2B.permissions.permission.isPermissionCreated) isCreated$: Observable<boolean>;

  @Input() infoType: string;

  public formGroup: FormGroup;
  public isEdit: boolean;
  public features: Feature[];
  public columns: Column[];
  public permissionTypeOptions = [
    { value: PermissionType.READ_WRITE, title: PermissionTitleType.READ_WRITE },
    { value: PermissionType.YES_NO, title: PermissionTitleType.YES_NO }
  ];

  protected selectedIdSelector = PermissionState.getItemId;
  protected selectedNameSelector = PermissionState.getItemName;

  constructor(
    private readonly route: ActivatedRoute,
    store: Store,
    formBuilder: FormBuilder,
    notificationService: NotificationService,
    translationService: TranslationService
  ) {
    super(store, formBuilder, notificationService, translationService);

    this.initializeSubscriptions();
  }

  public onCreate(): void {
    if (this.formGroup.invalid) {
      return;
    }

    const permissionForSaving = this.formGroup.getRawValue();

    this.store.dispatch(new CreatePermission(permissionForSaving));
  }

  public onDelete(): void {
    this.store.dispatch(new DeletePermission(this.currentId));
  }

  protected onEdit(): void {
    this.setInitialFormValues(this.formGroup);
    this.store.dispatch(new UpdatePermission(this.currentId, this.initialFormValues));
  }

  protected initEditData(): void {
    this.currentId = this.route.snapshot.paramMap.get('id');

    this.initializeValues();
    this.initSubscriptions();
    this.dispatchActions(this.currentId);
  }

  private initializeValues(): void {
    this.columns = [
      {
        title: ColumnDataKey.Name, 
        translationKey: PermissionsTranslation.Name,
        valueType: ValueType.Text,
        textType: TextType.Simple,
      },
    ];
  }

  protected initFormGroup(selectedPermission?: Permission): void {
    this.formGroup = this.formBuilder.group({
      name: [selectedPermission ? selectedPermission.name : '', Validators.required],
      code: [{ value: selectedPermission ? selectedPermission.code : '', disabled: true }],
      description: selectedPermission ? selectedPermission.description : '',
      permissionType: [
        {
          value: selectedPermission ? selectedPermission.permissionType : PermissionType.READ_WRITE,
          disabled: this.isEdit,
        }, 
        {
          validators: [Validators.required],
        },
      ]
    });
  }

  private initializeSubscriptions(): void {
    this.subscribeTo = this.isCreated$.subscribe(isCreated => {
      if (isCreated) {
        this.formGroup.get('permissionType').disable({ emitEvent: false });
      }
    });
  }

  private dispatchActions(id: string): void {
    this.store.dispatch(new FetchPermission(id));
  }
}
