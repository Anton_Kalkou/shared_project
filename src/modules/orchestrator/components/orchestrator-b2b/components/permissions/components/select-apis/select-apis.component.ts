import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum';
import { Column } from 'src/modules/shared/models/data-table.model';
import { Option } from 'src/modules/shared/models/dropdown.model';
import { Version } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';
import { Api } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/permissions/permissions.model';
import { FiltersService } from 'src/modules/shared/services/filters.service';

import { ColumnDataKey, Translation as PermissionsTranslation } from '../../enum/apis.enum';

@Component({
  selector: 'select-add-apis',
  templateUrl: './select-apis.component.html',
  styleUrls: ['./select-apis.component.scss']
})
export class SelectApisComponent extends Unsubscribe implements OnInit {
  @Select(state => state.orchestratorB2B.version.selectedVersion) selectedVersion$: Observable<Version>;
  @Select(state => state.orchestratorB2B.version.tableData) apis$: Observable<Api[]>;

  private sourceApis: Api[];

  public columns: Column[];
  public filteredApis: Api[];
  public selectedItems: Api[];
  public apisServicesOptions: Option[];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialog: MatDialogRef<SelectApisComponent>,
    private filtersService: FiltersService
  ) {
    super();
  }

  ngOnInit(): void {
    this.initValues();
    this.initSubscriptions();
  }

  public onSearch(key: string): void {
    this.filteredApis = this.filtersService.filterDataOnSearch(this.sourceApis, key, ColumnDataKey.Service);
  }

  public onSave(): void {
    this.dialog.close(this.selectedItems);
  }

  private getApisServicesOptions(apis: Api[]): Option[] {
    const apisServices = apis.map(api => api.service);
    const apisUniqueServices = [...new Set(apisServices)];

    return apisUniqueServices.map(apisService => ({ value: apisService, label: apisService }));
  }

  private initValues(): void {
    this.columns = [
      { title: ColumnDataKey.Id, valueType: ValueType.Select, isSortableDisabled: true },
      { title: ColumnDataKey.Service, translationKey: PermissionsTranslation.Service, valueType: ValueType.Text, textType: TextType.Simple },
      { title: ColumnDataKey.Method, translationKey: PermissionsTranslation.Method, valueType: ValueType.Text, textType: TextType.Simple },
      { title: ColumnDataKey.Api, translationKey: PermissionsTranslation.Api, valueType: ValueType.Text, textType: TextType.Simple }
    ];
    this.apisServicesOptions = this.getApisServicesOptions(this.data.apis);
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.apis$.subscribe(permissions => {
      this.sourceApis = permissions;
      this.filteredApis = permissions;
    });
  }
}
