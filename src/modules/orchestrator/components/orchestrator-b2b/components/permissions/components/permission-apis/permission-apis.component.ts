import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSelectChange } from '@angular/material/select';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { ActionType, TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum';
import { Column } from 'src/modules/shared/models/data-table.model';
import { Api, Permission } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/permissions/permissions.model';
import { Version } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { TranslationService } from 'src/modules/shared/services/translation.service';

import { ColumnDataKey, Translation as APIsTranslation } from '../../enum/apis.enum';
import { CreateVersionDialogComponent } from '../../../../shared/components/create-version-dialog/create-version-dialog.component';
import { BaseVersionPage } from '../../../../shared/classes/base/base-version-page.class';
import { PermissionState } from '../../ngxs/permission/permission.state';
import { DialogSize, DataLabel } from '../../../../enum/orchestrator-b2b.enum';
import { SelectApisComponent } from '../select-apis/select-apis.component';
import { versionExistsValidator } from '../../../../shared/validators/versionExists.validator';
import {
  ChangeSelectedVersion,
  ChangeVersionState,
  CreateVersion,
  FetchVersionTableData,
  PublishVersion,
  UpdateVersion,
} from '../../../../shared/ngxs/version/version.actions';

@Component({
  selector: 'app-permission-apis',
  templateUrl: './permission-apis.component.html',
  styleUrls: ['./permission-apis.component.scss']
})
export class PermissionAPIsComponent extends BaseVersionPage<Permission, Api> {
  @Select(state => state.orchestratorB2B.version.selectedItem) selectedItem$: Observable<Permission>;
  @Select(state => state.orchestratorB2B.version.selectedVersion) selectedVersion$: Observable<Version>;
  @Select(state => state.orchestratorB2B.version.isVersionPuplished) isVersionPuplished$: Observable<boolean>;
  @Select(state => state.orchestratorB2B.version.tableData) tableData$: Observable<Api[]>;

  constructor(
    protected readonly route: ActivatedRoute,
    protected readonly store: Store,
    protected readonly formBuilder: FormBuilder,
    protected readonly notificationService: NotificationService,
    protected readonly translationService: TranslationService,
    private readonly dialog: MatDialog
  ) {
    super(
      route,
      store,
      formBuilder,
      notificationService,
      translationService,
      PermissionState
    );
  }

  public onVersionChange({ value }: MatSelectChange): void {
    this.store.dispatch(new ChangeSelectedVersion(value));
  }

  public onPublish(): void {
    this.store.dispatch(new PublishVersion());
  }

  public onVersion(isDuplicate?: boolean): void {
    this.dialog.open(CreateVersionDialogComponent, this.getVersionDialogOptions(isDuplicate))
      .afterClosed()
      .subscribe(version => {
        return version && this.store.dispatch(new CreateVersion(
          { ...version, isDisabled: false },
          isDuplicate
        ));
      });
  }

  public onVersionState(): void {
    this.store.dispatch(new ChangeVersionState(!this.formGroup.get(DataLabel.IsDisabled).value));
  }

  public onSelectTableData(): void {
    this.subscribeTo = this.dialog
      .open(SelectApisComponent, {
        width: DialogSize.AddTableData,
        data: {
          apis: this.initialFormValues[DataLabel.Apis]
        }
      })
      .afterClosed()
      .subscribe((selectedApis: Api[]) => {
        selectedApis && this.store.dispatch(new UpdateVersion({ ...this.formGroup.value, apis: selectedApis }, true));
      });
  }

  public onDeleteTableData(api: Api): void {
    const selectedApis = this.initialFormValues[DataLabel.Apis] as Api[];
    const apisAfterDeleting = selectedApis.filter(({ id }) => id !== api.id);

    this.store.dispatch(new UpdateVersion({ ...this.formGroup.value, apis: apisAfterDeleting }, true));
  }

  protected initFormGroup(selectedVersion?: Version): void {
    const { isDisabled = true, name = '', description = '', apis = [] } = selectedVersion || {};

    this.formGroup = this.formBuilder.group({
      isDisabled,
      name: [
        { value: name, disabled: isDisabled },
        {
          validators: [Validators.required],
          asyncValidators: [versionExistsValidator(this.selectedItem$, this.selectedVersion$)],
        }
      ],
      description: { value: description, disabled: isDisabled }
    });
    
    this.setSelectedDataFormControl(DataLabel.Apis, apis as Api[]);
  }

  protected getColumns(): Column[] {
    return [
      { title: ColumnDataKey.Service, translationKey: APIsTranslation.Service, valueType: ValueType.Text, textType: TextType.Simple },
      { title: ColumnDataKey.Method, translationKey: APIsTranslation.Method, valueType: ValueType.Text, textType: TextType.HttpMethod },
      { title: ColumnDataKey.Api, translationKey: APIsTranslation.Api, valueType: ValueType.Text, textType: TextType.Simple },
      { title: ColumnDataKey.Profile, translationKey: APIsTranslation.Profile, valueType: ValueType.Dropdown },
      {
        title: ColumnDataKey.Action,
        valueType: ValueType.Action,
        actionType: ActionType.SquareButton,
        isSortableDisabled: true,
        isDelete: true,
        popupTitle: APIsTranslation.DeleteApiTitle,
        popupCaption: APIsTranslation.DeleteApiCaption,
        popupAcceptTitle: APIsTranslation.DeleteApiAcceptTitle
      }
    ];
  }

  protected dispatchActions(): void {
    this.store.dispatch(new FetchVersionTableData());
  }
}
