export enum Translation {
  Name = 'shared.name',
  Description = 'shared.description',
  Permissions = 'menu.permissions',
  CreateInfoContent = 'orchestrator-b2b.default-roles.create-info-content',
}

export enum ColumnDataKey {
  Id = 'id',
  Name = 'name',
  Description = 'description',
  Permissions = 'permissions',
  Action = 'action',
  RadioButtons = 'radioButtons'
}
