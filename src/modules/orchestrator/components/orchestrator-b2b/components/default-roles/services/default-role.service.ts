import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { DefaultRole } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/default-roles/default-roles.model';
import { CreatedData } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';
import { ApiService } from 'src/modules/shared/services/api.service';

@Injectable({ providedIn: 'root' })
export class DefaultRoleService {

  constructor(private apiService: ApiService) {}

  public loadDefaultRole(id: string): Observable<DefaultRole> {
    return this.apiService.get(`${ORCHESTRATOR.orchestratorB2B.endpoints.defaultRoles}/${id}`);
  }

  public createDefaultRole(role: DefaultRole): Observable<CreatedData> {
    return this.apiService.post(ORCHESTRATOR.orchestratorB2B.endpoints.defaultRoles, role);
  }

  public updateDefaultRole(id: string, role: DefaultRole): Observable<void> {
    return this.apiService.put(`${ORCHESTRATOR.orchestratorB2B.endpoints.defaultRoles}/${id}`, role);
  }
}
