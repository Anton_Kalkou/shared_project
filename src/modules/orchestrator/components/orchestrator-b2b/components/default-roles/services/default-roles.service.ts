import { Injectable } from '@angular/core';
import { Observable, } from 'rxjs';

import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { FilterGroup } from 'src/modules/shared/models/filter.model';
import { DefaultRole } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/default-roles/default-roles.model';
import { ApiService } from 'src/modules/shared/services/api.service';
import { FiltersService } from 'src/modules/shared/services/filters.service';

@Injectable({ providedIn: 'root' })
export class DefaultRolesService {

  constructor(private apiService: ApiService, private filtersService: FiltersService) {}

  public loadDefaultRoles(): Observable<DefaultRole[]> {
    return this.apiService.get(ORCHESTRATOR.orchestratorB2B.endpoints.defaultRoles);
  }

  public filterDefaultRoles(dataSource: DefaultRole[], filterType: string, filterValue: string | FilterGroup[], searchColumnKey: string): DefaultRole[] {
    return this.filtersService.filterDataOnSearch(dataSource, filterValue as string, searchColumnKey);
  }
}
