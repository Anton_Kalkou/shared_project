export const PERMISSIONS_TABLES = [
  {
    title: 'Property Setup',
    data: [
      {
        name: 'General Information',
        radioButtons: [
          { label: 'Edit', checked: true },
          { label: 'View', checked: false },
          { label: 'None', checked: false }
        ]
      },
      {
        name: 'Property Description',
        radioButtons: [
          { label: 'Edit', checked: true },
          { label: 'View', checked: false },
          { label: 'None', checked: false }
        ]
      },
      {
        name: 'Openning Hours',
        radioButtons: [
          { label: 'Edit', checked: true },
          { label: 'View', checked: false },
          { label: 'None', checked: false }
        ]
      },
      {
        name: 'Edit Floor Plan',
        radioButtons: [
          { label: 'Yes', checked: true },
          { label: 'No', checked: false }
        ]
      }
    ]
  },
  {
    title: 'Food an Bverage',
    data: [
      {
        name: 'General Information',
        radioButtons: [
          { label: 'Edit', checked: true },
          { label: 'View', checked: false },
          { label: 'None', checked: false }
        ]
      },
      {
        name: 'Property Description',
        radioButtons: [
          { label: 'Edit', checked: true },
          { label: 'View', checked: false },
          { label: 'None', checked: false }
        ]
      },
      {
        name: 'Openning Hours',
        radioButtons: [
          { label: 'Edit', checked: true },
          { label: 'View', checked: false },
          { label: 'None', checked: false }
        ]
      },
      {
        name: 'Edit Floor Plan',
        radioButtons: [
          { label: 'Yes', checked: true },
          { label: 'No', checked: false }
        ]
      }
    ]
  },
  {
    title: 'HR',
    data: [
      {
        name: 'General Information',
        radioButtons: [
          { label: 'Edit', checked: true },
          { label: 'View', checked: false },
          { label: 'None', checked: false }
        ]
      },
      {
        name: 'Property Description',
        radioButtons: [
          { label: 'Edit', checked: true },
          { label: 'View', checked: false },
          { label: 'None', checked: false }
        ]
      },
      {
        name: 'Openning Hours',
        radioButtons: [
          { label: 'Edit', checked: true },
          { label: 'View', checked: false },
          { label: 'None', checked: false }
        ]
      },
      {
        name: 'Edit Floor Plan',
        radioButtons: [
          { label: 'Yes', checked: true },
          { label: 'No', checked: false }
        ]
      }
    ]
  },
  {
    title: 'Reports',
    data: [
      {
        name: 'Transactions',
        radioButtons: [
          { label: 'Yes', checked: true },
          { label: 'No', checked: false }
        ]
      },
      {
        name: 'Batches',
        radioButtons: [
          { label: 'Yes', checked: true },
          { label: 'No', checked: false }
        ]
      },
      {
        name: 'Salaries and Tips',
        radioButtons: [
          { label: 'Yes', checked: true },
          { label: 'No', checked: false }
        ]
      },
      {
        name: 'Sales',
        radioButtons: [
          { label: 'Yes', checked: true },
          { label: 'No', checked: false }
        ]
      }
    ]
  }
];
