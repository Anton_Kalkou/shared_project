import { Injectable } from '@angular/core';
import { Action, Selector, State, StateContext, Store } from '@ngxs/store';
import {flatMap} from 'rxjs/operators';
import { combineLatest } from 'rxjs';
import { cloneDeep } from 'lodash';

import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { SetNavigationButtonStatesTitle } from 'src/modules/layout/ngxs/layout.actions';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { TranslationService } from 'src/modules/shared/services/translation.service';
import { DefaultRole } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/default-roles/default-roles.model';

import { INITIAL_STATE } from '../../constants/default-role.constants';
import { DefaultRoleService } from '../../services/default-role.service';
import { Translation as DefaultRolesTranslation } from '../../enum/default-roles.enum';
import { getValuesWithoutCode } from '../../../../utils/orchestrator-b2b.utils';
import { SetVersionState } from '../../../../shared/ngxs/version/version.actions';
import { DataLabel } from '../../../../enum/orchestrator-b2b.enum';
import {
  FetchDefaultRole,
  CreateDefaultRole,
  UpdateDefaultRole,
  ResetDefaultRoleState
} from './default-role.actions';
import { VersionService } from '../../../../shared/services/version.service';

export interface DefaultRoleStateModel {
  selectedRole: DefaultRole;
  isRoleCreated: boolean;
}

@State<DefaultRoleStateModel>({
  name: 'defaultRole',
  defaults: INITIAL_STATE
})
@Injectable()
export class DefaultRoleState {

  constructor(
    private readonly store: Store,
    private readonly defaultRoleService: DefaultRoleService,
    private readonly notificationService: NotificationService,
    private readonly translationService: TranslationService,
    private readonly versionService: VersionService,
  ) {}

  @Selector()
  static getItemId({ selectedRole }: DefaultRoleStateModel): string {
    return selectedRole.id;
  }

  @Selector()
  static getItemName({ selectedRole }: DefaultRoleStateModel): string {
    return selectedRole && selectedRole.name;
  }

  @Action(FetchDefaultRole)
  fetchDefaultRole({ patchState }: StateContext<DefaultRoleStateModel>, { id }: FetchDefaultRole) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.defaultRoleService.loadDefaultRole(id).subscribe(
      selectedRole => {
        const { name, versions } = selectedRole;
        const selectedVersion = this.versionService.getSelectedVersion(versions);

        this.store.dispatch([
          new SetVersionState(
            `${ORCHESTRATOR.orchestratorB2B.endpoints.defaultRoles}/${id}`,
            selectedRole,
            selectedVersion,
          ),
          new SetSpinnerVisibility(false),
          new SetNavigationButtonStatesTitle(true, `Edit ${name}`)
        ]);

        patchState({ selectedRole });
      },
      () => this.store.dispatch(new SetSpinnerVisibility(false))
    );
  }

  @Action(CreateDefaultRole)
  createDefaultRole({ patchState }: StateContext<DefaultRoleStateModel>, { role }: CreateDefaultRole) {
    const defaultRoleForSaving = getValuesWithoutCode(role);
    const createdDefaultRole$ = this.defaultRoleService.createDefaultRole(defaultRoleForSaving);
    const translatedMessage$ = this.translationService.translate(DefaultRolesTranslation.CreateInfoContent);

    this.store.dispatch(new SetSpinnerVisibility(true));

    return createdDefaultRole$
      .pipe(flatMap(({ id }) => {
        const getDefaultRoleById$ = this.defaultRoleService.loadDefaultRole(id);

        return combineLatest([getDefaultRoleById$, translatedMessage$]);
      }))
      .subscribe(([{ id, code }, createInfoContent]) => {
        const selectedDefaultRole = { id, code, ...role, versions: [] };

        this.store.dispatch([
          new SetVersionState(
            `${ORCHESTRATOR.orchestratorB2B.endpoints.defaultRoles}/${selectedDefaultRole.id}`,
            selectedDefaultRole,
            undefined,
          ),
          new SetSpinnerVisibility(false)
        ]);

        this.notificationService.showSuccessMessage(null, createInfoContent);

        patchState({ isRoleCreated: true, selectedRole: selectedDefaultRole });
      });
  }

  @Action(UpdateDefaultRole)
  updateDefaultRole({ getState, patchState }: StateContext<DefaultRoleStateModel>, { id, role }: UpdateDefaultRole) {
    const defaultRoleForSaving = getValuesWithoutCode(role);

    return this.defaultRoleService.updateDefaultRole(id, defaultRoleForSaving).subscribe(
      () => {
        this.store.dispatch(new SetNavigationButtonStatesTitle(true, `Edit ${role.name}`));

        const { selectedRole } = getState();

        patchState({ selectedRole: { ...cloneDeep(selectedRole), id, ...role } });
      }
    );
  }

  @Action(ResetDefaultRoleState)
  resetDefaultRoleState({ patchState }: StateContext<DefaultRoleStateModel>) {
    patchState({ ...INITIAL_STATE });
  }
}
