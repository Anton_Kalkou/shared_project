import { Injectable } from '@angular/core';
import { Action, State, StateContext, Store } from '@ngxs/store';

import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { DefaultRole } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/default-roles/default-roles.model';

import { DefaultRolesService } from '../../services/default-roles.service';
import { DefaultRoleState } from '../default-role/default-role.state';
import { FetchDefaultRoles } from './default-roles.actions';

export interface DefaultRolesStateModel {
  defaultRoles: DefaultRole[];
}

@State<DefaultRolesStateModel>({
  name: 'defaultRoles',
  defaults: {
    defaultRoles: []
  },
  children: [DefaultRoleState]
})
@Injectable()
export class DefaultRolesState {

  constructor(private store: Store, private defaultRolesService: DefaultRolesService) {}

  @Action(FetchDefaultRoles)
  fetchDefaultRoles({ patchState }: StateContext<DefaultRolesStateModel>) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.defaultRolesService.loadDefaultRoles().subscribe(
      defaultRoles => {
        this.store.dispatch(new SetSpinnerVisibility(false));
        patchState({ defaultRoles });
      },
      () => this.store.dispatch(new SetSpinnerVisibility(false))
    );
  }
}
