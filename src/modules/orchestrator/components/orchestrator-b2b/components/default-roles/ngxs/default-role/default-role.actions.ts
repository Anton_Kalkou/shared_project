import { DefaultRole } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/default-roles/default-roles.model';

export class FetchDefaultRole {
  static readonly type = '[Default Role] Fetch';

  constructor(public id: string) {}
}

export class CreateDefaultRole {
  static readonly type = '[Default Role] Create';

  constructor(public role: DefaultRole) {}
}

export class UpdateDefaultRole {
  static readonly type = '[Default Role] Update';

  constructor(public id: string, public role: DefaultRole) {}
}

export class ResetDefaultRoleState {
  static readonly type = '[Default Role State] Reset';
}
