import { Route } from '@angular/router';

import { AuthenticationGuard } from 'src/modules/shared/guards/auth.guard';

import { DefaultRoleAddEditComponent } from '../components/default-role-add-edit/default-role-add-edit.component';
import { DefaultRolesComponent } from '../components/default-roles/default-roles.component';

export const DEFAULT_ROLES_ROUTES: Route[] = [
  { path: 'orchestrator-b2b/default-roles', component: DefaultRolesComponent, canActivate: [AuthenticationGuard] },
  { path: 'orchestrator-b2b/default-roles/add', component: DefaultRoleAddEditComponent, canActivate: [AuthenticationGuard] },
  { path: 'orchestrator-b2b/default-roles/:id/edit', component: DefaultRoleAddEditComponent, canActivate: [AuthenticationGuard] }
];
