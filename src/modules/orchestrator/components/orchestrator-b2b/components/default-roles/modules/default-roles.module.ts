import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { AddButtonModule } from 'src/modules/shared/components/add-button/add-button.module';
import { ConfirmationModule } from 'src/modules/shared/components/confirmation/confirmation.module';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { SectionHeaderModule } from 'src/modules/orchestrator/shared/components/section-header/section-header.module';
import { IconButtonModule } from 'src/modules/shared/components/icon-button/icon-button.module';
import { AutoSaveNotificationTemplateModule } from 'src/modules/shared/components/auto-save-notification-template/auto-save-notification-template.module';

import { DataListModule } from '../../../shared/components/data-list/data-list.module';
import { DefaultRolesComponent } from '../components/default-roles/default-roles.component';
import { DefaultRoleAddEditComponent } from '../components/default-role-add-edit/default-role-add-edit.component';
import { DefaultRoleInfoComponent } from '../components/default-role-info/default-role-info.component';
import { DefaultRolePermissionsComponent } from '../components/default-role-permissions/default-role-permissions.component';
import { DefaultRoleChangelogsComponent } from '../components/default-role-changelogs/default-role-changelogs.component';

@NgModule({
  imports: [
    SharedModule,
    RouterModule,
    MaterialModule,
    DataListModule,
    AddButtonModule,
    ConfirmationModule,
    SectionHeaderModule,
    IconButtonModule,
    AutoSaveNotificationTemplateModule
  ],
  declarations: [
    DefaultRolesComponent,
    DefaultRoleAddEditComponent,
    DefaultRoleInfoComponent,
    DefaultRolePermissionsComponent,
    DefaultRoleChangelogsComponent
  ],
  exports: [DefaultRolesComponent]
})
export class DefaultRolesModule {}
