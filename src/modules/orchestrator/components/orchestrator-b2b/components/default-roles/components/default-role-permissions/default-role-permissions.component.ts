import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { MatSelectChange } from '@angular/material/select';
import { Observable } from 'rxjs';

import { Column } from 'src/modules/shared/models/data-table.model';
import { Permission } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/permissions/permissions.model';
import { DefaultRole } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/default-roles/default-roles.model';
import { Version } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { TranslationService } from 'src/modules/shared/services/translation.service';

import { CreateVersionDialogComponent } from '../../../../shared/components/create-version-dialog/create-version-dialog.component';
import { BaseVersionPage } from '../../../../shared/classes/base/base-version-page.class';
import { DefaultRoleState } from '../../ngxs/default-role/default-role.state';
import { DataLabel  } from '../../../../enum/orchestrator-b2b.enum';
import { PermissionsService } from '../../../permissions/services/permissions.service';
import { versionExistsValidator } from '../../../../shared/validators/versionExists.validator';
import {
  ChangeSelectedVersion,
  ChangeVersionState,
  CreateVersion,
} from '../../../../shared/ngxs/version/version.actions';

@Component({
  selector: 'app-default-role-permissions',
  templateUrl: './default-role-permissions.component.html',
  styleUrls: ['./default-role-permissions.component.scss']
})
export class DefaultRolePermissionsComponent extends BaseVersionPage<DefaultRole, Permission> {
  @Select(state => state.orchestratorB2B.version.selectedItem) selectedItem$: Observable<DefaultRole>;
  @Select(state => state.orchestratorB2B.version.selectedVersion) selectedVersion$: Observable<Version>;
  @Select(state => state.orchestratorB2B.version.isVersionPuplished) isVersionPuplished$: Observable<boolean>;
  @Select(state => state.orchestratorB2B.version.tableData) tableData$: Observable<Permission[]>;

  constructor(
    public readonly permissionsService: PermissionsService,
    protected readonly formBuilder: FormBuilder,
    protected readonly route: ActivatedRoute,
    protected readonly store: Store,
    protected readonly notificationService: NotificationService,
    protected readonly translationService: TranslationService,
    private readonly dialog: MatDialog
  ) {
    super(
      route,
      store,
      formBuilder,
      notificationService,
      translationService,
      DefaultRoleState,
    );
  }

  public onVersionChange({ value }: MatSelectChange): void {
    this.store.dispatch(new ChangeSelectedVersion(value));
  }

  public onVersion(isDuplicate?: boolean): void {
    this.subscribeTo = this.dialog.open(CreateVersionDialogComponent, this.getVersionDialogOptions(isDuplicate))
      .afterClosed()
      .subscribe(version => {
        return version && this.store.dispatch(new CreateVersion(
          { ...version, isDisabled: false },
          isDuplicate
        ));
      });
  }

  public onVersionState(): void {
    this.store.dispatch(new ChangeVersionState(!this.formGroup.get(DataLabel.IsDisabled).value));
  }


  protected initFormGroup(selectedVersion?: Version): void {
    const { isDisabled = true, name = '', description = '', permissions = [] } = selectedVersion || {};

    this.formGroup = this.formBuilder.group({
      isDisabled,
      name: [
        { value: name, disabled: isDisabled },
        {
          validators: [Validators.required],
          asyncValidators: [versionExistsValidator(this.selectedItem$, this.selectedVersion$)],
        }
      ],
      description: { value: description, disabled: isDisabled }
      
    });
  }

  protected getColumns(): Column[] {
    return [];
  }

  protected dispatchActions(): void {}
}
