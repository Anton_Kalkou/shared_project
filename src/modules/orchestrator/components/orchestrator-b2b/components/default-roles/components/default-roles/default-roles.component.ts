import { Component } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { ActionType, Icon, TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum';
import { DefaultRole } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/default-roles/default-roles.model';

import { InfoType } from '../../../../enum/orchestrator-b2b.enum';
import { BaseDataList } from '../../../../shared/classes/base/base-data-list.class';
import { Translation as DefaultRolesTranslation, ColumnDataKey } from '../../enum/default-roles.enum';
import { FetchDefaultRoles } from '../../ngxs/default-roles/default-roles.actions';
import { DefaultRolesService } from '../../services/default-roles.service';

@Component({
  selector: 'app-default-roles',
  templateUrl: './default-roles.component.html',
  styleUrls: ['./default-roles.component.scss']
})
export class DefaultRolesComponent extends BaseDataList<DefaultRole, FetchDefaultRoles> {

  @Select(state => state.orchestratorB2B.defaultRoles.defaultRoles) dataList$: Observable<DefaultRole[]>;

  constructor(public defaultRolesService: DefaultRolesService, protected store: Store) {
    super(
      new FetchDefaultRoles(),
      ORCHESTRATOR.orchestratorB2B.navButtonStateTitles.defaultRoles,
      store,
      false
    )
  }

  protected initValues(): void {
    this.displayedColumns = [
      { 
        title: ColumnDataKey.Name,
        translationKey: DefaultRolesTranslation.Name,
        valueType: ValueType.Text,
        textType: TextType.Simple
      },
      {
        title: ColumnDataKey.Description,
        translationKey: DefaultRolesTranslation.Description,
        valueType: ValueType.Text,
        textType: TextType.Simple
      },
      {
        title: ColumnDataKey.Permissions,
        translationKey: DefaultRolesTranslation.Permissions,
        valueType: ValueType.Text,
        textType: TextType.Array,
        arrayItemTitle: ColumnDataKey.Name
      },
      {
        title: ColumnDataKey.Action,
        valueType: ValueType.Action,
        actionType: ActionType.CustomIcon,
        actionDataKey: ColumnDataKey.Id,
        path: InfoType.Edit,
        icon: Icon.Pencil,
        isSortableDisabled: true
      }
    ];
  }
}
