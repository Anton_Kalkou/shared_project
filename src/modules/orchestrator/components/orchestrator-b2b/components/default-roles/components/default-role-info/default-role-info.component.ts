import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { getEntityRouteId, isFormValid } from 'src/modules/shared/helpers';
import { DefaultRole } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/default-roles/default-roles.model';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { TranslationService } from 'src/modules/shared/services/translation.service';

import { BaseInfoPage } from '../../../../shared/classes/base/base-info-page.class';
import { CreateDefaultRole, FetchDefaultRole, UpdateDefaultRole } from '../../ngxs/default-role/default-role.actions';
import { DefaultRoleState } from '../../ngxs/default-role/default-role.state';

@Component({
  selector: 'app-default-role-info',
  templateUrl: './default-role-info.component.html',
  styleUrls: ['./default-role-info.component.scss']
})
export class DefaultRoleInfoComponent extends BaseInfoPage<DefaultRole> {

  @Select(state => state.orchestratorB2B.defaultRoles.defaultRole.isRoleCreated) isCreated$: Observable<boolean>;
  @Select(state => state.orchestratorB2B.defaultRoles.defaultRole.selectedRole) selectedItem$: Observable<DefaultRole>;

  protected selectedIdSelector = DefaultRoleState.getItemId;
  protected selectedNameSelector = DefaultRoleState.getItemName;

  constructor(
    protected store: Store,
    protected formBuilder: FormBuilder,
    protected notificationService: NotificationService,
    protected translationService: TranslationService,
    private route: ActivatedRoute
  ) {
    super(store, formBuilder, notificationService, translationService);
  }

  public onCreate(): void {
    if (!isFormValid(this.formGroup)) {
      return;
    }

    this.store.dispatch(new CreateDefaultRole(this.formGroup.getRawValue()));
  }

  protected onEdit(): void {
    this.setInitialFormValues(this.formGroup);
    this.store.dispatch(new UpdateDefaultRole(this.currentId, this.initialFormValues));
  }

  public onDelete(): void {}

  protected initEditData(): void {
    this.currentId = getEntityRouteId(this.route);

    this.initSubscriptions();
    this.store.dispatch(new FetchDefaultRole(this.currentId));
  }

  protected initFormGroup(initialRole?: DefaultRole): void {
    const { name = '', code = '', description = '' } = initialRole || {};

    this.formGroup = this.formBuilder.group({
      name: [name, Validators.required],
      code: [{ value: code, disabled: true }],
      description
    });
  }
}
