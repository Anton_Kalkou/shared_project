import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { SetHeaderTitle, SetNavigationButtonStatesTitle } from 'src/modules/layout/ngxs/layout.actions';
import { isSubstring } from 'src/modules/shared/helpers';

import { Translation as OrchestratorB2bTranslation, InfoType } from '../../../../enum/orchestrator-b2b.enum';
import { ResetDefaultRoleState } from '../../ngxs/default-role/default-role.actions';

@Component({
  selector: 'app-default-role-add-edit',
  templateUrl: './default-role-add-edit.component.html',
  styleUrls: ['./default-role-add-edit.component.scss']
})
export class DefaultRoleAddEditComponent implements OnInit {

  @Select(state => state.orchestratorB2B.defaultRoles.defaultRole.isRoleCreated) isRoleCreated$: Observable<boolean>;

  public infoType: string;

  constructor(private router: Router, private store: Store) {}

  ngOnInit(): void {
    this.initValues();
    this.dispatchActions();
  }

  ngOnDestroy(): void {
    this.store.dispatch(new ResetDefaultRoleState());
  }

  private getInitialActions(): any {
    return [
      new SetHeaderTitle(''),
      this.infoType === InfoType.Add && new SetNavigationButtonStatesTitle(true, OrchestratorB2bTranslation.AddNavigationButtonStatesTitle)
    ]
  }

  private initValues(): void {
    this.infoType = isSubstring(this.router.url, InfoType.Add) ? InfoType.Add : InfoType.Edit;
  }

  private dispatchActions(): void {
    this.store.dispatch(this.getInitialActions());
  }
}
