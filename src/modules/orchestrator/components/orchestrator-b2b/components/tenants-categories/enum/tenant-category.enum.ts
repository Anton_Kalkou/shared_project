export enum Translation {
  CreateInfoContent = 'orchestrator-b2b.tenants-categories.create-info-content',
  DeleteInfoContent = 'orchestrator-b2b.tenants-categories.delete-info-content',
  Status = 'orchestrator-b2b.tenants-categories.status',
  DeploymentDate = 'orchestrator-b2b.tenants-categories.deployment-date',
  Proceed = 'orchestrator-b2b.tenants-categories.processed',
  Remaning = 'orchestrator-b2b.tenants-categories.remaning',
  Stop = 'orchestrator-b2b.tenants-categories.stop',
  ReSchedule = 'orchestrator-b2b.tenants-categories.re-schedule',
  ViewAll = 'orchestrator-b2b.tenants-categories.view-all',
  Name = 'shared.name',
  ScheduledDeploymentDate = 'orchestrator-b2b.tenants-categories.scheduled-deployment-date',
  AclResource = 'orchestrator-b2b.tenants-categories.acl-resource',
  Method = 'shared.method',
  Api = 'shared.api',
  ApiVersion = 'shared.api-version',
  PosApp = 'orchestrator-b2b.tenants-categories.pos-app',
  BackofficeFrontend = 'orchestrator-b2b.tenants-categories.backoffice-frontend',
  CatalogWhite = 'orchestrator-b2b.tenants-categories.catalog-white'
}

export enum ColumnDataKey {
  Name = 'name',
  ScheduledDeploymentDate = 'scheduledDeploymentDate',
  Status = 'status'
}

export namespace DeploymentProgress {
  export enum Tile {
    ViewAll = 'View All'
  }

  export enum Status {
    Processed = 'Processed',
    Pending = 'Pending'
  }
}
