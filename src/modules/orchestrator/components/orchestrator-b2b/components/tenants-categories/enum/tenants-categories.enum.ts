export enum Translation {
  Name = 'shared.name',
  Tags = 'orchestrator-b2b.tenants-categories.tags',
  Resources = 'orchestrator-b2b.tenants-categories.resources',
  ACLs = 'orchestrator-b2b.tenants-categories.acls',
  PermissionResource = 'orchestrator-b2b.tenants-categories.permission-resource',
  Method = 'shared.method',
  Api = 'shared.api',
  PosApp = 'orchestrator-b2b.tenants-categories.pos-app',
  BackofficeFrontend = 'orchestrator-b2b.tenants-categories.backoffice-frontend',
  CatalogWrite = 'orchestrator-b2b.tenants-categories.catalog-write',
  CompareTenantsCategories = 'orchestrator-b2b.tenants-categories.compare-tenants-categories'
}

export enum FilterTitle {
  Tag = 'Tag',
  Product = 'Product',
  Feature = 'Feature'
}

export enum ColumnDataKey {
  Id = 'id',
  Select = 'select',
  Name = 'name',
  Tags = 'tags',
  Resources = 'resources',
  ACLs = 'acls',
  Edit = 'edit',
  Action = 'action',
  Version = 'version'
}

export enum SearchFieldBehaviourClass {
  Focus = 'search-focus'
}

export enum SearchFieldDropdownBehaviourClass {
  Active = 'active'
}
