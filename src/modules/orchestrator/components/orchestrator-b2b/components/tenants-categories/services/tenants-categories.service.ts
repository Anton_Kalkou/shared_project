import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { ApiService } from 'src/modules/shared/services/api.service';
import { FilterGroup } from 'src/modules/shared/models/filter.model';
import { FilterType } from 'src/modules/support/enum/support.enum';
import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { Translation as FiltersTranslation } from 'src/modules/shared/components/filters/enum/filters.enum';
import { FiltersService } from 'src/modules/shared/services/filters.service';
import { TenantCategory, TenantCategoryResponse } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/tenants-categories/tenants-categories.model';

import { ColumnDataKey } from '../enum/tenants-categories.enum';


@Injectable({ providedIn: 'root' })
export class TenantsCategoriesService {

  constructor(private apiService: ApiService, private filtersService: FiltersService) {}

  public loadTenantsCategories(): Observable<TenantCategory[]> {
    return this.apiService.get(ORCHESTRATOR.orchestratorB2B.endpoints.tenantsCategories).pipe(
      switchMap((tenantsCategories: TenantCategoryResponse[]) => {
        const tenantCategories = tenantsCategories.map(tenantCategory => this.getTenantCategory(tenantCategory));

        return of(tenantCategories);
      })
    );
  }

  public filterTenantsCategories(dataSource: TenantCategory[], filterType: string, filterValue: string | FilterGroup[], searchColumnKey: string): TenantCategory[] {
    if (filterType === FilterType.Search) {
      return this.filtersService.filterDataOnSearch(dataSource, filterValue as string, searchColumnKey);
    } else {
      const filter = filterValue as FilterGroup[];

      if (filter.length) {
        return dataSource.filter(dataSourceItem => {
          if (filter.length === 1) {
            return filter.some(filterValueItem => {
              return this.filtersService.isFilterValueMatch(dataSourceItem[this.getColumnDataKey(filterValueItem.title)], filterValueItem.filters);
            });
          } else {
            const columnDataKeys = filter.map(filterValueItem => this.getColumnDataKey(filterValueItem.title));

            return filter.every((filterValueItem, filterValueItemIndex) => {
              return this.filtersService.isFilterValueMatch(dataSourceItem[columnDataKeys[filterValueItemIndex]], filterValueItem.filters);
            });
          }
        });
      } else {
        return dataSource;
      }
    }
  }

  public getTenantCategory(tenantCategoryResponse: TenantCategoryResponse): TenantCategory {
    return {
      id: tenantCategoryResponse.id,
      vmwareId: tenantCategoryResponse.vmwareId,
      name: tenantCategoryResponse.name,
      description: tenantCategoryResponse.description,
      tags: tenantCategoryResponse.tags,
      resources: tenantCategoryResponse.pos_versions,
      acls: tenantCategoryResponse.acl
    };
  }

  private getColumnDataKey(title: string): string {
    switch (title) {
      case FiltersTranslation.Common.Tag: {
        return ColumnDataKey.Tags;
      }
      case FiltersTranslation.Common.Product: {
        return ColumnDataKey.Resources;
      }
      case FiltersTranslation.Common.Feature: {
        return ColumnDataKey.ACLs;
      }
    }
  }
}
