import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import { APP } from 'src/modules/shared/constants';
import { ApiService } from 'src/modules/shared/services/api.service';
import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { CreatedData } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';
import {
  Tag,
  TenantCategory,
  TenantCategoryResponse
} from 'src/modules/shared/models/orchestrator/orchestrator-b2b/tenants-categories/tenants-categories.model';

import { TenantsCategoriesService } from './tenants-categories.service';

@Injectable({ providedIn: 'root' })
export class TenantCategoryService {

  constructor(
    private readonly apiService: ApiService,
    private readonly tenantsCategoriesService: TenantsCategoriesService
  ) {}

  public loadTenantCategory(id: string): Observable<TenantCategory> {
    return this.apiService.get(`${ORCHESTRATOR.orchestratorB2B.endpoints.tenantsCategories}/${id}`).pipe(
      switchMap((tenantCategoryResponse: TenantCategoryResponse) => of(this.tenantsCategoriesService.getTenantCategory(tenantCategoryResponse)))
    );
  }

  public loadTenantsCategoriesTags(tagName: string): Observable<Tag[]> {
    const getTagsByNameUrl = `${APP.endpoints.tags}?name=${tagName}`;

    return this.apiService.get(getTagsByNameUrl);
  }

  public loadPermissions(): Observable<any[]> {
    return of([]);
  }

  public loadDeploymentStatus(): Observable<any> {
    return of({});
  }

  public createTenantCategory(tenantCategory: TenantCategory): Observable<CreatedData> {
    const { name: Name, description: Description, tags: Tags } = tenantCategory;

    return this.apiService.post(`${ORCHESTRATOR.orchestratorB2B.endpoints.tenantsCategories}`, { Name, Description, Tags });
  }

  public updateTenantCategory(id: string, tenantCategory: TenantCategory): Observable<void> {
    return this.apiService.put(`${ORCHESTRATOR.orchestratorB2B.endpoints.tenantsCategories}/${id}`, tenantCategory);
  }

  public deleteTenantCategory(id: string): Observable<void> {
    return this.apiService.delete(`${ORCHESTRATOR.orchestratorB2B.endpoints.tenantsCategories}/${id}`);
  }
}
