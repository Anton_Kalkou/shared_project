export const INITIAL_STATE = {
  isTenantCategoryCreated: false,
  tenantsCategoriesTagsNames: undefined,
  selectedTenantCategory: undefined,
  permissions: [],
  deploymentStatus: []
};
