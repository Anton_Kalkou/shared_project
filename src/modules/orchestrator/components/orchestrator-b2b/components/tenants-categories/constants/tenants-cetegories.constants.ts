import { Route } from '@angular/router';
import { AuthenticationGuard } from 'src/modules/shared/guards/auth.guard';
import { CompareTanantsCategoriesComponent } from '../components/compare-tanants-categories/compare-tanants-categories.component';
import { TenantCategoryAddEditComponent } from '../components/tenant-category-add-edit/tenant-category-add-edit.component';
import { TenantsCategoriesComponent } from '../components/tenants-categories/tenants-categories.component';

export const TENANTS_CATEGORIES_ROUTES: Route[] = [
  { path: 'orchestrator-b2b/tenants-categories', component: TenantsCategoriesComponent, canActivate: [AuthenticationGuard] },
  { path: 'orchestrator-b2b/tenants-categories/compare', component: CompareTanantsCategoriesComponent, canActivate: [AuthenticationGuard] },
  { path: 'orchestrator-b2b/tenants-categories/:id/edit', component: TenantCategoryAddEditComponent, canActivate: [AuthenticationGuard] },
  { path: 'orchestrator-b2b/tenants-categories/add', component: TenantCategoryAddEditComponent, canActivate: [AuthenticationGuard] },
];

export const DEPLOYMENT_PROGRESS_DATA = ['IN PROGRESS', 'Thu, Jan 5 2020'];
