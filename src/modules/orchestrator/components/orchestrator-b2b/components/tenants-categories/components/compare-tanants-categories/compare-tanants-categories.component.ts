import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component } from '@angular/core';
import { Store } from '@ngxs/store';
import { SetNavigationButtonStatesTitle } from 'src/modules/layout/ngxs/layout.actions';
import { CompareTenantsCategories } from '../../ngxs/tenants-categories/mock-data/tenants-categories';
import { Translation as TenantsCategoriesTranslation } from '../../enum/tenants-categories.enum';

@Component({
  selector: 'app-compare-tanants-categories',
  templateUrl: './compare-tanants-categories.component.html',
  styleUrls: ['./compare-tanants-categories.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 0)')),
    ]),
  ]
})
export class CompareTanantsCategoriesComponent {
  public columns: any[];
  public catalogWrite: any[];
  public comparableTenantsCategories: any[];

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.initializeValues();
    this.dispatchActions();
  }

  public getComparableTenantsCategoryData(comparableTenantsCategory: any, columnIndex: number): any {
    return Object.values(comparableTenantsCategory)[columnIndex + 1];
  }

  public getTenantsCategoryCatalogWriteData(comparableTenantsCategory: any, columnIndex: number): any {
    return Object.values(comparableTenantsCategory.catalogWriteDetails)[columnIndex];
  }

  private initializeValues(): void {
    this.columns = [
      { header: TenantsCategoriesTranslation.PermissionResource, title: TenantsCategoriesTranslation.PosApp },
      { header: TenantsCategoriesTranslation.Method, title: TenantsCategoriesTranslation.BackofficeFrontend },
      { header: TenantsCategoriesTranslation.Api, title: TenantsCategoriesTranslation.CatalogWrite },
    ];
    this.catalogWrite = CompareTenantsCategories.catalogWrite;
    this.comparableTenantsCategories = CompareTenantsCategories.compareTanantCategories;
  }

  private dispatchActions(): void {
    this.store.dispatch(new SetNavigationButtonStatesTitle(true, TenantsCategoriesTranslation.CompareTenantsCategories));
  }
}
