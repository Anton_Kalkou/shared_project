import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Column } from 'src/modules/shared/models/data-table.model';
import { ColumnDataKey, Translation as TenantCategoryTranslation } from '../../enum/tenant-category.enum';

@Component({
  selector: 'app-deployment-status-modal',
  templateUrl: './deployment-status-modal.component.html',
  styleUrls: ['./deployment-status-modal.component.scss']
})
export class DeploymentStatusModalComponent {

  @Select(state => state.orchestratorB2B.tenantsCategories.tenantsCategory.permissions.deploymentStatus) deploymentStatus$: Observable<any[]>;

  public columns: Column[];

  constructor(
    public dialogRef: MatDialogRef<DeploymentStatusModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  ngOnInit(): void {
    this.initializeValues();
  }

  private initializeValues(): void {
    this.columns = [
      { title: ColumnDataKey.Name, translationKey: TenantCategoryTranslation.Name },
      { title: ColumnDataKey.ScheduledDeploymentDate, translationKey: TenantCategoryTranslation.ScheduledDeploymentDate },
      { title: ColumnDataKey.Status, translationKey: TenantCategoryTranslation.Status }
    ];
  }
}
