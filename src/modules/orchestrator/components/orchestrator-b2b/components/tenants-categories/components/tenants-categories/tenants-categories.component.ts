import { Component, OnDestroy, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';

import { Observable } from 'rxjs';
import { SetHeaderTitle, SetNavigationButtonStatesTitle } from 'src/modules/layout/ngxs/layout.actions';
import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { TenantCategory } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/tenants-categories/tenants-categories.model';
import { Column } from 'src/modules/shared/models/data-table.model';
import { APP } from 'src/modules/shared/constants';
import { ActionType, Icon, TextType, ValueType } from 'src/modules/shared/components/data-table/enum/data-table.enum';

import { FetchTenantsCategories, SelectTenantsCategories } from '../../ngxs/tenants-categories/tenants-categories.actions';
import { TenantsCategoriesService } from '../../services/tenants-categories.service';
import { ColumnDataKey, Translation as TenantCategoryTranslation } from '../../enum/tenants-categories.enum';
import { InfoType } from '../../../../enum/orchestrator-b2b.enum';

@Component({
  selector: 'app-tenants-categories',
  templateUrl: './tenants-categories.component.html',
  styleUrls: ['./tenants-categories.component.scss']
})
export class TenantsCategoriesComponent implements OnInit, OnDestroy {
  @Select(state => state.orchestratorB2B.tenantsCategories.tenantsCategories) tenantsCategories$: Observable<TenantCategory[]>;

  public displayedColumns: Column[];
  public filtersEndpoint: string;

  constructor(
    public readonly tenantsCategoriesService: TenantsCategoriesService,
    private readonly store: Store
  ) {}

  ngOnInit(): void {
    this.initializeValues();
    this.dispatchActions();
  }

  ngOnDestroy(): void {
    this.store.dispatch(new SetNavigationButtonStatesTitle(false));
  }

  public onSelect(selectedTenantsCategories: TenantCategory[]): void {
    this.store.dispatch(new SelectTenantsCategories(selectedTenantsCategories));
  }

  private initializeValues(): void {
    this.displayedColumns = [
      {
        title: ColumnDataKey.Select,
        valueType: ValueType.Select,
        isSortableDisabled: true
      },
      {
        title: ColumnDataKey.Name,
        translationKey: TenantCategoryTranslation.Name,
        valueType: ValueType.Text,
        textType: TextType.Simple
      },
      {
        title: ColumnDataKey.Tags,
        translationKey: TenantCategoryTranslation.Tags,
        valueType: ValueType.Text,
        textType: TextType.Array,
        arrayItemTitle: ColumnDataKey.Name
      },
      {
        title: ColumnDataKey.Resources,
        translationKey: TenantCategoryTranslation.Resources,
        valueType: ValueType.Text,
        textType: TextType.Array,
        arrayItemTitle: ColumnDataKey.Version
      },
      {
        title: ColumnDataKey.ACLs,
        translationKey: TenantCategoryTranslation.ACLs,
        valueType: ValueType.Text,
        textType: TextType.Array,
        arrayItemTitle: ColumnDataKey.ACLs
      },
      {
        title: ColumnDataKey.Action,
        valueType: ValueType.Action,
        actionType: ActionType.CustomIcon,
        actionDataKey: ColumnDataKey.Id,
        path: InfoType.Edit,
        icon: Icon.Pencil,
        isSortableDisabled: true
      }
    ];
    this.filtersEndpoint = APP.endpoints.tenantsCategoriesFilters;
  }

  private dispatchActions(): void {
    const dispatchedActions = [
      new SetHeaderTitle(ORCHESTRATOR.orchestratorB2B.headerTitles.orchestratorB2B),
      new SetNavigationButtonStatesTitle(true, ORCHESTRATOR.orchestratorB2B.navButtonStateTitles.tenantsCategories),
      new SetSpinnerVisibility(true),
      new FetchTenantsCategories(),
      new SetNavigationButtonStatesTitle(true, ORCHESTRATOR.orchestratorB2B.navButtonStateTitles.tenantsCategories)
    ];

    this.store.dispatch(dispatchedActions);
  }
}
