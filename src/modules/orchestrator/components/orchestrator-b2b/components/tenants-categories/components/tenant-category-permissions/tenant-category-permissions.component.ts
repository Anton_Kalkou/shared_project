import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { MatSelectChange } from '@angular/material/select';
import { MatDialog } from '@angular/material/dialog';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { Version } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';
import { Column } from 'src/modules/shared/models/data-table.model';
import { Feature } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/features/features.model';
import { Permission } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/permissions/permissions.model';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { TranslationService } from 'src/modules/shared/services/translation.service';
import { TenantCategory } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/tenants-categories/tenants-categories.model';

import { ScheduleDeploymentModalComponent } from '../schedule-deployment-modal/schedule-deployment-modal.component';
import { BaseVersionPage } from '../../../../shared/classes/base/base-version-page.class';
import { PermissionsService } from '../../../permissions/services/permissions.service';
import { TenantCategoryState } from '../../ngxs/tenant-category/tenant-category.state';
import { CreateVersionDialogComponent } from '../../../../shared/components/create-version-dialog/create-version-dialog.component';
import { DataLabel } from '../../../../enum/orchestrator-b2b.enum';
import { versionExistsValidator } from '../../../../shared/validators/versionExists.validator';
import {
  ChangeSelectedVersion,
  ChangeVersionState,
  CreateVersion,
  PublishVersion,
} from '../../../../shared/ngxs/version/version.actions';

@Component({
  selector: 'app-tenant-category-permissions',
  templateUrl: './tenant-category-permissions.component.html',
  styleUrls: ['./tenant-category-permissions.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 0)')),
    ]),
  ]
})
export class TenantCategoryPermissionsComponent extends BaseVersionPage<Feature, Permission> {
  @Select(state => state.orchestratorB2B.version.selectedItem) selectedItem$: Observable<TenantCategory>;
  @Select(state => state.orchestratorB2B.version.selectedVersion) selectedVersion$: Observable<Version>;
  @Select(state => state.orchestratorB2B.version.isVersionPuplished) isVersionPuplished$: Observable<boolean>;
  @Select(state => state.orchestratorB2B.version.tableData) tableData$: Observable<Permission[]>;

  constructor(
    public readonly permissionsService: PermissionsService,
    protected readonly formBuilder: FormBuilder,
    protected readonly route: ActivatedRoute,
    protected readonly store: Store,
    protected readonly notificationService: NotificationService,
    protected readonly translationService: TranslationService,
    private readonly dialog: MatDialog
  ) {
    super(
      route,
      store,
      formBuilder,
      notificationService,
      translationService,
      TenantCategoryState
    );
  }

  public onVersionChange({ value }: MatSelectChange): void {
    this.store.dispatch(new ChangeSelectedVersion(value));
  }

  public onPublish(): void {
    this.store.dispatch(new PublishVersion());
  }

  public onVersion(isDuplicate?: boolean): void {
    this.dialog.open(CreateVersionDialogComponent, this.getVersionDialogOptions(isDuplicate))
      .afterClosed()
      .subscribe(version =>
        version && this.store.dispatch(new CreateVersion(
          { ...version, isDisabled: false },
          isDuplicate
        ))
      );
  }

  public onVersionState(): void {
    const isDisabled = this.formGroup.get(DataLabel.IsDisabled).value;
    
    this.store.dispatch(new ChangeVersionState(!isDisabled));
  }

  public getPermissionsData(permissions: any[], columnIndex: number): any {
    return Object.values(permissions)[columnIndex]
  }

  public getPermissionCatalogWhiteData(permissions: any, columnIndex: number): any {
    return Object.values(permissions.catalogWhiteDetails)[columnIndex];
  }

  public onSearch(key: string): void  {}

  public onOpenScheduleDeploymentModal(): void {
    this.dialog.open(ScheduleDeploymentModalComponent, {
      height: '550px',
      width: '950px'
    });
  }

  protected initFormGroup(selectedVersion?: Version): void {
    const { isDisabled = true, name = '', description = '' } = selectedVersion || {};

    this.formGroup = this.formBuilder.group({
      isDisabled,
      name: [
        { value: name, disabled: isDisabled },
        {
          validators: [Validators.required],
          asyncValidators: [versionExistsValidator(this.selectedItem$, this.selectedVersion$)],
        }
      ],
      description: { value: description, disabled: isDisabled }
    });
  }

  protected getColumns(): Column[] {
    return [];
  }

  protected dispatchActions(): void {}
}
