import { Component, } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Select, Store } from '@ngxs/store';

import { Observable } from 'rxjs';
import { TenantCategory } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/tenants-categories/tenants-categories.model';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { TranslationService } from 'src/modules/shared/services/translation.service';

import { CreateTenantCategory, DeleteTenantCategory, FetchTenantCategoriesTagsByName, FetchTenantCategory, UpdateTenantCategory } from '../../ngxs/tenant-category/tenant-category.actions';
import { BaseInfoPage } from '../../../../shared/classes/base/base-info-page.class';
import { getEntityRouteId, isFormValid } from 'src/modules/shared/helpers';
import { TenantCategoryState } from '../../ngxs/tenant-category/tenant-category.state';
import { Tag } from '../../../../../../../shared/models/tag.model';

@Component({
  selector: 'app-tenant-category-info',
  templateUrl: './tenant-category-info.component.html',
  styleUrls: ['./tenant-category-info.component.scss']
})
export class TenantCategoryInfoComponent extends BaseInfoPage<TenantCategory> {
  @Select(state => state.orchestratorB2B.tenantsCategories.tenantCategory.isTenantCategoryCreated) isCreated$: Observable<boolean>;
  @Select(state => state.orchestratorB2B.tenantsCategories.tenantCategory.selectedTenantCategory) selectedItem$: Observable<TenantCategory>;
  @Select(state => state.orchestratorB2B.tenantsCategories.tenantCategory.tenantsCategoriesTagsNames) tenantsCategoriesTagsNames$: Observable<Tag[]>;

  protected selectedIdSelector = TenantCategoryState.getItemId;
  protected selectedNameSelector = TenantCategoryState.getItemName;

  constructor(
    protected readonly formBuilder: FormBuilder,
    protected readonly store: Store,
    protected readonly notificationService: NotificationService,
    protected readonly translationService: TranslationService,
    private readonly route: ActivatedRoute,
  ) {
    super(store, formBuilder, notificationService, translationService);
  }

  public handleNewTagInputChanges(tagName: string): void {
    this.store.dispatch(new FetchTenantCategoriesTagsByName(tagName));
  }

  public onCreate(): void {
    if (!isFormValid(this.formGroup)) {
      return;
    }

    this.store.dispatch(new CreateTenantCategory(this.formGroup.getRawValue()));
  }

  public onDelete(): void {
    this.store.dispatch(new DeleteTenantCategory(this.currentId));
  }

  protected onEdit(): void {
    this.setInitialFormValues(this.formGroup);
    this.store.dispatch(new UpdateTenantCategory(this.currentId, this.initialFormValues));
  }

  protected initEditData(): void {
    this.currentId = getEntityRouteId(this.route);

    this.initSubscriptions();
    this.store.dispatch([new FetchTenantCategory(this.currentId)]);
  }

  protected initFormGroup(selectedTenantCategory?: TenantCategory): void {
    this.formGroup = this.formBuilder.group({
      name: [selectedTenantCategory ? selectedTenantCategory.name : '', Validators.required],
      description: selectedTenantCategory ? selectedTenantCategory.description : '',
      tags: [selectedTenantCategory ? selectedTenantCategory.tags : [], Validators.required],
    });
  }
}
