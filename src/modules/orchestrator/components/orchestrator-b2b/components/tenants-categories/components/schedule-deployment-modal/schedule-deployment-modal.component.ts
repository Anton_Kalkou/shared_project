import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-schedule-modal-deployment',
  templateUrl: './schedule-deployment-modal.component.html',
  styleUrls: ['./schedule-deployment-modal.component.scss']
})
export class ScheduleDeploymentModalComponent {

  constructor(
    public dialogRef: MatDialogRef<ScheduleDeploymentModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

}
