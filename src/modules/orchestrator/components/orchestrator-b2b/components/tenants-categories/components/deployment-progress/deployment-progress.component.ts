import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { TranslationService } from 'src/modules/shared/services/translation.service';
import { DeploymentStatusModalComponent } from '../deployment-status-modal/deployment-status-modal.component';
import { Translation as TenantCategoryTranslation, DeploymentProgress } from '../../enum/tenant-category.enum';
import { DEPLOYMENT_PROGRESS_DATA } from '../../constants/tenants-cetegories.constants';

@Component({
  selector: 'app-deployment-progress',
  templateUrl: './deployment-progress.component.html',
  styleUrls: ['./deployment-progress.component.scss']
})
export class DeploymentProgressComponent {

  @Input() deploymentStatus: any[];
  
  public data: string[];
  public tiles: any[];
  public deploymentProcessedCount: number;
  public deploymentRemaningCount: number;

  constructor(private dialog: MatDialog, private translationService: TranslationService) {}

  ngOnInit(): void {
    this.initializeValues();
  }

  public onTileButtonClick(buttonTitle: string): void {;
    this.translationService.translate(buttonTitle).subscribe(tile => {
      if (tile === DeploymentProgress.Tile.ViewAll) {
        this.dialog.open(DeploymentStatusModalComponent, {
          height: '900px',
          width: '1200px'
        });
      }
    });
  }

  private initializeValues(): void {
    this.data = DEPLOYMENT_PROGRESS_DATA;
    this.tiles = [
      { title: TenantCategoryTranslation.Status, buttonTitle: TenantCategoryTranslation.Stop },
      { title: TenantCategoryTranslation.DeploymentDate, buttonTitle: TenantCategoryTranslation.ReSchedule },
      { title: TenantCategoryTranslation.Proceed, buttonTitle: TenantCategoryTranslation.ViewAll },
      { title: TenantCategoryTranslation.Remaning, buttonTitle: TenantCategoryTranslation.ViewAll }
    ];
    this.deploymentProcessedCount = this.deploymentStatus.reduce((acc, statusItem) => statusItem.status === DeploymentProgress.Status.Processed ? ++acc : acc, 0);
    this.deploymentRemaningCount = this.deploymentStatus.reduce((acc, statusItem) => statusItem.status === DeploymentProgress.Status.Pending ? ++acc : acc, 0);
  }
}
