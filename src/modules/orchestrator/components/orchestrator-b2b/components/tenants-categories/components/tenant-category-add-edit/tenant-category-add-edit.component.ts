import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';

import { SetHeaderTitle, SetNavigationButtonStatesTitle } from 'src/modules/layout/ngxs/layout.actions';
import { isSubstring } from 'src/modules/shared/helpers';

import { InfoType } from '../../../../enum/orchestrator-b2b.enum';
import { Translation as OrchestratorB2bTranslation } from '../../../../enum/orchestrator-b2b.enum';

@Component({
  selector: 'app-tenant-category-add-edit',
  templateUrl: './tenant-category-add-edit.component.html',
  styleUrls: ['./tenant-category-add-edit.component.scss']
})
export class TenantCategoryAddEditComponent implements OnInit {
  public infoType: string;

  constructor(private store: Store, private router: Router) {}

  ngOnInit(): void {
    this.initializeValues();
    this.dispatchActions();
  }

  private initializeValues(): void {
    this.infoType = isSubstring(this.router.url, InfoType.Add) ? InfoType.Add : InfoType.Edit;
  }

  private dispatchActions(): void {
    this.store.dispatch(new SetHeaderTitle(''));

    if (this.infoType === InfoType.Add) {
      this.store.dispatch(new SetNavigationButtonStatesTitle(true, OrchestratorB2bTranslation.AddNavigationButtonStatesTitle));
    }
  }
}
