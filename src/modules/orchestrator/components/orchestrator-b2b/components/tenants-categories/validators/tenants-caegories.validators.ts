import { FormControl } from '@angular/forms';

export const areNotEmptyTagsValidator = (formControl: FormControl) => {
  const value = formControl.value;
  const required = !value.length

  return required ? { required } : null;
}
