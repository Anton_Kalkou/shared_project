import { Injectable } from '@angular/core';
import { Action, State, StateContext, Store } from '@ngxs/store';

import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { TenantCategory } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/tenants-categories/tenants-categories.model';

import { TenantsCategoriesService } from '../../services/tenants-categories.service';
import { TenantCategoryState } from '../tenant-category/tenant-category.state';
import { FetchTenantsCategories } from './tenants-categories.actions';

interface TenantsCategoryStateModel {
  tenantsCategories: TenantCategory[];
}

@State<TenantsCategoryStateModel>({
  name: 'tenantsCategories',
  defaults: {
    tenantsCategories: []
  },
  children: [TenantCategoryState]
})
@Injectable()
export class TenantsCategoryState {

  constructor(private tenantsCategoriesService: TenantsCategoriesService, private store: Store) {}

  @Action(FetchTenantsCategories)
  fetchTenantsCategories({ patchState }: StateContext<TenantsCategoryStateModel>) {
    return this.tenantsCategoriesService.loadTenantsCategories().subscribe(tenantsCategories => {
      this.store.dispatch(new SetSpinnerVisibility(false));
      patchState({ tenantsCategories });
    });
  }
}
