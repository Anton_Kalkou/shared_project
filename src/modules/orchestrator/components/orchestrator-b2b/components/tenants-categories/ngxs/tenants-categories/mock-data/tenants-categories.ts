export namespace CompareTenantsCategories {
  export const compareTanantCategories = [
    {
      name: 'Early Adopter - USA',
      pos: '1.11.10',
      backofficeFrontend: '1.11.10',
      catalogWrite: 'v2',
      catalogWriteDetails: {
        catalog1: 'v1',
        order1: 'v2',
        catalog2: 'v1',
        order2: 'v1',
      }
    },
    {
      name: 'Mid Adopter - USA',
      pos: '1.11.10',
      backofficeFrontend: '1.11.10',
      catalogWrite: 'v2',
      catalogWriteDetails: {
        catalog1: 'v2',
        order1: 'v2',
        catalog2: 'v3',
        order2: 'v1',
      }
    },
    {
      name: 'Late Adopter - USA',
      pos: '1.11.10',
      backofficeFrontend: '1.11.10',
      catalogWrite: 'v2',
      catalogWriteDetails: {
        catalog1: 'v3',
        order1: 'v2',
        catalog2: 'v3',
        order2: 'v1',
      }
    },
  ];

  export const catalogWrite = [
    { permissionResource: 'Catalog', method: 'GET', api: '/consumer/item/itemid' },
    { permissionResource: 'Order', method: 'POST', api: '/item' },
    { permissionResource: 'Catalog', method: 'GET', api: '/item/improvement' },
    { permissionResource: 'Order', method: 'POST', api: '/item/improvement' },
  ];
}
