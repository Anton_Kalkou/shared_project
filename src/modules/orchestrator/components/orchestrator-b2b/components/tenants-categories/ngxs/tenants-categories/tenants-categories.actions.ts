import { TenantCategory } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/tenants-categories/tenants-categories.model';

export class FetchTenantsCategories {
  static readonly type = '[Tenants Categories] Fetch';

  constructor() {}
}

export class SelectTenantsCategories {
  static readonly type = '[Tenants Categories] Select';

  constructor(public selectedTenantsCategories: TenantCategory[]) {}
}
