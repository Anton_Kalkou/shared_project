import { TenantCategory } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/tenants-categories/tenants-categories.model';

export class FetchTenantCategory {
  static readonly type = '[Tenant Category] Fetch';

  constructor(public id: string) {}
}

export class FetchTenantCategoriesTagsByName {
  static readonly type = '[Tenant Category] Fetch Tags By Name';

  constructor(public name: string) {}
}

export class CreateTenantCategory {
  static readonly type = '[Tenant Category] Create';

  constructor(public tenantCategory: TenantCategory) {}
}

export class UpdateTenantCategory {
  static readonly type = '[Tenant Category] Update';

  constructor(public id: string, public tenantCategory: TenantCategory) {}
}

export class DeleteTenantCategory {
  static readonly type = '[Tenant Category] Delete';

  constructor(public id: string) {}
}

export class ResetTenantCategoryState {
  static readonly type = '[Tenant Category] Reset';
}
