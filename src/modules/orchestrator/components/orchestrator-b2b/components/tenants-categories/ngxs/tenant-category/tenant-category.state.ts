import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Action, Selector, State, StateContext, Store } from '@ngxs/store';

import { combineLatest } from 'rxjs';

import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { TranslationService } from 'src/modules/shared/services/translation.service';
import { SetNavigationButtonStatesTitle } from 'src/modules/layout/ngxs/layout.actions';
import { Permission } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/permissions/permissions.model';

import { TenantCategoryService } from '../../services/tenant-category.service';
import { Translation as TenantCategoryTranslation } from '../../enum/tenant-category.enum';
import { INITIAL_STATE } from '../../constants/tenant-category.constains';
import { VersionService } from '../../../../shared/services/version.service';
import { SetVersionState } from '../../../../shared/ngxs/version/version.actions';
import { DataLabel } from '../../../../enum/orchestrator-b2b.enum';
import {
  FetchTenantCategory,
  CreateTenantCategory,
  UpdateTenantCategory,
  DeleteTenantCategory,
  ResetTenantCategoryState,
  FetchTenantCategoriesTagsByName
} from './tenant-category.actions';
import { Tag, TenantCategory } from '../../../../../../../shared/models/orchestrator/orchestrator-b2b/tenants-categories/tenants-categories.model';

interface TenantCategoryStateModel {
  isTenantCategoryCreated: boolean;
  selectedTenantCategory: TenantCategory;
  tenantsCategoriesTagsNames: Tag[];
  permissions: Permission[];
  deploymentStatus: any[];
}

@State<TenantCategoryStateModel>({
  name: 'tenantCategory',
  defaults: INITIAL_STATE
})
@Injectable()
export class TenantCategoryState {
  constructor(
    private readonly router: Router,
    private readonly store: Store,
    private readonly tenantCategoryService: TenantCategoryService,
    private readonly translationService: TranslationService,
    private readonly notificationService: NotificationService,
    private readonly versionService: VersionService
  ) {}

  @Selector()
  static getItemId({ selectedTenantCategory }: TenantCategoryStateModel) {
    return selectedTenantCategory.id;
  }

  @Selector()
  static getItemName({ selectedTenantCategory }: TenantCategoryStateModel) {
    return selectedTenantCategory && selectedTenantCategory.name;
  }

  @Action(FetchTenantCategory)
  fetchTenantCategory({ patchState }: StateContext<TenantCategoryStateModel>, { id }: FetchTenantCategory) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.tenantCategoryService.loadTenantCategory(id).subscribe(
      selectedTenantCategory => {
        const { name, versions } = selectedTenantCategory;
        const selectedVersion = this.versionService.getSelectedVersion(versions);

        this.store.dispatch([
          new SetVersionState(
            `${ORCHESTRATOR.orchestratorB2B.endpoints.tenantsCategories}/${id}`,
            selectedTenantCategory,
            selectedVersion,
            ORCHESTRATOR.orchestratorB2B.endpoints.permissions,
            DataLabel.Permissions,
          ),
          new SetSpinnerVisibility(false),
          new SetNavigationButtonStatesTitle(true, `Edit ${name}`)
        ]);
        patchState({ selectedTenantCategory });
      },
      () => this.store.dispatch(new SetSpinnerVisibility(false))
    );
  }

  @Action(FetchTenantCategoriesTagsByName)
  fetchTenantCategoriesTags({ patchState }: StateContext<TenantCategoryStateModel>, { name }: FetchTenantCategoriesTagsByName) {
    return this.tenantCategoryService.loadTenantsCategoriesTags(name).subscribe(tenantsCategoriesTagsNames => patchState({ tenantsCategoriesTagsNames }));
  }

  @Action(CreateTenantCategory)
  createTenantCategory({ patchState }: StateContext<TenantCategoryStateModel>, { tenantCategory }: CreateTenantCategory) {
    const createdTenantCategory$ = this.tenantCategoryService.createTenantCategory(tenantCategory);
    const translatedMessage$ = this.translationService.translate(TenantCategoryTranslation.CreateInfoContent);

    this.store.dispatch(new SetSpinnerVisibility(true));

    return combineLatest([createdTenantCategory$, translatedMessage$]).subscribe(
      ([{ id }, translatedMessage]) => {
        const selectedTenantCategory = { id, ...tenantCategory, versions: [] };

        this.store.dispatch([
          new SetVersionState(
            `${ORCHESTRATOR.orchestratorB2B.endpoints.tenantsCategories}/${selectedTenantCategory.id}`,
            selectedTenantCategory,
            undefined,
            ORCHESTRATOR.orchestratorB2B.endpoints.permissions,
            DataLabel.Permissions,
          ),
          new SetSpinnerVisibility(false)
        ]);
        this.notificationService.showSuccessMessage(translatedMessage);

        patchState({ selectedTenantCategory, isTenantCategoryCreated: true });
      },
      () => this.store.dispatch(new SetSpinnerVisibility(false))
    );
  }

  @Action(UpdateTenantCategory)
  updateTenantCategory({ patchState }: StateContext<TenantCategoryStateModel>, { id, tenantCategory }: UpdateTenantCategory) {
    return this.tenantCategoryService.updateTenantCategory(id, tenantCategory).subscribe(() => {
      patchState({ selectedTenantCategory: { ...tenantCategory } });
    });
  }

  @Action(DeleteTenantCategory)
  deleteTenantCategory({ patchState }: StateContext<TenantCategoryStateModel>, { id }: DeleteTenantCategory) {
    const deletedTenantCategory$ = this.tenantCategoryService.deleteTenantCategory(id);
    const translatedMessage$ = this.translationService.translate(TenantCategoryTranslation.DeleteInfoContent);

    this.store.dispatch(new SetSpinnerVisibility(true));

    return combineLatest([deletedTenantCategory$, translatedMessage$]).subscribe(
      ([, translatedMessage]) => {
        this.store.dispatch([
          new SetSpinnerVisibility(false),
          new ResetTenantCategoryState()
        ]);
        this.router.navigate([`/${ORCHESTRATOR.orchestratorB2B.tenantsCategories.tenantsCategoriesPath}`]);
        this.notificationService.showSuccessMessage(translatedMessage);

        patchState({ selectedTenantCategory: null });
      },
      () => this.store.dispatch(new SetSpinnerVisibility(false))
    );
  }

  @Action(ResetTenantCategoryState)
  resetTenantCategoryState({ patchState }: StateContext<TenantCategoryStateModel>) {
    patchState({ ...INITIAL_STATE });
  }
}
