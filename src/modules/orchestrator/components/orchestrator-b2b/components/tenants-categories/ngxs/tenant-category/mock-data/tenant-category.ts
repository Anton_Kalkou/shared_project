import * as moment from 'moment';

export namespace PermissionsTable {
  export const permissions =  {
    pos: 'v2',
    backofficeFrontend: 'v2',
    catalogWhite: 'v2',
    catalogWhiteDetails: {
      catalog1: 'v2',
      order1: 'v2',
      catalog2: 'v2',
      order2: 'v2',
    }
  };

  export const catalogWhite = [
    { permissionResource: 'Catalog', method: 'GET', api: '/consumer/item/itemid' },
    { permissionResource: 'Order', method: 'POST', api: '/item' },
    { permissionResource: 'Catalog', method: 'GET', api: '/item/improvement' },
    { permissionResource: 'Order', method: 'POST', api: '/receip/rayment' },
  ];
}

export const deploymentStatus  = [
  { name: 'Indigo', scheduledDeploymentDate: moment().toLocaleString(), status: 'Processed' },
  { name: 'RockSpoon Cafe', scheduledDeploymentDate: moment().toLocaleString(), status: 'Processed' },
  { name: 'McDonalds', scheduledDeploymentDate: moment().toLocaleString(), status: 'Pending' },
  { name: 'Happy Meal', scheduledDeploymentDate: moment().toLocaleString(), status: 'Pending' },
  { name: 'Indigo', scheduledDeploymentDate: moment().toLocaleString(), status: 'Pending' },
  { name: 'RockSpoon Cafe', scheduledDeploymentDate: moment().toLocaleString(), status: 'Pending' },
  { name: 'McDonalds', scheduledDeploymentDate: moment().toLocaleString(), status: 'Pending' },
  { name: 'Happy Meal', scheduledDeploymentDate: moment().toLocaleString(), status: 'Pending' },
  { name: 'Indigo', scheduledDeploymentDate: moment().toLocaleString(), status: 'Pending' },
  { name: 'RockSpoon Cafe', scheduledDeploymentDate: moment().toLocaleString(), status: 'Pending' },
  { name: 'McDonalds', scheduledDeploymentDate: moment().toLocaleString(), status: 'Pending' },
  { name: 'Happy Meal', scheduledDeploymentDate: moment().toLocaleString(), status: 'Pending' },
];
