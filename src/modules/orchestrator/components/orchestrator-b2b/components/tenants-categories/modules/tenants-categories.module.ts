import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { SectionHeaderModule } from 'src/modules/orchestrator/shared/components/section-header/section-header.module';
import { AddButtonModule } from 'src/modules/shared/components/add-button/add-button.module';
import { IconButtonModule } from 'src/modules/shared/components/icon-button/icon-button.module';
import { SearchModule } from 'src/modules/shared/components/search/search.module';
import { SquareButtonModule } from 'src/modules/shared/components/square-button/square-button.module';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { DataTableModule } from 'src/modules/shared/components/data-table/data-table.module';
import { ConfirmationModule } from 'src/modules/shared/components/confirmation/confirmation.module';
import { AutoSaveNotificationTemplateModule } from 'src/modules/shared/components/auto-save-notification-template/auto-save-notification-template.module';
import { InputWithChipsModule } from 'src/modules/shared/components/controls/input-with-chips/input-with-chips.module';

import { TenantsCategoriesComponent } from '../components/tenants-categories/tenants-categories.component';
import { CompareTanantsCategoriesComponent } from '../components/compare-tanants-categories/compare-tanants-categories.component';
import { DeploymentProgressComponent } from '../components/deployment-progress/deployment-progress.component';
import { ScheduleDeploymentModalComponent } from '../components/schedule-deployment-modal/schedule-deployment-modal.component';
import { TenantCategoryAddEditComponent } from '../components/tenant-category-add-edit/tenant-category-add-edit.component';
import { TenantCategoryChangelogsComponent } from '../components/tenant-category-changelogs/tenant-category-changelogs.component';
import { TenantCategoryInfoComponent } from '../components/tenant-category-info/tenant-category-info.component';
import { TenantCategoryPermissionsComponent } from '../components/tenant-category-permissions/tenant-category-permissions.component';
import { DeploymentStatusModalComponent } from '../components/deployment-status-modal/deployment-status-modal.component';
import { DataListModule } from '../../../shared/components/data-list/data-list.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule,
    TranslateModule,
    IconButtonModule,
    SquareButtonModule,
    SearchModule,
    SectionHeaderModule,
    DataTableModule,
    MaterialModule,
    AddButtonModule,
    NgxMaterialTimepickerModule,
    ConfirmationModule,
    DataListModule,
    AutoSaveNotificationTemplateModule,
    InputWithChipsModule
  ],
  declarations: [
    TenantsCategoriesComponent,
    TenantCategoryAddEditComponent,
    TenantCategoryInfoComponent,
    TenantCategoryPermissionsComponent,
    TenantCategoryChangelogsComponent,
    ScheduleDeploymentModalComponent,
    DeploymentProgressComponent,
    DeploymentStatusModalComponent,
    CompareTanantsCategoriesComponent
  ],
  entryComponents: [
    ScheduleDeploymentModalComponent,
    DeploymentStatusModalComponent
  ],
  exports: [
    TenantsCategoriesComponent
  ]
})
export class TenantsCategoryModule {}
