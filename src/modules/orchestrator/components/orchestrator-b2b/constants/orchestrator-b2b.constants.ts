import { Route } from '@angular/router';

import { AuthenticationGuard } from 'src/modules/shared/guards/auth.guard';

import { PRODUCTS_ROUTES } from '../components/products/constants/products.constants';
import { FEATURES_ROUTES } from '../components/features/constants/features.constants';
import { TENANTS_CATEGORIES_ROUTES } from '../components/tenants-categories/constants/tenants-cetegories.constants';
import { PERMISSIONS_ROUTES } from '../components/permissions/constants/permissions.constants';
import { DEFAULT_ROLES_ROUTES } from '../components/default-roles/constants/default-roles.constants';
import { TENANTS_ROUTES } from '../components/tenants/constants/tenants-routes.constants';

export const ORCHESTRATOR_B2B_ROUTES: Route[] = [
  { path: 'orchestrator-b2b', redirectTo: 'orchestrator-b2b/products', canActivate: [AuthenticationGuard] },
  ...PRODUCTS_ROUTES,
  ...FEATURES_ROUTES,
  ...PERMISSIONS_ROUTES,
  ...TENANTS_CATEGORIES_ROUTES,
  ...TENANTS_ROUTES,
  ...DEFAULT_ROLES_ROUTES
];

export const AUTO_SAVE_TIMOUT_MS = 3000;
