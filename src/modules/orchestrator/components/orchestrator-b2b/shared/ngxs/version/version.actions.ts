import { Feature } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/features/features.model';
import { Version } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';
import { Api, Permission } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/permissions/permissions.model';
import { Product } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/products/product.model';

export class SetVersionState {
  static readonly type = '[Version State] Set';

  constructor(
    public readonly itemEndpoint: string,
    public readonly selectedItem: SelectedItem,
    public readonly selectedVersion: Version,
    public readonly tableDataEndpoint?: string,
    public readonly tableDataKey?: string
  ) {}
}

export class CreateVersion {
  static readonly type = '[Version] Create';

  constructor(public readonly version: Version, public readonly isDuplicate?: boolean) {}
}

export class ChangeSelectedVersion {
  static readonly type = '[Version] Change';

  constructor(public readonly versionName: string) {}
}

export class PublishVersion {
  static readonly type = '[Version] Publish';
}

export class ChangeVersionState {
  static readonly type = '[Version State] Change';

  constructor(public readonly isDisabled: boolean) {}
}

export class FetchVersionTableData {
  static readonly type = '[Version Table Data] Fetch';
}

export class UpdateVersion {
  static readonly type = '[Version] Update';

  constructor(public readonly version: Version, public readonly isNotificationMessage?: boolean) {}
}

export class ResetVersionState {
  static readonly type = '[Version State] Reset';
}

export type SelectedItem = Product | Feature | Permission | Api;
export type TableData = SelectedItem;
