import { Injectable } from '@angular/core';
import { Action, State, StateContext, Store } from '@ngxs/store';
import { isNull } from 'lodash';
import { combineLatest } from 'rxjs';

import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { Version } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { TranslationService } from 'src/modules/shared/services/translation.service';

import { VersionService } from '../../services/version.service';
import { Translation as OrchestratorB2BTranslation } from '../../../enum/orchestrator-b2b.enum';
import { INTIAIL_STATE } from '../../../constants/version.constants';
import {
  ChangeSelectedVersion,
  ChangeVersionState,
  CreateVersion,
  FetchVersionTableData,
  PublishVersion,
  ResetVersionState,
  SelectedItem,
  SetVersionState,
  TableData,
  UpdateVersion
} from './version.actions';

export interface VersionStateModel {
  itemEndpoint: string;
  selectedItem: SelectedItem;
  selectedVersion: Version;
  isVersionPuplished: boolean;
  tableDataEndpoint?: string;
  tableData?: TableData[];
  tableDataKey?: string;
}

@State<VersionStateModel>({
  name: 'version',
  defaults: INTIAIL_STATE
})
@Injectable()
export class VersionState {

  constructor(
    private readonly store: Store,
    private readonly translationService: TranslationService,
    private readonly notificationService: NotificationService,
    private readonly versionService: VersionService
  ) {}

  @Action(SetVersionState)
  setVersionState({ patchState }: StateContext<VersionStateModel>, action: SetVersionState) {
    const { selectedVersion } = action;
    const isVersionPuplished = selectedVersion
      ? this.versionService.isVersionPuplished(selectedVersion)
      : false

    patchState({ ...action, isVersionPuplished });
  }

  @Action(CreateVersion)
  createVersion({ getState, patchState }: StateContext<VersionStateModel>, { version, isDuplicate }: CreateVersion) {
    const { selectedItem, selectedVersion, tableDataKey, tableData, itemEndpoint } = this.versionService.cloneSelectedData(getState());
    const createdVersionData = { ...version, version: version.name };
    let createdVersionTableData;

    if (tableDataKey) {
      createdVersionTableData = isDuplicate ? selectedVersion[tableDataKey] : [];

      createdVersionData[tableDataKey] = this.versionService.getTableDataIds<typeof tableData[0]>(createdVersionTableData);
    }

    const createdVersion$ = this.versionService.createVersion(itemEndpoint, createdVersionData);
    const translatedMessage$ = this.translationService.translate(OrchestratorB2BTranslation.UpdateInfoContent);

    this.store.dispatch(new SetSpinnerVisibility(true));

    return combineLatest([createdVersion$, translatedMessage$]).subscribe(
      ([createdData, translatedMessage]) => {
        const version = {
          id: !isNull(createdData) ? createdData.id : '',
          ...createdVersionData,
        };

        if (tableDataKey) {
          version[tableDataKey] = isDuplicate ? createdVersionTableData : [];
        }

        const item = {
          ...selectedItem,
          versions: [...selectedItem.versions, version]
        };

        this.store.dispatch(new SetSpinnerVisibility(false));
        this.notificationService.showSuccessMessage(translatedMessage);

        patchState({
          selectedItem: item,
          selectedVersion: version,
          isVersionPuplished: false,
        });
      },
      () => this.store.dispatch(new SetSpinnerVisibility(false))
    );
  }

  @Action(UpdateVersion)
  updateVersion({ getState, patchState }: StateContext<VersionStateModel>, { version, isNotificationMessage }: UpdateVersion) {
    const { itemEndpoint, selectedItem, selectedVersion, tableData, tableDataKey } = this.versionService.cloneSelectedData(getState());
    const versionData = { ...version };

    if (tableDataKey) {
      versionData[tableDataKey] = this.versionService.getTableDataIds<typeof tableData[0]>(version[tableDataKey]);
    }

    const updatedVersion$ = this.versionService.updateVersionTableData(
      itemEndpoint,
      selectedVersion.id,
      versionData,
    );
    const translatedMessage$ = this.translationService.translate(OrchestratorB2BTranslation.UpdateInfoContent);

    if (isNotificationMessage) {
      this.store.dispatch(new SetSpinnerVisibility(true));
    }

    return combineLatest([updatedVersion$, translatedMessage$]).subscribe(
      ([, translatedMessage]) => {
        const udpatedVersion = {
          ...selectedVersion,
          ...version
        };
        const updatedItem = {
          ...selectedItem,
          versions: this.versionService.replaceVersion(selectedVersion.id, selectedItem.versions, udpatedVersion)
        };

        if (isNotificationMessage) {
          this.store.dispatch(new SetSpinnerVisibility(false));
          this.notificationService.showSuccessMessage(translatedMessage);
        }

        patchState({ selectedItem: updatedItem, selectedVersion: udpatedVersion });
      },
      () => isNotificationMessage && this.store.dispatch(new SetSpinnerVisibility(false))
    );
  }

  @Action(ChangeSelectedVersion)
  changeSelectedVersion({ getState, patchState }: StateContext<VersionStateModel>, { versionName }: ChangeSelectedVersion) {
    const { selectedItem: { versions } } = getState();
    const selectedVersion = this.versionService.getSelectedVersionByName(versions, versionName);

    patchState({
      selectedVersion,
      isVersionPuplished: this.versionService.isVersionPuplished(selectedVersion)
    });
  }

  @Action(PublishVersion)
  publishVersion({ getState, patchState }: StateContext<VersionStateModel>) {
    const { itemEndpoint, selectedVersion } = this.versionService.cloneSelectedData(getState());
    const publishedVersion$ = this.versionService.publishVersion(itemEndpoint, selectedVersion.id);
    const translatedMessage$ = this.translationService.translate(OrchestratorB2BTranslation.PublishedVersionChangesTitle);

    this.store.dispatch(new SetSpinnerVisibility(true));

    return combineLatest([publishedVersion$, translatedMessage$]).subscribe(
      ([, translatedMessage]) => {
        this.store.dispatch(new SetSpinnerVisibility(false));
        this.notificationService.showSuccessMessage(translatedMessage);

        patchState({ isVersionPuplished: true });
      },
      () => this.store.dispatch(new SetSpinnerVisibility(false)),
    );
  }

  @Action(ChangeVersionState)
  changeVersionState({ getState, patchState }: StateContext<VersionStateModel>, { isDisabled }: ChangeVersionState) {
    const { itemEndpoint, selectedItem, selectedVersion } = this.versionService.cloneSelectedData(getState());
    const changedVersionState$ = this.versionService.changeVersionStatus(itemEndpoint, selectedVersion.id, isDisabled);
    const translatedMessage$ = this.translationService.translate(OrchestratorB2BTranslation.UpdateInfoContent);

    this.store.dispatch(new SetSpinnerVisibility(true));

    return combineLatest([changedVersionState$, translatedMessage$]).subscribe(
      ([, translatedMessage]) => {
        const version = {
          ...selectedVersion,
          isDisabled
        };
        const item = {
          ...selectedItem,
          versions: this.versionService.replaceVersion(version.id, selectedItem.versions, version)
        }

        this.store.dispatch(new SetSpinnerVisibility(false));
        this.notificationService.showSuccessMessage(translatedMessage);

        patchState({ selectedItem: item, selectedVersion: version })
      },
      () => this.store.dispatch(new SetSpinnerVisibility(false))
    )
  }

  @Action(FetchVersionTableData)
  fetchVersionTableData({ getState, patchState }: StateContext<VersionStateModel>) {
    const { tableDataEndpoint } = getState();

    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.versionService.loadVersionTableData(tableDataEndpoint).subscribe(
      tableData => {
        this.store.dispatch(new SetSpinnerVisibility(false));
        patchState({ tableData });
      },
      () => this.store.dispatch(new SetSpinnerVisibility(false))
    );
  }

  @Action(ResetVersionState)
  resetVersionState({ patchState }: StateContext<VersionStateModel>) {
    patchState({ ...INTIAIL_STATE });
  }
}
