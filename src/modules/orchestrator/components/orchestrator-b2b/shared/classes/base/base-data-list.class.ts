import { OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { HeaderActions, SetHeaderTitle, SetNavigationButtonStatesTitle } from 'src/modules/layout/ngxs/layout.actions';
import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { SetFilters } from 'src/modules/shared/components/filters/ngxs/filters.actions';
import { FiltersService } from 'src/modules/shared/components/filters/services/filters.service';
import { Column } from 'src/modules/shared/models/data-table.model';

export abstract class BaseDataList<Data, DataListAction> extends Unsubscribe implements OnInit, OnDestroy {
  protected dataList$: Observable<Data[]>;
  protected filtersData: any[];
  protected applyCommonFilters = true;

  public displayedColumns: Column[];
  public filtersEndpoint: string;

  constructor(
    private dataListAction: DataListAction,
    private buttonStatesTitle: string,
    protected store: Store,
    protected isFilters: boolean,
    protected filtersService?: FiltersService
  ) {
    super();
  }

  protected abstract initValues(): void;

  ngOnInit(): void {
    this.initValues();
    this.dispatchActions();

    this.isFilters && this.initSubscriptions();
  }

  ngOnDestroy(): void {
    this.store.dispatch(new SetNavigationButtonStatesTitle(false));
  }

  private setFilters(data: Data[]): void {
    this.store.dispatch(new SetFilters(this.filtersService.prepareFilters<Data>(this.filtersData, data)));
  }

  private getInitialActions(): (HeaderActions | DataListAction)[] {
    return [
      new SetHeaderTitle(ORCHESTRATOR.orchestratorB2B.headerTitles.orchestratorB2B),
      new SetNavigationButtonStatesTitle(true, this.buttonStatesTitle),
      this.dataListAction
    ];
  }

  private dispatchActions(): void {
    this.store.dispatch(this.getInitialActions());
  }

  private initSubscriptions(): void {
    if (this.applyCommonFilters) {
      this.subscribeTo = this.dataList$.subscribe(this.setFilters.bind(this));
    }
  }
}
