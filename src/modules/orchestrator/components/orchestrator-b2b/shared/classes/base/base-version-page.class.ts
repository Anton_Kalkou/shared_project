import { Input, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatSelectChange } from '@angular/material/select';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { getEntityRouteId } from 'src/modules/shared/helpers';
import { Column } from 'src/modules/shared/models/data-table.model';
import { VersionDialogOptions, Version } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';
import { NotificationService } from 'src/modules/shared/services/notification.service';
import { TranslationService } from 'src/modules/shared/services/translation.service';

import { DialogSize, InfoType } from '../../../enum/orchestrator-b2b.enum';
import { UpdateVersion } from '../../ngxs/version/version.actions';
import { BaseAutoSavingPage } from './base-auto-saving-page.class';

export abstract class BaseVersionPage<SelectedItem, TableData> extends BaseAutoSavingPage<Version> implements OnInit {

  @Input() infoType: string;

  private versionDialogOptions: VersionDialogOptions;
  private isTableDataLoaded: boolean;

  protected versionId: string;

  public selectedItem$: Observable<SelectedItem>;
  public selectedVersion$: Observable<Version>;
  public tableData$: Observable<TableData[]>;
  public isVersionExist: boolean;

  public currentId: string;
  public columns: Column[];

  public abstract onVersionChange(version: MatSelectChange): void;
  public abstract onVersion(isDuplicate?: boolean): void;
  public abstract onVersionState(): void;

  protected abstract getColumns(): Column[];
  protected abstract dispatchActions(): void;

  protected constructor(
    protected readonly route: ActivatedRoute,
    protected readonly store: Store,
    protected readonly formBuilder: FormBuilder,
    protected readonly notificationService: NotificationService,
    protected readonly translationService: TranslationService,
    protected readonly ItemState: any
  ) {
    super(store, formBuilder, notificationService, translationService);
  }

  ngOnInit(): void {
    this.initValues();
    this.initSubsriptions();
  }

  public onPublish(): void {};

  protected onSelectTableData(data?: TableData[]): void {};
  protected onDeleteTableData(data: TableData): void {};

  protected getVersionDialogOptions(isDuplicate: boolean): VersionDialogOptions {
    return {
      ...this.versionDialogOptions,
      data: {
        ...this.versionDialogOptions.data,
        isDuplicate
      }
    };
  }

  protected setSelectedDataFormControl(controlName: string, selectedData: TableData[]): void {
    this.formGroup.setControl(controlName, this.formBuilder.array(selectedData));
  }

  protected onEdit(): void {
    const { isDisabled: initialIsDisabled, ...initialVersion } = this.initialFormValues;
    const { isDisabled: updatedIsDisabled, ...updatedVersion } = this.formGroup.value;

    this.store.dispatch(new UpdateVersion({ ...initialVersion, ...updatedVersion }));
  }

  protected onUndo(): void {
    this.initFormGroup(this.initialFormValues);
    this.formChangesHandler();
  }

  private initValues(): void {
    this.versionDialogOptions = {
      width: DialogSize.Version,
      data: { selectedItem$: this.selectedItem$ }
    };
    this.columns = this.getColumns();
    this.currentId = this.infoType === InfoType.Edit
      ? getEntityRouteId(this.route)
      : this.store.selectSnapshot(this.ItemState.getItemId);

    this.initFormGroup();
  }

  private initSubsriptions(): void {
    this.subscribeTo = this.selectedVersion$.subscribe(selectedVersion => {
      this.isVersionExist = !!selectedVersion;

      if (this.isVersionExist) {
        this.versionId = selectedVersion.id;
        this.initFormGroup(selectedVersion);
        this.setInitialFormValues(this.formGroup);
        this.formChangesHandler();

        if (!this.isTableDataLoaded) {
          this.isTableDataLoaded = true;
          this.dispatchActions();
        }
      } else {
        this.onVersion();
      }
    });
  }
}
