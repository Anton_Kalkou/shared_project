import { FormBuilder, FormGroup } from '@angular/forms';
import { Input, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';

import { Observable } from 'rxjs';

import { NotificationService } from '../../../../../../shared/services/notification.service';
import { TranslationService } from '../../../../../../shared/services/translation.service';
import { InfoType } from '../../../enum/orchestrator-b2b.enum';
import { SetNavigationButtonStatesTitle } from '../../../../../../layout/ngxs/layout.actions';
import { BaseAutoSavingPage } from './base-auto-saving-page.class';

export abstract class BaseInfoPage<T extends { name: string; }> extends BaseAutoSavingPage<T> implements OnInit {
  @Input() public infoType: string;

  protected abstract selectedItem$: Observable<T>;
  protected abstract isCreated$: Observable<boolean>;
  protected abstract selectedIdSelector: (state: any, ...states: any[]) => string;
  protected abstract selectedNameSelector: (state: any, ...states: any[]) => string;

  public formGroup: FormGroup;
  public isEdit: boolean;

  protected currentId: string;
  protected initialFormValues: T;

  public abstract onCreate(): void;
  public abstract onDelete(): void;

  protected abstract onEdit(): void;
  protected abstract initEditData(): void;
  protected abstract initFormGroup(initialValue?: T): void;

  protected constructor(
    store: Store,
    formBuilder: FormBuilder,
    notificationService: NotificationService,
    translationService: TranslationService
  ) {
    super(
      store,
      formBuilder,
      notificationService,
      translationService
    );
  }

  ngOnInit(): void {
    this.isEdit = this.infoType === InfoType.Edit;

    this.initFormGroup();

    if (this.isEdit) {
      this.initEditData();
    } else {
      this.initCreateData();
    }
  }

  protected initCreateData(): void {
    this.initCreateSubscriptions();
  }

  protected initCreateSubscriptions(): void {
    this.subscribeTo = this.isCreated$.subscribe(isCreated$ => {
      if (isCreated$) {
        const name = this.store.selectSnapshot(this.selectedNameSelector);

        this.currentId = this.store.selectSnapshot(this.selectedIdSelector);
        this.store.dispatch(new SetNavigationButtonStatesTitle(true, `Edit ${name}`));

        this.initSubscriptions();
      }
    });
  }

  protected initSubscriptions(): void {
    this.formChangesHandler();
    this.subscribeTo = this.selectedItem$.subscribe((selectedItem: T) => {
      if (!selectedItem) {
        return;
      }

      if (this.isEdit) {
        this.store.dispatch(new SetNavigationButtonStatesTitle(true, `Edit ${selectedItem.name}`));
      }

      this.updateFormGroup(selectedItem);
    });
  }
}
