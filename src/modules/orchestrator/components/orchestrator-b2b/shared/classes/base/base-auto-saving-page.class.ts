import { FormBuilder, FormGroup } from '@angular/forms';
import { OnDestroy, TemplateRef, ViewChild } from '@angular/core';
import { Store } from '@ngxs/store';

import { NotificationType } from 'angular2-notifications';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { isEqual } from 'lodash';

import { Unsubscribe } from '../../../../../../shared/classes/unsubscribe.class';
import { NotificationService } from '../../../../../../shared/services/notification.service';
import { TranslationService } from '../../../../../../shared/services/translation.service';
import { Translation, ActionNotificationButtonClass } from '../../../../../../shared/enums';
import { AUTO_SAVE_TIMOUT_MS } from '../../../constants/orchestrator-b2b.constants';

export abstract class BaseAutoSavingPage<T> extends Unsubscribe implements OnDestroy {
  @ViewChild('actionMessage') actionMessage: TemplateRef<any>;

  public formGroup: FormGroup;

  protected initialFormValues: T;

  private updateInfoContentMessage = Translation.UpdateInfoContent;

  protected abstract onEdit(): void;
  protected abstract initFormGroup(initialValue?: T): void;

  protected constructor(
    protected readonly store: Store,
    protected readonly formBuilder: FormBuilder,
    protected readonly notificationService: NotificationService,
    protected readonly translationService: TranslationService
  ) {
    super();
  }

  protected setInitialFormValues(formGroup: FormGroup): void {
    this.initialFormValues = formGroup.getRawValue();
  }

  protected updateFormGroup(initialFormValues?: T): void {
    if (!this.formGroup) {
      this.initFormGroup(initialFormValues);
    } else {
      this.formGroup.patchValue(initialFormValues);
    }

    this.setInitialFormValues(this.formGroup);
  }

  protected formChangesHandler(): void {
    this.subscribeTo = this.formGroup.valueChanges
      .pipe(
        tap(() => this.notificationService.removeNotification()),
        debounceTime(AUTO_SAVE_TIMOUT_MS),
        distinctUntilChanged((prev, curr) => isEqual(prev, curr))
      )
      .subscribe(() => {
        if (this.formGroup.valid && !isEqual(this.initialFormValues, this.formGroup.getRawValue())) {
          this.startSavingProcess();
        }
      });
  }

  protected onUndo(): void {
    this.formGroup.patchValue(this.initialFormValues, { emitEvent: false });
  }

  private startSavingProcess(): void {
    this.translationService.translate(this.updateInfoContentMessage).subscribe(content => {
      const options = { id: `${ActionNotificationButtonClass.Undo}`, clickToClose: false };
      const context = { title: content };

      this.notificationService.showActionMessage(
        this.actionMessage,
        NotificationType.Success,
        ActionNotificationButtonClass.Undo,
        this.onUndo.bind(this),
        this.onEdit.bind(this),
        options,
        null,
        context
      );
    });
  }
}
