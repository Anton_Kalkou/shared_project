import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { combineLatest, Observable } from 'rxjs';
import { first, map } from 'rxjs/operators';
import { Version } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';

import { VersionExistsConstraint } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/validators/validators.model';

const isVersionExist = (versions: Version[], versionName: string): boolean => {
  return versions && !!versions.find(version => version.name === versionName);
};

const isVersionChanged = (selectedVersionName: string, formVersionName: string): boolean => {
  return selectedVersionName !== formVersionName;
};

export const versionExistsValidator = (
  <T extends VersionExistsConstraint>(selectedItem$: Observable<T>, selectedVersion$?: Observable<Version>): AsyncValidatorFn => {
    return ({ value }: AbstractControl): Observable<ValidationErrors | null> => {
      return combineLatest([selectedItem$, selectedVersion$]).pipe(
        map(([{ versions }, selectedVersion]) => {
          return isVersionChanged(selectedVersion.name, value)
            ? isVersionExist(versions, value) ? { exists: { value } } : null
            : null;
        }),
        first(),
      );
    };
  }
);
