import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Select } from '@ngxs/store';
import { BehaviorSubject, Observable } from 'rxjs';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { Version, VersionDialogData } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';

import { versionExistsValidator } from '../../validators/versionExists.validator';

@Component({
  selector: 'app-create-version-dialog',
  templateUrl: './create-version-dialog.component.html',
  styleUrls: ['./create-version-dialog.component.scss']
})
export class CreateVersionDialogComponent extends Unsubscribe implements OnInit {
  @Select(state => state.orchestratorB2B.version.selectedVersion) selectedVersion$: Observable<Version>;

  private isVersionChanged$: BehaviorSubject<boolean>;

  public formGroup: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: VersionDialogData,
    private formBuilder: FormBuilder,
    private dialog: MatDialogRef<CreateVersionDialogComponent>
  ) {
    super();
  }

  ngOnInit(): void {
    this.initValues();
  }

  public onSubmit(): void {
    this.formGroup.valid && this.dialog.close(this.formGroup.value);
  }

  private initValues(): void {
    this.isVersionChanged$ = new BehaviorSubject(false);
    this.formGroup = this.formBuilder.group({
      name: [
        '',
        {
          validators: [Validators.required],
          asyncValidators: [versionExistsValidator(this.data.selectedItem$, this.selectedVersion$)],
        }
      ],
      description: '',
    });
  }
}
