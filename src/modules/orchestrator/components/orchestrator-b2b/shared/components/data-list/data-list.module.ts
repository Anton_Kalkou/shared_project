import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SectionHeaderModule } from 'src/modules/orchestrator/shared/components/section-header/section-header.module';
import { AddButtonModule } from 'src/modules/shared/components/add-button/add-button.module';
import { DataTableModule } from 'src/modules/shared/components/data-table/data-table.module';
import { IconButtonModule } from 'src/modules/shared/components/icon-button/icon-button.module';
import { SearchModule } from 'src/modules/shared/components/search/search.module';
import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { DataListComponent } from './data-list.component';

@NgModule({
  imports: [
    RouterModule,
    SharedModule,
    SectionHeaderModule,
    IconButtonModule,
    SearchModule,
    AddButtonModule,
    DataTableModule
  ],
  declarations: [
    DataListComponent
  ],
  exports: [
    DataListComponent
  ]
})
export class DataListModule {}
