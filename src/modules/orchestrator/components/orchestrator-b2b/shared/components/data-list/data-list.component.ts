import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Select } from '@ngxs/store';

import { Observable } from 'rxjs';

import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { FiltersService } from 'src/modules/shared/services/filters.service';
import { FiltersService as FiltersSliderService } from 'src/modules/shared/components/filters/services/filters.service';

@Component({
  selector: 'app-data-list',
  templateUrl: './data-list.component.html',
  styleUrls: ['./data-list.component.scss']
})
export class DataListComponent extends Unsubscribe implements OnInit, OnDestroy {
  @Select(state => state.filters.filtersVisibility) filtersVisibility$: Observable<boolean>;

  @Output() select: EventEmitter<any[]> = new EventEmitter<any[]>();
  @Output() delete: EventEmitter<any> = new EventEmitter<any>();

  @Input() isTableDisabled: boolean;
  @Input() tableName: string;
  @Input() headerTitle: string;
  @Input() searchPlaceholder: string;
  @Input() isAddButton = true;
  @Input() isSearch = true;
  @Input() isFilters: boolean;
  @Input() isNotHeaderSearch: boolean;
  @Input() isPaginator: boolean;
  @Input() tableData: any[];
  @Input() selectedTableData: any[];
  @Input() tableColumns: any[];
  @Input() tableDataService: any;
  @Input() tableDataServiceFilteringMethod: string;
  @Input() tableDataFilteringColumnDataKey: string;
  @Input() filteredSource: any[];
  @Input() filtersEndpoint: string;

  public searchFilter: string;

  private filterType: string;
  private filteredFiltersSource: any[];
  private filteredSearchSource: any[];

  constructor(
    private readonly filtersService: FiltersService,
    private readonly filtersSliderService: FiltersSliderService,
  ) {
    super();
  }

  ngOnInit(): void {
    this.initSubscriptions();
  }

  public onSearch(key: string): void {
    if (this.isSearch || this.isNotHeaderSearch) {
      this.filtersService.handleFilteringDataOnSearch(
        this,
        this.tableData,
        this.tableDataService[this.tableDataServiceFilteringMethod].bind(this.tableDataService),
        key,
        this.tableDataFilteringColumnDataKey
      );
    }
  }

  public onSelect(selectedData: any[]): void {
    this.select.emit(selectedData);
  }

  public onDelete(item: any): void {
    this.delete.emit(item);
  }

  private initSubscriptions(): void {
    if (this.isFilters) {
      this.subscribeTo = this.filtersVisibility$.subscribe(filtersVisibility => {
        if (!filtersVisibility && this.tableData) {
          const filtersValues = this.filtersService.getFiltersValues(this.filtersSliderService.getActivatedFilters());

          this.filtersService.handleFilteringDataOnFilters(
            this,
            this.tableData,
            this.tableDataService[this.tableDataServiceFilteringMethod].bind(this.tableDataService),
            filtersValues
          );
        }
      });
    }
  }
}
