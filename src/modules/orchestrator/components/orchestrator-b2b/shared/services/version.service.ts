import { Injectable } from '@angular/core';
import { cloneDeep } from 'lodash';
import * as moment from 'moment';
import { Observable } from 'rxjs';

import { CreatedData, Version, VersionData } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/orchestrator-b2b.model';
import { RelatedDataConstraint } from 'src/modules/shared/models/orchestrator/orchestrator-b2b/related-data/related-data.model';
import { ApiService } from 'src/modules/shared/services/api.service';

import { SelectedItem, TableData } from '../ngxs/version/version.actions';
import { VersionStateModel } from '../ngxs/version/version.state';

@Injectable({ providedIn: 'root' })
export class VersionService {
  constructor(private readonly apiService: ApiService) {}

  public createVersion(endpoint: string, versionData: VersionData): Observable<CreatedData> {
    return this.apiService.post(`${endpoint}/version`, versionData);
  }

  public publishVersion(endpoint: string, versionId: string): Observable<void> {
    return this.apiService.put(`${endpoint}/version/${versionId}/publish`, null);
  }

  public changeVersionStatus(endpoint: string, versionId: string, isDisabled: boolean): Observable<void> {
    const status = isDisabled ? 'disable' : 'enable';

    return this.apiService.put(`${endpoint}/version/${versionId}/${status}`, null);
  }

  public loadVersionTableData(endpoint: string): Observable<any[]> {
    return this.apiService.get(endpoint);
  }

  public updateVersionTableData(endpoint: string, versionId: string, versionData: VersionData): Observable<void> {
    return this.apiService.put(`${endpoint}/version/${versionId}`, versionData);
  }

  public getRelatedData<T extends RelatedDataConstraint>(sourceDataId: string, sourceDataKey: string, responseData: T[]): T[] {
    return responseData.filter(({ versions }) =>
      versions.some(version => version[sourceDataKey].some(({ id }) => id === sourceDataId))
    );
  }

  public getSelectedVersion(versions: Version[]): Version {
    const selectedVersion = versions && versions.length && versions.reduce((latestVersion, currentVersion) => {
      const latestVersionValue = latestVersion.version.replace(/\D/g, '');
      const currentVersionValue = currentVersion.version.replace(/\D/g, '');

      return latestVersionValue > currentVersionValue ? currentVersion : latestVersion;
    });

    return selectedVersion || null;
  }

  public getSelectedVersionIndex<Item extends SelectedItem>({ versions: itemVersions }: Item, { id }: Version): number {
    return itemVersions.findIndex(({ id: itemVersionId }) => itemVersionId === id);
  }

  public getSelectedVersionByName(versions: Version[], versionName: string): Version {
    return versions.find(({ version: selectedVersionName }) => selectedVersionName === versionName);
  }

  public getTableDataIds<Data extends TableData>(tableData: Data[]): string[] {
    return tableData.map(({ id }) => id);
  }

  public replaceVersion(versionId: string, versions: Version[], version: Version): Version[] {
    return versions.map(versionItem => versionItem.id === versionId ? version : versionItem);
  }

  public isVersionPuplished(selectedVersion: Version): boolean {
    return selectedVersion.publishedAt
      ? moment(selectedVersion.updatedAt) > moment(selectedVersion.publishedAt)
      : true;
  }

  public cloneSelectedData(versionModel: VersionStateModel): VersionStateModel {
    return cloneDeep(versionModel);
  }
}
