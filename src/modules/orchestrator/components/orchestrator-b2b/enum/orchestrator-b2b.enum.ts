export enum InfoType {
  Add = 'add',
  Edit = 'edit'
}

export enum Translation {
  PublishedVersionChangesTitle = 'shared.published-changes-title',
  AddNavigationButtonStatesTitle = 'shared.Add',
  UpdateInfoContent = 'shared.update-info-content'
}

export enum PermissionType {
  YES_NO = 'YES/NO',
  READ_WRITE = 'READ/WRITE'
}

export enum PermissionTitleType {
  YES_NO = 'Yes/No',
  READ_WRITE = 'Read/Write'
}

export enum DialogSize {
  Version = '1000px',
  AddTableData = '1200px'
}

export enum DataLabel {
  IsDisabled = 'isDisabled',
  Features = 'features',
  Permissions = 'permissions',
  Apis = 'apis'
}
