import { Injectable } from '@angular/core';
import { State } from '@ngxs/store';

import { ProductsState } from '../components/products/ngxs/products/products.state';
import { FeaturesState } from '../components/features/ngxs/features/features.state';
import { PermissionsState } from '../components/permissions/ngxs/permissions/permissions.state';
import { TenantsCategoryState } from '../components/tenants-categories/ngxs/tenants-categories/tenants-categories.state';
import { TenantsState } from '../components/tenants/ngxs/tenants/tenants.state';
import { TenantState } from '../components/tenants/ngxs/tenant/tenant.state';
import { DefaultRolesState } from '../components/default-roles/ngxs/default-roles/default-roles.state';
import { VersionState } from '../shared/ngxs/version/version.state';

interface OrchestratorB2BStateModel {}

@State<OrchestratorB2BStateModel>({
  name: 'orchestratorB2B',
  children: [
    VersionState,
    ProductsState,
    FeaturesState,
    PermissionsState,
    TenantsCategoryState,
    TenantsState,
    TenantState,
    DefaultRolesState
  ]
})
@Injectable()
export class OrchestratorB2BState {}
