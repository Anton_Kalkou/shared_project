import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NgxsModule } from '@ngxs/store';

import { FeaturesModule } from '../components/features/modules/features.module';
import { FeatureState } from '../components/features/ngxs/feature/feature.state';
import { FeaturesState } from '../components/features/ngxs/features/features.state';
import { PermissionsModule } from '../components/permissions/modules/permissions.module';
import { PermissionState } from '../components/permissions/ngxs/permission/permission.state';
import { PermissionsState } from '../components/permissions/ngxs/permissions/permissions.state';
import { ProductsModule } from '../components/products/modules/products.module';
import { ProductState } from '../components/products/ngxs/product/product.state';
import { ProductsState } from '../components/products/ngxs/products/products.state';
import { TenantsCategoryModule } from '../components/tenants-categories/modules/tenants-categories.module';
import { TenantCategoryState } from '../components/tenants-categories/ngxs/tenant-category/tenant-category.state';
import { TenantsCategoryState } from '../components/tenants-categories/ngxs/tenants-categories/tenants-categories.state';
import { OrchestratorB2BState } from '../ngxs/orchestrator-b2b.state';
import { TenantsState } from '../components/tenants/ngxs/tenants/tenants.state';
import { TenantsModule } from '../components/tenants/modules/tenants.module';
import { TenantState } from '../components/tenants/ngxs/tenant/tenant.state';
import { DefaultRolesState } from '../components/default-roles/ngxs/default-roles/default-roles.state';
import { DefaultRolesModule } from '../components/default-roles/modules/default-roles.module';
import { DefaultRoleState } from '../components/default-roles/ngxs/default-role/default-role.state';
import { VersionState } from '../shared/ngxs/version/version.state';

@NgModule({
  imports: [
    RouterModule,
    TranslateModule,
    TenantsCategoryModule,
    TenantsModule,
    ProductsModule,
    FeaturesModule,
    PermissionsModule,
    DefaultRolesModule,
    NgxsModule.forFeature([
      OrchestratorB2BState,
      VersionState,
      ProductsState,
      ProductState,
      FeaturesState,
      FeatureState,
      PermissionsState,
      PermissionState,
      TenantsCategoryState,
      TenantCategoryState,
      TenantsState,
      TenantState,
      DefaultRolesState,
      DefaultRoleState
    ])
  ],
  declarations: [],
  exports: []
})
export class OrchestratorB2BModule {}
