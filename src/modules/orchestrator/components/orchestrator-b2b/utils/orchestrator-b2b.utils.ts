import { isSubstring } from 'src/modules/shared/helpers';
import { InfoType } from '../enum/orchestrator-b2b.enum';

export const getInfoType = (url: string): InfoType => {
  return isSubstring(url, InfoType.Add) ? InfoType.Add : InfoType.Edit;
};

export const getValuesWithoutCode = <T extends { code?: string; }>(value: T): Omit<T, 'code'> => {
  const { code, ...rest } = value;

  return rest;
};
