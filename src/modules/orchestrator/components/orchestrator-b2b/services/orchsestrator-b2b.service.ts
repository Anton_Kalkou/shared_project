import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NavigationButtonStates } from 'src/modules/layout/enum/layout.enum';
import { ORCHESTRATOR } from 'src/modules/orchestrator/constants/constants';
import { APP } from 'src/modules/shared/constants';
import { isSubstring } from 'src/modules/shared/helpers';

@Injectable({ providedIn: 'root' })
export class OrchestratorB2BService {
  private homeRoute: string;

  constructor(private router: Router) {
    this.homeRoute = `/${APP.pages.home}`;
  }

  public getNavigationButtonState(): NavigationButtonStates {
    switch (true) {
      case this.router.url === `/${ORCHESTRATOR.orchestratorB2B.orchestratorB2BPath}`:
      case this.router.url === `/${ORCHESTRATOR.orchestratorB2B.products.productsPath}`:
      case this.router.url === `/${ORCHESTRATOR.orchestratorB2B.features.featuresPath}`:
      case this.router.url === `/${ORCHESTRATOR.orchestratorB2B.permissions.permissionsPath}`:
      case this.router.url === `/${ORCHESTRATOR.orchestratorB2B.tenantsCategories.tenantsCategoriesPath}`:
      case this.router.url === `/${ORCHESTRATOR.orchestratorB2B.tenants.tenantsPath}`:
      case this.router.url === `/${ORCHESTRATOR.orchestratorB2B.defaultRoles.defaultRolesPath}`:
        return NavigationButtonStates.sidenav;
      case (isSubstring(this.router.url, ORCHESTRATOR.orchestratorB2B.editPath)) :
      case (isSubstring(this.router.url, ORCHESTRATOR.orchestratorB2B.addPath)) :
      case (isSubstring(this.router.url, ORCHESTRATOR.orchestratorB2B.tenantsCategories.comparePath)) :
        return NavigationButtonStates.close;
    }
  }

  public getHomeRoute(): string {
    this.homeRoute = `/${ORCHESTRATOR.orchestratorB2B.orchestratorB2BPath}`;

    return this.homeRoute;
  }
}
