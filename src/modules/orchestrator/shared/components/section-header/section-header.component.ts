import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Store } from '@ngxs/store';
import { OpenFilters } from 'src/modules/shared/components/filters/ngxs/filters.actions';

@Component({
  selector: 'app-section-header',
  templateUrl: './section-header.component.html',
  styleUrls: ['./section-header.component.scss']
})
export class SectionHeaderComponent {

  @Input() isFilters: boolean;
  @Input() isSearch: boolean;
  @Input() title: string;
  @Input() searchPlaceholder: string;
  @Input() filtersEndpoint: string;

  @Output() showFilters: EventEmitter<void> = new EventEmitter<void>();
  @Output() search: EventEmitter<string> = new EventEmitter<string>();

  constructor(private store: Store) {}

  public openFilters(): void {
    this.store.dispatch(new OpenFilters(this.filtersEndpoint));
    this.showFilters.emit();
  }

  public onSearch(key: string): void {
    this.search.emit(key);
  }
}
