import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SearchModule } from 'src/modules/shared/components/search/search.module';
import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { SectionHeaderComponent } from './section-header.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SearchModule
  ],
  declarations: [
    SectionHeaderComponent
  ],
  exports: [
    SectionHeaderComponent
  ]
})
export class SectionHeaderModule {}
