import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Store } from '@ngxs/store';

import { Login } from '../ngxs/authentication.actions';

@Component({
  selector: 'app-authentication',
  templateUrl: 'authentication.component.html',
  styleUrls: ['authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {

  public authenticationForm: FormGroup;
  public passwordVisibility: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private store: Store
  ) {}

  ngOnInit() {
    this.initForm();
  }

  public reactOnChangePasswordVisibility(visibility: boolean): void {
    this.passwordVisibility = visibility;
  }

  public login(): void {
    if (this.authenticationForm.valid) {
      this.store.dispatch(new Login(this.authenticationForm.value.email, this.authenticationForm.value.password));
    }
  }

  private initForm(): void {
    this.authenticationForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

}
