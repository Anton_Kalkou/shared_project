import { NgModule } from '@angular/core';

import { NgxsModule } from '@ngxs/store';

import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { AuthenticationComponent } from '../component/authentication.component';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { AuthenticationState } from '../ngxs/authentication.state';
import { DirectivesModule } from 'src/modules/shared/directives/directives.module';

@NgModule({
  imports: [
    SharedModule,
    MaterialModule,
    NgxsModule.forFeature([AuthenticationState]),
    DirectivesModule
  ],
  declarations: [
    AuthenticationComponent
  ],
  exports: [
    AuthenticationComponent
  ]
})
export class AuthenticationModule {}
