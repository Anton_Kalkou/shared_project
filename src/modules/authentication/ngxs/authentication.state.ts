import { Injectable } from '@angular/core';

import { State, Action, StateContext, Store } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { Navigate } from '@ngxs/router-plugin';

import { AuthenticationApiService } from '../services/authentication.service';
import { Login } from './authentication.actions';
import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { SharedDataService } from 'src/modules/shared/services/shared-data.service';
import { APP } from '../../shared/constants';

@State<any>({
  name: 'authentication',
  defaults: {}
})
@Injectable()
export class AuthenticationState {

  constructor(
    private authenticationApiService: AuthenticationApiService,
    private store: Store,
    private sharedDataService: SharedDataService,
  ) {}

  @Action(Login)
  login({patchState}: StateContext<{}>, {email, password}: Login) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.authenticationApiService.login(email, password)
      .pipe(
        tap(authenticationData => {
          this.sharedDataService.authenticationData = authenticationData;
          this.store.dispatch(new Navigate([APP.pages.home]));
        })
      );
  }

}
