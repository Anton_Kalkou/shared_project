import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { ApiService } from 'src/modules/shared/services/api.service';
import { APP } from 'src/modules/shared/constants';
import { Authentication } from 'src/modules/shared/models/authentication.model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationApiService {

  constructor(
    private apiService: ApiService
  ) {}

  public login(username: string, password: string): Observable<Authentication> {
    return this.apiService.post(APP.endpoints.login, {username, password});
  }

}
