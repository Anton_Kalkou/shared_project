import { Component, OnInit } from '@angular/core';

import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { User } from 'src/modules/shared/models/user.model';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { GetUserProfileData } from '../../ngxs/layout.actions';
import { ResolutionService } from 'src/modules/shared/services/resolution.service';

@Component({
  selector: 'app-layout',
  templateUrl: 'layout.component.html',
  styleUrls: ['layout.component.scss']
})
export class LayoutComponent extends Unsubscribe implements OnInit {

  @Select(state => state.layout.userProfileData) userProfileData$: Observable<User>;

  public layoutVisibility: boolean;

  constructor(
    private store: Store,
    private resolutionService: ResolutionService
  ) {
    super();
  }

  ngOnInit() {
    this.dispatchActions();
    this.initSubscriptions();
  }

  public sideNavVisibility(): boolean {
    return this.resolutionService.tabletAndLess();
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.userProfileData$
    .subscribe(userProfileData => {
      if (userProfileData) {
        this.layoutVisibility = true;
      }
    });
  }

  private dispatchActions(): void {
    this.store.dispatch(new GetUserProfileData());
  }

}
