import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';

import { APP } from 'src/modules/shared/constants';
import { User } from 'src/modules/shared/models/user.model';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { ResolutionService } from 'src/modules/shared/services/resolution.service';

@Component({
  selector: 'app-menu',
  templateUrl: 'menu.component.html',
  styleUrls: ['menu.component.scss']
})
export class MenuComponent extends Unsubscribe implements OnInit {

  @Select(state => state.layout.userProfileData) userProfileData$: Observable<User>;

  public menuItems = APP.homeNavItems;
  public userProfileData: User;
  
  constructor(
    private resolutionService: ResolutionService
  ) {
    super();
  }

  ngOnInit() {
    this.initSubscriptions();
  }

  public catchSubMenuClick(event: MouseEvent): void {
    event.stopPropagation();
  }

  public subMenuVisibility(): boolean {
    return this.resolutionService.tabletAndLess();
  }

  private initSubscriptions(): void {
    this.subscribeTo = this.userProfileData$
      .subscribe(user => this.userProfileData = user);
  }

}
