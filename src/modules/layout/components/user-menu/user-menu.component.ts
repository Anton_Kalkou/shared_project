import { Component } from '@angular/core';

import { Store } from '@ngxs/store';

import { Logout } from '../../ngxs/layout.actions';

@Component({
  selector: 'app-user-menu',
  templateUrl: 'user-menu.component.html',
  styleUrls: ['user-menu.component.scss']
})
export class UserMenuComponent {

  public userMenuVisibility: boolean;

  constructor(
    private store: Store
  ) {}

  public logout(): void {
    this.store.dispatch(new Logout());
  }

}
