import { Component, OnInit } from '@angular/core';
import { Router, Event, NavigationEnd } from '@angular/router';
import { Select, Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { APP } from 'src/modules/shared/constants';
import { Menu } from 'src/modules/shared/models/menu.model';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { fromLeftToRight } from 'src/modules/shared/animations';
import { ToggleSidenav } from '../../ngxs/layout.actions';

@Component({
  selector: 'app-side-nav',
  templateUrl: 'side-nav.component.html',
  styleUrls: ['side-nav.component.scss'],
  animations: [fromLeftToRight]
})
export class SideNavComponent extends Unsubscribe implements OnInit {

  @Select(state => state.layout.isSliderSidenav) isSliderSidenav$: Observable<boolean>;

  public menuItem: Menu;
  public sideNavVisibility: boolean;
  public isSliderSidenav: boolean;

  constructor(private router: Router, private store: Store) {
    super();
  }

  ngOnInit() {
    this.getMenuItem();
    this.checkRoute();
    this.subscribeToRouteChanges();
  }

  public hideSidenav(): void {
    this.store.dispatch(new ToggleSidenav());
  }

  private subscribeToRouteChanges(): void {
    this.subscribeTo = this.router.events
      .subscribe((routerEvent: Event) => {
        if (routerEvent instanceof NavigationEnd) {
          this.getMenuItem();
          this.checkRoute();
        }
      });
    this.subscribeTo = this.isSliderSidenav$.subscribe(isSliderSidenav => {
      this.isSliderSidenav = isSliderSidenav;
    });
  }

  private checkRoute(): void {
    this.sideNavVisibility = !!APP.routesWithSidebar.find(route => this.router.url === route);
  }

  private getMenuItem(): void {
    this.menuItem = APP.menuItems.find(menuItem => this.router.url.includes(menuItem.link));
  }

}
