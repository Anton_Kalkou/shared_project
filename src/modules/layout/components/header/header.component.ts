import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, Event } from '@angular/router';

import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs';

import { SharedDataService } from 'src/modules/shared/services/shared-data.service';
import { scale } from 'src/modules/shared/animations';
import { User } from 'src/modules/shared/models/user.model';
import { Unsubscribe } from 'src/modules/shared/classes/unsubscribe.class';
import { APP } from 'src/modules/shared/constants';
import { LocalStorageService } from 'src/modules/shared/services/local-storage.service';
import { ResolutionService } from 'src/modules/shared/services/resolution.service';
import { CustomerSupportService } from 'src/modules/support/services/customer-support.service';
import { isSubstring } from 'src/modules/shared/helpers';
import { OrchestratorB2BService } from 'src/modules/orchestrator/components/orchestrator-b2b/services/orchsestrator-b2b.service';
import { HeaderType, NavigationButtonStates } from '../../enum/layout.enum';
import { OpenMenu, ToggleSidenav } from '../../ngxs/layout.actions';

@Component({
  selector: 'app-header',
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.scss'],
  animations: [
    scale
  ]
})
export class HeaderComponent extends Unsubscribe implements OnInit {

  @Select(state => state.layout.userProfileData) userProfileData$: Observable<User>;
  @Select(state => state.layout.headerTitle) headerTitle$: Observable<string>;
  @Select(state => state.layout.isNavigationButtonStatesTitle) isNavigationButtonStatesTitle$: Observable<boolean>;
  @Select(state => state.layout.navigationButtonStatesTitle) navigationButtonStatesTitle$: Observable<string>;

  public userMenuVisibility: boolean;
  public userProfileData: User;
  public prevRoute: string;
  public homeRoute: string;

  public isNavigationButtonStateTitle: boolean;
  public navigationButtonStateTitle: string;

  public headerType: string;
  public headerTitle: string;

  private currentRoute: string;
  private navigationButtonStates: NavigationButtonStates;

  constructor(
    public sharedDataService: SharedDataService,
    private store: Store,
    private router: Router,
    private localStorageService: LocalStorageService,
    private resolutionService: ResolutionService,
    private customerSupportService: CustomerSupportService,
    private orchestratorB2BService: OrchestratorB2BService
  ) {
    super();
  }

  ngOnInit(): void {
    this.subscribeToUserProfileData();
    this.getCurrentRoutes();
    this.checkUrl(this.router.url);
  }

  public mobileMenuVisibility(): boolean {
    return this.resolutionService.tabletAndLess();
  }

  public isMenu(): boolean {
    return this.navigationButtonStates === NavigationButtonStates.menu;
  }

  public isSidenav(): boolean {
    return this.navigationButtonStates === NavigationButtonStates.sidenav;
  }

  public isBack(): boolean {
    return this.navigationButtonStates === NavigationButtonStates.back;
  }

  public isClose(): boolean {
    return this.navigationButtonStates === NavigationButtonStates.close;
  }

  public hideUserMenu(): void {
    this.userMenuVisibility = false;
  }

  public openUserMenu(): void {
    this.userMenuVisibility = true;
  }

  public openMenu(): void {
    if (this.navigationButtonStates === NavigationButtonStates.menu) {
      this.store.dispatch(new OpenMenu());
    } else {
      this.store.dispatch(new ToggleSidenav());
    }
  }

  private getCurrentRoutes(): void {
    this.prevRoute = this.localStorageService.getCachedData(APP.cache.prevRoute);
    this.currentRoute = this.router.url;
  }

  private subscribeToUserProfileData(): void {
    this.subscribeTo = this.userProfileData$
      .subscribe(userProfileData => this.userProfileData = userProfileData);
    this.subscribeTo = this.router.events
      .subscribe((routerEvent: Event) => {
        if (routerEvent instanceof NavigationEnd) {
          this.processUrl(routerEvent.urlAfterRedirects);
        }
      });
    this.subscribeTo = this.headerTitle$
      .subscribe(headerTitle => this.headerTitle = headerTitle)
  }

  private processUrl(url: string): void {
    this.checkUrl(url);
    this.saveUrl(url);
  }

  private checkUrl(url: string): void {
    this.headerType = null;

    if (isSubstring(url, APP.pages.customerSupport, true)) {
      this.navigationButtonStates = this.customerSupportService.getNavigationButtonState();
      this.headerType = HeaderType.CustomerSupport;
      this.homeRoute = this.customerSupportService.getHomeRoute();
    } else if (isSubstring(url, APP.pages.orchestratorB2B, true)) {
      this.homeRoute = this.orchestratorB2BService.getHomeRoute();
      this.navigationButtonStates = this.orchestratorB2BService.getNavigationButtonState();
    } else {
      this.homeRoute = `/${APP.pages.home}`;

      if (`/${APP.pages.home}` === url) {
        this.navigationButtonStates = NavigationButtonStates.menu;
      } else if (APP.routesBackToHome.includes(url)) {
        this.navigationButtonStates = NavigationButtonStates.back;
      } else {
        this.navigationButtonStates = NavigationButtonStates.close;
      }
    }
  }

  private saveUrl(url: string): void {
    this.prevRoute = this.currentRoute;
    this.currentRoute = url;
    this.localStorageService.cacheData(APP.cache.prevRoute, this.prevRoute);
  }
}
