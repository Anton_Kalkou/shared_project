import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LayoutComponent } from '../components/layout/layout.component';
import { DashboardComponent } from 'src/modules/dashboard/components/dashboard/dashboard.component';
import { StaffComponent } from 'src/modules/staff/components/staff/staff.component';
import { HomeComponent } from 'src/modules/home/components/home/home.component';
import { AuthenticationGuard } from 'src/modules/shared/guards/auth.guard';
import { SalesComponent } from 'src/modules/sales/component/sales.component';
import { EditorsComponent } from 'src/modules/editors/components/editors/editors.component';
import { SupportComponent } from 'src/modules/support/components/support/support.component';
import { StaffListComponent } from 'src/modules/staff/components/staff-list/staff-list.component';
import { ItemsComponent } from 'src/modules/editors/components/items/items.component';
import { EditorsHomeComponent } from 'src/modules/editors/components/home/editors-home.component';
import { MerchantsListComponent } from 'src/modules/merchants/merchants-list/components/merchant-list/merchants-list.component';

// TODO: Temporary Global Items, Remove later
import { TempItemsComponent } from '../../editors/components/temp-global-items/items/items.component';
import { TempApproveItemsComponent } from '../../editors/components/temp-global-items/approve-items/approve-items.component';

import {OntologyEditorsFormComponent} from '../../editors/components/ontology/form/edit-ontology-form.component';
import { PaymentComponent } from 'src/modules/support/components/payment/components/payment/payment.component';
import { SystemsHealthComponent } from 'src/modules/support/components/systems-health/components/systems-health/systems-health.component';
import { AccessRightsComponent } from 'src/modules/support/components/access-rights/components/access-rights/access-rights.component';
import { SupportRequestsComponent } from 'src/modules/support/components/support-requests/components/support-requests/support-requests.component';
import { SupportRequestComponent } from 'src/modules/support/components/support-request/components/support-request/support-request.component';
import { NavigationComponent } from 'src/modules/support/components/support-request/components/navigation-items/navigation.component';
import { ORCHESTRATOR_B2B_ROUTES } from 'src/modules/orchestrator/components/orchestrator-b2b/constants/orchestrator-b2b.constants';
import { DetailsComponent } from 'src/modules/support/components/access-rights/components/details/details.component';
import { MerchantProfileComponent } from 'src/modules/merchants/merchant-profile/merchant-profile/components/merchant-profile/merchant-profile.component';
import { MerchantsComponent } from 'src/modules/merchants/merchants.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: LayoutComponent,
        children: [
          { path: 'home', component: HomeComponent, canActivate: [AuthenticationGuard] },
          { path: 'dashboard', component: DashboardComponent, canActivate: [AuthenticationGuard] },
          {
            path: 'staff',
            component: StaffComponent,
            canActivate: [AuthenticationGuard],
            children: [
              { path: '', redirectTo: '/staff/list', pathMatch: 'full' },
              { path: 'list', component: StaffListComponent }
            ]
          },
          { path: 'sales', component: SalesComponent, canActivate: [AuthenticationGuard] },
          {
            path: 'merchants',
            component: MerchantsComponent,
            canActivate: [AuthenticationGuard],
            children: [
              { path: '', redirectTo: '/merchants/list', pathMatch: 'full' },
              { path: 'list', component: MerchantsListComponent },
              // { path: 'transactions', component: MerchantTransactionsComponent },
              // { path: 'fees', component: FeesComponent },
              // { path: 'profitability', component: ProfitabilityComponent },
              { path: ':id', component: MerchantProfileComponent }
            ]
          },
          {
            path: 'editors',
            component: EditorsComponent,
            canActivate: [AuthenticationGuard],
            children: [
              { path: '', redirectTo: '/editors/home', pathMatch: 'full' },
              { path: 'home', component: EditorsHomeComponent },
              { path: 'items/:type', component: ItemsComponent },
              // TODO: Temporary Global Items, Remove later
              { path: 'global-items', component: TempItemsComponent },
              { path: 'approve-items', component: TempApproveItemsComponent },
              { path: 'ontology', component: OntologyEditorsFormComponent }
            ]
          },
          { path: 'support', component: SupportComponent, canActivate: [AuthenticationGuard] },
          {
            path: 'customer-support',
            component: SupportComponent,
            canActivate: [AuthenticationGuard],
            children: [
              { path: '', redirectTo: '/customer-support/requests', pathMatch: 'full' },
              { path: 'requests', component: SupportRequestsComponent },
              { path: 'requests/:id', component: SupportRequestComponent,
                children: [
                  { path: 'navigation', component: NavigationComponent },
                  { path: 'payment', component: PaymentComponent },
                  { path: 'systems-health', component: SystemsHealthComponent },
                  { path: 'access-rights', component: AccessRightsComponent },
                ]
              },
            ]
          },
          { path: 'customer-support/requests/:requestId/access-rights/:id/details', component: DetailsComponent },
          ...ORCHESTRATOR_B2B_ROUTES
        ]
      }
    ])
  ],
  exports: [
    RouterModule
  ]
})
export class LayoutRoutingModule {}
