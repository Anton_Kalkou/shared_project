import { NgModule } from '@angular/core';

import { NgxsModule } from '@ngxs/store';
import { ClickOutsideModule } from 'ng-click-outside';
import { TranslateModule } from '@ngx-translate/core';

import { SharedModule } from 'src/modules/shared/modules/shared.module';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from '../components/layout/layout.component';
import { HeaderComponent } from '../components/header/header.component';
import { DashboardModule } from 'src/modules/dashboard/modules/dashboard.module';
import { HomeModule } from 'src/modules/home/modules/home.module';
import { StaffModule } from 'src/modules/staff/modules/staff.module';
import { MaterialModule } from 'src/modules/shared/modules/material.module';
import { UserIconModule } from 'src/modules/shared/components/user-icon/user-icon.module';
import { LayoutState } from '../ngxs/layout.state';
import { UserMenuComponent } from '../components/user-menu/user-menu.component';
import { SalesModule } from 'src/modules/sales/module/sales.module';
import { EditorsModule } from 'src/modules/editors/modules/editors.module';
import { SupportModule } from 'src/modules/support/modules/support.module';
import { PipesModule } from 'src/modules/shared/pipes/pipes.module';
import { MenuComponent } from '../components/menu/menu.component';
import { SideNavComponent } from '../components/side-nav/side-nav.component';
import { OrchestratorModule } from 'src/modules/orchestrator/modules/orchestrator.module';
import { MerchantsModule } from 'src/modules/merchants/merchants.module';

@NgModule({
  imports: [
    SharedModule,
    LayoutRoutingModule,
    MaterialModule,
    HomeModule,
    DashboardModule,
    StaffModule,
    SalesModule,
    MerchantsModule,
    EditorsModule,
    SupportModule,
    OrchestratorModule,
    UserIconModule,
    NgxsModule.forFeature([LayoutState]),
    ClickOutsideModule,
    PipesModule,
    TranslateModule
  ],
  declarations: [
    LayoutComponent,
    HeaderComponent,
    UserMenuComponent,
    MenuComponent,
    SideNavComponent
  ],
  exports: [
    LayoutComponent
  ]
})
export class LayoutModule {}

