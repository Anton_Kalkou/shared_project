export class Logout {
  static readonly type = '[User] Logout';
}

export class GetUserProfileData {
  static readonly type = '[User Profile Data] Get';
}

export class OpenMenu {
  static readonly type = '[Menu] Open';
}

export class ToggleSidenav {
  static readonly type = '[Sidenav] Toggle';
}

export class SetHeaderTitle {
  static readonly type = '[Header Title] Set';

  constructor(public headerTitle: string) {}
}

export class SetNavigationButtonStatesTitle {
  static readonly type = '[Header Navigation Button States Title] Set';

  constructor(public flag: boolean, public title?: string) {}
}

export type HeaderActions =
  | SetHeaderTitle
  | SetNavigationButtonStatesTitle;
