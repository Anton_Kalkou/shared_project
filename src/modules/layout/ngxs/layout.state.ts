import { Injectable, NgZone } from '@angular/core';

import { State, Action, StateContext, Store } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { Navigate } from '@ngxs/router-plugin';
import { MatDialog } from '@angular/material/dialog';

import { SetSpinnerVisibility } from 'src/modules/root/ngxs/root.actions';
import { SharedDataService } from 'src/modules/shared/services/shared-data.service';
import { APP } from 'src/modules/shared/constants';
import { User } from 'src/modules/shared/models/user.model';
import { Logout, GetUserProfileData, OpenMenu, SetHeaderTitle, ToggleSidenav, SetNavigationButtonStatesTitle } from './layout.actions';
import { MenuComponent } from '../components/menu/menu.component';
import { LayoutService } from '../services/layout.service';

interface LayoutStateModel {
  userProfileData: User;
  headerTitle: string;
  isNavigationButtonStatesTitle: boolean;
  navigationButtonStatesTitle: string;
  isSliderSidenav: boolean;
}

@State<LayoutStateModel>({
  name: 'layout',
  defaults: {
    userProfileData: null,
    headerTitle: 'Rockspoon Admin Portal',
    isSliderSidenav: false,
    isNavigationButtonStatesTitle: false,
    navigationButtonStatesTitle: '',
  }
})
@Injectable()
export class LayoutState {

  constructor(
    private store: Store,
    private layoutService: LayoutService,
    private sharedDataService: SharedDataService,
    private dialog: MatDialog,
    private ngZone: NgZone
  ) {}

  @Action(OpenMenu)
  openMenu() {
    this.ngZone.run(() => {
      this.dialog.open(MenuComponent, {
        width: '100vw',
        height: '100vh'
      });
    });
  }

  @Action(ToggleSidenav)
  toggleSidenav({ getState, patchState }: StateContext<LayoutStateModel>) {
    patchState({ isSliderSidenav: !getState().isSliderSidenav });
  }

  @Action(GetUserProfileData)
  getUserProfileData({patchState}: StateContext<LayoutStateModel>) {
    this.store.dispatch(new SetSpinnerVisibility(true));

    return this.layoutService.getUserProfileData()
      .pipe(
        tap(userProfileData => {
          patchState({
            userProfileData
          });
          this.store.dispatch(new SetSpinnerVisibility(false));
        })
      );
  }

  @Action(SetHeaderTitle)
  setHeaderTitle({ patchState }: StateContext<LayoutStateModel>, action: SetHeaderTitle) {
    patchState({ headerTitle: action.headerTitle });
  }

  @Action(SetNavigationButtonStatesTitle)
  setNavigationButtonStatesTitle({ patchState }: StateContext<LayoutStateModel>, action: SetNavigationButtonStatesTitle) {
    patchState({ isNavigationButtonStatesTitle: action.flag, navigationButtonStatesTitle: action.title });
  }

  @Action(Logout)
  logout({patchState}: StateContext<{}>) {
    this.sharedDataService.authenticationData = null;
    this.store.dispatch(new Navigate([APP.pages.authentication]));
  }

}
