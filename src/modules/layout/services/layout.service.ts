import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { ApiService } from 'src/modules/shared/services/api.service';
import { APP } from 'src/modules/shared/constants';
import { User } from 'src/modules/shared/models/user.model';

@Injectable({
  providedIn: 'root'
})
export class LayoutService {

  constructor(
    private apiService: ApiService
  ) {}

  public getUserProfileData(): Observable<User> {
    return this.apiService.get(APP.endpoints.userProfileData);
  }

}
