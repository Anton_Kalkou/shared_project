export enum NavigationButtonStates {
  menu,
  sidenav,
  back,
  close,
  empty
}

export enum HeaderType {
  CustomerSupport = 'customer-support'
}
